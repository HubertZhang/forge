--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details
The source codes does not come with any warranty including
the implied warranty of merchandise.
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

env._G = GLOBAL
env.require = _G.require
env.STRINGS = _G.STRINGS

if _G.TheNet:GetServerGameMode() ~= "lavaarena" then
	print("ERROR: Forge mod was disabled because of not correct GameMode")
	return
end

local function GetKeyFromConfig(config)
    local key = GetModConfigData(config)
    return key and (type(key) == "number" and key or _G[key])
end

_G.Forge_options = {
	mod_root 				= MODROOT,
	force_start_delay		= GetModConfigData("FORCE_START_DELAY_TIME", true),

	-- Display Options
	damage_number_display	= GetModConfigData("DAMAGE_NUMBER_OPTIONS", true),
	damage_number_font_size = GetModConfigData("DAMAGE_NUMBER_FONT_SIZE", true),
    damage_number_height    = GetModConfigData("DAMAGE_NUMBER_HEIGHT", true),
	only_show_nonzero_stats	= GetModConfigData("ONLY_SHOW_NONZERO_STATS", true),
	display_colored_stats	= GetModConfigData("DISPLAY_COLORED_STATS", true),
	default_rotation		= GetModConfigData("DEFAULT_ROTATION", true),
	rotation				= GetModConfigData("ROTATION", true),
	hide_indicators			= GetModConfigData("HIDE_INDICATORS", true),
	ADJ_FILTER_KEY 			= GetKeyFromConfig("ADJUST_FILTER", true),
	default_filter 			= GetModConfigData("DEFAULT_FILTER", true),

	--TEMPORARY CONFIG OPTIONS FOR MODIFIERS (Will be migrated into worldgen params UNLESS we make our UI modify modconfig params)
	waveset					= GetModConfigData("WAVESET", true),
	mobdamage_dealt			= GetModConfigData("MOBDAMAGE_DEALT", true),
	mobdamage_taken			= GetModConfigData("MOBDAMAGE_TAKEN", true),
	btlstandard_efficiency	= GetModConfigData("BATTLESTANDARD_EFFICIENCY", true),
	redlight_greenlight		= GetModConfigData("REDLIGHT", true),
	invisible_players		= GetModConfigData("INVISIBLE_PLAYERS", true),
	invisible_mobs			= GetModConfigData("INVISIBLE_MOBS", true),
	--nosleeps				= GetModConfigData("NOSLEEPS", true),
	--strongershields		= GetModConfigData("STRONGERSHIELDS", true),
	superknockback			= GetModConfigData("SUPERKNOCKBACK", true),

	-- Performance Options


	-- Other Options
	gift_side               = GetModConfigData("GIFT_SIDE", true),
}

modimport("scripts/forge_strings.lua")
PrefabFiles = require("forge_prefabs")
Assets = require("forge_assets")
_G.TUNING.FORGE = require("forge_tuning")


modimport("scripts/forge_main.lua")
modimport("scripts/forge_actions.lua")

require("forge_rounds/_common_functions")

local Debug = require "util/debug"
_G.Debug = Debug(false)

for i, v in ipairs(PrefabFiles) do
	_G.TUNING.WINTERS_FEAST_LOOT_EXCLUSION[string.upper(v)] = true
end

-- Removes the extra circle frame from the health badge.
AddClassPostConstruct("widgets/healthbadge", function(self)
    self.circleframe2:Hide()
end)
