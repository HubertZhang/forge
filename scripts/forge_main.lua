--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local Widget = require "widgets/widget"
local Text = require "widgets/text"
local TextButton = require "widgets/textbutton"
local Image = require "widgets/image"
local ImageButton = require "widgets/imagebutton"
local ForgeNetworking = require "forge_networking"
local ForgeUI = require "forge_ui"
local ForgeRPC = require "forge_rpc"
local Vector3 = _G.Vector3
local json = _G.json
local FRAMES = _G.FRAMES
local EQUIPSLOTS = _G.EQUIPSLOTS
local STRINGS = _G.STRINGS
local unpack = _G.unpack
local TheInput = _G.TheInput

for classif, fn in pairs(ForgeNetworking) do
	AddPrefabPostInit(classif.."_classified", fn)
end

for class, fn in pairs(ForgeUI) do
	AddClassPostConstruct(class, fn)
end

_G.WORLD_FESTIVAL_EVENT = _G.FESTIVAL_EVENTS.LAVAARENA

--Apply character changes
modimport("scripts/prefabs/forge_characters.lua")

--Update requireeventfile to grab our code
local _oldrequireeventfile = _G.requireeventfile
_G.requireeventfile = function(path)
	local mod_path = path:gsub("lavaarena_event_server", "forge_data")
	return _G.softresolvefilepath("scripts/"..mod_path..".lua") ~= nil and require(mod_path) or _oldrequireeventfile(path)
end

-- Initializing client components
AddPrefabPostInit("lavaarena", function(w)
    if not _G.TheNet:IsDedicated() and not w.components.talking_crowd then
		w:AddComponent("talking_crowd")
    end
end)

--Adding our RPCs
for name, fn in pairs(ForgeRPC) do
	AddModRPCHandler("FORGE", name, fn)
end

--Fixing name extensions
function _G.EntityScript:GetDisplayName()
    local name = self:GetAdjectivedName()

    if self.prefab ~= nil then
        local name_extention = STRINGS.NAME_DETAIL_EXTENTION[string.upper(self.nameoverride ~= nil and self.nameoverride or self.prefab)]
        if name_extention ~= nil then
            return name.."\n"..name_extention
        end
    end
    return name
end

--Fixing playeravatarpopup images for forge items
local PlayerAvatarPopup = _G.require("widgets/playeravatarpopup")
local EquipSlot = _G.require("equipslotutil")
local _oldUpdateEquipWidgetForSlot = PlayerAvatarPopup.UpdateEquipWidgetForSlot
function PlayerAvatarPopup:UpdateEquipWidgetForSlot(image_group, slot, equipdata)
	local name = equipdata ~= nil and equipdata[EquipSlot.ToID(slot)] or nil
    name = name ~= nil and #name > 0 and name or "none"
	if _G.AllForgePrefabs[name] and _G.AllForgePrefabs[name].image and not _G.AllForgePrefabs[name].is_mod then equipdata[EquipSlot.ToID(slot)] = _G.AllForgePrefabs[name].image:gsub(".tex", "") end
	_oldUpdateEquipWidgetForSlot(self, image_group, slot, equipdata)
	if name == "riledlucy" then image_group._text:SetMultilineTruncatedString(STRINGS.NAMES.LAVAARENA_LUCY, 2, TEXT_WIDTH, 25, true) end --Because Klei forgot Lucy :c
end

-- Add stat tracking
AddPrefabPostInit("world", function(self) self:AddComponent("stat_tracker") end)



--[[-----------------------------------------------------------------------------------------
										Lobby
-------------------------------------------------------------------------------------------]]
--Fixing the lobby screen
local LobbyScreen = require("screens/redux/lobbyscreen")
local _oldToNextPanel = LobbyScreen.ToNextPanel
LobbyScreen.ToNextPanel = function(self, dir)
	_oldToNextPanel(self, dir)
	if _G.TheNet:GetServerGameMode() == "lavaarena" and self.panel and self.panel.name then
		if self.panel.name == "CharacterSelectPanel" then
			self.panel:SetPosition(300, 170) --I have no idea why Klei offsets it.
		-- Reposition death text, klei messed up their offsets
		elseif self.panel.name == "WxpPanel" then
			for i,child in pairs(self.panel:GetChildren()) do
				local text = child.GetString ~= nil and child:GetString() or ""
				if text and (string.sub(text,1,12) == "Total Deaths" or text == STRINGS.UI.WXPLOBBYPANEL.NO_DEATHS) then -- TODO need to check global string for no deaths
					local old_pos = child:GetPosition()
					child:SetPosition(old_pos.x, old_pos.y - 20)
				end
			end
		elseif self.panel.name == "LavaarenaFestivalBookPannel" then
			self.next_button:Enable() --BANDAIIIID
		end
	end
end

------------------------------
-- Force Start/Cancel Setup --
------------------------------
local UserCommands = require "usercommands"
local PopupDialogScreen = require "screens/redux/popupdialog"
local PlayerList = require "widgets/redux/playerlist"
local TEMPLATES = require "widgets/redux/templates"

local subfmt = _G.subfmt
local next = _G.next
local assert = _G.assert
local tonumber = _G.tonumber
local getlocal = _G.debug.getlocal
local getupvalue = _G.debug.getupvalue
local setupvalue = _G.debug.setupvalue
local getinfo = _G.debug.getinfo
AddClassPostConstruct("screens/redux/lobbyscreen", function(self)
	-- new inst vars
	self.countdown_started = false
	self.countdown_time = 255
	self.spawndelaytext = self.panel_root:AddChild(Text(_G.CHATFONT, 50))
    self.spawndelaytext:SetPosition(0, -290)
    self.spawndelaytext:SetColour(_G.UICOLOURS.GOLD)
    self.spawndelaytext:SetString("")
    self.spawndelaytext:Hide()
	
	local old_OnControl = self.OnControl
	self.OnControl = function(self, control, down)
		if not self.enabled then
			return false
		end
		
		-- breaks the MVP widget
		--if self._base.OnControl(self, control, down) then return true end
		
		-- make it so cant use "escape" to disconnect or go back if back button is disbaled and <= 5 on countdown
		if not down and control == _G.CONTROL_CANCEL and not self.back_button.enabled and self.countdown_started and self.countdown_time <= 5 then
			return false 
		end

		return old_OnControl(self, control, down)
	end
	
	local function StartGame(this)
		if this.startbutton then
			this.startbutton:Disable()
		end

		if this.cb then
			local skins = this.currentskins
			this.cb(this.character_for_game, skins.base, skins.body, skins.hand, skins.legs, skins.feet) --parameters are base_prefab, skin_base, clothing_body, clothing_hand, then clothing_legs
		end
	end
	
	local old_ToNextPanel = self.ToNextPanel
	self.ToNextPanel = function(self, dir)
		old_ToNextPanel(self, dir)
		if self.countdown_started then
			-- panel we want to go to has a disconnect button and time <= 5, hide it, otherwise show em
			if self.countdown_time <= 5 and (self.back_button.text:GetString() == STRINGS.UI.LOBBYSCREEN.DISCONNECT or self.panel.title == STRINGS.UI.LOBBYSCREEN.WAITING_FOR_PLAYERS_TITLE) then
				self.back_button:Hide()
				self.back_button:Disable()
			else
				self.back_button:Enable()
				self.back_button:Show()
			end
			-- waiting for players, set their countdown to active to immediately start their countdown text n tick sound, otherwise display our lobbyscreen one
			if self.panel.title == STRINGS.UI.LOBBYSCREEN.WAITING_FOR_PLAYERS_TITLE then
				self.panel.waiting_for_players.spawn_countdown_active = true
			else
				self.spawndelaytext:Show()
			end
		end
		-- Add a spawn button if forge is in progress and hide the vote to start option
		if _G.TheWorld.components.lavaarenaevent and _G.TheWorld.components.lavaarenaevent.current_round > 0 and self.panel.title == _G.STRINGS.UI.LOBBYSCREEN.WAITING_FOR_PLAYERS_TITLE then
			self.startbutton = self.root:AddChild(TEMPLATES.StandardButton(function() StartGame(self) end, "Spawn", {200, 50}))
			self.startbutton:SetPosition(500, self.back_button:GetPosition().y - 5)
			
			self.panel.waiting_for_players.playerready_checkbox:Hide()
		end
	end
	
	local function PickRandomCharacterAndSkins()
		if self.character_for_game == nil then
			local all_chars = _G.ExceptionArrays(_G.GetActiveCharacterList(), _G.MODCHARACTEREXCEPTIONS_DST)
			self.character_for_game = all_chars[math.random(#all_chars)]
			print("Selected Random Character: " .. self.character_for_game)
		end
		if self.currentskins == nil then
			self.currentskins = self.profile:GetSkinsForCharacter(self.character_for_game)
			-- if cant grab preset skins, pick some random ones from profile
			if next(self.currentskins) == nil then
				print("Selected Random Skins:")
				for _,clothing_type in ipairs({ "base", "body", "hand", "legs", "feet" }) do
					if clothing_type ~= "base" then
						self.currentskins[clothing_type] = _G.GetRandomItem(self.profile:GetClothingOptionsForType(clothing_type)) or clothing_type.."_default1"
					else
						self.currentskins[clothing_type] = _G.GetRandomItem(self.profile:GetSkinsForPrefab(self.character_for_game)) or self.character_for_game.."_none"
					end
					print("\t" .. clothing_type .. ": " .. self.currentskins[clothing_type])
				end
			else
				print("Selected Preset Skins for " .. self.character_for_game)
			end
		end
	end
	
	local function OnCountdown()
		if self.countdown_started then
			-- make users pop disconnect/ready/any dialog box if countdown <= 5
			local active_screen = _G.TheFrontEnd:GetActiveScreen()
			if active_screen and active_screen.name and active_screen.name == "PopupDialogScreen" and (active_screen.dialog.title:GetString() == STRINGS.UI.LOBBYSCREEN.FORCESTARTTITLE or self.countdown_time <= 5) then
				_G.TheFrontEnd:PopScreen()
			end
			-- if time <= 5, dont show disconnect button and dont show back button for waiting panel 
			if self.countdown_time <= 5 and (self.back_button.text:GetString() == STRINGS.UI.LOBBYSCREEN.DISCONNECT or self.panel.title == STRINGS.UI.LOBBYSCREEN.WAITING_FOR_PLAYERS_TITLE) then
				self.back_button:Hide()
				self.back_button:Disable()
			else
				self.back_button:Enable()
				self.back_button:Show()
			end
			-- always update our countdown incase it is shown offtick
			local str = _G.subfmt(STRINGS.UI.LOBBY_WAITING_FOR_PLAYERS_SCREEN.SPAWN_DELAY, { time = math.max(0, self.countdown_time) })
			if str ~= self.spawndelaytext:GetString() then
				self.spawndelaytext:SetString(str)
			end
			-- on countdown tick check to see if at waiting players, if so hide our countdown, else play sound and show
			if self.panel.title ~= STRINGS.UI.LOBBYSCREEN.WAITING_FOR_PLAYERS_TITLE then
				self.spawndelaytext:Show()
				_G.TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/WorldDeathTick")
			else
				self.spawndelaytext:Hide()
			end
		else
			self.spawndelaytext:Hide()
			self.spawndelaytext:SetString("")
			self.back_button:Enable()
			self.back_button:Show()
			if self.panel.title == STRINGS.UI.LOBBYSCREEN.WAITING_FOR_PLAYERS_TITLE then
				local children = self.panel.waiting_for_players:GetChildren()
				for i, child in pairs(children) do
					-- Currently only 2 children, but technically the only thing that should be hidden is the countdown so should be fine.
					if child.name == "Text" then
						child:Hide()
					else
						child:Show()
					end
				end
				self.panel.waiting_for_players.spawn_countdown_active = false
				self.panel.waiting_for_players.playerready_checkbox.checked = false
				self.panel.waiting_for_players.playerready_checkbox.votestart_warned = false
				self.panel.waiting_for_players.playerready_checkbox:Enable()
				self.panel.waiting_for_players.playerready_checkbox:Refresh()
			end
		end
	end
	
    self.inst:ListenForEvent("lobbyplayerspawndelay", function(world, data)
        if data then
			-- if 1 second left on clock, force character/skin selection
			if data.active and data.time == 1 then
				PickRandomCharacterAndSkins()
			end
			self.countdown_started = data.active
			-- subtract one so we hang on 0 for a second
			self.countdown_time = self.countdown_started and (data.time - 1) or 255
			OnCountdown()
        end
    end, _G.TheWorld)
end)

---------------------------------------------------------------------------------------------------------------------------

local function FindUpvalue(fn, upvalue_name)
	assert(type(fn) == "function", "Function expected as 'fn' parameter.")
	local info = getinfo(fn, "u")
	local nups = info and info.nups
	if not nups then return end
	for i = 1, nups do
		local name, val = getupvalue(fn, i)
		if name == upvalue_name then
			return val, i
		end
	end
end

-- ugly but easiest way to change size of playerinfolist
local PlayerInfoListing_width, PlayerInfoListing_widthIndex = FindUpvalue(PlayerList.BuildPlayerList, "PlayerInfoListing_width")
if PlayerInfoListing_widthIndex then
	setupvalue(PlayerList.BuildPlayerList, PlayerInfoListing_widthIndex, PlayerInfoListing_width + 30)
else
	print("[Forged Forge] ERROR: Failed to find PlayerInfoListing_width upvalue!")
end
 
local function SendCommand(fnstr)
	local x, _, z = _G.TheSim:ProjectScreenPos(_G.TheSim:GetPosition())
	local is_valid_time_to_use_remote = _G.TheNet:GetIsClient() and _G.TheNet:GetIsServerAdmin()
	if is_valid_time_to_use_remote then
		_G.TheNet:SendRemoteExecute(fnstr, x, z)
	else
		_G.ExecuteConsoleCommand(fnstr)
	end
end

local old_BuildPlayerList = PlayerList.BuildPlayerList
PlayerList.BuildPlayerList = function(self, players, nextWidgets)
	
	old_BuildPlayerList(self, players, nextWidgets)
	
	local this_user = _G.TheNet:GetClientTableForUser(_G.TheNet:GetUserID())
	
	self.match_countdown = false
	local function UpdateForceStart(has_started)
		if self.match_countdown ~= has_started then
			self.match_countdown = has_started
			if self.scroll_list and self.scroll_list.old_update then
				local all_playerInfoListings = self.scroll_list:GetListWidgets()
				for i, listing in ipairs(all_playerInfoListings) do
					if listing.forcestart then
						if self.match_countdown then
							listing.forcestart.image_focus = "cancel.tex"
							listing.forcestart:SetHoverText(STRINGS.UI.LOBBYSCREEN.CANCELSTART)
							listing.forcestart.image:SetTexture("images/force_start.xml", "cancel.tex")
							listing.forcestart:SetTextures("images/force_start.xml", "cancel.tex")  
						else
							listing.forcestart.image_focus = "start.tex"
							listing.forcestart:SetHoverText(STRINGS.UI.LOBBYSCREEN.FORCESTART)							
							listing.forcestart.image:SetTexture("images/force_start.xml", "start.tex")
							listing.forcestart:SetTextures("images/force_start.xml", "start.tex") 
						end
					end
				end
			end
		end
	end
	
	self.inst:ListenForEvent("lobbyplayerspawndelay", function(world, data) 
		if data then
			UpdateForceStart(data.active)
		end	
	end, _G.TheWorld)
	
	if not self.scroll_list.old_update then
		local MOVE_RIGHT = _G.MOVE_RIGHT
		local MOVE_LEFT = _G.MOVE_LEFT
		local MOVE_DOWN = _G.MOVE_DOWN
		local x_offset = 29
		local all_playerInfoListings = self.scroll_list:GetListWidgets()
		local old_mute_position = all_playerInfoListings[1].mute and all_playerInfoListings[1].mute:GetPosition()
		local old_viewprofile_position = all_playerInfoListings[1].viewprofile and all_playerInfoListings[1].viewprofile:GetPosition()
		local old_netscore_position = all_playerInfoListings[1].netscore and all_playerInfoListings[1].netscore:GetPosition()
		for i, listing in ipairs(all_playerInfoListings) do
			
			listing.empty = not listing.bg:IsVisible()
			
			-- hardcoded scaling since they did too
			listing.highlight:ScaleToSize(250,50)
			local highlight_position = listing.highlight:GetPosition()
			listing.highlight:SetPosition(highlight_position.x + 13.5, 0)	-- 13.5 looks good
			
			listing.kick = listing:AddChild(ImageButton("images/scoreboard.xml", "kickout.tex", "kickout.tex", "kickout_disabled.tex", "kickout.tex", nil, {1,1}, {0,0}))
			listing.kick:SetPosition(old_mute_position.x + x_offset, 0)
			listing.kick:SetNormalScale(0.234)
			listing.kick:SetFocusScale(0.234 * 1.1)
			listing.kick:SetFocusSound("dontstarve/HUD/click_mouseover")
			listing.kick:SetHoverText(STRINGS.UI.PLAYERSTATUSSCREEN.KICK, { font = _G.NEWFONT_OUTLINE, offset_x = -35, offset_y = 0, colour = {1,1,1,1} })
			listing.kick:SetOnClick(function()
				-- doesnt really matter since will be updated
				if listing.userid then
					UserCommands.RunUserCommand("kick", { user = listing.userid }, this_user)
				end
			end)
			-- empty, admin, or if its yourself, hide kick (no voting...)
			if listing.empty or listing.adminBadge:IsVisible() or (listing.userid and listing.userid == this_user.userid) then
				listing.kick:Hide()
			else
				listing.kick:Show()
			end
			
			listing.forcestart = listing:AddChild(ImageButton("images/force_start.xml", "start.tex"))
			listing.forcestart:SetPosition(old_mute_position.x + x_offset, 0)
			-- same scale as theirs for 100x100px icon
			listing.forcestart:SetNormalScale(0.234)
			listing.forcestart:SetFocusScale(0.234 * 1.1)
			-- 0.6 makes it look about same birghtness as other buttons
			listing.forcestart.image:SetTint(1, 1, 1, 0.6)
			listing.forcestart:SetFocusSound("dontstarve/HUD/click_mouseover")
			listing.forcestart:SetHoverText(STRINGS.UI.LOBBYSCREEN.FORCESTART, { font = _G.NEWFONT_OUTLINE, offset_x = -50, offset_y = 0, colour = {1,1,1,1} })
			listing.forcestart:SetOnClick(function()
				if self.match_countdown then
					SendCommand("TheWorld.net.components.worldcharacterselectlobby:CancelForceStart()")
					_G.TheNet:Say(STRINGS.UI.LOBBYSCREEN.SAYCANCEL)
				else
					_G.TheFrontEnd:PushScreen(
						PopupDialogScreen(STRINGS.UI.LOBBYSCREEN.FORCESTARTTITLE,
						string.format(STRINGS.UI.LOBBYSCREEN.FORCESTARTDESC, _G.Forge_options.force_start_delay), {
							{ text = STRINGS.UI.PLAYERSTATUSSCREEN.OK, cb = function()
								_G.TheFrontEnd:PopScreen()
								if not self.match_countdown then
									SendCommand("TheWorld.net.components.worldcharacterselectlobby:ForceStart(" .. tostring(_G.Forge_options.force_start_delay) .. ")")								
									_G.TheNet:Say(string.format(STRINGS.UI.LOBBYSCREEN.SAYSTART, _G.Forge_options.force_start_delay))
								end
							end },
							{ text = STRINGS.UI.PLAYERSTATUSSCREEN.CANCEL, cb = function() _G.TheFrontEnd:PopScreen() end } 
						} )
					)
				end
			end)
			
			-- by default move netscore to right edge of new listing
			listing.netscore:SetPosition(old_netscore_position.x + x_offset, 0)
			
			-- new focus that changes based on if empty or not, klei bug fix
			listing.OnGainFocus = function()
				if not listing.empty then
					listing.highlight:Show()
				end
			end
			listing.OnLoseFocus = function()
				listing.highlight:Hide()
			end
		end
		self.scroll_list.old_update = self.scroll_list.update_fn
		self.scroll_list.update_fn = function(context, widget, data, index)
			self.scroll_list.old_update(context, widget, data, index)
			widget.empty = data == nil or next(data) == nil
			widget.kick:SetOnClick(function()
				-- dont care if not admin, should be hidden anyway
				if data ~= nil and data.userid ~= nil then
					UserCommands.RunUserCommand("kick", { user = data.userid }, this_user)
				end
			end)
			-- hide if empty, im not admin, this is my listing, or target is admin
			if widget.empty or not this_user.admin or data.userid == this_user.userid or data.admin then
				widget.kick:Hide()
			else
				widget.kick:Show()
			end
			-- if kick shown, then leave viewprofile and mute at original positions
			widget.viewprofile:SetPosition(old_viewprofile_position.x + (widget.kick.shown and 0 or x_offset), 0)
			widget.mute:SetPosition(old_mute_position.x + (widget.kick.shown and 0 or x_offset), 0)
			
			-- only show for your listing, and if you are admin (need vote!)
			if data and data.userid == this_user.userid and data.admin then
				widget.forcestart:Show()
				widget.netscore:SetPosition(old_netscore_position.x, 0)
			else
				widget.forcestart:Hide()
				widget.netscore:SetPosition(old_netscore_position.x + x_offset, 0)
			end
			
			-- new focus hookups thats ran onupdate incase you lose/gain buttons (before only once on creation)
			local function newFocus(self)
				local buttons = {}
				if self.viewprofile:IsVisible() then table.insert(buttons, self.viewprofile) end
				if self.mute:IsVisible() then table.insert(buttons, self.mute) end
				if self.kick:IsVisible() then table.insert(buttons, self.kick) end
				if self.forcestart:IsVisible() then table.insert(buttons, self.forcestart) end
				
				self.focus_forward = nil
				local focusforwardset = false
				for i,button in ipairs(buttons) do
					if not focusforwardset then
						focusforwardset = true
						self.focus_forward = button
					end
					if buttons[i-1] then
						button:SetFocusChangeDir(MOVE_LEFT, buttons[i-1])
					end
					if buttons[i+1] then
						button:SetFocusChangeDir(MOVE_RIGHT, buttons[i+1])
					end
				end
			end
			if not widget.empty then
				newFocus(widget)
			end
		end
	end	
end

-------------------------------------------------------------------------------------------------------------------------------------------

-- Force Start/Cancel commands
AddComponentPostInit("worldcharacterselectlobby", function(self)
	local function GetPlayersClientTable()
		local clients = _G.TheNet:GetClientTable() or {}
		if not _G.TheNet:GetServerIsClientHosted() then
			for i, v in ipairs(clients) do
				if v.performance ~= nil then
					table.remove(clients, i) -- remove "host" object
					break
				end
			end
		end
		return clients
	end
	
	if _G.TheWorld and _G.TheWorld.ismastersim then
		local force_start = false
		local initial_delay = false
		local total_delay = _G.Forge_options.force_start_delay
		
		-- Initiates a timer that will start the game when completed
		function self:ForceStart(delay_time)
			total_delay = delay_time or _G.Forge_options.force_start_delay
			force_start = true
			for _,data in pairs(GetPlayersClientTable()) do
				local userid = data.userid
				if userid then
					if not self:IsPlayerReadyToStart(userid) then
						UserCommands.RunUserCommand("playerreadytostart", { ready = "true" }, _G.TheNet:GetClientTableForUser(userid), true)
					end
				end
			end
		end
		
		-- Stops the game from starting and resets the timer
		function self:CancelForceStart()
			force_start = false
			self.inst:StopWallUpdatingComponent(self)
			_G.TheNet:SetAllowNewPlayersToConnect(true)
			_G.TheNet:SetIsMatchStarting(false)
			-- param is reverse logic to set _countdowni to COUNTDOWN_INACTIVE of 255
			self:OnWallUpdate(-255 + self:GetSpawnDelay())
			initial_delay = false
			print("[WorldCharacterSelectLobby] Countdown cancelled")
		end
		
		-- Updates the current delay
		local old_OnWallUpdate = self.OnWallUpdate
		function self:OnWallUpdate(dt)
			-- add -5 to delay to compensate for default _countdowni
			old_OnWallUpdate(self, force_start and not initial_delay and -math.max(0, total_delay - 5 - dt) or dt)
			initial_delay = true
		end
		
		-- Allow players to change characters when force start has been activated
		local old_IsAllowingCharacterSelect = self.IsAllowingCharacterSelect
		function self:IsAllowingCharacterSelect()
			return force_start and self:GetSpawnDelay() > 1 or old_IsAllowingCharacterSelect(self)
		end
	end
end)

--------------------------
-- CharacterSelectPanel --
--------------------------
-- Add detailed description of the character to their potrait on the character select screen
local function EditOvalPortrait(self)
	local old_BuildCharacterDetails = self._BuildCharacterDetails
	
	self._BuildCharacterDetails = function()
		local portrait_root = old_BuildCharacterDetails(self)
		if _G.TheNet:GetServerGameMode() == "lavaarena" then
			self.eventid = _G.TheNet:GetServerGameMode() --Note(Peter):Ahhhhh! we're mixing game mode and event id and server event name, it works though because it's all "lavarena" due to the c-side being case-insensitive
			portrait_root:SetPosition(0, -50)
		
			self.character_text:SetPosition(0, -150)

			self.la_health = self.character_text:AddChild(Text(_G.HEADERFONT, 28))
			self.la_health:SetHAlign(_G.ANCHOR_LEFT)
			self.la_health:SetRegionSize(300, 30)
			self.la_health:SetColour(_G.UICOLOURS.WHITE)
			self.la_health:SetPosition(15, -210)

			self.la_difficulty= self.character_text:AddChild(Text(_G.HEADERFONT, 20))
			self.la_difficulty:SetHAlign(_G.ANCHOR_LEFT)
			self.la_difficulty:SetRegionSize(300, 30)
			self.la_difficulty:SetColour(_G.UICOLOURS.EGGSHELL)
			self.la_difficulty:SetPosition(15, -235)
			
			self.la_items = self.character_text:AddChild(Text(_G.HEADERFONT, 20))
			self.la_items:SetVAlign(_G.ANCHOR_TOP)
			self.la_items:SetHAlign(_G.ANCHOR_LEFT)
			self.la_items:SetRegionSize(300, 70)
			self.la_items:SetColour(_G.UICOLOURS.EGGSHELL)
			self.la_items:EnableWordWrap(true)
			self.la_items:SetPosition(15, -280)
			
		end

		return portrait_root
	end
	
	self.portrait_root:KillAllChildren()
	self.portrait_root = self:AddChild(self:_BuildCharacterDetails(self))
	
	local old_SetPortrait = self.SetPortrait
	
	self.SetPortrait = function(self, character)
		--old_SetPortrait(self, character)
		_G.assert(character)

		self.currentcharacter = character

		local found_name = _G.SetHeroNameTexture_Gold(self.heroname, character)
		if found_name then 
			self.heroname:Show()
		else
			self.heroname:Hide()
		end

		_G.SetOvalPortraitTexture(self.heroportrait, character)

		if self.charactername then
			self.charactername:SetString(STRINGS.CHARACTER_NAMES[character] or "")
		end
		if self.charactertitle then
			self.charactertitle:SetString(STRINGS.CHARACTER_TITLES[character] or "")
		end
		if self.characterquote then
			self.characterquote:SetString(STRINGS.CHARACTER_QUOTES[character] or "")
		end
		if self.characterdetails then
			self.characterdetails:SetString(self.description_getter_fn(character) or "")
		end
		
		if self.la_health then
			if _G.TUNING.LAVAARENA_STARTING_HEALTH[string.upper(character)] ~= nil then
				self.la_health:SetString(STRINGS.UI.PORTRAIT.HP .. " : " .. _G.TUNING.LAVAARENA_STARTING_HEALTH[string.upper(character)])
			else
				self.la_health:SetString("")
			end
		end
		
		if self.la_items then
			local hero_items = _G.TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA[string.upper(character)]
			if hero_items ~= nil then
				local items_str = STRINGS.UI.PORTRAIT.ITEMS .. " : "
				for i, item in pairs(hero_items) do
					local item_name = type(item) == "table" and i or item
					items_str = items_str .. ((STRINGS.FORGED_FORGE.WEAPONS[string.upper(item_name)] and STRINGS.FORGED_FORGE.WEAPONS[string.upper(item_name)].NAME) or (STRINGS.FORGED_FORGE.ARMOR[string.upper(item_name)] and STRINGS.FORGED_FORGE.ARMOR[string.upper(item_name)].NAME)) .. ", "
				end
				self.la_items:SetString(string.sub(items_str, 1, -3))
			else
				self.la_items:SetString("")
			end
		end
		
		if self.la_difficulty then
			local dif = _G.TUNING.LAVAARENA_SURVIVOR_DIFFICULTY[string.upper(character)]
			if dif ~= nil then
				self.la_difficulty:SetString(STRINGS.UI.PORTRAIT.DIFFICULTY .. " : " .. tostring((dif == 1 and "+") or (dif == 2 and "++") or "+++"))
			else
				self.la_difficulty:SetString("")
			end
		end
	end
end
AddClassPostConstruct( "widgets/redux/ovalportrait", EditOvalPortrait )

------------------
-- WaitingPanel --
------------------
-- Display the loadout of the player in their portrait (weapon and armor)
local WaitingForPlayers = require("widgets/waitingforplayers")
local _oldRefresh = WaitingForPlayers.Refresh
WaitingForPlayers.Refresh = function(self, force)
	_oldRefresh(self, force)
	for i, widget in ipairs(self.player_listing) do
		local player = self.players[i]
		if player ~= nil then
			local prefab = player.lobbycharacter or player.prefab or ""
			if prefab ~= "" and prefab ~= "random" and TUNING.GAMEMODE_STARTING_ITEMS[string.upper(_G.TheNet:GetServerGameMode())][string.upper(prefab)] then
				local starting_inv = TUNING.GAMEMODE_STARTING_ITEMS[string.upper(_G.TheNet:GetServerGameMode())][string.upper(prefab)]
				if starting_inv ~= nil then
					if widget.puppet._lastequips then
						if widget.puppet._lastequips.swaps then for _,v in pairs(widget.puppet._lastequips.swaps) do widget.puppet.animstate:ClearOverrideSymbol(v) end end
						if widget.puppet._lastequips.show then for _,v in pairs(widget.puppet._lastequips.show) do widget.puppet.animstate:Show(v) end end
						if widget.puppet._lastequips.hide then for _,v in pairs(widget.puppet._lastequips.hide) do widget.puppet.animstate:Hide(v) end end
						widget.puppet._lastequips = nil
					end
					local equipdata = {}
					for _,item in pairs(starting_inv) do
						local data = _G.AllForgePrefabs[item] ~= nil and _G.AllForgePrefabs[item].swap_data
						if data ~= nil and type(data) == "table" then
							if data.swap then
								if not equipdata.swaps then equipdata.swaps = {} end
								--Can be a table with 3 variables {player_symbol, build, item_symbol}
								--Can also be set as a string value ("swap_hat", "swap_body", OR "swap_object") and it will automatically fill in the 3 needed arguments!
								for _,v in pairs(data.swap) do
									if type(v) == "string" then
										local itemswap = _G.AllForgePrefabs[item].swap_build or item
										widget.puppet.animstate:OverrideSymbol(v, itemswap, v == "swap_object" and itemswap or v)
										table.insert(equipdata.swaps, v)
									else
										widget.puppet.animstate:OverrideSymbol(v[1], v[2], v[3])
										table.insert(equipdata.swaps, v[1])
									end
								end
							end
							if data.hide then
								if not equipdata.show then equipdata.show = {} end
								for _,v in pairs(data.hide) do
									widget.puppet.animstate:Hide(v)
									table.insert(equipdata.show, v)
								end
							end
							if data.show then
								if not equipdata.hide then equipdata.hide = {} end
								for _,v in pairs(data.show) do
									widget.puppet.animstate:Show(v)
									table.insert(equipdata.hide, v)
								end
							end
						end
					end
					if _G.GetTableSize(equipdata) >= 1 then
						widget.puppet._lastequips = equipdata
					end
				end
			end
			
		end
	end
end

--[[
TODO dynamically change it based on current players in session and not max players
add listener for new players?
--]]
local Grid = require "widgets/grid"
-- Scales the player portraits in the waiting lobby fit the maximum number of player slots.
local function AdjustWaitingLobby(self)
	local screen_width = 812 -- This was found through testing
	local widget_width = 125
	local widget_height = 250
	local offset_width = 125
	local offset_height = 30
	local col = 3
	local row = 2
	local scalar = 2/row
	while col*row < #self.player_listing do
		if (col * (widget_width + offset_width) - offset_width) * scalar > screen_width then
			row = row + 1
			scalar = 2 / row
		else
			col = col + 1
		end
	end
	local scalars = {}
	
	-- Scale each widget based on number of players
	for i, widget in pairs(self.player_listing) do
		widget:SetScale(scalar)
	end
	-- Clear and Update grid based on amount of players
	local old_grid = self.list_root
	self.list_root = self.proot:AddChild(Grid())
	self.list_root:FillGrid(col, (widget_width + offset_width) * scalar, (widget_height + offset_height) * scalar, self.player_listing)
	self.list_root:SetPosition(-(widget_width + offset_width) * scalar * col / 2 + offset_width * scalar, (#self.player_listing > 3 and ((widget_height + offset_height)*scalar*(row - 1)/2) or 0) + 20)
	old_grid:Kill()
	self:RefreshPlayersReady()
end
AddClassPostConstruct( "widgets/waitingforplayers", AdjustWaitingLobby )

--------------
-- WxpPanel --
--------------
local DetailedSummaryWidget = require "widgets/detailedsummarywidget"
local TeamStatsWidget = require "widgets/teamstatswidget"
local MvpWidgetTracker = require "widgets/mvploadingwidget"
local SideScrollingList = require "widgets/side_scrolling_list"
local PlayerAvatarPortrait = require "widgets/redux/playeravatarportrait"
local PlayerBadge = require "widgets/playerbadge"

-- TODO replace with regular postinit
-- called shortly after mvp widgets are created
-- Updates player stats for their badges and creates the detailed summary screen for each player.
local function StartTrackingHook(self)
	local function UpdatePlayerListing(widget, data)
		local empty = data == nil or _G.next(data) == nil

		widget.userid = not empty and data.user.userid or nil
		widget.performance = not empty and data.user.performance or nil
		
		if empty then
			widget.badge:Hide()
			widget.puppet:Hide()
		else
			local prefab = data.user.lobbycharacter or data.user.prefab or ""
			if prefab == "" then
				widget.badge:Set(prefab, _G.DEFAULT_PLAYER_COLOUR, false, 0)
				widget.badge:Show()
				widget.puppet:Hide()
			else
				widget.badge:Hide()
				widget.puppet:SetSkins(prefab, data.user.base, data.user, true)
				widget.puppet:SetBackground(data.user.portrait)
				widget.puppet:Show()
			end
		end

		widget.playername:SetColour(unpack(not empty and data.user.colour or _G.DEFAULT_PLAYER_COLOUR))
		widget.playername:SetTruncatedString((not empty) and (data.user.name) or "", 200, nil, "...")

		widget.fake_rand = not empty and data.user.colour ~= nil and (data.user.colour[1] + data.user.colour[2] + data.user.colour[3]) / 3 or .5
	end
	mvp_inst = self
	
	local old_PopulateData = self.PopulateData -- not used atm
	function self:PopulateData(ent, ...)
		local mvp_cards = _G.Settings.match_results.mvp_cards or TheFrontEnd.match_results.mvp_cards
		local player_stats = _G.Settings.match_results.player_stats or _G.TheFrontEnd.match_results.player_stats		
		
		self.list_root:KillAllChildren()
		self.mvp_widgets = {}
		
		-- TODO add more anim???
		local card_anims = {{"emoteXL_waving1", 0.5}, {"emote_loop_sit4", 0.5}, {"emoteXL_loop_dance0", 0.5}, {"emoteXL_happycheer", 0.5}, {"emote_loop_sit1", 0.5}, {"emote_strikepose", 0.25}}

		if mvp_cards ~= nil and #mvp_cards > 0 then
			local list_options = {
				max_items_displayed = 6,
				button_offsets = {x = 0, y = -300, z = 0},
				height = 700,
				item_width = 250,
			}
			self.mvp_widget_list = self:AddChild( SideScrollingList(self.mvp_widgets, list_options) )
			self.mvp_widget_list:SetPosition(0,0)
			
			local function ToggleMVPWidgets()
				if self.mvp_widget_list:IsVisible() then
					self.mvp_widget_list:Hide()
					self.mvp_widget_list:HideItems()
					self.team_stats_button:Hide()
					return true
				else
					self.mvp_widget_list:Show()
					self.mvp_widget_list:ShowItems()
					self.team_stats_button:Show()
					return false
				end
			end
			
			-- Player ID link to index
			local player_slots = {}
			local stat_rankings = {}
			local team_stat_rankings = {}
			if player_stats then
				for i,stats in pairs(player_stats.data) do
					player_slots[stats[1]] = i
				end
				
				-- Find each players rank within each stat
				for i=4, #player_stats.fields, 1 do
					-- Get each players stat info
					local stat_ranks = {}
					for j,stats in pairs(player_stats.data) do
						local player = stats[1] or _G.STRINGS.UI.MVP_LOADING_WIDGET.LAVAARENA.DESCRIPTIONS.unknown
						local val = stats[i] or 0
						table.insert(stat_ranks, {player = player, val = val})
					end
					-- Rank each player by highest stat value
					table.sort(stat_ranks, function(a,b)
						return a.val > b.val
					end)
					-- Link player id to their stat rankings
					for rank,stat_data in pairs(stat_ranks) do
						if not stat_rankings[stat_data.player] then stat_rankings[stat_data.player] = {} end
						-- If the player above the current rank has the same stat value then they will copy that players rank else they will get the current rank
						stat_rankings[stat_data.player][i] = ((rank > 1) and (stat_ranks[rank - 1].val == stat_data.val) and stat_rankings[stat_ranks[rank - 1].player][i]) or rank
					end
					-- Keep list of stats in ranking order for team stats
					team_stat_rankings[i] = stat_ranks
				end
			else
				print("[ERROR] MVPLoadingWidget - player stats are nil")
			end		
		
			-- Build the required widgets
			for _, data in ipairs(mvp_cards) do
				local widget = self.list_root:AddChild(Widget("playerwidget"))

				local backing = widget:AddChild(Image("images/global_redux.xml", "mvp_panel.tex"))
				backing:SetPosition(0, 30)
				backing:SetScale(0.85, 1)

				widget.badge = widget:AddChild(PlayerBadge("", _G.DEFAULT_PLAYER_COLOUR, false, 0))

				widget.puppet = widget:AddChild(PlayerAvatarPortrait())
				widget.puppet:SetScale(1.25)
				widget.puppet:SetPosition(0, 140)
				widget.puppet:SetClickable(false)
				widget.puppet:AlwaysHideRankBadge() -- no space and mine is shown on XP bar
				local random_anim = math.floor((data.beststat[2] or 0) % #card_anims) + 1
				widget.puppet.puppet.animstate:SetBank("wilson")
				widget.puppet.puppet.animstate:SetPercent(card_anims[random_anim][1], card_anims[random_anim][2])
				widget.puppet:DoNotAnimate()

				widget.playername = widget:AddChild(Text(_G.TITLEFONT, 45))
				widget.playername:SetPosition(2, -38)
				widget.playername:SetHAlign(_G.ANCHOR_LEFT)

				local line = widget:AddChild(Image("images/ui.xml", "line_horizontal_white.tex"))
				line:SetScale(.8, .9)
				line:SetPosition(0, -67)
				local c = .6
				line:SetTint(_G.UICOLOURS.GOLD[1]*c, _G.UICOLOURS.GOLD[2]*c, _G.UICOLOURS.GOLD[3]*c, _G.UICOLOURS.GOLD[4])

				widget.title = widget:AddChild(Text(_G.TITLEFONT, 40, (_G.STRINGS.UI.MVP_LOADING_WIDGET[self.current_eventid].TITLES[data.participation and "none" or data.beststat[1]]), _G.UICOLOURS.GOLD))
				widget.title:SetPosition(0, -98)

				widget.score = widget:AddChild(Text(_G.CHATFONT, 45, tostring((data.beststat[2]) or _G.STRINGS.UI.MVP_LOADING_WIDGET[self.current_eventid].NO_STAT_VALUE), _G.UICOLOURS.EGGSHELL))
				widget.score:SetPosition(0, -146)

				widget.description = widget:AddChild(Text(_G.CHATFONT, 30, _G.STRINGS.UI.MVP_LOADING_WIDGET[self.current_eventid].DESCRIPTIONS[data.beststat[1] or "none"], _G.UICOLOURS.EGGSHELL))
				widget.description:SetPosition(0, -203)
				widget.description:SetRegionSize( 200, 66 )
				widget.description:SetVAlign(_G.ANCHOR_TOP)
				widget.description:EnableWordWrap(true)
				widget.beststat = data.beststat
				widget.participator = data.participator
				
				UpdatePlayerListing(widget, data)
				
				--[[
				DETAILED SUMMARY
				--]]
				if player_stats then
					print("[Stats] Detailed Summary Widget added!")
					widget.detailed_summary = self:AddChild( DetailedSummaryWidget(_G.Forge_options, player_stats.data[(player_slots[widget.userid])], player_stats.fields, stat_rankings[widget.userid], widget, (self.current_eventid or string.upper(TheNet:GetServerGameMode()))) )
					-- Access summary screen on click
					widget:SetClickable(true)
					local old_OnControl = widget.OnControl
					widget.OnControl = function(inst, control, down) --TODO setonclick????
						-- Only clickable if team stats are not displayed
						if not self.team_stats:IsVisible() then
							-- check for mouse click
							if down and control == 29 then
								local display = ToggleMVPWidgets()
								_G.TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/click_move")
								widget.detailed_summary:DisplaySummaryScreen(display)
								self.active_mvp = widget
								return true
							end
						end
						
						return old_OnControl(inst, control, down)
					end
						
					-- Highlight the mvp widget the mouse is on and team stats are not displayed
					widget:SetOnGainFocus(function()
						if not self.team_stats:IsVisible() then
							if widget.mvp_outline == nil then
								widget.mvp_outline = widget:AddChild(Image("images/forged_forge.xml", "mvp_widget_outline.tex"))
								widget.mvp_outline:SetPosition(0, 30) -- this needs to be the same as backing (which is a local value)
								widget.mvp_outline:SetScale(0.85, 1) -- this needs to be the same as backing (which is a local value)
							end
							widget.mvp_outline:Show()
							_G.TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/click_mouseover")
						end
					end)
					
					-- Hide outline when mouse is not on the mvp widget
					widget:SetOnLoseFocus(function()
						if not self.team_stats:IsVisible() and widget.mvp_outline then
							widget.mvp_outline:Hide()
						end
					end)
				else
					print("[ERROR] MVPLoadingWidget - player stats are nil and detailed summary will not be displayed")
				end
				widget:Hide()
				table.insert(self.mvp_widgets, widget)
			end
			-- Sort the mvp badges based on their best stats
			table.sort(self.mvp_widgets, function(a,b)
				local function GetStatStrAndTier(stat)
					local is_tier_2 = string.sub(stat, string.len(stat)) == "2"
					return {data = _G.TUNING.FORGE.DEFAULT_FORGE_TITLES[(stat ~= nil and is_tier_2 and string.sub(stat,1,-2) or stat)], tier = (is_tier_2 and 2 or 1)}
				end
				if a ~= nil and b ~= nil then
					local stat_a = GetStatStrAndTier(a.beststat[1])
					local stat_b = GetStatStrAndTier(b.beststat[1])
					if stat_a.str == nil then
						return false
					elseif stat_b.str == nil then
						return true
					else
						return not a.participator and not b.participator and (stat_a.tier > stat_b.tier or stat_a.tier == stat_b.tier and (stat_a.data.priority < stat_b.data.priority or (stat_a.data.priority == stat_b.data.priority and a.beststat[2] > b.beststat[2]))) or not a.participator and b.participator or a.participator and not b.participator
					end
				else
					return false
				end
			end)
			self.mvp_widget_list:UpdateItems(self.mvp_widgets, options)
			
			-- Create Team Stats
			self.team_stats = self:AddChild( TeamStatsWidget(_G.Forge_options, player_stats, team_stat_rankings, player_slots, mvp_cards, self.mvp_widgets, (self.current_eventid or string.upper(TheNet:GetServerGameMode()))) )
			self.team_stats_button = self:AddChild( TextButton() )
			self.team_stats_button:SetPosition(0,450)
			self.team_stats_button.text:SetSize(50)
			self.team_stats_button:SetText(STRINGS.UI.DETAILEDSUMMARYSCREEN.TEAMSTATS.SHOW)
			
			-- Toggle Display on click
			self.team_stats_button:SetOnClick(function()
				if self.team_stats:IsVisible() then
					self.team_stats:Hide()
					self.team_stats_button:SetText(STRINGS.UI.DETAILEDSUMMARYSCREEN.TEAMSTATS.SHOW)
					self.team_stats:ResetMVP()
					self.mvp_widget_list:Show()
					self.mvp_widget_list:ShowItems()
				else					
					self.team_stats:Show()
					self.mvp_widget_list:Hide()
					self.mvp_widget_list:HideItems()
					self.team_stats_button:SetText(STRINGS.UI.DETAILEDSUMMARYSCREEN.TEAMSTATS.HIDE)
					self.team_stats:SetMVP(self.mvp_widgets[1], "0")
				end
			end)
			self.team_stats_button:Show()
			self.mvp_widget_list:MoveToBack()
		end
	end
end
StartTrackingHook(MvpWidgetTracker)

-- overrides the mvploadingwidget to be clickable
AddClassPostConstruct( "widgets/mvploadingwidget", function(self) self:SetClickable(true) end)



--[[-----------------------------------------------------------------------------------------
										Combat
-------------------------------------------------------------------------------------------]]

--New combat stuff for alt attacks and damage buffs
--TODO: Make sure EVERY single damage source has at least a damagetype clarified
--(Might set it so that it automatically has physical as a default) and also fill in stimuli wherever it seems reasonable to do so
_G.DAMAGETYPES = {
	PHYSICAL = 1,
	MAGIC = 2,
	SOUND = 3,
	GAS = 4,
	LIQUID = 5
}
_G.DAMAGETYPE_IDS = {}
for k,v in pairs(_G.DAMAGETYPES) do
	_G.DAMAGETYPE_IDS[v] = k
end

--TODO: Make this all compatible with mounts and saddle bonuses
AddComponentPostInit("combat", function(self)
	self.damage_override = nil
	self.damagetype = nil
	self.damagebuffs = { dealt = {}, recieved = {} }
	
	_OldStartTrackingTarget = self.StartTrackingTarget
	function self:StartTrackingTarget(target)
		if target then
			self.target_aggro_start = _G.GetTime()
		end
		_OldStartTrackingTarget(self, target)
	end
	
	_OldStopTrackingTarget = self.StopTrackingTarget
	function self:StopTrackingTarget(target)
		if target ~= nil then
			local target_player = target:HasTag("player") and target or target.components.follower and target.components.follower.leader and target.components.follower.leader:HasTag("player") and target.components.follower.leader or nil
			if _G.TheWorld and _G.TheWorld.components.stat_tracker and target_player then
				_G.TheWorld.components.stat_tracker:AdjustStat("aggroheld", target_player, _G.GetTime() - (self.target_aggro_start or _G.GetTime()))
			end
		end
		_OldStopTrackingTarget(self, target)
	end
	
	function self:SetDamageType(damagetype)
		self.damagetype = damagetype
	end
	
	function self:HasDamageBuff(buffname, recieved)
		return self.damagebuffs[recieved and "recieved" or "dealt"][buffname] ~= nil
	end
	
	function self:AddDamageBuff(buffname, data, recieved)
		--buffname = unique string name pertaining to this buff (Make sure it is a very unique name as to not conflict with other damage buffs)
		--data.buff = buff value (Remember, as additive add 0.1 for +10%, as multiplicative add 1.1 for +10%, and as a flat add value put in an actual damage number that adds to the overall damage)
		--(Remember, data.buff can be a number value OR a function using the arguments [attacker, victim, weapon, stimuli]. Just make sure it returns the correct number value.)
		--data.addtype = nil or "mult" if multiplicative, "add" if additive, "flat" if flat added damage
		--data.stimuli = string value for a buff pertaining to a specific stimuli
		--data.damagetype = number value (matching DAMAGETYPES table) for a buff pertaining to a specific damagetype
		--data.weapontype = string value for a buff pertaining to a specific weapontype
		--(If multiple of stimuli/damagetype/weapontype are set, all requirements must be met for the buff to apply)
		--recieved = true if modifier on damage taken rather than damage dealt
		if not self:HasDamageBuff(buffname, recieved) then
			local buff = type(data) == "number" and {buff = data} or data
			self.damagebuffs[recieved and "recieved" or "dealt"][buffname] = buff
		end
	end
	
	function self:RemoveDamageBuff(buffname, recieved)
		self.damagebuffs[recieved and "recieved" or "dealt"][buffname] = nil
	end
	
	function self:CopyBuffsTo(ent)
		--Used to make 1 entity's damage component read buffs off of another (like shadow dueslists off of maxwell)
		--WARNING: When using this function, if either entity adds or removes a buff it will affect BOTH entities.
		if ent and ent.components.combat then
			ent.components.combat.damagebuffs = self.damagebuffs
		end
	end
	
	_oldGetAttackRange = self.GetAttackRange
	function self:GetAttackRange()
		local weapon = self:GetWeapon()
		if weapon and weapon.components.weapon:CanAltAttack() then
			return weapon.components.weapon.altattackrange
		else
			return _oldGetAttackRange(self)
		end
	end
	
	_oldGetHitRange = self.GetHitRange
	function self:GetHitRange()
		local weapon = self:GetWeapon()
		if weapon and weapon.components.weapon:CanAltAttack() then
			return self.attackrange + weapon.components.weapon.althitrange
		else
			return _oldGetHitRange(self)
		end
	end
	
	--cuz SourceModifierList is stupid
	local function calcbuff(recieved, attacker, victim, weapon, stimuli)
		local mult = 1
		local add = 1
		local flat = 0
		
		local source = weapon ~= nil and weapon.components.weapon or self
		for buff,data in pairs(self.damagebuffs[recieved and "recieved" or "dealt"]) do
			local canbuff = true
			if data.stimuli and stimuli ~= data.stimuli then
				canbuff = false
			end
			if data.damagetype and source.damagetype ~= data.damagetype then
				canbuff = false
			end
			if data.weapontype then
				if weapon == nil or not (weapon.components.itemtype ~= nil and weapon.components.itemtype:IsType(data.weapontype)) then
					canbuff = false
				end
			end
			
			if canbuff then
				local buff = type(data.buff) == "function" and (data.buff(attacker, victim, weapon, stimuli) or (data.addtype == "flat" and 0 or 1)) or data.buff
				if not data.addtype or data.addtype == "mult" then
					mult = mult * buff
				elseif data.addtype == "add" then
					add = add + buff
				elseif data.addtype == "flat" then
					flat = flat + buff
				end
			end
		end
		
		return mult, add, flat
	end
	
	--function self:GetBuffMults(recieved, attacker, victim, weapon, stimuli)
		--return calcbuff(recieved, attacker, victim, weapon, stimuli)
	--end
	
	_oldCalcDamage = self.CalcDamage
	function self:CalcDamage(target, weapon, multiplier)
		if target:HasTag("alwaysblock") then
			return 0
		end
		
		--Fid: Weird hack, but it's mathematically sound so I'm pretty sure it's not going to cause any explosions...?
        local dmg = _oldCalcDamage(self, target, weapon, multiplier)
        local basedamage = weapon ~= nil and weapon.components.weapon.damage or self.defaultdamage
        if (weapon and weapon.components.weapon:CanAltAttack()) or self.damage_override then
            local altdamage = 0
            if self.damage_override then
                altdamage = type(self.damage_override) == "function" and self.damage_override(self.inst, target) or self.damage_override
            else
                altdamage = weapon.components.weapon.altdamagecalc ~= nil and weapon.components.weapon.altdamagecalc(weapon, self.inst, target) or (weapon.components.weapon.altdamage or basedamage)
            end
            dmg = (dmg - (self.damagebonus or 0)) / basedamage
            dmg = dmg * altdamage + (self.damagebonus or 0)
        end
		
		--Also plugging here to apply damage dealt buffs since this function only ever runs from the dealing damage side
		local mult, add, flat = calcbuff(false, self.inst, target, weapon, self.currentstimuli)
		self.currentstimuli = nil
		return dmg * mult * add + flat
	end
	
	_oldGetWeapon = self.GetWeapon
    function self:GetWeapon()
        return not self.damage_override and _oldGetWeapon(self) or nil
    end
	
	_oldDoAttack = self.DoAttack
	function self:DoAttack(target_override, weapon, projectile, stimuli, instancemult)
		self.currentstimuli = stimuli
		_oldDoAttack(self, target_override, weapon, projectile, stimuli, instancemult)
	end
	
	_oldDoAreaAttack = self.DoAreaAttack
	function self:DoAreaAttack(target, range, weapon, validfn, stimuli, excludetags)
		self.currentstimuli = stimuli
		return _oldDoAreaAttack(self, target, range, weapon, validfn, stimuli, excludetags)
	end
	
	_oldGetAttacked = self.GetAttacked
	function self:GetAttacked(attacker, damage, weapon, stimuli)
		local mult, add, flat = calcbuff(true, attacker, self.inst, weapon, stimuli)
		damage = damage * mult * add + flat
		return _oldGetAttacked(self, attacker, damage, weapon, stimuli)
	end
	
	function self:DoSpecialAttack(dmg, target_override, stimuli, instancemult, aoe_data) --Used for special attacks from players, like WX's shock attack
		self.damage_override = dmg --dmg can be a function or a number, as a function the arguments will be: attacker, target
		if aoe_data then --target_override and instancemult arguments do not get used if doing an aoe attack and valid aoe_data variables are target, range, validfn, and excludetags
			self:DoAreaAttack(aoe_data.target, aoe_data.range, nil, aoe_data.validfn, stimuli, aoe_data.excludetags)
		else
			self.ignorehitrange = true --Don't mind me, just slapping on a bandaid fix
			self:DoAttack(target_override, nil, nil, stimuli, instancemult)
			self.ignorehitrange = false
		end
		self.damage_override = nil
	end
	
	
	if self.inst:HasTag("LA_mob") then
		self:AddDamageBuff("mobdamage_dealt", _G.Forge_options.mobdamage_dealt)
		self:AddDamageBuff("mobdamage_taken", _G.Forge_options.mobdamage_taken, true)
	end
end)

--TODO: Maybe put altcondition in an OnUpdate check to automatically swap the weapon's alt state?
AddComponentPostInit("weapon", function(self)
	self.hasaltattack = false
	self.isaltattacking = false
	self.altattackrange = nil
	self.althitrange = nil
	self.altdamage = nil
	self.altdamagecalc = nil
	self.altcondition = nil
	self.altprojectile = nil
	self._projectile = nil --ZARKLORD: FIDOOOP YOU LITERALY POOPED OUR CODEBASE!!?!?!?! --"poop" -- Just in case. It'll be overided in SetProjectile
							--Fid: It was Cunning Fox actually... @n@
							--Fox: No u
							--Fid: Ok it was probably Chris
	self.damagetype = nil
	self.altdamagetype = nil
	self._damagetype = nil
	
	function self:SetDamageType(damagetype)
		self.damagetype = damagetype
		self._damagetype = damagetype
	end
	
	function self:SetStimuli(stimuli)
		self.stimuli = stimuli
	end
	
	--Trying to fix invisible poj after using the AltAtk
	function self:SetProjectile(projectile)
		self.projectile = projectile
		self._projectile = projectile
	end
	
	function self:SetAltAttack(damage, range, proj, damagetype, damagecalcfn, conditionfn) --Used to give weapons a 2nd attack
		self.hasaltattack = true
		self.altdamage = damage ~= nil and damage or self.damage
		if range ~= nil then
			self.altattackrange = (type(range) == "table" and range[1] or range) or self.attackrange
			self.althitrange = type(range) == "table" and range[2] or self.altattackrange
		else
			--Uhmmmm.......... Is this healthy?
			self.altattackrange = 20
			self.althitrange = 20
		end
		self.altprojectile = proj --Not required, only uses projectile and complexprojectile. An aimedprojectile alt needs to be used through something like aoespell
		self.altdamagetype = damagetype ~= nil and damagetype or self.damagetype --Used incase an alt attack is a different damagetype than the main attack
		self.altdamagecalc = damagecalcfn --Used for when the damage could turn out as a different value under a specific circumstance like with infernal staff (Will default to self.altdamage)
		self.altcondition = conditionfn --Used for telling the weapon when it should alt attack
		
		if self.altdamagecalc then
			local _olddamagecalc = self.altdamagecalc
			self.altdamagecalc = function(weapon, attacker, target)
				local dmg = _olddamagecalc(weapon, attacker, target)
				return dmg ~= nil and dmg or self.altdamage
			end
		end
		
		if self.inst.replica.inventoryitem then
			self.inst.replica.inventoryitem:SetAltAttackRange(self.altattackrange)
		end
	end
	
	function self:SetIsAltAttacking(alt, no_netvar)
		self.isaltattacking = alt
		if self.inst.replica.inventoryitem then--and not no_netvar then
			self.inst.replica.inventoryitem:SetIsAltAttacking(alt)
		end
		--*intense grumbling*
		if alt then
			self.projectile = self.altprojectile
			self.damagetype = self.altdamagetype
		else
			self.projectile = self._projectile
			self.damagetype = self._damagetype
		end
	end
	
	function self:HasAltAttack()
		return self.hasaltattack
	end
	
	function self:CanAltAttack()
		if self:HasAltAttack() and self.isaltattacking then
			return self.altcondition ~= nil and self.altcondition(self.inst) or true
		end
	end
	
	_oldCanRangedAttack = self.CanRangedAttack
	function self:CanRangedAttack()
		return self:CanAltAttack() and self.altprojectile or _oldCanRangedAttack(self)
	end
	
	function self:DoAltAttack(attacker, target_override, projectile, stimuli, instancemult) --Used to force an alt attack out for situations where the alt system is handled seperately like with aoespell
		self:SetIsAltAttacking(true, true)
		--Set setalt to true if the alt is a single attack or AOE attack.
		--If the alt hits rapidly though, leave it false and control SetIsAltAttacking before and after the rapid attacks all trigger (This way we avoid rapidly pushing a netbool)
		if attacker and attacker.components.combat and self:CanAltAttack() then
			if target_override and type(target_override) == "table" and not target_override.prefab then --You can pass a table of collected entities through the target_override argument to do an AOE attack
				for _,ent in ipairs(target_override) do
					if ent ~= attacker and attacker.components.combat:IsValidTarget(ent) and ent.components.health and not ent.components.health:IsDead() then
						--I'm deciding not to push this event since mob collection occurs before this function runs and
						--I don't want 2 events pushed for every attack
						--attacker:PushEvent("onareaattackother", { target = ent, weapon = self.inst, stimuli = stimuli }) --
						attacker.components.combat:DoAttack(ent, self.inst, projectile, stimuli, instancemult)
					end
					--attacker:PushEvent("alt_attack_complete", {})
				end
			else
				if attacker.components.combat:IsValidTarget(target_override) then
					attacker.components.combat:DoAttack(target_override, self.inst, projectile, stimuli, instancemult)
				end
			end
		end
		attacker:PushEvent("alt_attack_complete", {weapon = self.inst})
		self:SetIsAltAttacking(false, true)
	end
end)

AddComponentPostInit("equippable", function(self)
	self.uniquebuffs = {}
	self.damagebuffs = { dealt = {}, recieved = {} }
	
	function self:AddBuffsTo(owner)
		if owner then
			if owner.components.buffable then
				owner.components.buffable:AddBuff(self.inst.prefab, self.uniquebuffs)
			end
			if owner.components.combat then
				for buff,data in pairs(self.damagebuffs.dealt) do
					owner.components.combat:AddDamageBuff(buff, data)
				end
				for buff,data in pairs(self.damagebuffs.recieved) do
					owner.components.combat:AddDamageBuff(buff, data, true)
				end
			end
		end
	end
	
	function self:RemoveBuffsFrom(owner)
		if owner then
			if owner.components.buffable then
				owner.components.buffable:RemoveBuff(self.inst.prefab)
			end
			if owner.components.combat then
				for buff,data in pairs(self.damagebuffs.dealt) do
					owner.components.combat:RemoveDamageBuff(buff)
				end
				for buff,data in pairs(self.damagebuffs.recieved) do
					owner.components.combat:RemoveDamageBuff(buff, true)
				end
			end
		end
	end
	
	function self:AddUniqueBuff(data)
		for bufftype,buff in pairs(data) do
			self.uniquebuffs[bufftype] = buff
		end
		if self:IsEquipped() then
			local owner = self.inst.components.inventoryitem.owner
			if owner and owner.components.buffable then
				owner.components.buffable:AddBuff(self.inst.prefab, self.uniquebuffs)
			end
		end
	end
	
	function self:RemoveUniqueBuff(bufftype)
		self.uniquebuffs[bufftype] = nil
		if self:IsEquipped() then
			local owner = self.inst.components.inventoryitem.owner
			if owner and owner.components.buffable then
				owner.components.buffable:AddBuff(self.inst.prefab, self.uniquebuffs)
			end
		end
	end
	
	--All damagebuff related functions work exactly the same as in combat for sake of simplicity
	function self:HasDamageBuff(buffname, recieved)
		return self.damagebuffs[recieved and "recieved" or "dealt"][buffname] ~= nil
	end
	
	function self:AddDamageBuff(buffname, data, recieved)
		if not self:HasDamageBuff(buffname, recieved) then
			local buff = type(data) == "number" and {buff = data} or data
			self.damagebuffs[recieved and "recieved" or "dealt"][buffname] = buff
			
			if self:IsEquipped() then
				local owner = self.inst.components.inventoryitem.owner
				if owner and owner.components.combat then
					owner.components.combat:AddDamageBuff(buffname, data, recieved)
				end
			end
		end
	end
	
	function self:RemoveDamageBuff(buffname, recieved)
		self.damagebuffs[recieved and "recieved" or "dealt"][buffname] = nil
		
		if self:IsEquipped() then
			local owner = self.inst.components.inventoryitem.owner
			if owner and owner.components.combat then
				owner.components.combat:RemoveDamageBuff(buffname, recieved)
			end
		end
	end
	
	local _oldequip = self.Equip
	function self:Equip(owner)
		self:AddBuffsTo(owner)
		_oldequip(self, owner)
	end
	
	local _oldunequip = self.Unequip
	function self:Unequip(owner)
		self:RemoveBuffsFrom(owner)
		_oldunequip(self, owner)
	end
end)

AddClassPostConstruct("components/inventoryitem_replica", function(self)
	self.can_altattack_fn = function() return true end
	
	function self:SetIsAltAttacking(alt)
		self.classified.isaltattacking:set(alt)
	end
	
	function self:SetAltAttackRange(range)
		self.classified.altattackrange:set(range or self:AttackRange())
	end
	
	function self:AltAttackRange()
		if self.inst.components.weapon then
			return self.inst.components.weapon.altattackrange
		elseif self.classified ~= nil then
			return math.max(0, self.classified.altattackrange:value())
		else
			return self:AttackRange()
		end
	end
	
	function self:SetAltAttackCheck(fn)
		if fn and type(fn) == "function" then
			self.can_altattack_fn = fn
		end
	end
	
	function self:CanAltAttack()
		if self.classified.isaltattacking:value() then
			return self.inst.components.weapon and self.components.weapon:CanAltAttack() or self.can_altattack_fn(self.inst)
		end
	end
	
	_oldAttackRange = self.AttackRange
	function self:AttackRange()
		return self:CanAltAttack() and self:AltAttackRange() or _oldAttackRange(self)
	end
end)

--No more attacking your friends!
AddClassPostConstruct("components/combat_replica", function(self)
    local _oldCanBeAttacked = self.CanBeAttacked
    function self:CanBeAttacked(attacker)
        if self.inst:HasTag("companion") and not self.inst:HasTag("LAmob") and attacker:HasTag("player") then
            return false
        end
        return _oldCanBeAttacked(self, attacker)
    end
end)



--[[-----------------------------------------------------------------------------------------
									Admin/Debug Menu
-------------------------------------------------------------------------------------------]]
require("admin_command_list")
local AdminCommandTabs = require "widgets/admin_command_tabs"
local admin_commands

-- Add the Admin/Debug menu if the client is admin
local function AddAdminCommands(self)
	-- FOR IMAGEBUTTON
	local function OnGainFocus(self)
		self._base.OnGainFocus(self)

		if self.hover_overlay then
			self.hover_overlay:Show()
		end

		if self:IsSelected() then return end

		if self:IsEnabled() then
			self.image:SetTexture(self.atlas, self.image_focus)

			if self.size_x and self.size_y then 
				self.image:ScaleToSize(self.size_x, self.size_y)
			end
		end

		if self.image_focus == self.image_normal and self.scale_on_focus and self.focus_scale then
			--self.image:SetScale(self.focus_scale[1], self.focus_scale[2], self.focus_scale[3])
			self.image:ScaleToSize(self.size_x * self.focus_scale[1], self.size_y * self.focus_scale[2])
		end

		if self.imagefocuscolour then
			self.image:SetTint(unpack(self.imagefocuscolour))
		end

		if self.focus_sound then
			TheFrontEnd:GetSound():PlaySound(self.focus_sound)
		end
	end

	local function OnLoseFocus(self)
		self._base.OnLoseFocus(self)

		if self.hover_overlay then
			self.hover_overlay:Hide()
		end

		if self:IsSelected() then return end

		if self:IsEnabled() then
			self.image:SetTexture(self.atlas, self.image_normal)

			if self.size_x and self.size_y then 
				self.image:ScaleToSize(self.size_x, self.size_y)
			end
		end

		if self.image_focus == self.image_normal and self.scale_on_focus and self.normal_scale then
			--self.image:SetScale(self.normal_scale[1], self.normal_scale[2], self.normal_scale[3])
			self.image:ScaleToSize(self.size_x, self.size_y)
		end

		if self.imagenormalcolour then
			self.image:SetTint(self.imagenormalcolour[1], self.imagenormalcolour[2], self.imagenormalcolour[3], self.imagenormalcolour[4])
		end
	end
	
	local controls = self
	
	-- Only admins get the admin/debug menu
	if _G.ThePlayer.Network:IsServerAdmin() then
		admin_commands = controls.left_root:AddChild(AdminCommandTabs(controls.owner, controls.top_root))
		
		local width, height = 30, 30
		controls.button_bg = controls:AddChild(Image("images/servericons.xml", "bg_rust.tex"))
		controls.button = controls:AddChild(ImageButton("images/avatars.xml", "avatar_admin.tex"))
		controls.button:ForceImageSize(width, height)
		controls.button.OnGainFocus = function(controls) OnGainFocus(controls) end
		controls.button.OnLoseFocus = function(controls) OnLoseFocus(controls) end
		controls.button_bg:SetSize(width, height)
		controls.button:SetPosition(width/2, height/2, 0)
		controls.button_bg:SetPosition(width/2, height/2, 0)
		controls.button:SetOnClick(function()
			if admin_commands.display then
				controls.button_bg:SetTexture("images/servericons.xml", "bg_rust.tex")
			else
				controls.button_bg:SetTexture("images/servericons.xml", "bg_green.tex")
			end
			controls.button_bg:SetSize(width, height)
			admin_commands:ToggleDisplay()
		end)
	end
end
AddClassPostConstruct( "widgets/controls", AddAdminCommands )

AddClassPostConstruct("screens/playerhud", function(self)
	local is_forge_admin = _G.TheNet:GetServerGameMode() == "lavaarena" and _G.ThePlayer.Network:IsServerAdmin()
	local PlayerHudIsCraftingOpen = self.IsCraftingOpen
	self.IsCraftingOpen = function(self)
		if is_forge_admin then
			return admin_commands.crafting.open or admin_commands.controllercraftingopen
		else
			return PlayerHudIsCraftingOpen(self)
		end
	end
end)

--[[-----------------------------------------------------------------------------------------
										Other
-------------------------------------------------------------------------------------------]]
-------------
-- Filters --
-------------
local filter_index = _G.Forge_options.default_filter
local filters = {"lavaarena2_cc", "day05_cc", "dusk03_cc", "night03_cc", "snow_cc", "snowdusk_cc", "night04_cc", "summer_day_cc", "summer_dusk_cc", "summer_night_cc", "spring_day_cc", "spring_dusk_cc", "spring_night_cc", "insane_day_cc", "insane_dusk_cc", "insane_night_cc", "purple_moon_cc", "sw_mild_day_cc", "sw_wet_day_cc", "sw_green_day_cc", "sw_volcano_cc", "beaver_vision_cc", "caves_default", "fungus_cc", "ghost_cc", "identity_colourcube", "mole_vision_off_cc", "mole_vision_on_cc", "ruins_dark_cc", "ruins_dim_cc", "ruins_light_cc", "sinkhole_cc", "quagmire_cc"}

local function NotInGame()
	return not _G.ThePlayer or not _G.ThePlayer.HUD or _G.ThePlayer.HUD:HasInputFocus()
end

local function ApplyFilter(filter)
	_G.TheWorld:PushEvent("overridecolourcube", _G.resolvefilepath("images/colour_cubes/" .. tostring(filter) .. ".tex"))
end

AddSimPostInit(function()
	if _G.TheNet:IsDedicated() then return end
	-- Apply default filter
	ApplyFilter(filters[_G.Forge_options.default_filter])
end)

-- Change filter to the next one in the list
TheInput:AddKeyDownHandler(_G.Forge_options.ADJ_FILTER_KEY, function()
	if NotInGame() then return end
	filter_index = filter_index + (TheInput:IsKeyDown(_G.KEY_SHIFT) and -1 or 1)
	if filter_index > #filters then
		filter_index = 1
	elseif filter_index < 0 then
		filter_index = #filters
	end
	_G.ThePlayer.components.talker:Say("Current Filter: " .. tostring(filters[filter_index]))
	ApplyFilter(filters[filter_index])
end)


--Make those pesky indicators less pesky
AddClassPostConstruct("widgets/targetindicator", function(self)
	self:SetClickable(false)
	local _oldOnUpdate = self.OnUpdate
	function self:OnUpdate()
		if _G.Forge_options.hide_indicators then
			self:Hide()
		else
			_oldOnUpdate(self)
			self:SetScale(0.65)
		end
	end
end)

AddPrefabPostInitAny(function(inst)
	inst:DoTaskInTime(0, function()
		if inst:IsValid() and _G.TheNet:GetIsServer() and not inst:HasTag("NOCLICK") and not inst:HasTag("LA_mob") and not inst:HasTag("corpse") and not inst.replica.inventoryitem then
			inst:AddTag("NOCLICK")
			if _G.TheWorld and _G.TheWorld.ismastersim then
				if _G.TheWorld.components.lavaarenaevent:IsIntermission() or inst:HasTag("player") then --Left a player check in here because corpse tag isn't being added fast enough... We'll have to fix it.
					inst:RemoveTag("NOCLICK")
				else
					inst.tempnoclick = true
				end
			end
		end
	end)
	
	if (_G.Forge_options.invisible_players and (inst:HasTag("player") or inst:HasTag("companion"))) or (_G.Forge_options.invisible_mobs and inst:HasTag("LA_mob")) then
		if inst.AnimState and not _G.Forge_options.redlight_greenlight then
			inst.AnimState:SetMultColour(0,0,0,0)
		end
	end
	
	if _G.Forge_options.superknockback and inst:HasTag("LA_mob") and inst.components and inst.components.combat then
		local REPEL_RADIUS = 5
		local REPEL_RADIUS_SQ = REPEL_RADIUS * REPEL_RADIUS

		local function OnHitOther(inst, other) --knockback
			if other.sg and other.sg.sg.events.knockback then
				other:PushEvent("knockback", {knocker = inst, radius = 10})
			else
				--this stuff below is mostly left here for creatures in basegame. For modders that are reading this, use the knockback event above.
				if other ~= nil and not (other:HasTag("epic") or other:HasTag("largecreature")) then
					if other:IsValid() and other.entity:IsVisible() and not (other.components.health ~= nil and other.components.health:IsDead()) then
						if other.components.combat ~= nil then
							--other.components.combat:GetAttacked(inst, 10)
							if other.Physics ~= nil then
								local x, y, z = inst.Transform:GetWorldPosition()
								local distsq = other:GetDistanceSqToPoint(x, 0, z)
								--if distsq < REPEL_RADIUS_SQ then
									if distsq > 0 then
										other:ForceFacePoint(x, 0, z)
									end
									local k = .5 * distsq / REPEL_RADIUS_SQ - 1
									other.speed = 60 * k
									other.dspeed = 2
									other.Physics:ClearMotorVelOverride()
									other.Physics:Stop()
									
									if other.components.inventory and other.components.inventory:ArmorHasTag("heavyarmor") or other:HasTag("heavybody") then 
										--Leo: Need to change this to check for bodyslot for these tags.
										other:DoTaskInTime(0.1, function(inst) 
											other.Physics:SetMotorVelOverride(-_G.TUNING.FORGE.KNOCKBACK_RESIST_SPEED, 0, 0) 
										end)
									else
										other:DoTaskInTime(0, function(inst) 
											other.Physics:SetMotorVelOverride(-_G.TUNING.FORGE.BOARILLA.ATTACK_KNOCKBACK, 0, 0) 
										end)
									end
									other:DoTaskInTime(0.4, function(inst) 
										other.Physics:ClearMotorVelOverride()
										other.Physics:Stop()
									end)
								--end
							end
						end
					end
				end
			end
		end
		
		inst.components.combat.onhitotherfn = OnHitOther
	end
	
	if inst:HasTag("player") and _G.Forge_options.redlight_greenlight then
		if _G.TheNet:GetIsServer() then
			inst:DoTaskInTime(0, function()
				if _G.TheWorld and _G.TheWorld.components.lavaarenaevent and _G.TheWorld.components.lavaarenaevent:GetBoarlord() and _G.TheWorld.components.lavaarenaevent:GetBoarlord().current_light_phase then
					_G.TheWorld.components.lavaarenaevent:GetBoarlord():ColorPlayer(inst)
				end
			end)
			
			inst.TryKillPlayer = function()
				if _G.TheWorld and _G.TheWorld.components.lavaarenaevent:GetBoarlord().current_light_phase == "red" and not inst.components.health:IsDead() then
					inst.components.health:DoDelta(-inst.components.health.currenthealth, nil, "pugnas_redlight", true, nil, true)
				end
			end
			
			local isvalidstate = function(name)
				if name == "emote" or name == "death" or name == "corpse" or name == "hit" or name == "repelled" or string.find(name, "hit_") or string.find(name, "knockback") then
					return true
				end
			end
			
			inst:ListenForEvent("newstate", function(inst, data)
				if not inst.sg:HasStateTag("idle") and not (data and isvalidstate(data.statename)) then
					inst:TryKillPlayer()
				end
			end)
		end
	end
end)

-- Fox: We need to changethe projectile sounds
-- but they are triggered in SG!
local function ChooseSound(inst, equip)
	local sounds = {
		livingstaff = "heal_staff",
		infernalstaff = "fireball",
	}
	
	if sounds[equip.prefab] then
		inst.sg.statemem.projectilesound = "dontstarve/common/lava_arena/"..sounds[equip.prefab]
	end
end

AddStategraphPostInit("wilson", function(self)
	local _attack = self.states.attack.onenter
	self.states.attack.onenter = function(inst)
		local equip = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
		
		if equip then
			ChooseSound(inst, equip)
		end
	
		_attack(inst)
	end
end)

AddStategraphPostInit("wilson_client", function(self)
	local _attack = self.states.attack.onenter
	self.states.attack.onenter = function(inst)
		local equip = inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
		
		if equip then
			ChooseSound(inst, equip)
		end
	
		_attack(inst)
	end
end)



local old_SaveGame = _G.SaveGame
function _G.SaveGame(isshutdown, cb)
    if _G.TheNet:GetServerGameMode() == "lavaarena" and _G.TheNet:GetIsServer() then
        --we don't save for lavaarena.
        if cb ~= nil then
            cb(true)
        end
        return
    else
        old_SaveGame(isshutdown, cb)
    end
end

-- Fox: This was made for UI stuff
local function SetDirty(netvar, val)
	netvar:set_local(val)
	netvar:set(val)
end

AddComponentPostInit("debuffable", function(self)
	local _AddDebuff = self.AddDebuff
	local _RemoveDebuff = self.RemoveDebuff
	
	function self:AddDebuff(name, ...)
		if self.debuffs[name] == nil and self.inst.player_classified then
			SetDirty(self.inst.player_classified.net_buffs, json.encode({name = name, removed = false}))
		end
		
		_AddDebuff(self, name, ...)
	end
	
	function self:RemoveDebuff(name, ...)
		if self.debuffs[name] ~= nil and self.inst.player_classified then
			SetDirty(self.inst.player_classified.net_buffs, json.encode({name = name, removed = true}))
		end
		
		_RemoveDebuff(self, name, ...)
	end
end)

-- Fox: We are doing this because we need to spawn ret right after pinging
--[[
AddComponentPostInit("reticule", function(self)
	local _ping = self.PingReticuleAt
	
	function self:PingReticuleAt(pos, ...)
		_ping(self, pos, ...)
		
		SendModRPCToServer(MOD_RPC.FORGE.SPAWN_RET, json.encode({x = pos.x, z = pos.z}))
	end
end)
]]

-- Fox: Also we need to track is the cast was successful or not
-- If not then instatnly removong our reticule
-- Only tracking those states
local states = {
	book = 25 * FRAMES,
	castspell = 53 * FRAMES,
}

local p = function(...) end

for state, time in pairs(states) do
	AddStategraphPostInit("wilson", function(self)
		local _onenter, _onexit, _onupdate = 
		self.states[state].onenter or p,
		self.states[state].onexit or p,
		self.states[state].onupdate or p
		
		
		self.states[state].onenter = function(inst)
			inst.sg.statemem.tm = 0
			inst.sg.statemem.trgt = time
			inst.sg.statemem.equip = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
			
			_onenter(inst)
		end
		
		self.states[state].onexit = function(inst)	
			if not inst.sg.statemem.equip then
				print("ERROR: Failed to get statemem.equip!")
				return
			end
			
			if inst.sg.statemem.tm < inst.sg.statemem.trgt and inst.sg.statemem.equip.components.reticule_spawner then
				inst.sg.statemem.equip.components.reticule_spawner:Interrupt()
			end
			
			_onexit(inst)
		end
		
		self.states[state].onupdate = function(inst, dt)
			inst.sg.statemem.tm = inst.sg.statemem.tm + dt
			
			_onupdate(inst)
		end
	end)
end

AddPrefabPostInit("explosivehit", function(inst)
	inst:DoTaskInTime(3, inst.Remove)
end)

--Zarklord: FORGIVE ME IT WAS THE ONLY WAY!
--Fox: This is why god stays in heaven
--[[
local debug = _G.debug
local __metatable = debug.getmetatable(_G.TheSim)
local old_GetEntitiesAtScreenPoint = __metatable.__index.GetEntitiesAtScreenPoint
__metatable.__index.GetEntitiesAtScreenPoint = function(...)
    local retvals = {old_GetEntitiesAtScreenPoint(...)}
    if _G.TheWorld and _G.TheWorld.components.lavaarenalivemobtracker and
	#_G.TheWorld.components.lavaarenalivemobtracker:GetAllLiveMobs() ~= 0 then
        for i, ent in ipairs(retvals[1]) do
            if ent and (ent:HasTag("player") or ent:HasTag("companion")) and not ent:HasTag("corpse") then
                table.remove(retvals[1], i)
            end
        end
    end
    return unpack(retvals)
end]]

--[[
AddComponentPostInit("locomotor", function(self)
	local _PushAction = self.PushAction
	function self:PushAction(bufferedaction, run, try_instant)
		print("PushAction:", tostring(bufferedaction), tostring(run), tostring(try_instant))
		return _PushAction(self, bufferedaction, run, try_instant)
	end
end)]]

-- Client-Host player level fix
AddPrefabPostInit("lavaarena_network", function(inst)
    local _client_host_level = _G.net_ushortint(inst.GUID, "lavaarena_network._client_host_level")
    
    if _G.TheWorld and _G.TheWorld.ismastersim then
        _client_host_level:set(0)
        if _G.TheNet and _G.TheNet:GetServerIsClientHosted() then
            _G.wxputils.GetEventStatus("lavaarena", 2, function(success)
                if success then
                    local host_level = _G.TheInventory:GetWXPLevel("lavaarena")
                    _client_host_level:set(host_level)
                    print("[Forged Forge] Setting host level to " .. tostring(host_level))
                else
                    print("[Forged Forge] Unable to find host level!")
                end
            end)
        end
	else
        _G.wxputils.GetEventStatus("lavaarena", 2, function() end)
    end
    
    function inst:GetClientHostLevel()
        return _client_host_level:value()
    end
end)

-- overrides for syncing client host level to widgets
local TrueScrollList = require "widgets/truescrolllist"
local old_TrueScrollList_SetItemsData = TrueScrollList.SetItemsData
TrueScrollList.SetItemsData = function(self, items)
    if items and next(items) ~= nil then
        for _,client in pairs(items) do
            if client.userid and client.performance and client.eventlevel and client.eventlevel == 0 then
                client.eventlevel = _G.TheWorld.net and _G.TheWorld.net and _G.TheWorld.net:GetClientHostLevel() or 0
                break
            end
        end
    end
    old_TrueScrollList_SetItemsData(self, items)
end

local old_GetSkinsDataFromClientTableData = _G.GetSkinsDataFromClientTableData
_G.GetSkinsDataFromClientTableData = function(client)
    if client and client.userid and client.performance and client.eventlevel and client.eventlevel == 0 then
        client.eventlevel = _G.TheWorld.net and _G.TheWorld.net and _G.TheWorld.net:GetClientHostLevel() or 0
    end
    return old_GetSkinsDataFromClientTableData(client)
end

-- Reposition Gift Icon based on user config
local GiftItemToast = require "widgets/giftitemtoast"
AddClassPostConstruct("widgets/controls", function(self)
	if _G.Forge_options.gift_side == "left" then
        -- add 200 into offset to be right next to max width team health badge name
        self.item_notification:SetPosition(315, 150, 0)
    elseif _G.Forge_options.gift_side == "right" then
        self.item_notification:Kill()
        self.item_notification = self.topright_root:AddChild(GiftItemToast(self.owner))
        self.item_notification:SetPosition(-115, 150, 0)
    end
end)

-- Allow gifts to be seen and opened in the Forge
AddComponentPostInit("giftreceiver", function(self)
    local distance = _G.TUNING.RESEARCH_MACHINE_DIST*_G.TUNING.RESEARCH_MACHINE_DIST
	local old_SetGiftMachine = self.SetGiftMachine
    self.SetGiftMachine = function(self, inst)
        -- only want to check portal dist if intermission
        if _G.TheWorld.components.lavaarenaevent and _G.TheWorld.components.lavaarenaevent:IsIntermission() then
            local portal = _G.TheWorld.multiplayerportal
            -- distsq check over findent for performance and since hardcoded for just player portal
            if portal and _G.distsq(self.inst:GetPosition(), portal:GetPosition()) <= distance then
                inst = _G.CanEntitySeeTarget(self.inst, portal) and self.inst.components.inventory.isopen and portal or inst
            end
        end
        old_SetGiftMachine(self, inst)
    end
end)

-- Edit stat file name so you can differentiate between forgedforge and regular forge
local old_GetActiveFestivalEventStatsFilePrefixfunction = _G.GetActiveFestivalEventStatsFilePrefix
_G.GetActiveFestivalEventStatsFilePrefix = function()
	local stats_file_prefix = "forgedforge_stats"
	return stats_file_prefix
end

--Temp(?) Fix for the RoT merge crash. Force loads lavaarena presets.
local _oldGetSlotGenOptions = _G.SaveIndex.GetSlotGenOptions
_G.SaveIndex.GetSlotGenOptions = function(self, slot)
    local Levels = require("map/levels")
    local level_data = Levels.GetDataForLevelID("LAVAARENA")
    local world_data = level_data and {[1] = level_data} or _oldGetSlotGenOptions(self, slot)
    return world_data
end

local function MapSideFix(inst)
	_G.TheWorld.Map:SetUndergroundFadeHeight(0)
	if not _G.TheWorld.ismastersim then
        return inst
    end

end

AddPrefabPostInit("lavaarena", MapSideFix)