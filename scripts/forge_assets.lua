--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

return {
	Asset("ANIM", "anim/lavaarena_doydoy.zip"),
	-------------------------------------------------
	--Alt palettes for mobs, mostly for doubletrouble or other modding reasons.
	Asset("ANIM", "anim/lavaarena_beetletaur_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_rhinodrill_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_rhinodrill_clothed_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_rhinodrill_damaged_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_boarrior_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_trails_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_turtillus_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_peghook_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_snapper_alt1.zip"),
	Asset("ANIM", "anim/lavaarena_boaron_alt1.zip"),
	
	Asset("ATLAS", "images/rhinocebro_icon.xml"),
	Asset("IMAGE", "images/rhinocebro_icon.tex"),
	
	Asset("ATLAS", "images/swineclops_icon.xml"),
	Asset("IMAGE", "images/swineclops_icon.tex"),
	-------------------------------------------------
	
	Asset("ANIM", "anim/lavaarena_buff_build.zip"),
	Asset("ANIM", "anim/lavaarena_debuff_build.zip"),
	Asset("ANIM", "anim/lavaarena_bufficons.zip"),
	Asset("ANIM", "anim/lavaarena_buffindex.zip"),
	
	Asset("ATLAS", "images/servericons.xml"),
	Asset("IMAGE", "images/servericons.tex"),

	Asset("ATLAS", "images/forged_forge.xml"),
	
	Asset("ATLAS", "images/force_start.xml"),
    Asset("IMAGE", "images/force_start.tex"),
	
	-- Colour Cubes
	-- Default
    Asset("IMAGE", "images/colour_cubes/day05_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/dusk03_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/night03_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/snow_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/snowdusk_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/night04_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/summer_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/summer_dusk_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/summer_night_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/spring_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/spring_dusk_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/spring_night_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/insane_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/insane_dusk_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/insane_night_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/purple_moon_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/beaver_vision_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/caves_default.tex"),
	Asset("IMAGE", "images/colour_cubes/fungus_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/ghost_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/identity_colourcube.tex"),
	Asset("IMAGE", "images/colour_cubes/mole_vision_off_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/mole_vision_on_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/ruins_dark_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/ruins_dim_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/ruins_light_cc.tex"),
	Asset("IMAGE", "images/colour_cubes/sinkhole_cc.tex"),
	
	-- Shipwrecked
    Asset("IMAGE", "images/colour_cubes/sw_mild_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/sw_wet_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/sw_green_day_cc.tex"),
    Asset("IMAGE", "images/colour_cubes/sw_volcano_cc.tex"),
	
	-- Forge
	Asset("IMAGE", "images/colour_cubes/lavaarena2_cc.tex"),
	
	-- Gorge
	Asset("IMAGE", "images/colour_cubes/quagmire_cc.tex"),
}