--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("forge_prefab")
return {
	"forgedarts",
	"forginghammer",
	"riledlucy",
	"pithpike",
	"livingstaff",
	"moltendarts",
	"infernalstaff",
	"spiralspear",
	"firebomb",
    "blacksmithsedge",
	"forge_books",
	"forge_armor",
	"forge_hats",
	
	"pitpig",
	"boarilla",
	"snortoise",
	"scorpeon",
	"crocommander",
	"boarrior",
	"rhinocebro",
	"swineclops",
	"groundlifts",
	"battlestandards",
	----
	"golem",
	"babyspider",
	"forge_abigail",
	"forge_abigail_flower",
	"forge_bernie",
	
	"forge_fireball_projectile",
	"infernalstaff_meteor",
	"weaponsparks_fx",
	"forgedebuff_fx",
	"forgespear_fx",
	"healingcircle",
	"healingcircle_regenbuff",
	
	"passive_battlecry_fx",
	"passive_amplify_fx",
	"passive_shadow_fx",
	
	"damage_number",
}