--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require "behaviours/wander"
require "behaviours/chaseandattack"
require "behaviours/chaseandattack_forge"
require "behaviours/panic"
require "behaviours/attackwall"
require "behaviours/minperiod"
require "behaviours/leash"
require "behaviours/faceentity"
require "behaviours/doaction"
require "behaviours/standstill"

local CrocommanderBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local SEE_DIST = 30

local MIN_FOLLOW_LEADER = 2
local MAX_FOLLOW_LEADER = 6
local TARGET_FOLLOW_LEADER = (MAX_FOLLOW_LEADER+MIN_FOLLOW_LEADER)/2

local LEASH_RETURN_DIST = 10
local LEASH_MAX_DIST = 40

local HOUSE_MAX_DIST = 40
local HOUSE_RETURN_DIST = 50 

local SIT_BOY_DIST = 10

--
local MIN_FOLLOW_DIST = 0
local MAX_FOLLOW_DIST = 13
local TARGET_FOLLOW_DIST = 13

local MIN_MELEE_DIST = 0
local MAX_MELEE_DIST = 2
local TARGET_MELEE_DIST = 2

--ThePlayer:DoPeriodicTask(0.25, function(inst) local mob = SpawnPrefab("crocommander") mob.Transform:SetPosition(inst:GetPosition():Get()) end)

local function IsTargetNear(inst)
	if inst.components.combat and inst.components.combat.target ~= nil then
		return inst:IsNear(self.inst.components.combat.target, TUNING.FORGE.CROCOMMANDER.MELEE_RANGE) or false
	end
end

local function CanAttackNow(inst)
    return inst.components.combat.target == nil or not inst.components.combat:InCooldown()
end

local function GetWanderPoint(inst)
    return TheWorld.components.lavaarenaevent and TheWorld.components.lavaarenaevent:GetArenaCenterPoint() or nil
end

function CrocommanderBrain:OnStart()
    
    local root = PriorityNode(
    {
		WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire", Panic(self.inst)),
		

        WhileNode(function() return self.inst:HasTag("pet") end, "Is Pet", ChaseAndAttack_Forge(self.inst, 8)),
        WhileNode(function() return not self.inst:HasTag("pet") end, "Not Pet", ChaseAndAttack_Forge(self.inst, 8)),

        Wander(self.inst, GetWanderPoint, 5),
    }, .25)
    
    self.bt = BT(self.inst, root)
    
end

return CrocommanderBrain
