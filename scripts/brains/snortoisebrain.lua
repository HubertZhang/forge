--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require "behaviours/wander"
require "behaviours/chaseandattack_forge"
require "behaviours/panic"
require "behaviours/attackwall"
require "behaviours/minperiod"
require "behaviours/faceentity"
require "behaviours/doaction"
require "behaviours/standstill"
require "behaviours/useshield_forge"

local SnortoiseBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local DAMAGE_UNTIL_SHIELD = 140 --rough estimate
local AVOID_PROJECTILE_ATTACKS = false
local HIDE_WHEN_SCARED = false
local SHIELD_TIME = 7 

local function GetLeader(inst)
    return inst.components.follower ~= nil and inst.components.follower.leader or nil
end

local function GetNewTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
    return distsq ~= nil and distsq < 225 and not (player:HasTag("notarget") or player:HasTag("LA_mob")) and player
end

local function GetWanderPoint(inst)
    return TheWorld.components.lavaarenaevent and TheWorld.components.lavaarenaevent:GetArenaCenterPoint() or nil
end

local function ShouldShellAttack(inst)
	if inst.components.health:GetPercent() <= 0.3 and inst.altattack == true and inst.components.combat.target then
		inst:PushEvent("start_shellattack")
	end
end

function SnortoiseBrain:OnStart()
    
    local root = PriorityNode(
    {
		WhileNode(function() return self.inst.canshield == true and not (self.inst.sg:HasStateTag("attack") and self.inst.sg:HasStateTag("busy")) end, "Shield", UseShield_Forge(self.inst, DAMAGE_UNTIL_SHIELD, SHIELD_TIME, true)),
		WhileNode(function() return self.inst:HasTag("pet") end, "Is Pet", ChaseAndAttack_Forge(self.inst, 8)),
        WhileNode(function() return not self.inst:HasTag("pet") end, "Not Pet", ChaseAndAttack_Forge(self.inst, 8)),
		
        Wander(self.inst, GetWanderPoint, 5),
    }, .25)
    
    self.bt = BT(self.inst, root)
    
end

return SnortoiseBrain
