--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require "behaviours/wander"
require "behaviours/boarriorchaseandattack"
require "behaviours/chaseandattackandavoid"
require "behaviours/chaseandattack_forge"
require "behaviours/panic"
require "behaviours/attackwall"
require "behaviours/minperiod"
require "behaviours/leash"
require "behaviours/faceentity"
require "behaviours/doaction"
require "behaviours/standstill"

local BoarriorBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local function GetNewTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
    return distsq ~= nil and distsq < 225 and not (player:HasTag("notarget") or player:HasTag("LA_mob")) and player
end

local function GetWanderPoint(inst)
    return TheWorld.components.lavaarenaevent and TheWorld.components.lavaarenaevent:GetArenaCenterPoint() or nil
end


local function AvoidCircle(inst) --Leo: reminder that Boarrior will only recognize one circle atm.
	local circle = FindEntity(inst, 255,  nil, nil, {"notarget"}, {"healingcircle"}, {"healingcircle"}) or nil
	return circle
end

function BoarriorBrain:OnStart()
    
    local root = PriorityNode(
    {
		
        WhileNode(function() return self.inst.level and self.inst.level >= 2 and (self.inst.components.health and self.inst.components.health:GetPercent() <= 0.45) and AvoidCircle(self.inst) ~= nil end, "Avoid HealingCircle", BoarriorChaseAndAttack(self.inst, AvoidCircle , 30, 20)),
		--WhileNode(function() return self.inst.level and self.inst.level >= 2 and (self.inst.components.health and self.inst.components.health:GetPercent() <= 0.45) and AvoidCircle(self.inst) ~= nil end, "Avoid HealingCircle", ChaseAndAttackAndAvoid(self.inst, AvoidCircle , 30, 20)),
        WhileNode(function() return (self.inst.components.health and self.inst.components.health:GetPercent() > 0.45) or not AvoidCircle(self.inst) end, "Normal Attack", ChaseAndAttack_Forge(self.inst, 8)),

        Wander(self.inst, GetWanderPoint, 5),
    }, .25)
    
    self.bt = BT(self.inst, root)
    
end

return BoarriorBrain
