--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require "behaviours/wander"
require "behaviours/chaseandattack"
require "behaviours/chaseandattack_forge"
require "behaviours/rhinocebrobuff_forge"
require "behaviours/panic"
require "behaviours/attackwall"
require "behaviours/minperiod"
require "behaviours/faceentity"
require "behaviours/doaction"
require "behaviours/standstill"
require "behaviours/useshield_forge"

local BUFF_DISTANCE = 6--6
local VICTORY_DISTANCE = 4

local RhinocebroBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local function GetNewTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
    return distsq ~= nil and distsq < 225 and not (player:HasTag("notarget") or player:HasTag("LA_mob")) and player
end

local function GetLeader(inst)
    return inst.components.follower ~= nil and inst.components.follower.leader or nil
end

local function ReviveAction(inst)
    if inst.bro and inst.bro.sg and inst.bro.sg:HasStateTag("corpse") then
        return BufferedAction(inst, inst.bro, ACTIONS.REVIVE_CORPSE)
    end
end

local function WalkToAction(inst)
    if inst.bro and inst.bro.sg then
        return BufferedAction(inst, inst.bro, ACTIONS.WALKTO)
    end
end

--once they get to a certain distance from each other, they will start cheering, the states will handle it from there.
local function DoBuff(inst, bro)
		if inst.cancheer and inst.cancheer == true then
			print("Starting to buff")
			inst:PushEvent("startcheer")
			return
			
		end	
		return
end

local function GetBro(inst)
	return inst.bro and inst.bro.components.health and not inst.bro.components.health:IsDead()
end

local function GetNoBusyTags(inst)
	return not (inst.sg:HasStateTag("attack") or inst.sg:HasStateTag("frozen") or inst.sg:HasStateTag("sleeping"))
end

local function GetWanderPoint(inst)
    return TheWorld.components.lavaarenaevent and TheWorld.components.lavaarenaevent:GetArenaCenterPoint() or nil
end

function RhinocebroBrain:OnStart()
    --TODO: shorten the checks by turning the coniditions into functions.
    local root = PriorityNode(
    {
		--Reviving takes top priority, no matter what.
		WhileNode(function() return self.inst.bro and self.inst.bro.sg and self.inst.bro.sg:HasStateTag("corpse") and not self.inst.sg:HasStateTag("reviving") end, "Bro Is Dead", DoAction(self.inst, ReviveAction)),

		--Victory 
		WhileNode(function() if GetBro(self.inst) and TheWorld.components.lavaarenaevent.lost and GetNoBusyTags(self.inst) and not self.inst:IsNear(self.inst.bro, VICTORY_DISTANCE) then return self.inst.bro else return nil end end, "Bro Is Dead", DoAction(self.inst, WalkToAction)),
		--Follow(self.inst, function() if self.inst.bro and self.inst.bro.components.health and not self.inst.bro.components.health:IsDead() and TheWorld.components.lavaarenaevent.lost and not self.inst:IsNear(self.inst.bro, VICTORY_DISTANCE) and not (self.inst.sg:HasStateTag("attack") or self.inst.sg:HasStateTag("frozen") or self.inst.sg:HasStateTag("sleeping")) then return self.inst.bro else return nil end end, VICTORY_DISTANCE/2, VICTORY_DISTANCE, VICTORY_DISTANCE*2),
		WhileNode(function() return GetBro(self.inst) and TheWorld.components.lavaarenaevent.lost and self.inst:IsNear(self.inst.bro, VICTORY_DISTANCE + 2) and GetNoBusyTags(self.inst) end, "Bro Pose", RhinocebroBuff_Forge(self.inst, VICTORY_DISTANCE + 1, true)),
		
		--Buff
		--Follow(self.inst, function() if self.inst.bro and self.inst.bro.components.health and not self.inst.bro.components.health:IsDead() and self.inst.cancheer and self.inst.cancheer == true and not TheWorld.components.lavaarenaevent.lost and not self.inst:IsNear(self.inst.bro, BUFF_DISTANCE) then return self.inst.bro else return nil end end, BUFF_DISTANCE, BUFF_DISTANCE, BUFF_DISTANCE),
		WhileNode(function() if GetBro(self.inst) and self.inst.cancheer and self.inst.cancheer == true and not TheWorld.components.lavaarenaevent.lost and not self.inst:IsNear(self.inst.bro, BUFF_DISTANCE) then return self.inst.bro else return nil end end, "Bro Is Dead", DoAction(self.inst, WalkToAction)),
		WhileNode(function() return GetBro(self.inst) and self.inst.cancheer and self.inst.cancheer == true and not TheWorld.components.lavaarenaevent.lost and self.inst:IsNear(self.inst.bro, BUFF_DISTANCE) end, "Bro Buff", RhinocebroBuff_Forge(self.inst, BUFF_DISTANCE)),
		
		WhileNode(function() return self.inst:HasTag("pet") end, "Is Pet", ChaseAndAttack_Forge(self.inst, 8)),
        WhileNode(function() return not self.inst:HasTag("pet") end, "Not Pet", ChaseAndAttack_Forge(self.inst, 8)),
		--WhileNode(function() return VictoryConditionsHere end, "PLAYER_DEFEAT", DoTaunt),

        --Wander(self.inst, GetWanderPoint, 7),
		Wander(self.inst, GetWanderPoint, 5),
    }, .25)
    
    self.bt = BT(self.inst, root)
    
end

return RhinocebroBrain
