--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local RecipeTile = require "widgets/recipetile"

local ItemTile = Class(RecipeTile, function(self, item)
    RecipeTile._ctor(self, item)
	if item ~= nil then
        self.item = item
        local image = item.imagefn ~= nil and item.imagefn() or item.image
        self.img:SetTexture(item.atlas, image, image ~= item.image and item.image or nil)
    end
end)

function ItemTile:SetItem(item)
    self.item = item
    local image = item.imagefn ~= nil and item.imagefn() or item.image
    self.img:SetTexture(item.atlas, image, image ~= item.image and item.image or nil)
end

function ItemTile:SetSize(width, height)
	self.img:SetSize(width, height)
end

return ItemTile