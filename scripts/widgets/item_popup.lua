--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local Widget = require "widgets/widget"
local Image = require "widgets/image"
local ImageButton = require "widgets/imagebutton"
local Text = require "widgets/text"
local RecipePopup = require "widgets/recipepopup"
local Spinner = require "widgets/spinner"
local TextIcon = require "widgets/text_icon"
local ALIGN_L = 1
local ALIGN_C = 0
local ALIGN_R = -1

local TEASER_SCALE_TEXT = 1
local TEASER_SCALE_BTN = 1.5
local TEASER_TEXT_WIDTH = 64 * 3 + 24
local TEASER_BTN_WIDTH = TEASER_TEXT_WIDTH / TEASER_SCALE_BTN
local TEXT_WIDTH = 64 * 3 + 30

--local recipe_desc_fontSize = PLATFORM ~= "WIN32_RAIL" and 33 or 30
local recipe_desc_fontSize = 20

local function MakeSpinner(labeltext, min_num, max_num, onchanged_fn, opts)
	local width_label = 150
	local width_spinner = opts and opts.width or 100
	local height = opts and opts.height or 40	
	local spacing = opts and opts.spacing or 5
	local font = opts and opts.font or HEADERFONT
	local font_size = opts and opts.font_size or 40
	
	local labeltext = labeltext or ""
	local min_num = min_num or 1
	local max_num = max_num or 10
	--[[
	local total_width = width_label + width_spinner + spacing
	local wdg = Widget("labelspinner")
	wdg.label = wdg:AddChild( Text(font, font_size, labeltext) )
	wdg.label:SetPosition( (-total_width/2)+(width_label/2), 0 )
	wdg.label:SetRegionSize( width_label, height )
	wdg.label:SetHAlign( ANCHOR_RIGHT )
	wdg.label:SetColour(UICOLOURS.BROWN_DARK)--]]
	
	local spinner_data = {}
	for i = min_num, max_num, 1 do
		local current_data = {text = tostring(i), data = i}
		spinner_data[i] = current_data
		--table.insert(spinner_data, )
	end

	local lean = true
	local spinner = Spinner(spinner_data, width_spinner, height, {font = font, size = font_size}, nil, "images/quagmire_recipebook.xml", nil, lean)
	spinner:SetTextColour(UICOLOURS.BROWN_DARK)
	spinner:SetOnChangedFn(onchanged_fn)
	--spinner:SetPosition((total_width/2)-(width_spinner/2), 0)

	spinner:SetSelected(min_num)

	--return wdg
	return spinner
end

local ItemPopup = Class(RecipePopup, function(self, horizontal)
    RecipePopup._ctor(self, horizontal)
end)

function ItemPopup:BuildNoSpinner(horizontal)
    self:KillAllChildren()

    self.skins_spinner = nil

    local hud_atlas = GetGameModeProperty("hud_atlas") or resolvefilepath(HUD_ATLAS)

    self.bg = self:AddChild(Image())
    local img = horizontal and "craftingsubmenu_fullvertical.tex" or "craftingsubmenu_fullhorizontal.tex"

    if horizontal then
        self.bg:SetPosition(240,40,0)
    else
        self.bg:SetPosition(210,16,0)
    end
    self.bg:SetTexture(hud_atlas, img)
	--self.bg:SetSize(500,500)

    if horizontal then
        self.bg.light_box = self.bg:AddChild(Image(hud_atlas, "craftingsubmenu_litehorizontal.tex"))
        self.bg.light_box:SetPosition(0, -50)
    else
        self.bg.light_box = self.bg:AddChild(Image(hud_atlas, "craftingsubmenu_litevertical.tex"))
        self.bg.light_box:SetPosition(30, -22)
    end

    --

    self.contents = self:AddChild(Widget(""))
    self.contents:SetPosition(-75,0,0)

    if self.smallfonts then
        self.name = self.contents:AddChild(Text(UIFONT, 40 * 0.8))
        self.desc = self.contents:AddChild(Text(BODYTEXTFONT, 33 * 0.8))
        self.desc:SetPosition(320, -10, 0)
    else
        self.name = self.contents:AddChild(Text(UIFONT, 40))
        self.desc = self.contents:AddChild(Text(BODYTEXTFONT, recipe_desc_fontSize))
        self.desc:SetPosition(320, -5, 0)
    end
    self.name:SetPosition(320, 142, 0)
    self.name:SetHAlign(ANCHOR_MIDDLE)

    self.ing = {}
	
	-- Icon
	--self.icon = self.contents:AddChild(Image())
	--self.icon:SetPosition(415, -105, 0)
	
	-- Stats
	--[[
	Enemy: Health, Damage, Speed 
	Weap: Damage, Element
	Helm: phys dmg increase, magic dmg increase, speed increase, cooldown increase
	Armor: Armor, knockback resistance, speed, phys dmg increase, cooldown increase
	stats: Armor, Cooldown, Health, Damage: (Physical Damage, Magic Damage), Element, Knockback Resistance, Speed
	--]]
	--self.stat_icon
	
	
	-- Abilities

	
	-- Buttons	TODO simplify buttons?
	--Description
	self.button_desc = self.contents:AddChild(ImageButton())
    self.button_desc:SetScale(.7,.7,.7)
    self.button_desc.image:SetScale(.45, .7)
    self.button_desc:SetPosition(270, 50, 0)
	self.button_desc:SetText(STRINGS.UI.ADMINMENU.BUTTONS.DESCRIPTION)
		
	-- Abilities
	self.button_ab = self.contents:AddChild(ImageButton())
    self.button_ab:SetScale(.7,.7,.7)
    self.button_ab.image:SetScale(.45, .7)
    self.button_ab:SetPosition(370, 50, 0)
	self.button_ab:SetText(STRINGS.UI.ADMINMENU.BUTTONS.ABILITIES)
	
	-- Spawn
    self.button = self.contents:AddChild(ImageButton())
    self.button:SetScale(.7,.7,.7)
    self.button.image:SetScale(.45, .7)
    self.button:SetOnClick(function()
			print("[AdminCommand] Spawning " .. tostring(self.item.name))
			SpawnPrefab(self.item.name).Transform:SetPosition(ThePlayer.Transform:GetWorldPosition())
		end)
		
	-- Equip
	self.button_equip = self.contents:AddChild(ImageButton())
    self.button_equip:SetScale(.7,.7,.7)
    self.button_equip.image:SetScale(.45, .7)
	self.button_equip:SetText(STRINGS.UI.ADMINMENU.BUTTONS.EQUIP)
	self.button_equip:SetPosition(370, -105, 0)
    self.button_equip:SetOnClick(function()
			EquipItem(self.item.name)
			--[[
			print("[AdminCommand] Equipping " .. tostring(self.item.name))
			SpawnPrefab(self.item.name).Transform:SetPosition(ThePlayer.Transform:GetWorldPosition())
			--]]
		end)
							
	-- Command

	-- Spinner
	self.spinner = self.contents:AddChild(MakeSpinner())
	self.spinner:SetPosition(380, -105, 0)
	local spinner_opts = {
		height = 30,
		font_size = 30,
	}
	self.rounds_data = require("forge_rounds/"..Forge_options.waveset)
	self.spinner_rounds = self.contents:AddChild(MakeSpinner("R", 1, #self.rounds_data, function(selected, old)
		if self.spinner_waves then
			local spinner_data = {}
			for i = 1, #self.rounds_data[selected].waves, 1 do
				local current_data = {text = tostring(i), data = i}
				spinner_data[i] = current_data
			end
			self.spinner_waves:SetOptions(spinner_data)
		end
	end), spinner_opts) -- TODO need constant for max rounds
	self.spinner_rounds:SetPosition(380, -85, 0)
	self.spinner_waves = self.contents:AddChild(MakeSpinner("W", 1, 3, nil, spinner_opts)) -- TODO need constant for max waves
	self.spinner_waves:SetPosition(380, -115, 0)
	spinner_opts.width = 150
	self.spinner_ents = self.contents:AddChild(MakeSpinner("P", 1, 1, nil, spinner_opts))
	self.spinner_ents:SetPosition(320, 75, 0)
	
    self.teaser = self.contents:AddChild(Text(BODYTEXTFONT, 28))
    self.teaser:SetPosition(320, -100, 0)
    self.teaser:Hide()
end

function ItemPopup:Refresh()
	local owner = self.owner
    if owner == nil then
        return false
    end
	
	local category = self.item.type
	self:BuildNoSpinner(self.horizontal) -- Need this TODO read through this function
	
	-- Display stats of item in rows of 3 below item name
	local function UpdateItemStats()
		if self.item.stats then
			local col = 1
			local row = 1
			local max_item_per_row = 3
			--local x_offset = 50
			local x_center = 320
			local y_offset = 70
			local y_offset_per_row = 20
			local spacing = 10
			local text_opts = {
				pos = {x = 350, y = 110, z = 0},
				--font = , -- TODO figure out what font to use
				font_size = 25,
				h_alignment = ALIGN_L,
				icon_after_text = false,
			}
			local icons = {
				stats = {
					--source = "forged_forge_icons.xml",
					source = "inventoryimages.xml",
					tex = "default.tex",
					opts = {	
						key = "stat",
						icon_size = {25, 25},
						pos_offset = {x = 0, y = 0, z = 0},
						hovertext = "",
					}
				}
			}
			
			self.stats = {}
			local total_stats = self.item.stat_count
			local final_col_count = total_stats % 3
			local x_spacing = 80
			local x_offset = 0
			local test_count = 0
			for stat, value in pairs(self.item.stats) do
				-- Adjust position
				test_count = test_count + 1
				if total_stats - max_item_per_row >= 0 then
					x_offset = x_spacing * (col - 2)
				elseif final_col_count == 2 then
					x_offset = -x_spacing/2 + x_spacing * (col - 1)
				else
					x_offset = 0
				end
				text_opts.pos.x = x_center + x_offset
				
				icons.stats.tex = STRINGS.FORGED_FORGE.STATS[string.upper(tostring(stat))] and (STRINGS.FORGED_FORGE.STATS[string.upper(tostring(stat))].ICON .. ".tex") or "torch.tex"
				--icons.stats.tex = value.ICON .. ".tex" or "torch.tex"
				icons.stats.opts.hovertext = tostring(stat)
				local stat_text = tostring(value) -- TODO need to add percent when neccessary or color code based on positive/negative numbers? green/red?
				self.stats[stat] = self.contents:AddChild( TextIcon(stat_text, icons, text_opts) )
				
				-- New Row Check
				col = col + 1
				if col > max_item_per_row then
					row = row + 1
					col = 1
					total_stats = total_stats - max_item_per_row
					text_opts.pos.y = y_offset + y_offset_per_row * row
				end
			end
		end
	end
	
	-- Display the description box to display item description or ability list
	local function UpdateDescBox()
		-- TODO add description/abilities toggle buttons
		-- only enemies and weapons should have both buttons?
		if category == "ENEMIES" or category == "WEAPONS" then
			self.show_desc = true
			self.desc:SetPosition(320, -5, 0)
			self.button_desc:SetOnClick(function()
				if not self.show_desc then
					self.show_desc = true
					self.desc:SetMultilineTruncatedString(STRINGS.FORGED_FORGE[category][string.upper(self.item.name)] and STRINGS.FORGED_FORGE[category][string.upper(self.item.name)].DESC or tostring(self.item.name), 3, TEXT_WIDTH, self.smallfonts and 40 or 33, true)
				end
			end)
			self.button_ab:SetOnClick(function()
				if self.show_desc then
					self.show_desc = false
					local abilities_str = ""
					local count = 0
					local abilities_num = self.item.ability_count
					if self.item.abilities then
						for i, ability in pairs(self.item.abilities) do
							--Lunge - Lunge that does x damage
							--abilities_str = abilities_str .. tostring(STRINGS.FORGED_FORGE[category][string.upper(self.item.name)].ABILITIES[string.upper(ability)].NAME) or tostring(ability)
							abilities_str = abilities_str .. tostring(ability.NAME)
							count = count + 1
							if abilities_num > count then
								abilities_str = abilities_str .. ", "
							end
							--abilities_str = abilities_str .. tostring(ability.NAME) .. " - " .. tostring(ability.DESC) .. "\n"
						end
					else
						abilities_str = STRINGS.UI.ADMINMENU.NO_ABILITIES
					end
					--#STRINGS.FORGED_FORGE[category][string.upper(self.item.name)].ABILITIES * 2
					self.desc:SetMultilineTruncatedString(tostring(abilities_str), 3, TEXT_WIDTH, self.smallfonts and 40 or 33, true)
				end
			end)
		else
			self.button_desc:Hide()
			self.button_ab:Hide()
			self.desc:SetPosition(320, -5, 0)
		end
		
		self.desc:SetMultilineTruncatedString(STRINGS.FORGED_FORGE[category][string.upper(self.item.name)] and STRINGS.FORGED_FORGE[category][string.upper(self.item.name)].DESC or tostring(self.item.name), 3, TEXT_WIDTH, self.smallfonts and 40 or 33, true)
	end
	
	-- Displays a "Spawn" button, "Equip" button, "Activate" button, or a spinner based on the item to match the items function
	local function UpdateButtonsAndSpinners()
		local function UpdateEntSpinnerOpts(location)
			if location == 1 then
				self.spinner_ents:SetScale(.75,.75,.75)
				self.spinner_ents:SetPosition(320, -70, 0)
			else
				self.spinner_ents:SetScale(1,1,1)
				self.spinner_ents:SetPosition(320, 75, 0)
			end
			local player_pos = Point(ThePlayer.Transform:GetWorldPosition())
			local ent_list = self.item.name == "debug_select" and TheSim:FindEntities(player_pos.x,player_pos.y,player_pos.z,100,{"inspectable"}) or AllPlayers
			local ent_spinner_opts = {}
			for i, ent in pairs(ent_list) do
				local atlas = (ent:HasTag("player") and (softresolvefilepath("images/avatars/avatar_"..ent.prefab..".xml") and "images/avatars/avatar_"..ent.prefab..".xml" or "images/avatars.xml"))
				or (ent.components.inventoryitem and "images/inventoryimages.xml")
				or (ent:HasTag("LA_mob") and "images/forged_forge.xml") or self.item.atlas
				
				local tex = (ent:HasTag("player") and ("avatar_" .. tostring(ent.prefab) .. ".tex")) or
				(ent.components.inventoryitem and ent.components.inventoryitem.imagename ~= nil and ent.components.inventoryitem.imagename .. ".tex") or
				(ent:HasTag("LA_mob") and tostring(ent.prefab) .. "_icon.tex") or self.item.image or
				"poop.tex"
				
				if ent == ThePlayer then
					table.insert(ent_spinner_opts, 1, {data = ent, image = {atlas, tex}, hover_text = ent.name})
				else
					table.insert(ent_spinner_opts, {data = ent, image = {atlas, tex}, hover_text = ent.name})
				end
				self.has_ents = true
			end
			
			self.spinner_ents:Show()
			self.spinner_ents:SetOptions(ent_spinner_opts)
			local init_sel = self.spinner_ents:GetSelectedData()
			self.spinner_ents:SetHoverText(init_sel:HasTag("player") and tostring(init_sel.name) or tostring(init_sel.prefab) .. "_" .. tostring(init_sel.GUID))
			--init_sel.AnimState:SetHighlightColour(0, 255, 0, 0)
			--self.highlight = true
			self.spinner_ents:SetOnChangedFn(function(selected, old)
				if self.spinner_ents.fgimage then
					self.spinner_ents:SetHoverText(selected:HasTag("player") and tostring(selected.name) or tostring(selected.prefab) .. "_" .. tostring(selected.GUID))
				end
				
				selected.AnimState:SetHighlightColour(255/255, 109/255, 100/255, 0)
				old.AnimState:SetHighlightColour()
				self.highlight = true
			end)
		end
		local spinner_x_offset = 0
		local spinner_y_offset = 0
		local buttonstr = (category == "ENEMIES" or category == "ARMOR" or category == "WEAPONS" or category == "BUFFS") and STRINGS.UI.ADMINMENU.BUTTONS.SPAWN or STRINGS.UI.ADMINMENU.BUTTONS.ACTIVATE -- TODO adjust based on what it does and add multiple buttons?
		
		if category == "ENEMIES" then
			self.button_equip:Hide()
			self.spinner:Show()
			self.spinner_rounds:Hide()
			self.spinner_waves:Hide()
			
			spinner_x_offset = 50
			spinner_y_offset = 0
			
			UpdateEntSpinnerOpts(1)
			
			self.button:SetScale(.75,.75,.75)
			self.button:SetOnClick(function()
				self.item:admin_fn({name = self.item.name, amount = self.spinner:GetSelectedData() or 1, player = self.spinner_ents:GetSelectedData()})
			end)
		elseif category == "ARMOR" or category == "WEAPONS" then
			self.spinner:Hide()
			self.spinner_rounds:Hide()
			self.spinner_waves:Hide()
			self.button_equip:Show()
			self.button_equip:Enable()
			-- Readjust button position and size to match equip?
			--self.button:SetPosition()
			self.button:SetScale(.75,.75,.75)
			self.button_equip:SetScale(.75,.75,.75)
			UpdateEntSpinnerOpts(1)
			self.button:SetOnClick(function()
				self.item:admin_fn({ name = self.item.name, amount = self.spinner:GetSelectedData() or 1, player = self.spinner_ents:GetSelectedData()})			
			end)
			self.button_equip:SetOnClick(function()
				EquipItem(self.item.name, self.spinner_ents:GetSelectedData())
			end)
			spinner_x_offset = 50
			spinner_y_offset = 0
		elseif category == "BUFFS" then
			self.button_equip:Hide()
			self.spinner:Show()
			self.spinner_rounds:Hide()
			self.spinner_waves:Hide()
			spinner_x_offset = 50
			spinner_y_offset = 0
			
			UpdateEntSpinnerOpts(1)
			
			self.button:SetScale(.75,.75,.75)
			self.button:SetOnClick(function() -- TODO FIX THIS SHIT
				--if self.item.name == "passive_battlecry_fx_self" then
					--ThePlayer.battlecry_fx = SpawnPrefab("passive_battlecry_fx_self")
					--ThePlayer.battlecry_fx:SetTarget(ThePlayer)
				--else
					self.item:admin_fn({name = self.item.name, amount = self.spinner:GetSelectedData() or 1, player = self.spinner_ents:GetSelectedData()})
				--end
			end)
		elseif category == "FORGE" or category == "GENERAL" then
			self.button_equip:Hide()
			self.spinner:Hide()
			self.spinner_ents:Hide()
			
			-- This needs 2 spinners next to button
			if self.item.name == "force_round" then
                self.spinner:Hide()
				self.spinner_rounds:Show()
				self.spinner_rounds:Changed(1)
                self.spinner_waves:Show()
				self.spinner_ents:Hide()
				self.button:SetScale(.75,.75,.75)
				self.button:SetOnClick(function()
					self.item:admin_fn({round = self.spinner_rounds:GetSelectedData() or 1, wave = self.spinner_waves:GetSelectedData() or 1})
				end)
				spinner_x_offset = 50
				spinner_y_offset = 0
			else
				self.spinner:Hide()
				self.spinner_rounds:Hide()
				self.spinner_waves:Hide()
				if self.item.name ~= "kill_all_mobs" and self.item.name ~= "reset_to_save" and category ~= "FORGE" then
					UpdateEntSpinnerOpts(0)
				end
				self.button:SetScale(1,1,1)
				self.button:SetOnClick(function()
					self.item:admin_fn({player = self.spinner_ents:GetSelectedData() or ThePlayer})
				end)
			end
		end
		
		-- Other Stuff -- TODO still need to go through this code
		-- vvvvvvvvvv
		self.teaser:Hide()
		-- ^^^^^^^^^^
		
		self.button:Show()
		if self.skins_spinner ~= nil then
			self.button:SetPosition(320, -155, 0)
		else
			self.button:SetPosition(320 - spinner_x_offset, -105 - spinner_y_offset, 0)
		end
		
		self.button:SetText(buttonstr)
		self.button:Enable()
	end
	
	-- Name
    self.name:SetTruncatedString(STRINGS.FORGED_FORGE[category] and STRINGS.FORGED_FORGE[category][string.upper(self.item.name)] and STRINGS.FORGED_FORGE[category][string.upper(self.item.name)].NAME or self.item.name or STRINGS.MVP_LOADING_WIDGET.LAVAARENA.DESCRIPTIONS.unknown, TEXT_WIDTH, self.smallfonts and 51 or 41, true)
    
	-- Icon -- TODO check for self.forge_prefab.stats to either center position or move to left?
	
	
	-- Stats
	UpdateItemStats()
	
	-- Description/Abilities
	UpdateDescBox()	

	-- Buttons
	UpdateButtonsAndSpinners()
	
end

function ItemPopup:SetItem(item, owner)
    self.item = item
    self.owner = owner
    self:Refresh()
end

function ItemPopup:HideReset()
	if self.spinner_ents and self.highlight and self.has_ents then
		--print("Hide data: " .. tostring(self.spinner_ents:GetSelectedData()))
		local selected = self.spinner_ents:GetSelectedData()
		if selected:HasTag("inspectable") then
			selected.AnimState:SetHighlightColour()
			self.highlight = false
			self.has_ents = false
		end
	end
end

return ItemPopup