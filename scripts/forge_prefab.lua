--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require "class"
require "util"
require "admin_commands"

local stat_list = {
	DAMAGE = {
		name = STRINGS.UI.ADMINMENU.STATS.DAMAGE,
		val_type = "number",
	},
	COOLDOWN = {
		name = STRINGS.UI.ADMINMENU.STATS.COOLDOWN,
		val_type = "number",
	},
	SPEEDMULT = {
		name = STRINGS.UI.ADMINMENU.STATS.SPEEDMULT,
		val_type = "multiplier",
	},
	BONUSDAMAGE = { 
		name = STRINGS.UI.ADMINMENU.STATS.BONUSDAMAGE,
		val_type = "multiplier",
	},
	BONUS_COOLDOWNRATE = {
		name = STRINGS.UI.ADMINMENU.STATS.BONUS_COOLDOWNRATE,
		val_type = "multiplier",
	},
	HEALINGRECEIVEDMULT = {
		name = STRINGS.UI.ADMINMENU.STATS.HEALINGRECEIVEDMULT,
		val_type = "multiplier",
	},
	HEALINGDEALTMULT = {
		name = STRINGS.UI.ADMINMENU.STATS.HEALINGDEALTMULT,
		val_type = "multiplier",
	},
	MAGE_BONUSDAMAGE = {
		name = STRINGS.UI.ADMINMENU.STATS.MAGE_BONUSDAMAGE,
		val_type = "multiplier",
	},
	DEFENSE = {
		name = STRINGS.UI.ADMINMENU.STATS.DEFENSE,
		val_type = "multiplier",
	},
	HEALTH = {
		name = STRINGS.UI.ADMINMENU.STATS.HEALTH,
		val_type = "number",
	},
	RUNSPEED = {
		name = STRINGS.UI.ADMINMENU.STATS.RUNSPEED,
		val_type = "number",
	},
}

-- TODO adjust stat sorter?
function StatDecoder(stats)
	local decoded_stats = {}
	local stat_value = 0
	
	for stat, value in pairs(stats) do
		if stat_list[stat] then
			local stat_name = stat_list[stat].name
			if stat_list[stat].val_type == "multiplier" then
				if stat == "SPEEDMULT" then
					decoded_stats[stat_name] = "+" .. ((value - 1) * 100) .. "%"
					stat_value = stat_value + (value - 1)
				else
					decoded_stats[stat_name] = "+" .. (value * 100) .. "%"
					if stat == "DEFENSE" then
						stat_value = stat_value + value * 100
					else
						stat_value = stat_value + value
					end
				end
			else
				decoded_stats[stat_name] = value
				stat_value = stat_value + value
			end
		end
	end

	return decoded_stats, stat_value
end

local num = 0
AllForgePrefabs = {}

ForgePrefab = Class(Prefab, function(self, name, fn, assets, deps, force_path_search, category, is_mod, atlas, image, mod_icon, stats, abilities, swap_build, swap_data, admin_fn_override) 
	Prefab._ctor(self, name, fn, assets, deps, force_path_search)

    self.name		 	= name
    self.type 			= category

    self.atlas	 		= (atlas and resolvefilepath(atlas)) or resolvefilepath("images/inventoryimages.xml")
    self.imagefn 		= type(image) == "function" and image or nil
    self.image 			= self.imagefn == nil and image or ("torch.tex")
	
	--print("[ForgePrefab] name: " .. tostring(name))
	
	-- TODO make this a parameter so you can set the admin function
	-- spawn prefab function
	self.admin_fn = admin_fn_override or function(self, data) SpawnEntity(data.name, data.amount, data.player) end
	
	--self.damage_type = stats.TYPE or "physical"
	self.stats, self.stat_value = StatDecoder(stats)
	self.stat_count = 0
	local count = 0
	if self.stats then
		count = 0
		for i, s in pairs(self.stats) do
			count = count + 1
		end
		self.stat_count = count
	end
		
	self.abilities = abilities
	self.ability_count = 0
	if self.abilities then
		count = 0
		for i, a in pairs(self.abilities) do
			count = count + 1
		end
		self.ability_count = count
	end
	
	--Used for lobbyscreen equips
	self.swap_build = swap_build
	local commonswapdata = {
		common_head1 = {swap = {"swap_hat"}, hide = {"HAIR_NOHAT", "HAIR", "HEAD"}, show = {"HAT", "HAIR_HAT", "HEAD_HAT"}},
		common_head2 = {swap = {"swap_hat"}, show = {"HAT"}},
		common_body = {swap = {"swap_body"}},
		common_hand = {swap = {"swap_object"}, hide = {"ARM_normal"}, show = {"ARM_carry"}}
	}
	self.swap_data = commonswapdata[swap_data] ~= nil and commonswapdata[swap_data] or swap_data
	
	--[[ TODO
		Mod Icon
			if given then use that else use default mod icon
			if not from mod and part of our mod then have no icon
			
			change or to be whatever our default icon will be
			mod_icon.image needs to be adjusted "self.name" is wrong
	--]]
	self.is_mod = is_mod
	if self.is_mod then
		self.mod_icon_atlas		= (atlas and resolvefilepath(mod_icon.atlas)) or resolvefilepath("images/inventoryimages.xml")
		self.mod_icon_imagefn 		= type(mod_icon.image) == "function" and mod_icon.image or nil
		self.mod_icon_image 			= self.mod_icon_imagefn == nil and mod_icon.image or (self.name .. ".tex")
	end
	
	-- Sort Key
	self.sortkey = self.stat_value or num

    num = num + 1
    AllForgePrefabs[name] = self
end)

function GetValidForgePrefab(name)
    return AllForgePrefabs[name]
end

function IsForgePrefabValid(name)
    return GetValidForgePrefab(name) ~= nil
end

function RemoveAllForgePrefabs()
    AllForgePrefabs = {}
    num = 0
end