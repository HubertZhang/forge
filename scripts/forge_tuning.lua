--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details
The source codes does not come with any warranty including
the implied warranty of merchandise.
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

return {
	--------------------
	--== Characters ==--
	--------------------
	AMPLIFY = {
		DAMAGE_MULTIPLIER_INCREASE = 1.5,
		DAMAGE_DEALT_THRESHOLD = 200,
		HEAL_RATE_INCREASE = 1.5, -- TODO spell mults are always 50%, should use DAMAGE_MULTIPLIER_INCREASE instead
		DURATION_MULT = 1.5,
	},

	BATTLECRY = {
		DAMAGE_MULTIPLIER_INCREASE = 1.25,
		DECAY_TIME = 5,
		STORE_TIME = 3,
		TRIGGER = 8,
		RADIUS = 8, -- Double check this, should be the diameter of a heal
	},

	MIGHTY = {
		SCALE = 1.5,
		DAMAGE_MULTIPLIER_INCREASE = 1,
		RESISTANCE_MULTIPLIER_INCREASE = 0.5,
		HEALTH_THRESHOLD = 120,
		HEALTH_TRIGGER = 60,
		DURATION = 10,
	},

	SHADOWS = {
		DAMAGE_DEALT_THRESHOLD = 300,
		DAMAGE = 30,
		LUNGE_SPEED = 30,
	},

	SHOCK = {
		TRIGGER = 4,
		DURATION = 2,
		DECAY_TIME = 30,
		DAMAGE = 30/TUNING.ELECTRIC_DAMAGE_MULT, --With the 50% electric stimuli buff, WX will deal 50 damage
	},

	WES = {
		REVIVE_SPEED_MULTIPLIER = 0.5,
	},

	WILSON = {
		BONUS_HEALTH_ON_REVIVE_PERCENT = 0.4,
		BONUS_REVIVE_SPEED_MULTIPLIER = 0.5,
	},

	WINONA = {
		BONUS_COOLDOWN_RATE = 0.1,
		MAX_HEALTH = 200,
	},

	WOODIE = {
		MAX_HEALTH = 200,
	},

	WOLFGANG = {
		MAX_HEALTH = 200,
	},

	HEALTH_ON_REVIVE = 0.2,


	--------------------
	--==    Pets    ==--
	--------------------
	BABY_SPIDERS = {
		AMOUNT = 3,
	},

	ABIGAIL = {
		HEALTH = 450,
		DAMAGE = 6,
		RESPAWN_TIME = 90,
	},

	GOLEM = {
		HEALTH = 1,
		RUNSPEED = 0,

		DAMAGE = 25,
		HIT_RANGE = 9,
		ATTACK_PERIOD = 0,

		LIFE_TIME = 10,
	},


	--------------------
	--==    Mobs    ==--
	--------------------
	TAUNT_CD = 8, --Leo: This is to prevent explosive-to-taunt spam. Not sure if its 100% accurate but there is evidence to prove that explosive indeed doesn't always stun.
	STUN_TIME = 1, --This only applies to the electric stun, all other stuns just end the stun when the animation ends. TODO this stun time might be inaccurate.
	HIT_RECOVERY = 0.75,
	KEEP_AGGRO_MELEE_DIST = 2, --the range for determining when a target is too close to draw away from under normal circumstances.
	PITPIG = {
		HEALTH = 340,
		RUNSPEED = 7.1/0.8, --divided by 0.8 because of the 0.8 scaling.
		DASHSPEED = 20,

		DAMAGE = 20,
		ATTACK_RANGE = 2,
		HIT_RANGE = 3, --Leo: PitPig currently just uses the default range, so this is pretty much ignored right now.
		ATTACK_PERIOD = 2,

		RAM_SPEED = 40,
		RAM_CD = 10,
	},

	CROCOMMANDER = {
		HEALTH            = 1350,
		RUNSPEED          = 7.1,
		DAMAGE            = 40,
		SPIT_DAMAGE       = 40,
		ATTACK_RANGE      = 2,
		SPIT_ATTACK_RANGE = 5,
		SPIT_ATTACK_PERIOD = 4,
		HIT_RANGE         = 3,
		MELEE_RANGE       = 7,
		SPIT_SPEED        = 3,
		SPIT_Y_POS        = 3,
		ATTACK_PERIOD     = 1.5,
		SPIT_CD           = 15,
		BANNER_CD         = 15,
	},

	CROCOMMANDER_RAPIDFIRE = {
		HEALTH = 1000,
		RUNSPEED = 8,

		DAMAGE = 20,
		SPIT_ATTACK_RANGE = 18,
		HIT_RANGE = 4,
		ATTACK_PERIOD = 0.2,
	},

	BATTLESTANDARD = {
		HEALTH = 30,

		PULSE_TICK = 0.1, --Leo: is this too fast?
		DEF_BUFF = 0.5,
		ATK_BUFF = 0.5, --set this to 0.5 instead of 1.5 due to some issues with the battlestandard efficiency option
		HEALTH_PER_TICK = 0.34,
		HEALTH_TICK = 1/30,
		SPEED_BUFF = 1.5,

		BUFF_WEAROFF_TIME = 3,

	},

	SNORTOISE = {
		HEALTH = 2550, --confirmed to be on point.

		RUNSPEED = 2,
		WALKSPEED = 2,

		DAMAGE = 50,
		SPIN_MULT = 0.5,

		ATTACK_RANGE = 2,
		HIT_RANGE = 3,

		PHASE2_TRIGGER = 0.5,
		SPIN_ATTACK_RANGE = 8,
		SPIN_HIT_RANGE = 2,

		ATTACK_PERIOD = 4,

		SPIN_CD = 30,
		SHEILD_CD = 7,
	},

	SCORPEON = {
		HEALTH = 3400, --confirmed to be on point.

		RUNSPEED = 3,
		WALKSPEED = 3,

		DAMAGE = 70,
		SPIT_DAMAGE = 5,
		ACID_DAMAGE_TOTAL = 70,
		ACID_WEAROFF_TIME = 8,
		ACID_DAMAGE_TICK = .2,

		ATTACK_RANGE = 3,
		HIT_RANGE = 3.5,

		PHASE2_TRIGGER = 0.75,
		SPIT_ATTACK_RANGE = 3.25,

		SPIT_SPEED = 6,
		SPIT_MAX_Y = 3,

		ATTACK_PERIOD = 3,
		ATTACK_PERIOD_ENRAGED = 2,

		SPIT_CD = 10,
	},

	BOARILLA = {
		HEALTH = 7700,
		--Leo: Please test the speeds before changing them, boarilla doesn't run at a flat speed. He has slow movements and fast movements in different parts of the runstate.
		--also these get multiplied by 1.2 due to scaling.
		RUNSPEED = 10, --Runner speed is 7.2 with +20 speed boost
		WALKSPEED = 8,

		DAMAGE = 150,
		--SLAM_MULT = 1.5, -- Glass: In videos slam was doing the same damage as punch, no mult needed

		ATTACK_RANGE = 2.5,
		HIT_RANGE = 4,
		AOE_HIT_RANGE = 2.25,

		ROLL_TRIGGER = 0.9,
		MAXROLLS = 30,
		
		PHASE2_TRIGGER = 0.5,
		JUMP_ATTACK_RANGE = 8,
		JUMP_HIT_RANGE = 4,

		ATTACK_PERIOD = 3,

		ROLL_CD = 20,
		SLAM_CD = 10,
		SHEILD_CD = 10,
		FOSSIL_TIME = 1,
		--Leo: Normally knockback is handled automatically via the knockback event, however these values are meant for entities that don't have a knockback listener.
		ATTACK_KNOCKBACK = 20, --was 25.
	},

	BOARRIOR = {
		HEALTH = 34000,

		WALKSPEED = 5,
		RUNSPEED = 20,

		DAMAGE = 200,

		ATTACK_RANGE = 3.5,
		HIT_RANGE = 6,
		ATTACK2_RANGE = 10,
		ATTACK2_HIT_RANGE = 10,
		AOE_HIT_RANGE = 4,

		ATTACK_PERIOD = 3,

		ATTACK2_CD = 8, --SLAM
		SPIN_CD = 5,

		PHASE1_TRIGGER = 0.9,
		PHASE2_TRIGGER = 0.75,
		PHASE3_TRIGGER = 0.5,
		PHASE4_TRIGGER = 0.25,
		FOSSIL_TIME = 1,

		--Leo: Normally knockback is handled automatically via the knockback event, however these values are meant for entities that don't have a knockback listener.
		ATTACK_KNOCKBACK = 12,
		SPIN_KNOCKBACK = 6
	},

	RHINOCEBRO = {
		HEALTH = 12750,

		REV_PERCENT = 0.20, --2560

		RUNSPEED = 7, --guesstimate, gets multiplied by 1.15 because of scale.

		DAMAGE = 150,

		CHEER_CD = 15,

		BUFF_MULT = 0.25,
		BUFF_CAP = 50, --no known cap yet, assume 50 for now.

		ATTACK_RANGE = 3.5, --either 2 or 3, will tune this later.
		HIT_RANGE = 4.5, --need more information on hitranges (can you even dodge it?)

		--Leo: these are no longer used since the ram activates via the running states now.
		ATTACK2_RANGE = 30, --to start ramming range
		ATTACK2_HIT_RANGE = 0,

		AOE_HIT_RANGE = 3,

		ATTACK_PERIOD = 3,

		ATTACK2_CD = 5, --Ram CD
		SPIN_CD = 5, --I have no idea why this is here, I'll remove it when I double check to make sure its not being used.

		PHASE1_TRIGGER = 0.2, --apply damaged builds at 20% HP.

		ATTACK_KNOCKBACK = 2,
		MAXRAMS = 30,
	},

	SWINECLOPS = {
		HEALTH = 42500, --Pretty sure this is exact
		
		GUARD_TIME = 15,
		--speed gets multiplied by 1.05 due to scaling.
		RUNSPEED = 10,
		WALKSPEED = 4,

		DAMAGE = 200, --confirmed to be the base. when buffed this increases to 250.

		TAUNT_CD = 10, --almost certain that its determined by battlecry.
		ATTACK2_CD = 3,

		BUFF_DAMAGE = 1.25,
		BUFF_DEFENSE = 0.5,

		ATTACK_RANGE = 3, --either 2 or 3, will tune this later.
		HIT_RANGE = 3, --need more information on hitranges.

		AOE_HIT_RANGE = 6, --going to assume that that all groundpounds and slams use the same radius.

		ATTACK2_RANGE = 8, --to start bellyflop range, mostly use if the target is on the run while hes walking.
		ATTACK2_HIT_RANGE = 0,

		ATTACK_PERIOD = 2, 
		ATTACK_GUARD_PERIOD = 1, --unused now

		ATTACK2_CD = 7, --Bellyflop

		--These are for determining the max
		INITIALSHIELD_TRIGGER = 0.9,
		PHASE1_TRIGGER = 0.3,

		ATTACK_KNOCKBACK = 2,
	},


	--------------------
	--==  Weapons   ==--
	--------------------
	--Elec said that adding 0.1 for every 4 in radius tuning resulted in a more accurate radius
	FORGEDARTS = {
		DAMAGE = 20,
		COOLDOWN = 18,
		--TYPE = "physical",
	},
	FORGINGHAMMER = {
		DAMAGE = 20,
		ALT_DAMAGE = 30 /TUNING.ELECTRIC_DAMAGE_MULT,
		ALT_RADIUS = 4.1,
		COOLDOWN = 18,
		--TYPE = "physical",
	},
	RILEDLUCY = {
		DAMAGE = 20,
		ALT_DAMAGE = 30,
		--TYPE = "physical",
	},
	PITHPIKE = {
		DAMAGE = 25,
		ALT_DAMAGE = 25,
		ALT_DIST = 6.5,
		ALT_WIDTH = 3.25,
		COOLDOWN = 12,
		--TYPE = "physical",
	},
	LIVINGSTAFF = {
		DAMAGE = 10,
		COOLDOWN = 24,
		DURATION = 10,
		HEAL_RATE = 6,
		RANGE = 4.1,
		--TYPE = "magical",
		SCALE_RNG = {30, 40},
	},
	MOLTENDARTS = {
		DAMAGE = 25,
		ALT_DAMAGE = 50,
		COOLDOWN = 18,
		--TYPE = "physical",
	},
	INFERNALSTAFF = {
		DAMAGE = 25,
		ALT_DAMAGE = {base = 200, center_mult = 0.25}, --Damage increases as the targets get closer to the center
		ALT_RADIUS = 4.1, --CunningFox: It's actually 6 --Fid: Let's try 4.1 out for now since the meteor's circle is the same size as the hammer's
		COOLDOWN = 24,
		--TYPE = "magical",
	},
	SPIRALSPEAR = {
		DAMAGE = 30,
		ALT_DAMAGE = 75,
		ALT_RANGE = 16,
		ALT_RADIUS = 2.05,
		COOLDOWN = 12,
		--TYPE = "physical",
	},
	PETRIFYINGTOME = {
		DAMAGE = 15,
		COOLDOWN = 18,
		DURATION = 10,
		ALT_RADIUS = 4.1,
		--TYPE = "physical",
	},
	BACONTOME = {
		DAMAGE = 15,
		COOLDOWN = 18,
		DURATION = 10,
		--TYPE = "physical",
	},
	FIREBOMB = {
		DAMAGE = 15,
		ALT_DAMAGE = 75,
		PROC_DAMAGE = 100,
		COOLDOWN = 6,
		ALT_RANGE = 3,
		HORIZONTAL_SPEED = 20,
		GRAVITY = -30,
		CHARGE_HITS_1 = 7,
		CHARGE_HITS_2 = 10,
		MAXIMUM_CHARGE_HITS = 13,
		CHARGE_DECAY_TIME = 5,
		
		--TYPE = "physical",
	},
    BLACKSMITHSEDGE = {
        DAMAGE = 30,
        HELMSPLIT_DAMAGE = 100, --this is multiplied by the battlecry and any shieldbreak mult.
        PARRY_DURATION = 5,
        COOLDOWN = 12,
    },

	--------------------
	--==   Equips   ==--
	--------------------
	--Forge Season 1
	FEATHEREDWREATH = {SPEEDMULT = 1.2},
	BARBEDHELM = {BONUSDAMAGE = 1.1},
	CRYSTALTIARA = {BONUS_COOLDOWNRATE = -0.1},
	FLOWERHEADBAND = {HEALINGRECEIVEDMULT = 1.25},
	WOVENGARLAND = {HEALINGDEALTMULT = 1.2},
	NOXHELM = {BONUSDAMAGE = 1.15},
	RESPLENDENTNOXHELM = {BONUSDAMAGE = 1.15, SPEEDMULT = 1.1, BONUS_COOLDOWNRATE = -0.1},
	BLOSSOMEDWREATH = {REGEN = {period = 0.5, delta = 1, limit = 0.8}, SPEEDMULT = 1.1, BONUS_COOLDOWNRATE = -0.1},
	CLAIRVOYANTCROWN = {MAGE_BONUSDAMAGE = 1.25, SPEEDMULT = 1.1, BONUS_COOLDOWNRATE = -0.1},

	REEDTUNIC = {DEFENSE = 0.5, BONUS_COOLDOWNRATE = -0.05},
	FEATHEREDTUNIC = {DEFENSE = 0.6, SPEEDMULT = 1.1},
	FORGE_WOODARMOR = {DEFENSE = 0.75},
	JAGGEDARMOR = {DEFENSE = 0.75, BONUSDAMAGE = 1.1},
	SILKENARMOR = {DEFENSE = 0.75, BONUS_COOLDOWNRATE = -0.1},
	SPLINTMAIL = {DEFENSE = 0.85},
	STEADFASTARMOR = {DEFENSE = 0.9, SPEEDMULT = 0.85},
	KNOCKBACK_RESIST_SPEED = 2,

	--Forge Season 2
	STEADFASTGRANDARMOR = {DEFENSE = 0.9, MAX_HP = 100},
	WHISPERINGGRANDARMOR = {DEFENSE = 0.8, MAX_HP = 75, PET_BUFF = 2},
	SILKENGRANDARMOR = {DEFENSE = 0.8, MAX_HP = 50, BONUS_COOLDOWNRATE = -0.15},
	JAGGEDGRANDARMOR = {DEFENSE = 0.8, MAX_HP = 50, BONUSDAMAGE = 1.2},

	--------------------
	--==   Other    ==--
	--------------------
	--HEALINGCIRCLE = {
		--COLOUR = RGB(75, 180, 180), --CunningFox: Replace this asap!!!
	--}, --Fid: I believe it can be removed now... Leaving it just in case
	--Leo: ^ the comment above is by a gay.
	--Fid: ^ the comment above is by a super furry
	--Leo: ^ who listens to fid anyways.
    --Zark: leo back to work! we dont pay you to slack off.
	--Fid: Yeah, we could totally move our budget of $0 somewhere more important!

	--used for aggroholds on "onattacked" targeting functions for mobs.
	AGGROTIMER_LUCY = 1,
	AGGROTIMER_STUN = 2,

	ROUND_3_DELAY = 15,
	BOARILLA_SPAWN_DELAY = 15,

	RET_DATA = {
		fossil = {"aoecctarget", 2},
		elemental = {"aoesummontarget", 1},
		hammer = {"aoehostiletarget", 0.9},
		livingstaff = {"aoefriendlytarget", 0.9},
		infernalstaff = {"aoehostiletarget", 0.7},
		spiralspear = {"aoesmallhostiletarget", 1},
		-- spike: aoesmallhostiletarget, firestaff:reticuleaoehostiletarget
	},

	BUFFS_DATA = {
		SCORPEON_DOT = {SYMBOL = "ACID", BUFF = false},

		HEALINGCIRCLE_REGENBUFF = {SYMBOL = "HEALING", BUFF = true},
		DAMAGER_BUFF = {SYMBOL = "WATHGRITHR", BUFF = true},
		WICK_BUFF = {SYMBOL = "WICKERBOTTOM", BUFF = true},
	},

	DEFAULT_FORGE_STATS = {"healingreceived","pet_damagetaken","altattacks","total_damagetaken","kills","turtillusflips","player_damagedealt","player_damagetaken","guardsbroken","cctime","healingdone","corpsesrevived","spellscast","petdeaths","deaths","total_damagedealt","attacks","ccbroken","pet_damagedealt","standards","blowdarts","stepcounter","aggroheld","numcc"},

	DEFAULT_FORGE_TITLES = {
		altattacks 			= {priority = 4,},
		total_damagetaken 	= {priority = 10,tier2 = 2000,},
		kills 				= {priority = 5,tier2 = 20,},
		turtillusflips 		= {priority = 9,},
		guardsbroken 		= {priority = 8,},
		healingdone 		= {priority = 3,tier2 = 3500,},
		corpsesrevived 		= {priority = 13,},
		spellscast 			= {priority = 2,},
		deaths 				= {priority = 15,},
		total_damagedealt 	= {priority = 1,tier2 = 18000,},
		attacks 			= {priority = 11,},
		standards 			= {priority = 14,},
		blowdarts 			= {priority = 12,tier2 = 1000,},
		stepcounter 		= {priority = 16,},
		aggroheld 			= {priority = 6,tier2 = 1000,},
		numcc 				= {priority = 7,},
		none 				= {priority = 99999,}
	},

	STAT_CATEGORIES = {
		total_damagedealt 	= {category = "attack", priority = 1,},
		player_damagedealt 	= {category = "attack", priority = 2,},
		pet_damagedealt 	= {category = "attack", priority = 3,},
		kills 				= {category = "attack", priority = 4,},
		attacks 			= {category = "attack", priority = 5,},
		altattacks 			= {category = "attack", priority = 6,},
		blowdarts 			= {category = "attack", priority = 7,},
		spellscast 			= {category = "attack", priority = 8,},
		guardsbroken 		= {category = "attack", priority = 9,},
		cctime 				= {category = "crowd_control", priority = 1,},
		numcc 				= {category = "crowd_control", priority = 2,},
		turtillusflips 		= {category = "crowd_control", priority = 3,},
		ccbroken 			= {category = "crowd_control", priority = 4,},
		total_damagetaken 	= {category = "defense", priority = 1,},
		player_damagetaken 	= {category = "defense", priority = 2,},
		pet_damagetaken 	= {category = "defense", priority = 3,},
		deaths 				= {category = "defense", priority = 4,},
		petdeaths 			= {category = "defense", priority = 5,},
		healingdone 		= {category = "healing", priority = 6,},
		healingreceived 	= {category = "healing", priority = 7,},
		corpsesrevived 		= {category = "healing", priority = 8,},
		aggroheld 			= {category = "other", priority = 1,},
		stepcounter 		= {category = "other", priority = 2,},
		standards 			= {category = "other", priority = 3,},
	}
}
