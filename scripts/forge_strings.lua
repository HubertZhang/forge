--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]
local STRINGS = _G.STRINGS

STRINGS.NAMES.PITPIG = STRINGS.NAMES.BOARON
STRINGS.NAMES.BOARILLA = STRINGS.NAMES.TRAILS
STRINGS.NAMES.SCORPEON = STRINGS.NAMES.PEGHOOK

STRINGS.NAMES.CROCOMMANDER_RAPIDFIRE = "Crocommancer"
STRINGS.NAMES.BATTLESTANDARD_SPEED = "Battle Standard"

STRINGS.NAMES.SCORPEON_ACID = "Scorpeon Acid"
STRINGS.NAMES.PUGNAS_REDLIGHT = "The Red Light"

STRINGS.BOARLORD_GREENLIGHT = {"GREEN LIGHT!"}
STRINGS.BOARLORD_REDLIGHT = {"RED LIGHT!"}
STRINGS.BOARLORD_GREENLIGHT_FAKEOUT_BANTER = { "GREEN FLIGHT", "GREEN SIGHT", "GREEN FIGHT", "GREEN LITE", "GREEN MIGHT", "GREEN RIGHT", "GREEN KITE", "GREEN WHITE" }

STRINGS.UI.LOBBYSCREEN.FORCESTART = "Force Start"
STRINGS.UI.LOBBYSCREEN.CANCELSTART = "Cancel Start"
STRINGS.UI.LOBBYSCREEN.SAYSTART = "Forcing Start in %s seconds. If you don't have a character selected before the countdown is over, then a random character will be selected for you."
STRINGS.UI.LOBBYSCREEN.SAYCANCEL = "Cancelling Start."
STRINGS.UI.LOBBYSCREEN.FORCESTARTTITLE = "Confirm Force Start"
STRINGS.UI.LOBBYSCREEN.FORCESTARTDESC = "Are you sure you want to force start the match in %s seconds?"

STRINGS.UI.DETAILEDSUMMARYSCREEN = {
	STATS = {
		TITLES = {
			attack = "Attack Stats:",
			crowd_control = "Crowd Control Stats:",
			defense = "Defense Stats:",
			healing = "Healing Stats:",
			other = "Other Stats:",
		},
	},
	TITLE = "Detailed Match Summary",
	TEAMSTATS = {
		TITLE = "Detailed Team Stats",
		SHOW = "View Team Stats",
		HIDE = "Go Back to MVP Badges", -- TODO better text for this?
		BACK = "Back",
		STATHEADERS = {
			deaths = "Deaths",
			total_damagedealt = "Total Damage Dealt",
			kills = "Killing Blows",
			aggroheld = "Aggro Time (seconds)",
			corpsesrevived = "Allies Revived",
			healingdone = "Health Restored",
			total_damagetaken = "Damage Taken",
			attacks = "Swings Landed",
			turtillusflips = "Snortoises Flipped",
			spellscast = "Spells Cast",
			altattacks = "Special Attacks Used",
			stepcounter = "Steps Taken",
			blowdarts = "Dart hits",
			standards = "Battle Standards Destroyed",
			numcc = "Enemies Immobilized",
			guardsbroken = "Enemy Guards Broken",
			healingreceived = "Healing Recieved",
			pet_damagetaken = "Pet Damage Taken",
			player_damagedealt = "Player Damage Dealt",
			player_damagetaken = "Player Damage Taken",
			cctime = "Seconds of Crowd Control",
			petdeaths = "Pet Deaths",
			ccbroken = "Crowd Control Broken",
			pet_damagedealt = "Pet Damage Dealt",
			unknown = "UNKNOWN",
		},
	},
}

STRINGS.UI.ADMINMENU = {
	TABS = {
		ENEMIES = "Spawn Enemy",
		WEAPONS = "Spawn Weapon",
		ARMOR = "Spawn Armor",
		BUFFS = "Spawn Buff",
		FORGE = "Forge Admin Commands",
		GENERAL = "General Admin Commands",
	},
	BUTTONS = {
		SPAWN = "Spawn",
		ACTIVATE = "Activate",
		ABILITIES = "Abilities",
		DESCRIPTION = "Description",
		EQUIP = "Equip",
	},
	NO_ABILITIES = "No abilities",
	STATS = {
		DAMAGE = "Damage",
		COOLDOWN = "Cooldown",
		SPEEDMULT = "Speed",
		BONUSDAMAGE = "Damage",
		BONUS_COOLDOWNRATE = "Cooldown",
		HEALINGRECEIVEDMULT = "Magic_Damage",
		HEALINGDEALTMULT = "Magic_Damage",
		MAGE_BONUSDAMAGE = "Magic_Damage",
		DEFENSE = "Defense",
		HEALTH = "Health",
		RUNSPEED = "Speed",
	},
}

local lavaarena_titles = STRINGS.UI.MVP_LOADING_WIDGET.LAVAARENA.TITLES
lavaarena_titles.player_damagedealt = "Merciless"
lavaarena_titles.player_damagetaken = "Protector"
lavaarena_titles.pet_damagedealt = "Zookeeper"
lavaarena_titles.pet_damagetaken = "Loyal Guardian"
lavaarena_titles.cctime = "Immobilizer"
lavaarena_titles.ccbroken = "Disruptor"
lavaarena_titles.petdeaths = "Veterinarian"
lavaarena_titles.healingreceived = "Spoiled"

local lavaarena_desc = STRINGS.UI.MVP_LOADING_WIDGET.LAVAARENA.DESCRIPTIONS
lavaarena_desc.healingreceived = "healing recieved"
lavaarena_desc.pet_damagetaken = "pet damage taken"
lavaarena_desc.player_damagedealt = "player damage dealt"
lavaarena_desc.player_damagetaken = "player damage taken"
lavaarena_desc.cctime = "seconds of crowd control"
lavaarena_desc.petdeaths = "pet deaths"
lavaarena_desc.ccbroken = "crowd control broken"
lavaarena_desc.pet_damagedealt = "pet damage dealt"
lavaarena_desc.unknown = "UNKNOWN"

STRINGS.FORGED_FORGE = {
	-----------
	-- STATS --
	-----------
	STATS = {
		HEALTH = {
			ICON = "half_health",
		},
		DAMAGE = {
			ICON = "spear",
		},
		MAGIC_DAMAGE = {
			ICON = "firestaff_meteor",
		},
		SPEED = {
			ICON = "cane",
		},
		--ELEMENT -- TODO
		DEFENSE = {
			ICON = "lavaarena_armorextraheavy",
		},
		COOLDOWN = {
			ICON = "torch",
		},
		KNOCKBACK = {
			ICON = "torch",
		},
	},
	-------------
	-- ENEMIES --
	-------------
	ENEMIES = { -- TODO if different skins then add that so ENEMIES.BOARON."skin_name".NAME etc default: ENEMIES.BOARON.DEFAULT.NAME
		PITPIG = {
			NAME = "Pit Pig",
			DESC = "A pig that grew up",
			ABILITIES = {
				LUNGE = {
					NAME = "Lunge",
					DESC = "Lunge that does x damage",
				},
			},
		},
		CROCOMMANDER = {
			NAME = "Crocommander",
			DESC = "Experienced commander who can inspire allies to be more formidable",
			ABILITIES = {
				BANNER_DEF = {
					NAME = "Defense Banner",
					DESC = "Increases the armor of allies nearby",
				},
				BANNER_ATK = {
					NAME = "Attack Banner",
					DESC = "Increases the dammage of allies nearby",
				},
				BANNER_HEAL = {
					NAME = "Regen Banner",
					DESC = "Gives nearby allies health regeneration",
				},
			},
		},
		CROCOMMANDER_RAPIDFIRE = {
			NAME = STRINGS.NAMES.CROCOMMANDER_RAPIDFIRE,
			DESC = "Crude, rude, and probably stole those slick glasses from a monkey. This cool Croc thinks he's hot stuff because he can breathe fire!",
			ABILITIES = {
				BANNER_SPEED = {
					NAME = "Speed Banner",
					DESC = "Increases the speed of allies nearby",
				},
			},
		},
		SNORTOISE = {
			NAME = "Snortoise",
			DESC = "A Snurtle forced to live in an armored shell",
			ABILITIES = {
				BUNKER = {
					NAME = "Bunker",
					DESC = "Retreats into its shell for x seconds unless interrupted by an ability",
				},
				SPIN = {
					NAME = "Spin",
					DESC = "Retreats into its shell and starts spinning while sliding towards its target for x seconds unless interrupted by an ability",
				},
			},
		},
		SCORPEON = {
			NAME = "Scorpeon",
			DESC = "A poisonous creature that insists on wanting to hug others",
			ABILITIES = {
				POISON = {
					NAME = "Poison Lob",
					DESC = "Lobs a poison ball with its tail. The poison lasts for x seconds.",
				},
			},
		},
		BOARILLA = {
			NAME = "Boarilla",
			DESC = "An orange gorilla who stole Boarrior's old armor",
			ABILITIES = {
				BUNKER = {
					NAME = "Bunker",
					DESC = "Bunkers into his armor for x seconds resisting all basic damage",
				},
				SLAM = {
					NAME = "Slam",
					DESC = "Jumps into the air and slams down on target doing damage in an area",
				},			
			},
		},
		BOARRIOR = {
			NAME = "Boarrior",
			DESC = "A boaron who spent all his life grinding levels. They used to laugh at him, now look at him.",
			ABILITIES = { -- TODO add in Boarriors abilities
				BUNKER = {
					NAME = "Bunker",
					DESC = "Bunkers into his armor for x seconds resisting all basic damage",
				},
				SLAM = {
					NAME = "Slam",
					DESC = "Jumps into the air and slams down on target doing damage in an area",
				},			
			},
		},
		RHINOCEBRO = {
			NAME = "Rhinocebro Flatbrim",
			DESC = "Would probably steal your lunch money",
			ABILITIES = { -- TODO add in Boarriors abilities
				CHEER = {
					NAME = "Bro Cheer",
					DESC = "Cheers his bro, Snapback, to gain a permanent +25 damage boost",
				},
				RAM = {
					NAME = "Ram",
					DESC = "Charges forward with his horn doing damage in a large radius",
				},			
			},
		},
		RHINOCEBRO2 = {
			NAME = "Rhinocebro Snapback",
			DESC = "Would probably stuff you in a locker",
			ABILITIES = { -- TODO add in Boarriors abilities
				CHEER = {
					NAME = "Bro Cheer",
					DESC = "Cheers his bro, Flatbrim, to gain a permanent +25 damage boost",
				},
				RAM = {
					NAME = "Ram",
					DESC = "Charges forward with his horn doing damage in a large radius",
				},			
			},
		},
		SWINECLOPS = {
			NAME = "Swineclops",
			DESC = "Mike Tyson of the Forge",
			ABILITIES = { -- 
				DEFENSIVE_STANCE = {
					NAME = "Defensive Stance",
					DESC = "Moves faster, takes 50% less damage, but can only do one attack",
				},
				OFFENSIVE_STANCE = {
					NAME = "Offensive Stance",
					DESC = "Can combo punches, can buff itself, can use bellyflop, but moves slower.",
				},			
			},
		}
	},
	-----------
	-- ARMOR --
	-----------
	ARMOR = {
		----------
		-- HELM --
		----------
		FEATHEREDWREATH = {
			NAME = "Feathered Wreath",
			DESC = "",
		},
        FLOWERHEADBAND = {
			NAME = "Flower Headband",
			DESC = "",
		},
        BARBEDHELM = {
			NAME = "Barbed Helm",
			DESC = "",
		},
        NOXHELM = {
			NAME = "Nox Helm",
			DESC = "",
		},
        WOVENGARLAND = {
			NAME = "Woven Garland",
			DESC = "",
		},
        CLAIRVOYANTCROWN = {
			NAME = "Clairvoyant Crown",
			DESC = "",
		},
        CRYSTALTIARA = {
			NAME = "Crystal Tiara",
			DESC = "",
		},
        BLOSSOMEDWREATH = {
			NAME = "Blossomed Wreath",
			DESC = "",
		},
        RESPLENDENTNOXHELM = {
			NAME = "Resplendent Nox Helm",
			DESC = "",
		},
		----------
		-- BODY --
		----------
		REEDTUNIC = {
			NAME = "Reed Tunic",
			DESC = "",
		},
        FEATHEREDTUNIC = {
			NAME = "Feathered Reed Tunic",
			DESC = "",
		},
        FORGE_WOODARMOR = {
			NAME = "Wood Armor",
			DESC = "",
		},
        JAGGEDARMOR = {
			NAME = "Jagged Wood Armor",
			DESC = "",
		},
        SILKENARMOR = {
			NAME = "Silken Wood Armor",
			DESC = "",
		},
        SPLINTMAIL = {
			NAME = "Stone Splint Mail",
			DESC = "",
		},
        STEADFASTARMOR = {
			NAME = "Steadfast Stone Armor",
			DESC = "",
		},
		--Forge Season 2
		STEADFASTGRANDARMOR = {
			NAME = "Steadfast Grand Armor",
			DESC = "",
		},
		WHISPERINGGRANDARMOR = {
			NAME = "Whispering Grand Armor",
			DESC = "",
		},
		SILKENGRANDARMOR = {
			NAME = "Silken Grand Armor",
			DESC = "",
		},
		JAGGEDGRANDARMOR = {
			NAME = "Jagged Grand Armor",
			DESC = "",
		},
	},
	-------------
	-- WEAPONS --
	-------------
	WEAPONS = {
		FORGEDARTS = {
			NAME = "Darts",
			DESC = "",
			ABILITIES = {
				BARRAGE = {
					NAME = "Barrage",
					DESC = "",
				},
			},
		},
		MOLTENDARTS = {
			NAME = "Molten Darts",
			DESC = "",
			ABILITIES = {
				FIRE_BLAST = {
					NAME = "Fire Blast",
					DESC = "",
				},
			},
		},
		INFERNALSTAFF = {
			NAME = "Infernal Staff",
			DESC = "",
			ABILITIES = {
				METEOR = {
					NAME = "Meteor",
					DESC = "",
				},
			},
		},
		FORGINGHAMMER = {
			NAME = "Forge Hammer",
			DESC = "",
			ABILITIES = {
				SLAM = {
					NAME = "Slam",
					DESC = "",
				},
			},
		},
		LIVINGSTAFF = {
			NAME = "Living Staff",
			DESC = "",
			ABILITIES = {
				HEALING_CIRCLE = {
					NAME = "Healing Circle",
					DESC = "",
				},
			},
		},
		PITHPIKE = {
			NAME = "Spear",
			DESC = "",
			ABILITIES = {
				CHARGE = {
					NAME = "Charge",
					DESC = "",
				},
			},
		},
		SPIRALSPEAR = {
			NAME = "Spiral Spear",
			DESC = "",
			ABILITIES = {
				SLAM = {
					NAME = "Slam",
					DESC = "",
				},
			},
		},
		RILEDLUCY = {
			NAME = "Riled Lucy",
			DESC = "",
			ABILITIES = {
				THROW = {
					NAME = "Throw",
					DESC = "",
				},
			},
		},
		BACONTOME = {
			NAME = "Tome of Beckoning",
			DESC = "",
			ABILITIES = {
				SUMMON_GOLEM = {
					NAME = "Summon Golem",
					DESC = "",
				},
			},
		},
		PETRIFYINGTOME = {
			NAME = "Petrifying Tome",
			DESC = "",
			ABILITIES = {
				PETRIFY = {
					NAME = "Petrify",
					DESC = "",
				},
			},
		},
        FIREBOMB = {
            NAME = "Hearthsfire Crystals",
            DESC = "",
            ABILITIES = {
                BARRAGE = {
                    NAME = "Explosive Throw",
                    DESC = "",
                },
            },
        },
        BLACKSMITHSEDGE = {
            NAME = "Blacksmiths Edge",
            DESC = "",
            ABILITIES = {
                BARRAGE = {
                    NAME = "Parry",
                    DESC = "",
                },
            },
        },
	},
	-----------
	-- BUFFS --
	-----------
	BUFFS = {
		BATTLESTANDARD_DAMAGER = {
			NAME = "Damage Banner",
			DESC = "Increases the damage of all mobs by x%",
		},
		BATTLESTANDARD_SHIELD = {
			NAME = "Shield Banner",
			DESC = "Decreases all damage done to mobs by x%",
		},
		BATTLESTANDARD_HEAL = {
			NAME = "Heal Banner",
			DESC = "Gives all mobs a regen of x%",
		},
		BATTLESTANDARD_SPEED = {
			NAME = "Speed Banner",
			DESC = "Increases the speed of all mobs by x%",
		},
		PASSIVE_BATTLECRY_FX_SELF = {
			NAME = "Battlecry",
			DESC = "If you yell at yourself you can hit harder.",
		},
	},
	-------------
	-- FORGE --
	-------------
	FORGE = {
		FORCE_ROUND = {
			NAME = "Force Round",
			DESC = "Select the round and wave you want to fight",
		},
		RESTART_FORGE = {
			NAME = "Restart Forge",
			DESC = "Reloads the server to completely restart the Forge",
		},
		RESTART_CURRENT_ROUND = {
			NAME = "Restart Current Round",
			DESC = "Restarts the current round and wave",
		},
		DISABLE_ROUNDS = {
			NAME = "Disable Rounds",
			DESC = "Rounds/Waves will not automatically start after killing all mobs",
		},
		ENABLE_ROUNDS = {
			NAME = "Enable Rounds",
			DESC = "Rounds/Waves will automatically start after killing all mobs",
		},
		END_FORGE = {
			NAME = "End Forge",
			DESC = "Ends the match immediately",
		},
	},
	-----------
	-- GENERAL --
	-----------
	GENERAL = {
		CHANGE_CHARACTER = {
			NAME = "Change Character",
			DESC = "Return to character select screen",
		},
		SUPER_GOD_MODE = {
			NAME = "Super God Mode",
			DESC = "What's better than god mode? SUPER GOD MODE!",
		},
		KILL_ALL_MOBS = {
			NAME = "Kill All Mobs",
			DESC = "Care to snap your fingers?",
		},
		PETRIFY_MOBS = {
			NAME = "Petrify",
			DESC = "Petrify all mobs around you. This has the same radius as the Petrifying Tome.",
		},
		OVER_9000 = {
			NAME = "Over 9000!!!",
			DESC = "His power level is over 9000!!!!!!",
		},
		RESET_TO_SAVE = {
			NAME = "Reset to Save",
			DESC = "Resets server to last save. Quick for testing out new builds!",
		},
		DEBUG_SELECT = {
			NAME = "Debug Select",
			DESC = "Select the chosen entity to be displayed in the debug screen. Hit backspace to access the debug screen.",
		},
		REVIVE = {
			NAME = "Revive Player",
			DESC = "Give the selected player mouth to mouth? Pfft, I'll just poke their body with a stick.",
		},
		SCORPEON_DOT = {
			NAME = "Spawn scorpeon poison",
			DESC = "Poison yourself!",
		},
	},
	-----------------
	-- BUFF WIDGET --
	-----------------
	BUFF_WDGT = {
		SCORPEON_DOT = "SCORPEON_DOT",
		HEALINGCIRCLE_REGENBUFF = "HEALINGCIRCLE_REGENBUFF",
		DAMAGER_BUFF = "DAMAGER_BUFF",
		WICK_BUFF = "WICK_BUFF",
	},
}