--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local function OnLoad(inst, data)
    if data ~= nil then
        inst.size_width = data.stand_width or 1
        inst.size_height = data.stand_height or 1
        if inst.size_width ~= nil then
            inst.stand_width:set(inst.size_width)
        end
        if inst.size_height ~= nil then
            inst.stand_height:set(inst.size_height)
        end
    end
end

local function StandPostInit(inst)
	inst:DoTaskInTime(0.5, function()
		inst.size_width = inst.size_width or 1
		inst.size_height = inst.size_height or 1
		if TheWorld and TheWorld.components.lavaarenaevent then
			local rot = inst:GetAngleToPoint(TheWorld.components.lavaarenaevent:GetArenaCenterPoint())
			rot = (math.floor((rot + 44) / 90) * 90)
			inst.Transform:SetRotation(rot)
			if not inst.teambanner then
			inst.teambanner = SpawnPrefab("lavaarena_teambanner")
			end
			local bannerpos = Vector3(0-(inst.size_height*2), 0, 0)
			inst.teambanner.Transform:SetPosition(bannerpos:Get())
			inst.teambanner.entity:SetParent(inst.entity)
			inst.teambanner.Transform:SetRotation(rot+90)
			inst.teambanner.AnimState:OverrideSymbol("banner1", "lavaarena_banner", "banner2")
			inst.teambanner.persists = false
			-----
			inst.spectators = {}
			local spectatorpositions = {}
			local xcount = inst.size_height+math.floor(inst.size_height/2)
			local xsize = (inst.size_height*4)-2
			local xposs = {}
			local zcount = inst.size_width+math.floor(inst.size_width/2)
			local zsize = (inst.size_width*4)-2
			local zposs = {}
			local instpos = inst:GetPosition()
			--print("INIT SPECTATOR SPAWN: xcount = ",xcount,"zcount = ",zcount)
			for i = -math.floor(xcount/2), math.floor(xcount/2) do
				--print("X#", i)
				local pos = (xsize/xcount)*i
				table.insert(xposs, pos)
			end
			for i = -math.floor(zcount/2), math.floor(zcount/2) do
				local pos = (zsize/zcount)*i
				table.insert(zposs, pos)
			end
			for k, v in pairs(xposs) do
				for k2, v2 in pairs(zposs) do
					table.insert(spectatorpositions, Vector3(v+(math.random(-50,50)/100), 0 , v2+(math.random(-50,50)/100)))
				end
			end
			for k, v in pairs(spectatorpositions) do
				if math.random() < 0.8 then
					local spec = SpawnPrefab("lavaarena_spectator")
					spec.Transform:SetScale(0.8, 0.8, 0.8)
					spec.Transform:SetPosition(v:Get())
					spec.entity:SetParent(inst.entity)
					spec.Transform:SetRotation(rot)
					table.insert(inst.spectators, spec)
					spec.persists = false
				end
			end
		end
	end)
    
    inst.OnLoad = OnLoad
end

local function SpectatorPostInit(inst)
	inst:AddTag("doydoyable")

	inst.AnimState:SetBuild("lavaarena_boaraudience1_build_"..tostring(math.random(3)))
	
	function inst:BecomeDoyDoy()
		inst.Transform:SetScale(1, 1, 1)
		inst.AnimState:SetBuild("lavaarena_doydoy")
		inst.AnimState:SetBank("doydoy")
		inst.AnimState:PlayAnimation("idle", true)
		
		inst:AddTag("doydoyed")
		inst:SetStateGraph("SGdoydoy")
	end
	
	local shade = math.random(75,100)/100
	inst.AnimState:SetMultColour(shade, shade, shade, 1)
	
	inst:AddComponent("crowd")
	inst:SetStateGraph("SGspectator")
end

return {
	stand_postinit = StandPostInit,
	spectator_postinit = SpectatorPostInit
}