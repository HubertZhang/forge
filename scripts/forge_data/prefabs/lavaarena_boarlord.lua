--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local function MasterPostInit(inst)
	inst:AddComponent("inspectable")
	
	TheWorld:PushEvent("ms_register_lavaarenaboarlord", inst)
	
	inst.lines_left = 0
	inst:SetStateGraph("SGboarlord")
	
	if Forge_options.redlight_greenlight then
		local function GetPlayers()
			local pt = TheWorld.components.lavaarenaevent:GetArenaCenterPoint()
			local ents = TheSim:FindEntities(pt.x, 0, pt.z, 300, {"player"})
			return ents
		end
		
		local colors = { green = {10, 200}, yellow = {255, 255}, red = {215, 10} }
		inst.ColorPlayer = function(inst, player)
			player.AnimState:SetMultColour(colors[inst.current_light_phase][1]/255, colors[inst.current_light_phase][2]/255, 0, 1)
			
			--Give players a little helping hand
			if inst.current_light_phase == "red" then
				player.components.locomotor:Stop()
				player.components.locomotor:Clear()
				player:ClearBufferedAction()
			end
		end
		
		local function ChangeLight(color)
			inst.current_light_phase = color
			local players = GetPlayers()
			for k,v in pairs(players) do
				if v and v.AnimState then inst:ColorPlayer(v) end
			end
		end
		
		local isgame_going = false
		inst.current_light_phase = "green"
		inst.time_to_next_phase = math.random(10,20)
		
		inst:ListenForEvent("ms_playerjoined", function(world, player) inst:ColorPlayer(player) end, TheWorld)
		inst:ListenForEvent("ms_lavaarena_allplayersspawned", function() isgame_going = true end, TheWorld)
		inst:DoPeriodicTask(1, function()
			if isgame_going and inst.components.talker.task == nil then
				if inst.time_to_next_phase == 0 then
					if inst.current_light_phase == "green" then
						ChangeLight("yellow")
						inst.time_to_next_phase = 2
					elseif inst.current_light_phase == "yellow" then
						ChangeLight("red")
						inst.time_to_next_phase = math.random(2, 5)
						TheWorld.components.lavaarenaevent:DoSpeech("BOARLORD_REDLIGHT")
					else
						ChangeLight("green")
						inst.time_to_next_phase = math.random(9, 20)
						TheWorld.components.lavaarenaevent:DoSpeech("BOARLORD_GREENLIGHT")
					end
				else
					inst.time_to_next_phase = inst.time_to_next_phase - 1
					if inst.current_light_phase == "red" and inst.time_to_next_phase > 2 and math.random() <= 0.05 then
						TheWorld.components.lavaarenaevent:DoSpeech("BOARLORD_GREENLIGHT_FAKEOUT_BANTER")
					end
				end
			end
		end)
	end
end

return {
	master_postinit = MasterPostInit
}