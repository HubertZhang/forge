--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local function MasterPostInit(inst)
	inst:AddComponent("inspectable")

    inst.postfxstates = {
        "doublefizzle",
        "convergetothemiddle",
        "righttoleftandback",
        "randomfizzle",
    }

    inst:SetStateGraph("SGlavaarenaspawner")
	
	inst:DoTaskInTime(0.1, function()
		local rot = inst:GetAngleToPoint(TheWorld.components.lavaarenaevent:GetArenaCenterPoint())
		rot = (math.floor((rot+44)/90) * 90) + 90
		inst.Transform:SetRotation(rot) 
		TheWorld:PushEvent("ms_register_lavaarenaspawner", { spawner = inst, id = inst:WhichPortalAmI() })
	end)
	
	function inst.SpawnMobs(inst, prefabtable, delay)
        inst:PushEvent("spawnlamobs", {prefabtable = prefabtable, delay = delay})
	end
	
	function inst.WhichPortalAmI(inst)
		local offsetfromcenter = inst:GetPosition() - TheWorld.components.lavaarenaevent:GetArenaCenterPoint()
		print(offsetfromcenter)
		if offsetfromcenter.z > 20 then
			print("SPAWNER ID MIDDLE")
			return "middle"
		elseif offsetfromcenter.x > 20 then
			print("SPAWNER ID LEFT")
			return "left"
		elseif offsetfromcenter.x < 20 then
			print("SPAWNER ID RIGHT")
			return "right"
		end
		return "middle"
	end
end

return {
	master_postinit = MasterPostInit
}
