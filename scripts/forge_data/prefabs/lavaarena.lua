--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local lavaarena = {}

lavaarena["master_postinit"] = function(inst)
	inst:AddComponent("lavaarenaevent")
	inst:AddComponent("lavaarenaanalytics")
	inst:AddComponent("lavaarenalivemobtracker")
	inst:AddComponent("wavetracker")
	
	inst:ListenForEvent("ms_register_lavaarenaportal", function(inst, portal)
		inst.multiplayerportal = portal
	end)
	
	inst.spawners = {}
	inst:ListenForEvent("ms_register_lavaarenaspawner", function(inst, data)
		inst.spawners[data.id] = data.spawner
	end)
	
	local _onstoptrackingold = inst.components.lavaarenamobtracker._onremovemob
	inst.components.lavaarenamobtracker._onremovemob = function(mob)
		_onstoptrackingold(mob)
		inst:PushEvent("ms_lavaarena_mobgone")
	end

	function inst:SpawnDoyDoy()
		if #TheSim:FindEntities(0,0,0, 1000, {"doydoyed"}) > 0 then print("ERROR: Tried to create 2 CarlZalph(c) Jr! "..CalledFrom()) end
		
		local specs = TheSim:FindEntities(0,0,0, 1000, {"doydoyable"}) 
		if #specs > 0 then 
			local Detroit = specs[math.random(#specs)] 
			Detroit:BecomeDoyDoy()
			Detroit.Transform:SetScale(1.15, 1.15, 1.15)
		end 
	end
    
    inst:ListenForEvent("ms_playerleft", function(inst, player)
        if #AllPlayers == 0 then
            WorldResetFromSim()
        end
    end)
end

return lavaarena