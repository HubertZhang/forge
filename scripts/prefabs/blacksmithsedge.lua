local assets = {
    Asset("ANIM", "anim/sword_buster.zip"),
    Asset("ANIM", "anim/swap_sword_buster.zip"),
}

local prefabs = {
    "weaponsparks_fx",
    "forgedebuff_fx",
    "superjump_fx",
    "reticulearc",
    "reticulearcping",
}

local function ReticuleTargetFn()
    return Vector3(ThePlayer.entity:LocalToWorldSpace(6.5, 0, 0))
end

local function ReticuleMouseTargetFn(inst, mousepos)
    if mousepos ~= nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        local dx = mousepos.x - x
        local dz = mousepos.z - z
        local l = dx * dx + dz * dz
        if l <= 0 then
            return inst.components.reticule.targetpos
        end
        l = 6.5 / math.sqrt(l)
        return Vector3(x + dx * l, 0, z + dz * l)
    end
end

local function ReticuleUpdatePositionFn(inst, pos, reticule, ease, smoothing, dt)
    local x, y, z = inst.Transform:GetWorldPosition()
    reticule.Transform:SetPosition(x, 0, z)
    local rot = -math.atan2(pos.z - z, pos.x - x) / DEGREES
    if ease and dt ~= nil then
        local rot0 = reticule.Transform:GetRotation()
        local drot = rot - rot0
        rot = Lerp((drot > 180 and rot0 + 360) or (drot < -180 and rot0 - 360) or rot0, rot, dt * smoothing)
    end
    reticule.Transform:SetRotation(rot)
end

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_sword_buster", "swap_sword_buster")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function OnAttack(inst, attacker, target)
    SpawnPrefab("weaponsparks_fx"):SetPosition(attacker, target)
    if target and target.components.armorbreak_debuff and not (attacker and attacker.components.passive_shock and attacker.components.passive_shock.shock) then
        target.components.armorbreak_debuff:ApplyDebuff()
    end
	if target and target.components.combat and attacker then
		--blacksmithsedge ALWAYS draws aggro on hit.
		target.components.combat:SetTarget(attacker)
	end
end

local function Parry(inst, caster, pos)
    caster:PushEvent("combat_parry", {
        direction = inst:GetAngleToPoint(pos),
        duration = inst.components.parryweapon.duration,
        weapon = inst
    })
end

local function OnParry(inst, caster)
    inst.components.rechargeable:StartRecharge()
end

local function OnHelmSplit(inst)
    SpawnPrefab("superjump_fx"):SetTarget(inst)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    
    inst.nameoverride = "lavaarena_heavyblade"

    inst.AnimState:SetBank("sword_buster")
    inst.AnimState:SetBuild("sword_buster")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("sharp")

    --parryweapon (from parryweapon component) added to pristine state for optimization
    inst:AddTag("parryweapon")

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting:SetAlwaysValid(true)
    inst.components.aoetargeting.reticule.reticuleprefab = "reticulearc"
    inst.components.aoetargeting.reticule.pingprefab = "reticulearcping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.mousetargetfn = ReticuleMouseTargetFn
    inst.components.aoetargeting.reticule.updatepositionfn = ReticuleUpdatePositionFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    
    inst:AddComponent("aoespell")
    inst.components.aoespell:SetAOESpell(Parry)
    
    inst:AddComponent("rechargeable")
    inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.BLACKSMITHSEDGE.COOLDOWN)
    
    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.BLACKSMITHSEDGE.DAMAGE)
    inst.components.weapon:SetOnAttack(OnAttack)
    inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)

    inst:AddComponent("parryweapon")
    inst.components.parryweapon.duration = TUNING.FORGE.BLACKSMITHSEDGE.PARRY_DURATION
    inst.components.parryweapon:SetOnParryStartFn(OnParry)
    
    inst:AddComponent("itemtype")
    inst.components.itemtype:SetType("melees")
    
    inst:AddComponent("inspectable")
    
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "lavaarena_heavyblade"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("helmsplitter")
    inst.components.helmsplitter:SetOnHelmSplitFn(OnHelmSplit)
    inst.components.helmsplitter.damage = TUNING.FORGE.BLACKSMITHSEDGE.HELMSPLIT_DAMAGE

    return inst
end


return ForgePrefab("blacksmithsedge", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "lavaarena_heavyblade.tex", nil, TUNING.FORGE.BLACKSMITHSEDGE, STRINGS.FORGED_FORGE.WEAPONS.BLACKSMITHSEDGE.ABILITIES, "swap_sword_buster", "common_hand")