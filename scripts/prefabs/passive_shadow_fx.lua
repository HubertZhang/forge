--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    Asset("ANIM", "anim/lavaarena_shadow_lunge.zip"),
    Asset("ANIM", "anim/waxwell_shadow_mod.zip"),
    Asset("ANIM", "anim/swap_nightmaresword_shadow.zip"),
}

local prefabs =
{
    "statue_transition_2",
    "shadowstrike_slash_fx",
    "shadowstrike_slash2_fx",
    "weaponsparks",
}

local function StartShadows(self)
	SpawnPrefab("statue_transition_2").Transform:SetPosition(self.Transform:GetWorldPosition())
	self.AnimState:PlayAnimation("lunge_pre")
	self.AnimState:PushAnimation("lunge_loop")
	self.AnimState:PushAnimation("lunge_pst")
	
	self:DoTaskInTime(12*FRAMES, function(inst) inst.Physics:SetMotorVel(TUNING.FORGE.SHADOWS.LUNGE_SPEED, 0, 0) end )
	self:DoTaskInTime(15*FRAMES, function(inst)
		inst:Attack()
	end )
	--self:DoTaskInTime(20*FRAMES, function(inst) inst.Physics:SetMotorVelOverride(5, 0, 0) end )
	self:DoTaskInTime(22*FRAMES, function(inst) inst.Physics:ClearMotorVelOverride() end)
	self:DoTaskInTime(35*FRAMES, function(inst) inst:Remove() end)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddPhysics()
    inst.entity:AddNetwork()

    inst.Transform:SetFourFaced(inst)

    inst.AnimState:SetBank("lavaarena_shadow_lunge")
    inst.AnimState:SetBuild("waxwell_shadow_mod")
    inst.AnimState:AddOverrideBuild("lavaarena_shadow_lunge")
    inst.AnimState:SetMultColour(0, 0, 0, .5)
    inst.AnimState:OverrideSymbol("swap_object", "swap_nightmaresword_shadow", "swap_nightmaresword_shadow")
    inst.AnimState:Hide("HAT")
    inst.AnimState:Hide("HAIR_HAT")

    inst.Physics:SetMass(1)
    inst.Physics:SetFriction(0)
    inst.Physics:SetDamping(5)
    inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.GROUND)
    inst.Physics:SetCapsule(.5, 1)

    inst:AddTag("scarytoprey")
    inst:AddTag("NOBLOCK")
	
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(TUNING.FORGE.SHADOWS.DAMAGE)
	
	inst.SetPlayer = function(self, player)
		inst.player = player
		if player and player.components.combat then player.components.combat:CopyBuffsTo(inst) end
	end
	
    inst.SetTarget = function(self, target)
		inst.target = target
		inst.target_pos = Point(target.Transform:GetWorldPosition())
		inst:FacePoint(inst.target_pos)
	end
	
	inst.SetPosition = function(self, target_pos, offset)
		inst.offset = offset
		self.Transform:SetPosition(target_pos.x + offset.x, target_pos.y + offset.y, target_pos.z + offset.z)
		StartShadows(self)
	end
	
	inst.Attack = function(self)
		local function RotateFX(fx)
			fx.Transform:SetPosition(self.target_pos.x, self.target_pos.y, self.target_pos.z)
			fx.Transform:SetRotation(self.Transform:GetRotation())
		end
		local rand = math.random(1,2)
		if rand == 1 then
			RotateFX(SpawnPrefab("shadowstrike_slash_fx"))
		else
			RotateFX(SpawnPrefab("shadowstrike_slash2_fx"))
		end
		--RotateFX(SpawnPrefab("weaponsparks"))
		local spark_offset_mult = 0.25
		--SpawnPrefab("weaponsparks").Transform:SetPosition(self.target_pos.x + self.offset.x * spark_offset_mult, self.target_pos.y + self.offset.y * spark_offset_mult, self.target_pos.z + self.offset.z * spark_offset_mult)
		SpawnPrefab("weaponsparks_fx"):SetThrusting(self.player, self.target, Vector3(self.offset.x * spark_offset_mult, self.offset.y * spark_offset_mult, self.offset.z * spark_offset_mult))
		--self.player.components.combat:DoAttack(self.target, nil, nil, nil, 1, TUNING.FORGE.SHADOWS.DAMAGE, false, true)
		self.components.combat:DoAttack(self.target, nil, nil, "strong")--"shadow")
	end
	
	inst:ListenForEvent("onhitother", function(inst, data)
		inst.player:SpawnChild("damage_number"):UpdateDamageNumbers(inst.player, data.target, math.floor(data.damageresolved + 0.5), nil, data.stimuli, true)
	end)
	
    return inst
end

return Prefab("passive_shadow_fx", fn, assets, prefabs)