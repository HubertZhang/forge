--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/spear_lance.zip"),
    Asset("ANIM", "anim/swap_spear_lance.zip"),
}

local prefabs = {
    "reticuleaoesmall",
    "reticuleaoesmallping",
    "reticuleaoesmallhostiletarget",
    "weaponsparks",
    "weaponsparks_thrusting",
    "forgespear_fx",
    "superjump_fx",
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_spear_lance", "swap_spear_lance")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    local player = ThePlayer
    local ground = TheWorld.Map
    local pos = Vector3()
    --Cast range is 8, leave room for error
    --2 is the aoe range
    for r = 5, 0, -.25 do
        pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
        if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
            return pos
        end
    end
    return pos
end

local function SkyLunge(inst, caster, pos)
	caster:PushEvent("combat_superjump", {
		targetpos = pos,
		weapon = inst
	})
end

local function OnLeap(inst)
	SpawnPrefab("superjump_fx"):SetTarget(inst)
	inst.components.rechargeable:StartRecharge()
end

local function OnAttack(inst, attacker, target)
	if not inst.components.weapon.isaltattacking then
		SpawnPrefab("weaponsparks_fx"):SetPosition(attacker, target)
	else
		SpawnPrefab("forgespear_fx"):SetTarget(target)
	end
end

local function fn()
    local inst = CreateEntity()
	
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
	
    MakeInventoryPhysics(inst)
	
	inst.nameoverride = "spear_lance"
	
    inst.AnimState:SetBank("spear_lance")
    inst.AnimState:SetBuild("spear_lance")
    inst.AnimState:PlayAnimation("idle")
	
    inst:AddTag("sharp")
    inst:AddTag("pointy")
    inst:AddTag("superjump")
	
    --aoeweapon_leap (from aoeweapon_leap component) added to pristine state for optimization
    inst:AddTag("aoeweapon_leap")
	
    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")
	
    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting:SetRange(TUNING.FORGE.SPIRALSPEAR.ALT_RANGE)
    inst.components.aoetargeting.reticule.reticuleprefab = "reticuleaoesmall"
    inst.components.aoetargeting.reticule.pingprefab = "reticuleaoesmallping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true
	
    inst.entity:SetPristine()
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	-- Fox: Initializing reticule_spawner before aoespell
	inst:AddComponent("reticule_spawner")
	inst.components.reticule_spawner:Setup(unpack(TUNING.FORGE.RET_DATA.spiralspear))
	
	inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(SkyLunge)
	
	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.SPIRALSPEAR.COOLDOWN)
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.SPIRALSPEAR.DAMAGE)
	inst.components.weapon:SetOnAttack(OnAttack)
	inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)
	--inst.components.weapon:SetAltDamage(TUNING.FORGE.SPIRALSPEAR.ALT_DAMAGE)
	inst.components.weapon:SetAltAttack(TUNING.FORGE.SPIRALSPEAR.ALT_DAMAGE, TUNING.FORGE.SPIRALSPEAR.ALT_RADIUS, nil, DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("aoeweapon_leap")
	--inst.components.aoeweapon_leap:SetDamage(TUNING.FORGE.SPIRALSPEAR.ALT_DAMAGE)
	--inst.components.aoeweapon_leap:SetRange(TUNING.FORGE.SPIRALSPEAR.ALT_RADIUS)
	inst.components.aoeweapon_leap:SetStimuli("explosive")
	inst.components.aoeweapon_leap:SetOnLeapFn(OnLeap)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("melees")
	
	inst:AddComponent("inspectable")
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "spear_lance"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

	inst:AddComponent("multithruster")
	
	--inst.damage_type = TUNING.FORGE.SPIRALSPEAR.TYPE
	
    return inst
end

return ForgePrefab("spiralspear", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "spear_lance.tex", nil, TUNING.FORGE.SPIRALSPEAR, STRINGS.FORGED_FORGE.WEAPONS.SPIRALSPEAR.ABILITIES, "swap_spear_lance", "common_hand")