--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local data = {
	feathercrown = {
		prefab = "featheredwreath",
		masterfn = function(inst)
			inst.components.itemtype:SetType("speed_hats")
			inst.components.equippable.walkspeedmult = TUNING.FORGE.FEATHEREDWREATH.SPEEDMULT
		end
	},
	
	lightdamager = {
		prefab = "barbedhelm",
		hidesymbols = true,
		masterfn = function(inst)
			inst.components.itemtype:SetType("damage_hats")
			inst.components.equippable:AddDamageBuff("barbedhelm", {buff = TUNING.FORGE.BARBEDHELM.BONUSDAMAGE, damagetype = DAMAGETYPES.PHYSICAL})
		end
	},
	
	recharger = {
		prefab = "crystaltiara",
		masterfn = function(inst)
			inst.components.itemtype:SetType("cooldown_hats")
			inst.components.equippable:AddUniqueBuff({
				{name = "cooldown", val = TUNING.FORGE.CRYSTALTIARA.BONUS_COOLDOWNRATE, type = "add"}
			})
		end
	},
	
	healingflower = {
		prefab = "flowerheadband",
		masterfn = function(inst)
			inst.components.itemtype:SetType("health_hats")
			inst.components.equippable:AddUniqueBuff({
				{name = "heal_recieved", val = TUNING.FORGE.FLOWERHEADBAND.HEALINGRECEIVEDMULT, type = "mult"}
			})
		end
	},
	
	tiaraflowerpetals = {
		prefab = "wovengarland",
		masterfn = function(inst)
			inst.components.itemtype:SetType("healing_hats")
			inst.components.equippable:AddUniqueBuff({
				{name = "heal_dealt", val = TUNING.FORGE.WOVENGARLAND.HEALINGDEALTMULT, type = "mult"}
			})
		end
	},
	
	strongdamager = {
		prefab = "noxhelm",
		hidesymbols = true,
		masterfn = function(inst)
			inst.components.itemtype:SetType("damage_hats")
			inst.components.equippable:AddDamageBuff("noxhelm", {buff = TUNING.FORGE.NOXHELM.BONUSDAMAGE, damagetype = DAMAGETYPES.PHYSICAL})
		end
	},
	
	crowndamager = {
		prefab = "resplendentnoxhelm",
		hidesymbols = true,
		masterfn = function(inst)
			inst.components.itemtype:SetType({"damage_hats", "speed_hats", "cooldown_hats"})
			inst.components.equippable.walkspeedmult = TUNING.FORGE.RESPLENDENTNOXHELM.SPEEDMULT
			inst.components.equippable:AddDamageBuff("resplendentnoxhelm", {buff = TUNING.FORGE.RESPLENDENTNOXHELM.BONUSDAMAGE, damagetype = DAMAGETYPES.PHYSICAL})
			inst.components.equippable:AddUniqueBuff({
				{name = "cooldown", val = TUNING.FORGE.RESPLENDENTNOXHELM.BONUS_COOLDOWNRATE, type = "add"}
			})
		end
	},
	
	healinggarland = {
		prefab = "blossomedwreath",
		masterfn = function(inst)
			inst.components.itemtype:SetType({"health_hats", "speed_hats", "cooldown_hats"})
			inst.HealOwner = function(inst, owner)
				inst.heal_task = inst:DoTaskInTime(TUNING.FORGE.BLOSSOMEDWREATH.REGEN.period, function(inst)
					if inst.components.equippable:IsEquipped() and not owner.components.health:IsDead() then
						if owner.components.health:GetPercent() < TUNING.FORGE.BLOSSOMEDWREATH.REGEN.limit then
							if not inst:HasTag("regen") then inst:AddTag("regen") end
							owner.components.health:DoDelta(TUNING.FORGE.BLOSSOMEDWREATH.REGEN.delta, true, "regen", true)
						else
							inst:RemoveTag("regen")
						end 
						inst:HealOwner(owner)
					else
						inst:RemoveTag("regen")
					end
				end)
			end
            inst:ListenForEvent("equipped", function(inst, data)
                inst:HealOwner(data.owner)
                inst._deathfunction = function(owner)
                    if inst.heal_task then
                        inst.heal_task:Cancel()
                        inst.heal_task = nil
                    end
                    inst:RemoveTag("regen")
                    owner.components.health:Kill()
                end
                inst._revivefunction = function(owner)
                    inst:HealOwner(owner)
                end
                data.owner:ListenForEvent("ms_respawnedfromghost", inst._revivefunction)
                data.owner:ListenForEvent("death", inst._deathfunction)
            end)
            inst:ListenForEvent("unequipped", function(inst, data)
                if inst.heal_task then
                    inst.heal_task:Cancel()
                    inst.heal_task = nil
                end
                inst:RemoveTag("regen")
                data.owner:RemoveEventCallback("ms_respawnedfromghost", inst._revivefunction)
                inst._revivefunction = nil
                data.owner:RemoveEventCallback("death", inst._deathfunction)
                inst._deathfunction = nil
            end)
			inst.components.equippable.walkspeedmult = TUNING.FORGE.BLOSSOMEDWREATH.SPEEDMULT
			inst.components.equippable:AddUniqueBuff({
				{name = "cooldown", val = TUNING.FORGE.BLOSSOMEDWREATH.BONUS_COOLDOWNRATE, type = "add"}
			})
		end
	},
	
	eyecirclet = {
		prefab = "clairvoyantcrown",
		masterfn = function(inst)
			inst.components.itemtype:SetType({"magic_hats", "speed_hats", "cooldown_hats"})
			inst.components.equippable.walkspeedmult = TUNING.FORGE.CLAIRVOYANTCROWN.SPEEDMULT
			inst.components.equippable:AddDamageBuff("clairvoyantcrown", {buff = TUNING.FORGE.CLAIRVOYANTCROWN.MAGE_BONUSDAMAGE, damagetype = DAMAGETYPES.MAGIC})
			inst.components.equippable:AddUniqueBuff({
				{name = "cooldown", val = TUNING.FORGE.CLAIRVOYANTCROWN.BONUS_COOLDOWNRATE, type = "add"}
			})
		end
	}
}

local function MakeHat(name)
    local build = "hat_"..name
    local symbol = name.."hat"

    local assets = {
        Asset("ANIM", "anim/"..build..".zip"),
    }
	
	local function onequip(inst, owner)
        owner.AnimState:OverrideSymbol("swap_hat", build, "swap_hat")
		owner.AnimState:Show("HAT")
		if data[name].hidesymbols then
			owner.AnimState:Show("HAIR_HAT")
			owner.AnimState:Show("HEAD_HAT")
			owner.AnimState:Hide("HAIR_NOHAT")
			owner.AnimState:Hide("HAIR")
			owner.AnimState:Hide("HEAD")
		end
    end

    local function onunequip(inst, owner)
        owner.AnimState:ClearOverrideSymbol("swap_hat")
		owner.AnimState:Hide("HAT")
		if data[name].hidesymbols then
			owner.AnimState:Hide("HAIR_HAT")
			owner.AnimState:Hide("HEAD_HAT")
			owner.AnimState:Show("HAIR_NOHAT")
			owner.AnimState:Show("HAIR")
			owner.AnimState:Show("HEAD")
		end
    end

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)
		
		inst.nameoverride = "lavaarena_"..name.."hat"

        inst.AnimState:SetBank(symbol)
        inst.AnimState:SetBuild(build)
        inst.AnimState:PlayAnimation("anim")

        inst:AddTag("hat")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.imagename = "lavaarena_"..name.."hat"
		
		inst:AddComponent("itemtype")
		inst.components.itemtype:SetType("hats")
		
        inst:AddComponent("inspectable")

        inst:AddComponent("equippable")
        inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
        inst.components.equippable:SetOnEquip(onequip)
        inst.components.equippable:SetOnUnequip(onunequip)
		
		data[name].masterfn(inst)

        return inst
    end

    return ForgePrefab(data[name].prefab, fn, assets, nil, nil, "ARMOR", false, "images/inventoryimages.xml", "lavaarena_" .. name .. "hat" .. ".tex", nil, TUNING.FORGE[string.upper(data[name].prefab)], nil, build, data[name].hidesymbols and "common_head1" or "common_head2")
end

local hats = {}
for k,v in pairs(data) do
	table.insert(hats, MakeHat(k))
end
return unpack(hats)