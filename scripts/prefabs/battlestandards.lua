--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local function CreatePulse(target)
    local inst = CreateEntity()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_battlestandard")
    inst.AnimState:SetBuild("lavaarena_battlestandard")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
    end
	
    inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:SetParent(target.entity)

    return inst
end

local function ApplyPulse(ent, params)
    if not (ent.components.health ~= nil and ent.components.health:IsDead()) then
        local fx = CreatePulse(ent)
        fx:ListenForEvent("animover", fx.Remove)
        fx.AnimState:PlayAnimation(params or "heal_fx")
		fx.Transform:SetScale(1, 1, 1)
    end
end

local function OnPulse(inst)
    if inst.fxanim ~= nil and inst.fx ~= nil then
        inst.fx.AnimState:PlayAnimation(inst.fxanim)
    end
	
	local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 255, nil, {"INLIMBO", "battlestandard", "playerghost"}, {"LA_mob"})
        for i, v in ipairs(ents) do
        ApplyPulse(v, inst.fxanim)	
		end
end

--Everything below this is my work.
--\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
--\\\\Copyright 2018, Leonardo Coxington, All rights reserved.\\\\
--\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

local AURA_INCLUDE_TAGS = { "LA_mob" }
local AURA_INCLUDE_TAGS_NO_PVP = { "LA_mob" } --leftover stuff from PP

local function GetTags(inst)
	if TheNet:GetPVPEnabled() then
		return AURA_INCLUDE_TAGS
	else
		return AURA_INCLUDE_TAGS_NO_PVP
	end
end

local function DoAttackBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.atkbuffed and target.components.debuffable then
		target.components.debuffable:AddDebuff("damager_buff", "damager_buff")	
	end
end

local function DoAttackAura(inst)
	--OnPulse(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 255, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, GetTags() )
    for i, v in ipairs(ents) do
        DoAttackBuff(v)
    end
end

local function DoDefenseBuff(target)
	--ToDo: Leo - Shorten the fuck out of this
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.defbuffed and not target:HasTag("structure") and not (target.sg:HasStateTag("hiding") or target.sg:HasStateTag("shielding") or target.sg:HasStateTag("nobuff")) and target.components.debuffable then
		target.components.debuffable:AddDebuff("shield_buff", "shield_buff")		
	end
end

local function DoDefenseAura(inst)
	--OnPulse(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 255, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, GetTags() )
    for i, v in ipairs(ents) do
        DoDefenseBuff(v)	
    end
end

local function DoHealBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.healbuffed and target.components.debuffable then
		--ApplyPulse(target, "heal_fx")
		target.components.debuffable:AddDebuff("healer_buff", "healer_buff")	
		--[[
		target.healbuffed = true
		target.components.health:StartRegen(TUNING.FORGE.BATTLESTANDARD.HEALTH_PER_TICK, TUNING.FORGE.BATTLESTANDARD.HEALTH_TICK)
		target:DoTaskInTime(TUNING.FORGE.BATTLESTANDARD.BUFF_WEAROFF_TIME, function(inst)
		inst.components.health:StopRegen()
		inst.healbuffed = nil
		end)]]

	end
end

local function DoHealAura(inst)
	--OnPulse(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 255, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, GetTags() )
    for i, v in ipairs(ents) do
        if v:IsValid() and v.components.health and not v.components.health:IsDead() and not v.buffimmune then
			DoHealBuff(v)	
		end
    end
end

local function DoSpeedBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.speedbuffed and target.components.debuffable then
		target.components.debuffable:AddDebuff("speed_buff", "speed_buff")	
	end
end

local function DoSpeedAura(inst)
	--OnPulse(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 255, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, GetTags() )
    for i, v in ipairs(ents) do
        DoSpeedBuff(v)
    end
end

local function OnKilled(inst)
	if inst.DoAura ~= nil then
	inst.DoAura:Cancel()
	end
    inst.AnimState:PlayAnimation("break")
	inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/banner_break")
	--inst.components.lootdropper:DropLoot(inst:GetPosition())
	inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, function(inst) inst:Remove() end)
end

local function OnHit(inst)
	if not inst.components.health:IsDead() then
    inst.AnimState:PlayAnimation("hit")
    inst.AnimState:PushAnimation("idle", true)
	inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_wood_armour_sharp")
	end
end

local function PlayPulse(inst) --to simulate buffing on the visual side.
	if not inst.components.health:IsDead() then
    inst.AnimState:PlayAnimation("active")
    inst.AnimState:PushAnimation("idle", true)
	end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle", true)
    inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/banner")
end

--TODO: Make it easier to make new battlestandards via addons
local function MakeBattleStandard(name, build_swap, debuffprefab, fx_anim, aura)
    local assets =
    {
        Asset("ANIM", "anim/lavaarena_battlestandard.zip"),
    }
    if build_swap ~= nil then
        table.insert(assets, Asset("ANIM", "anim/"..build_swap..".zip"))
    end

    local prefabs =
    {
        --debuffprefab,
    }

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

        inst.AnimState:SetBank("lavaarena_battlestandard")
        inst.AnimState:SetBuild("lavaarena_battlestandard")
        if build_swap ~= nil then
            inst.AnimState:AddOverrideBuild(build_swap)
        end
        inst.AnimState:PlayAnimation("place")
		inst.AnimState:PushAnimation("idle", true)
		inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/banner")
		
		--MakeCharacterPhysics(inst, 5, 0.5)

        inst:AddTag("battlestandard")
		inst:AddTag("structure")
        inst:AddTag("LA_mob")

        inst.fxanim = fx_anim

        --Dedicated server does not need local fx
        --if not TheNet:IsDedicated() then
           	--inst.fx = CreatePulse(inst)
        --end
        --inst.pulse = net_event(inst.GUID, "lavaarena_battlestandard_damager.pulse")

        inst.nameoverride = "lavaarena_battlestandard"

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
			--inst:ListenForEvent("lavaarena_battlestandard_damager.pulse", OnPulse)
            return inst
        end

        inst.debuffprefab = debuffprefab
        --inst.OnPulse = OnPulse
		
		if aura ~= nil then
			if aura == "Attack" then
				inst.DoAura = inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.PULSE_TICK, DoAttackAura)
			elseif aura == "Defense" then
				inst.DoAura = inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.PULSE_TICK, DoDefenseAura)
			elseif aura == "Heal" then
				inst.DoAura = inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.PULSE_TICK, DoHealAura)	
			elseif aura == "Speed" then
				inst.DoAura = inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.PULSE_TICK, DoSpeedAura)
			end		
		end
		
		inst:AddComponent("health")
        inst.components.health:SetMaxHealth(TUNING.FORGE.BATTLESTANDARD.HEALTH)
		
		inst:AddComponent("combat")
        inst.components.combat:SetOnHit(OnHit)
        inst:ListenForEvent("death", OnKilled)
		
		inst:AddComponent("inspectable")
		
		--inst:AddComponent("lootdropper")
		
		inst:ListenForEvent("onbuilt", onbuilt)
		--inst:ListenForEvent("onbuffed", PlayPulse)
		
		inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.BUFF_WEAROFF_TIME, PlayPulse)

        return inst
    end

    return ForgePrefab(name, fn, assets, prefabs, nil, "BUFFS", false, "images/forged_forge.xml", name .. ".tex", nil, TUNING.FORGE.BATTLESTANDARD)
end

local function OnAttached(inst, target)
    inst.entity:SetParent(target.entity)
    inst:ListenForEvent("death", function()
        inst.components.debuff:Stop()
    end, target)
	if target ~= nil then
		if inst.type and inst.type == "atk" and target.components.combat then
			target.components.combat:AddDamageBuff("atk_banner", 1 + (TUNING.FORGE.BATTLESTANDARD.ATK_BUFF * Forge_options.btlstandard_efficiency))
			inst.AnimState:PlayAnimation("attack_fx3")
			target.atkbuffed = true
		elseif inst.type and inst.type == "def" and target.components.combat then
			local modifier = Forge_options.btlstandard_efficiency
			if modifier and modifier == 0.5 then modifier = 0.6 end --Because 0.5 / 0.5 = 1
			target.components.combat:AddDamageBuff("def_banner", TUNING.FORGE.BATTLESTANDARD.DEF_BUFF / modifier, true)
			target.defbuffed = true
			inst.AnimState:PlayAnimation("defend_fx")
		elseif inst.type and inst.type == "healer" and target.components.health then
			target.healbuffed = true
			target.components.health:StartRegen(TUNING.FORGE.BATTLESTANDARD.HEALTH_PER_TICK * Forge_options.btlstandard_efficiency, TUNING.FORGE.BATTLESTANDARD.HEALTH_TICK)
			inst.AnimState:PlayAnimation("heal_fx")
		elseif inst.type and inst.type == "speed" and target.components.locomotor then
			target.speedbuffed = true
			target.components.locomotor:SetExternalSpeedMultiplier(inst, "speed_banner", TUNING.FORGE.BATTLESTANDARD.SPEED_BUFF * Forge_options.btlstandard_efficiency)
		end
	end
end

local function OnTimerDone(inst, data)
    if data.name == inst.type.."buffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)
    inst.components.timer:StopTimer(inst.type.."buffover")
    inst.components.timer:StartTimer(inst.type.."buffover", inst.duration)
end

local function OnDetached(inst, target)
	if target ~= nil then
		if inst.type and inst.type == "def" and target.defbuffed then
			if target.components.combat and not target.components.health:IsDead() then
				target.components.combat:RemoveDamageBuff("def_banner", true)
				target.defbuffed = nil
			end
		elseif inst.type and inst.type == "atk" and target.atkbuffed then
			if target.components.combat then
				target.components.combat:RemoveDamageBuff("atk_banner")
				target.atkbuffed = nil
			end
		elseif inst.type and inst.type == "healer" and target.healbuffed then
			if target.components.health then
				target.components.health:StopRegen()
				target.healbuffed = nil
			end
		elseif inst.type and inst.type == "speed" then
			if target.components.locomotor and target.speedbuffed then
				target.components.locomotor:RemoveExternalSpeedMultiplier(target, "speed_banner")
				target.speedbuffed = nil
			end
		end
	end
	inst:Remove()
end

local function MakeBuff(name, type)
	
	local function fn()
		local inst = CreateEntity()
		local trans = inst.entity:AddTransform()
		inst.entity:AddNetwork()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()

        inst.AnimState:SetBank("lavaarena_battlestandard")
        inst.AnimState:SetBuild("lavaarena_battlestandard")
		inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
		inst.AnimState:SetLayer(LAYER_BACKGROUND)
		inst.AnimState:SetSortOrder(3)
		
		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end
		
		inst:AddComponent("debuff")
		inst.components.debuff:SetAttachedFn(OnAttached)
		inst.components.debuff:SetDetachedFn(OnDetached)
		inst.components.debuff:SetExtendedFn(OnExtended)
		--inst.components.debuff.keepondespawn = true
	
		inst.duration = TUNING.FORGE.BATTLESTANDARD.BUFF_WEAROFF_TIME
		inst.type = type or nil

		inst:AddComponent("timer")
		inst:DoTaskInTime(0, function() -- in case we want to change duration
			inst.components.timer:StartTimer(inst.type.."buffover", inst.duration)
		end)
		inst:ListenForEvent("timerdone", OnTimerDone)
		
	
		return inst
	end

	return Prefab(name, fn)
end

------------------------------------------------------------------------------

return MakeBattleStandard("battlestandard_damager", "lavaarena_battlestandard_attack_build", "damager_buff", "attack_fx3", "Attack"),
    MakeBattleStandard("battlestandard_shield", nil, "shield_buff", "defend_fx", "Defense"),
    MakeBattleStandard("battlestandard_heal", "lavaarena_battlestandard_heal_build", "healer_buff", "heal_fx", "Heal"),
	MakeBattleStandard("battlestandard_speed", "lavaarena_battlestandard_attack_build", "speed_buff", "attack_fx3", "Speed"),
	MakeBuff("shield_buff", "def"),
	MakeBuff("damager_buff", "atk"),
	MakeBuff("healer_buff", "healer"),
	MakeBuff("speed_buff", "speed")
