--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local FRAME = 1/1000

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    --[[Non-networked entity]]

    inst:AddTag("CLASSIFIED")
	
	inst.blooms = {}
	inst.caster = nil
	
	inst:AddComponent("heal_aura")
	
	inst:DoTaskInTime(TUNING.FORGE.LIVINGSTAFF.DURATION, function(inst)
		for _,v in pairs(inst.blooms) do
			if v.Kill ~= nil then v:Kill(true) end
		end
		inst:Remove()
	end)

	function inst:SpawnBlooms()
		local bloomprefab = "healingcircle_bloom"
		for i = 1,15 do
			local pt = inst:GetPosition()
			if i == 1 then
				inst:DoTaskInTime(math.random(), function()
				local bloom = SpawnPrefab(bloomprefab)
				bloom.Transform:SetPosition(pt:Get())
				bloom.buffed = inst.buffed
				table.insert(inst.blooms, bloom)
				end)
			elseif i >= 2 and i < 7 then
				local theta = (i-1)/5 * 2 * PI
				local radius = inst.components.heal_aura.range/2
				local offset = FindWalkableOffset(pt, theta, radius, 2, true, true)
				if offset ~= nil then
					offset.x = offset.x + pt.x
					offset.z = offset.z + pt.z
					inst:DoTaskInTime(math.random(), function()
					local bloom = SpawnPrefab(bloomprefab)
					bloom.Transform:SetPosition(offset.x, 0, offset.z)
					bloom.buffed = inst.buffed
					table.insert(inst.blooms, bloom)
					end)
				end
			elseif i >= 7 then
				local theta = (i-5)/9 * 2 * PI
				local radius = inst.components.heal_aura.range
				local offset = FindWalkableOffset(pt, theta, radius, 2, true, true)
				if offset ~= nil then
					offset.x = offset.x + pt.x
					offset.z = offset.z + pt.z
					inst:DoTaskInTime(math.random(), function()
					local bloom = SpawnPrefab(bloomprefab)
					bloom.Transform:SetPosition(offset.x, 0, offset.z)
					bloom.buffed = inst.buffed
					table.insert(inst.blooms, bloom)
					end)
				end
			end
		end
	end
	
	function inst:SpawnCenter()
		local pt = inst:GetPosition()
		local center = SpawnPrefab("healingcircle_center")
		center.Transform:SetPosition(pt.x, 0, pt.z)
		center:DoTaskInTime(TUNING.FORGE.LIVINGSTAFF.COOLDOWN/2, inst.Remove)		
	end
	
	inst:DoTaskInTime(0, inst.SpawnCenter)
	inst:DoTaskInTime(0, inst.SpawnBlooms)
	
    return inst
end

local function bloom_fn()
	local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

	inst.AnimState:SetBuild("lavaarena_heal_flowers_fx")
	inst.AnimState:SetBank("lavaarena_heal_flowers")
	--inst.AnimState:HideSymbol("drop")
	
	if not TheWorld.ismastersim then
		return inst
	end 
	
	inst.variation = tostring(math.random(1, 6))
	
	inst:AddComponent("colourfader")
	
	local function PlayFlowerSound()
		inst.SoundEmitter:PlaySound("dontstarve/wilson/pickup_reeds", "flower_sound")
		inst.SoundEmitter:SetVolume("flower_sound", .25)
	end
	
	function inst:Start()
		PlayFlowerSound()
		
		inst.AnimState:PlayAnimation("in_"..inst.variation)
		inst.AnimState:PushAnimation("idle_"..inst.variation)
		
		if inst.buffed then
			local scale = 1 + (math.random(unpack(TUNING.FORGE.LIVINGSTAFF.SCALE_RNG)) + math.random())/100
			inst.Transform:SetScale(scale, scale, scale)
			--inst.AnimState:ShowSymbol("drop")
		end
		
		inst.components.colourfader:StartFade({0, 0.3, 0.1}, 650 * FRAME, function(inst)
			inst:DoTaskInTime(350*FRAME, function() inst.components.colourfader:StartFade({0, 0, 0}, 457 * FRAME) end)
		end)
	end
	
	function inst:Kill(withdelay)
		local delay = withdelay and math.random() or 0
		inst:DoTaskInTime(delay, function(inst)
			PlayFlowerSound()
			
			inst.AnimState:PushAnimation("out_"..inst.variation, false)
			inst:ListenForEvent("animover", inst.Remove)
		end)
	end

	inst.OnSave = inst.Remove
	
	inst:DoTaskInTime(0, inst.Start)
	
	return inst
end

local function center_fn()
	local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

	inst.AnimState:SetBank("lavaarena_heal_flowers")
	inst.AnimState:SetBuild("lavaarena_heal_flowers_fx")
	
	inst.AnimState:SetMultColour(0,0,0,0)
	--inst:Hide()
	
	inst:AddTag("healingcircle")
	
	if not TheWorld.ismastersim then
		return inst
	end 
	
	inst.variation = tostring(math.random(1, 6))
	inst.AnimState:PlayAnimation("in_"..inst.variation)
	inst.AnimState:PushAnimation("idle_"..inst.variation)
	
	inst:DoTaskInTime(12, inst.Remove)
	
	return inst
end

return Prefab("healingcircle", fn, nil, prefabs),
	Prefab("healingcircle_bloom", bloom_fn, nil, nil),
	Prefab("healingcircle_center", center_fn, nil, nil)