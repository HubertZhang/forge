--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/lavaarena_firestaff_meteor.zip"),
}

local assets_splash = {
    Asset("ANIM", "anim/lavaarena_fire_fx.zip"),
}

local prefabs = {
    "infernalstaff_meteor_splash",
    "infernalstaff_meteor_splashhit",
}

local prefabs_splash = {
    "infernalstaff_meteor_splashbase",
}

local range = TUNING.FORGE.INFERNALSTAFF.ALT_RADIUS

--[[local function CalcDamage(centerpos, target)
	local perc = (range - distsq(centerpos, target:GetPosition()))/range
	local max_dmg = TUNING.FORGE.INFERNALSTAFF.ALT_DAMAGE.maximum
	local min_dmg = TUNING.FORGE.INFERNALSTAFF.ALT_DAMAGE.minimum
	return min_dmg + (max_dmg - min_dmg)*perc
end]]

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_firestaff_meteor")
    inst.AnimState:SetBuild("lavaarena_firestaff_meteor")
    inst.AnimState:PlayAnimation("crash")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    inst:AddTag("notarget")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	--inst.range = 4
	inst.AttackArea = function(inst, attacker, weapon, pos)
		weapon.meteor = inst
		inst.attacker = attacker
		inst.owner = weapon
		inst.Transform:SetPosition(pos:Get())
	end
	
	inst:ListenForEvent("animover", function(inst)
		inst:DoTaskInTime(FRAMES*3, function(inst)
			SpawnPrefab("infernalstaff_meteor_splash"):SetPosition(inst:GetPosition())
			local found_mobs = {}
			local x, y, z = inst:GetPosition():Get()
			local ents = TheSim:FindEntities(x, y, z, range, nil, {"player", "companion"})
			for _,ent in ipairs(ents) do
				if inst.attacker ~= nil and ent ~= inst.attacker and ent.entity:IsValid() and ent.entity:IsVisible() and ent.components.health and not ent.components.health:IsDead() then
					table.insert(found_mobs, ent)
					--inst.attacker:PushEvent("onareaattackother", { target = ent, weapon = inst.owner, stimuli = "explosive" })
					-- change weapon damage
					--local damage = CalcDamage(inst:GetPosition(), ent)
					--inst.owner.components.weapon:SetAltDamage(damage)
					--inst.attacker.components.combat:DoAttack(ent, inst.owner, true, "explosive", nil, damage, true)
					--SpawnPrefab("infernalstaff_meteor_splashhit"):SetTarget(ent)
				end
			end
			if inst.owner.components.weapon and inst.owner.components.weapon:HasAltAttack() then
				inst.owner.components.weapon:DoAltAttack(inst.attacker, found_mobs, nil, "explosive")
			end
			inst.owner.components.aoespell:OnSpellCast(inst.attacker, ents)
			inst:Remove()
		end)
	end)

    return inst
end

local function splashfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_fire_fx")
    inst.AnimState:SetBuild("lavaarena_fire_fx")
    inst.AnimState:PlayAnimation("firestaff_ult")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetFinalOffset(1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.SetPosition = function(inst, pos)
		inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/meteor_strike")
		inst.Transform:SetPosition(pos:Get())
		SpawnPrefab("infernalstaff_meteor_splashbase"):SetPosition(pos)
	end
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function splashbasefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_fire_fx")
    inst.AnimState:SetBuild("lavaarena_fire_fx")
    inst.AnimState:PlayAnimation("firestaff_ult_projection")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.SetPosition = function(inst, pos)
		inst.Transform:SetPosition(pos:Get())
	end
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function splashhitfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_fire_fx")
    inst.AnimState:SetBuild("lavaarena_fire_fx")
    inst.AnimState:PlayAnimation("firestaff_ult_hit")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetFinalOffset(1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.SetTarget = function(inst, target)
		inst.Transform:SetPosition(target:GetPosition():Get())
		local scale = target:HasTag("minion") and .5 or (target:HasTag("largecreature") and 1.3 or .8)
		inst.AnimState:SetScale(scale, scale)
	end
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

return Prefab("infernalstaff_meteor", fn, assets, prefabs),
    Prefab("infernalstaff_meteor_splash", splashfn, assets_splash, prefabs_splash),
    Prefab("infernalstaff_meteor_splashbase", splashbasefn, assets_splash),
    Prefab("infernalstaff_meteor_splashhit", splashhitfn, assets_splash)