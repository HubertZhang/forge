--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/blowdart_lava2.zip"),
    Asset("ANIM", "anim/swap_blowdart_lava2.zip"),
}

local assets_projectile = {
    Asset("ANIM", "anim/lavaarena_blowdart_attacks.zip"),
}

local prefabs = {
    "moltendarts_projectile",
    "moltendarts_projectile_explosive",
    "reticulelong",
    "reticulelongping",
}

local prefabs_projectile = {
    "weaponsparks_piercing",
}

local prefabs_projectile_explosive = {
    "explosivehit",
}

local PROJECTILE_DELAY = 4 * FRAMES

--------------------------------------------------------------------------

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_blowdart_lava2", "swap_blowdart_lava2")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    return Vector3(ThePlayer.entity:LocalToWorldSpace(6.5, 0, 0))
end

local function ReticuleMouseTargetFn(inst, mousepos)
    if mousepos ~= nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        local dx = mousepos.x - x
        local dz = mousepos.z - z
        local l = dx * dx + dz * dz
        if l <= 0 then
            return inst.components.reticule.targetpos
        end
        l = 6.5 / math.sqrt(l)
        return Vector3(x + dx * l, 0, z + dz * l)
    end
end

local function ReticuleUpdatePositionFn(inst, pos, reticule, ease, smoothing, dt)
    local x, y, z = inst.Transform:GetWorldPosition()
    reticule.Transform:SetPosition(x, 0, z)
    local rot = -math.atan2(pos.z - z, pos.x - x) / DEGREES
    if ease and dt ~= nil then
        local rot0 = reticule.Transform:GetRotation()
        local drot = rot - rot0
        rot = Lerp((drot > 180 and rot0 + 360) or (drot < -180 and rot0 - 360) or rot0, rot, dt * smoothing)
    end
    reticule.Transform:SetRotation(rot)
end

local function MoltenBolt(inst, caster, pos)
	local dart = SpawnPrefab("moltendarts_projectile_explosive")
	dart.Transform:SetPosition(inst:GetPosition():Get())
	dart.components.aimedprojectile:Throw(inst, caster, pos)
	dart.components.aimedprojectile:DelayVisibility(inst.projectiledelay)
	caster.SoundEmitter:PlaySound("dontstarve/common/lava_arena/blow_dart")
	inst.components.rechargeable:StartRecharge()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
	inst.entity:AddSoundEmitter()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	inst.nameoverride = "blowdart_lava2"

    inst.AnimState:SetBank("blowdart_lava2")
    inst.AnimState:SetBuild("blowdart_lava2")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("blowdart")
    inst:AddTag("sharp")

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting:SetAlwaysValid(true)
    inst.components.aoetargeting.reticule.reticuleprefab = "reticulelong"
    inst.components.aoetargeting.reticule.pingprefab = "reticulelongping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.mousetargetfn = ReticuleMouseTargetFn
    inst.components.aoetargeting.reticule.updatepositionfn = ReticuleUpdatePositionFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.projectiledelay = PROJECTILE_DELAY

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(MoltenBolt)
	
	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.MOLTENDARTS.COOLDOWN)
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.MOLTENDARTS.DAMAGE)
    inst.components.weapon:SetRange(10, 15)
    inst.components.weapon:SetProjectile("moltendarts_projectile")
	inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)
	--inst.components.weapon:SetAltDamage(TUNING.FORGE.MOLTENDARTS.ALT_DAMAGE)
	inst.components.weapon:SetAltAttack(TUNING.FORGE.MOLTENDARTS.ALT_DAMAGE, {10, 15}, nil, DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("darts")
	
	inst:AddComponent("inspectable")
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "blowdart_lava2"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	
	--inst.damage_type = TUNING.FORGE.MOLTENDARTS.TYPE

    return inst
end

--------------------------------------------------------------------------

local FADE_FRAMES = 5

local tails = {
    ["tail_5_2"] = .15,
    ["tail_5_3"] = .15,
    ["tail_5_4"] = .2,
    ["tail_5_5"] = .8,
    ["tail_5_6"] = 1,
    ["tail_5_7"] = 1,
}

local thintails = {
    ["tail_5_8"] = 1,
    ["tail_5_9"] = .5,
}

local function CreateTail(thintail, tail_suffix)
    local inst = CreateEntity()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("lavaarena_blowdart_attacks")
    inst.AnimState:SetBuild("lavaarena_blowdart_attacks")
    inst.AnimState:PlayAnimation(weighted_random_choice(thintail and thintails or tails)..tail_suffix)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    if not thintail then
        inst.AnimState:SetAddColour(1, 1, 0, 0)
    end

    inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function OnUpdateProjectileTail(inst, tail_suffix)
    local c = (not inst.entity:IsVisible() and 0) or (inst._fade ~= nil and (FADE_FRAMES - inst._fade:value() + 1) / FADE_FRAMES) or 1
    if c > 0 then
        local tail = CreateTail(inst.thintailcount > 0, tail_suffix)
        tail.Transform:SetPosition(inst.Transform:GetWorldPosition())
        tail.Transform:SetRotation(inst.Transform:GetRotation())
        if c < 1 then
            tail.AnimState:SetTime(c * tail.AnimState:GetCurrentAnimationLength())
        end
        if inst.thintailcount > 0 then
            inst.thintailcount = inst.thintailcount - 1
        end
    end
end

local function OnHit(inst, owner, target, alt)
	if not alt then
		SpawnPrefab("weaponsparks_fx"):SetPiercing(inst, target)
	else
		SpawnPrefab("explosivehit").Transform:SetPosition(inst:GetPosition():Get())
	end
	inst:Remove()
end

local function OnMiss(inst, owner)
	inst:Remove()
end

local function commonprojectilefn(anim, tail_suffix, alt)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    RemovePhysicsColliders(inst)

    inst.AnimState:SetBank("lavaarena_blowdart_attacks")
    inst.AnimState:SetBuild("lavaarena_blowdart_attacks")
    inst.AnimState:PlayAnimation(anim, true)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetAddColour(1, 1, 0, 0)
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    --projectile (from projectile component) added to pristine state for optimization
    inst:AddTag("projectile")

    if not TheNet:IsDedicated() then
        inst.thintailcount = alt and math.random(3, 5) or math.random(2, 4)
        inst:DoPeriodicTask(0, OnUpdateProjectileTail, nil, tail_suffix)
    end

    if alt then
        inst._fade = net_tinybyte(inst.GUID, "blowdart_lava2_projectile_explosive._fade")
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    if not alt then
		inst:AddComponent("projectile")
		inst.components.projectile:SetSpeed(30)
		inst.components.projectile:SetRange(20)
		inst.components.projectile:SetOnHitFn(function(inst, attacker, target) OnHit(inst, inst.components.projectile.owner, target, alt) end)
		inst.components.projectile:SetOnMissFn(inst.Remove)
		inst.components.projectile:SetLaunchOffset(Vector3(-2, 1, 0))
	else
		inst:AddComponent("aimedprojectile")
		inst.components.aimedprojectile:SetSpeed(30)
		inst.components.aimedprojectile:SetRange(30)
		--inst.components.aimedprojectile:SetDamage(TUNING.FORGE.MOLTENDARTS.ALT_DAMAGE)
		inst.components.aimedprojectile:SetStimuli("explosive")
		inst.components.aimedprojectile:SetOnHitFn(OnHit)
		inst.components.aimedprojectile:SetOnMissFn(OnMiss)
	end
	
	inst:DoTaskInTime(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/blow_dart") end)

    return inst
end

local function projectilefn()
    return commonprojectilefn("attack_4", "", false)
end

local function projectileexplosivefn()
    return commonprojectilefn("attack_4_large", "_large", true)
end

return ForgePrefab("moltendarts", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "blowdart_lava2.tex", nil, TUNING.FORGE.MOLTENDARTS, STRINGS.FORGED_FORGE.WEAPONS.MOLTENDARTS.ABILITIES, "swap_blowdart_lava2", "common_hand"),
    Prefab("moltendarts_projectile", projectilefn, assets_projectile, prefabs_projectile),
    Prefab("moltendarts_projectile_explosive", projectileexplosivefn, assets_projectile, prefabs_projectile_explosive)