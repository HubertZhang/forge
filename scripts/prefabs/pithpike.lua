--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/spear_gungnir.zip"),
    Asset("ANIM", "anim/swap_spear_gungnir.zip"),
}

local assets_fx = {
    Asset("ANIM", "anim/lavaarena_staff_smoke_fx.zip"),
}

local prefabs = {
    "reticuleline",
    "reticulelineping",
    "spear_gungnir_lungefx",
    "weaponsparks",
    "firehit",
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_spear_gungnir", "swap_spear_gungnir")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    --Cast range is 8, leave room for error (6.5 lunge)
    return Vector3(ThePlayer.entity:LocalToWorldSpace(TUNING.FORGE.PITHPIKE.ALT_DIST, 0, 0))
end

local function ReticuleMouseTargetFn(inst, mousepos)
    if mousepos ~= nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        local dx = mousepos.x - x
        local dz = mousepos.z - z
        local l = dx * dx + dz * dz
        if l <= 0 then
            return inst.components.reticule.targetpos
        end
        l = 6.5 / math.sqrt(l)
        return Vector3(x + dx * l, 0, z + dz * l)
    end
end

local function ReticuleUpdatePositionFn(inst, pos, reticule, ease, smoothing, dt)
    local x, y, z = inst.Transform:GetWorldPosition()
    reticule.Transform:SetPosition(x, 0, z)
    local rot = -math.atan2(pos.z - z, pos.x - x) / DEGREES
    if ease and dt ~= nil then
        local rot0 = reticule.Transform:GetRotation()
        local drot = rot - rot0
        rot = Lerp((drot > 180 and rot0 + 360) or (drot < -180 and rot0 - 360) or rot0, rot, dt * smoothing)
    end
    reticule.Transform:SetRotation(rot)
end

local function PyrePoker(inst, caster, pos)
	caster:PushEvent("combat_lunge", {
		targetpos = pos,
		weapon = inst
	})
end

local function OnAttack(inst, attacker, target)
	if not inst.components.weapon.isaltattacking then
		SpawnPrefab("weaponsparks_fx"):SetPosition(attacker, target)
	else
		SpawnPrefab("forgespear_fx"):SetTarget(target)
		if target and target:HasTag("flippable") then
			target:PushEvent("flipped", {flipper = attacker})
		end
	end
end

local function OnLunge(inst)
	inst.components.rechargeable:StartRecharge()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	inst.nameoverride = "spear_gungnir"

    inst.AnimState:SetBank("spear_gungnir")
    inst.AnimState:SetBuild("spear_gungnir")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("sharp")
    inst:AddTag("pointy")

    --aoeweapon_lunge (from aoeweapon_lunge component) added to pristine state for optimization
    inst:AddTag("aoeweapon_lunge")

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting.reticule.reticuleprefab = "reticuleline"
    inst.components.aoetargeting.reticule.pingprefab = "reticulelineping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.mousetargetfn = ReticuleMouseTargetFn
    inst.components.aoetargeting.reticule.updatepositionfn = ReticuleUpdatePositionFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(PyrePoker)
	
	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.PITHPIKE.COOLDOWN)
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.PITHPIKE.DAMAGE)
	inst.components.weapon:SetOnAttack(OnAttack)
	inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)
	--inst.components.weapon:SetAltDamage(TUNING.FORGE.PITHPIKE.ALT_DAMAGE)
	inst.components.weapon:SetAltAttack(TUNING.FORGE.PITHPIKE.ALT_DAMAGE, TUNING.FORGE.PITHPIKE.ALT_DIST + TUNING.FORGE.PITHPIKE.ALT_WIDTH, nil, DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("aoeweapon_lunge")
	inst.components.aoeweapon_lunge:SetWidth(TUNING.FORGE.PITHPIKE.ALT_WIDTH)
	--inst.components.aoeweapon_lunge:SetDamage(TUNING.FORGE.PITHPIKE.ALT_DAMAGE)
	inst.components.aoeweapon_lunge:SetStimuli("explosive")
	inst.components.aoeweapon_lunge:SetOnLungeFn(OnLunge)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("melees")
	
	inst:AddComponent("inspectable")
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "spear_gungnir"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

	--inst.damage_type = TUNING.FORGE.PITHPIKE.TYPE
	
    return inst
end

return ForgePrefab("pithpike", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "spear_gungnir.tex", nil, TUNING.FORGE.PITHPIKE, STRINGS.FORGED_FORGE.WEAPONS.PITHPIKE.ABILITIES, "swap_spear_gungnir", "common_hand")