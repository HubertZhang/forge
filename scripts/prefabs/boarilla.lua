--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    --Asset("ANIM", "anim/lavaarena_trails_basic.zip"),
}

local prefabs =
{

}

------------------------------
--ToDo List--
------------------------------
--Adjust speed and hit/attack range of Boarilla to ensure you need speedboosts to dodge incoming punchs.
--Make small adjustments to CalcJumpSpeed in the SG to better mimic the slam ingame.
-----------------------------

local brain = require("brains/boarillabrain")

SetSharedLootTable( 'boarilla',
{
    --{'monstermeat', 1.000},
})

local REPEL_RADIUS = 5
local REPEL_RADIUS_SQ = REPEL_RADIUS * REPEL_RADIUS

local WAKE_TO_FOLLOW_DISTANCE = 8
local SLEEP_NEAR_HOME_DISTANCE = 10
local SHARE_TARGET_DIST = 30
local HOME_TELEPORT_DIST = 30

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }


local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function OnCollide(inst, other)
    if other ~= nil and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end

local function SetVariation(inst, var, build)
	if var then
		inst.AnimState:OverrideSymbol("armour", build and build or "lavaarena_trails_basic", "armour"..var)
		inst.AnimState:OverrideSymbol("mouth", build and build or "lavaarena_trails_basic", "mouth"..var)
    end
end

local function GetVariation(inst)
	if not inst.var then
		inst.var = math.random(0,2)
		if inst.var == 0 then
			inst.var = ""
		end	
		SetVariation(inst, inst.var)
	end
end


local function OnHitOther(inst, other) --knockback
	if other.sg and other.sg.sg.events.knockback then
	other:PushEvent("knockback", {knocker = inst, radius = inst.sg:HasStateTag("rolling") and 2.5 or 5})
	else
	--this stuff below is mostly left here for creatures in basegame. For modders that are reading this, use the knockback event above.
	if other ~= nil and not (other:HasTag("epic") or other:HasTag("largecreature")) then
		
		if other:IsValid() and other.entity:IsVisible() and not (other.components.health ~= nil and other.components.health:IsDead()) then
            if other.components.combat ~= nil then
                --other.components.combat:GetAttacked(inst, 10)
                if other.Physics ~= nil then
					local x, y, z = inst.Transform:GetWorldPosition()
                    local distsq = other:GetDistanceSqToPoint(x, 0, z)
					--if distsq < REPEL_RADIUS_SQ then
						if distsq > 0 then
							other:ForceFacePoint(x, 0, z)
						end
						local k = .5 * distsq / REPEL_RADIUS_SQ - 1
						other.speed = 60 * k
						other.dspeed = 2
						other.Physics:ClearMotorVelOverride()
						other.Physics:Stop()
						
						if other.components.inventory and other.components.inventory:ArmorHasTag("heavyarmor") or other:HasTag("heavybody") then 
						--Leo: Need to change this to check for bodyslot for these tags.
						other:DoTaskInTime(0.1, function(inst) 
							other.Physics:SetMotorVelOverride(-TUNING.FORGE.KNOCKBACK_RESIST_SPEED, 0, 0) 
						end)
						else
						other:DoTaskInTime(0, function(inst) 
							other.Physics:SetMotorVelOverride(-TUNING.FORGE.BOARILLA.ATTACK_KNOCKBACK, 0, 0) 
						end)
						end
						other:DoTaskInTime(0.4, function(inst) 
						other.Physics:ClearMotorVelOverride()
						other.Physics:Stop()
						end)
					--end
                end
            end
        end
	end
	end
end
-------------------------------------------------

local function FindPlayerTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
	if player then
		if not inst.components.follower.leader then --Don't attempt to find a target that isn't the leader's target. 
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) and player then
		return target
	else
		return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player and player
	end
end

local function OnDroppedTarget(inst)
	local playertargets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 50, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" }, {"player"} ) or {}
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not (inst.lasttarget and inst.lasttarget == v) then
			table.insert(playertargets, v)		
		end	
	end
	if not inst:HasTag("brainwashed") then
		--I assume first one in the table is the closest
		if #playertargets > 0 then
			inst.components.combat:SetTarget(playertargets[1])
		--Leo: This elseif check is just to make the mob not stop and continue on, making it look like the forge.
		elseif inst.lasttarget and inst.lasttarget.components and inst.lasttarget.components.health and not inst.lasttarget.components.health:IsDead() then
			inst.components.combat:SetTarget(inst.lasttarget)
		end
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
		
	end
end

local function RetargetFn(inst)
    local player, distsq = inst:GetNearestPlayer()
	if inst:HasTag("brainwashed") then
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "player", "companion", "brainwashed" })
	else
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not (inst.lastarget and inst.lasttarget == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
	end	
end

local function KeepTarget(inst, target)
	if inst:HasTag("brainwashed") then
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("player") and not target:HasTag("companion")
	else
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("LAmob") --inst:IsNear(target, 5)
	end
end

local function OnAttacked(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) then	
		if inst:HasTag("brainwashed") and not (data.attacker:HasTag("player") or data.attacker:HasTag("companion")) then
			inst.components.combat:SetTarget(data.attacker)
		elseif data.attacker:HasTag("brainwashed") and not inst:HasTag("brainwashed") then
			inst.components.combat:SetTarget(data.attacker)
		else
			local player, distsq = inst:GetNearestPlayer()	
			if player and player.components.health and not player.components.health:IsDead() and player == data.attacker then
				if inst.components.combat.target and not (data.attacker:HasTag("lessaggro") or inst:IsNear(inst.components.combat.target, 2.5)) then
					inst.components.combat:SetTarget(data.attacker)
				elseif not inst.components.combat.target then
					inst.components.combat:SetTarget(data.attacker)
				end		
			end
		end	
	end
end
-----------

local function OnAttackOther(inst, data)
    --inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("LA_mob") and dude.components.combat and not dude.components.combat.target and not dude.components.health:IsDead() end, 30)
end

local function GetReturnPos(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local rad = 2
    local angle = math.random() * 2 * PI
    return x + rad * math.cos(angle), y, z - rad * math.sin(angle)
end

local function DoReturn(inst)
    ----print("DoReturn", inst)
    if inst.components.homeseeker ~= nil and inst.components.homeseeker:HasHome() then
        if inst:HasTag("pet_hound") then
            if inst.components.homeseeker.home:IsAsleep() and not inst:IsNear(inst.components.homeseeker.home, HOME_TELEPORT_DIST) then
                inst.Physics:Teleport(GetReturnPos(inst.components.homeseeker.home))
            end
        elseif inst.components.homeseeker.home.components.childspawner ~= nil then
            inst.components.homeseeker.home.components.childspawner:GoHome(inst)
        end
    end
end

local function StartAltAttacking(inst) --Used to reapply alt_attack var when reloaded.
	if inst.is_enraged and inst.is_enraged == true then
		inst.altattack = true
		inst.components.combat:SetRange(TUNING.FORGE.BOARILLA.JUMP_ATTACK_RANGE, 0)
	end
end

local function EnterRollTrigger(inst)
    if not inst.classic and inst.altattack2 then
		inst.altattack2 = true
		inst.sg:GoToState("special_atk2ev")
	end
end

local function EnterPhase2Trigger(inst)
    if inst.is_enraged and inst.is_enraged ~= true then
		inst.is_enraged = true
		inst.altattack = true
		inst.sg:GoToState("special_atk2ev")
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 500, 1.25)
	inst:SetPhysicsRadiusOverride(1.25)
	inst.DynamicShadow:SetSize(3.25, 1.75)
	inst.Transform:SetScale(1.2, 1.2, 1.2)
    inst.Transform:SetFourFaced()

    inst:AddTag("monster")
    inst:AddTag("hostile")
	inst:AddTag("epic")
	inst:AddTag("largecreature")
    inst:AddTag("LA_mob")

	inst:AddTag("fossilizable")
	
    inst.AnimState:SetBank("trails")
    inst.AnimState:SetBuild("lavaarena_trails_basic")
    inst.AnimState:PlayAnimation("idle_loop", true)
	
	inst.AnimState:AddOverrideBuild("fossilized")

    inst:AddComponent("spawnfader")
	
	inst.nameoverride = "trails" --So we don't have to make the describe strings.
	
	if TheWorld.components.lavaarenamobtracker ~= nil then
        TheWorld.components.lavaarenamobtracker:StartTracking(inst)
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("item_launcher")
	
	inst.SetVariation = SetVariation
	
	inst:AddComponent("fossilizable")

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.walkspeed = TUNING.FORGE.BOARILLA.WALKSPEED
    inst.components.locomotor.runspeed = TUNING.FORGE.BOARILLA.RUNSPEED
    inst:SetStateGraph("SGboarilla")
	inst.sg.mem.radius = inst.physicsradiusoverride
	
	
    inst:SetBrain(brain)

    inst:AddComponent("follower")
	
	inst:AddComponent("bloomer")
	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE.BOARILLA.HEALTH)

    --inst:AddComponent("sanityaura")
    --inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED
	inst.rams = 0
	inst.altattack = true
	inst.altattack2 = true
	inst.is_enraged = false
	inst.canshield = nil
	inst.should_taunt = true
	
	inst.jump_range = math.min(TUNING.KLAUS_CHOMP_MAX_RANGE, TUNING.KLAUS_CHOMP_RANGE)
	
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)
	
	inst:DoTaskInTime(TUNING.FORGE.BOARILLA.SHEILD_CD, function(inst) inst.canshield = true end)
	--inst:DoTaskInTime(0, GetVariation)
	
	
	
	inst:AddComponent("healthtrigger")
	inst.components.healthtrigger:AddTrigger(TUNING.FORGE.BOARILLA.ROLL_TRIGGER, EnterRollTrigger) 
	inst.components.healthtrigger:AddTrigger(0.5, EnterPhase2Trigger) 
	--Leo: HealthTrigger doesn't seem to work, at all. 
	--Leo: Instead I messed with the onattack event on their stategraph.

    inst:AddComponent("combat")
	--inst.components.combat.playerdamagepercent = 0.5
	inst.components.combat:SetRange(TUNING.FORGE.BOARILLA.ATTACK_RANGE, TUNING.FORGE.BOARILLA.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(5, 1)
    inst.components.combat:SetDefaultDamage(TUNING.FORGE.BOARILLA.DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.FORGE.BOARILLA.ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(1, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat.battlecryenabled = false
	inst.components.combat:SetDamageType(DAMAGETYPES.PHYSICAL)
	--I know this looks horrifyingly disgusting but we're running out of ideas here....
	local _oldGetAttacked = inst.components.combat.GetAttacked
	inst.components.combat.GetAttacked = function(self, attacker, damage, weapon, stimuli)
		local absorb = 0
		if self.inst.sg:HasStateTag("hiding") then
			if not (stimuli == "explosive" or stimuli == "electric" or stimuli == "acid") then
				absorb = 1
			end
		end
		if self.inst.components.health.absorb ~= absorb then self.inst.components.health:SetAbsorptionAmount(absorb) end
		return _oldGetAttacked(self, attacker, damage, weapon, stimuli)
	end
	
	inst:AddComponent("armorbreak_debuff")

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('boarilla')

    inst:AddComponent("inspectable")

    inst:AddComponent("sleeper")
	
	inst:AddComponent("debuffable")
	
	inst.components.combat.onhitotherfn = OnHitOther
	
    MakeHauntablePanic(inst)
	
	MakeMediumBurnableCharacter(inst, "body")

    inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
    --inst:ListenForEvent("onattackother", OnAttackOther)
	inst:DoTaskInTime(0.5, function(inst)
		local target = FindPlayerTarget(inst) or nil
		inst.components.combat:SetTarget(target)
	end)

    return inst
end

return ForgePrefab("boarilla", fn, assets, prefabs, nil, "ENEMIES", false, "images/forged_forge.xml", "boarilla_icon.tex", nil, TUNING.FORGE.BOARILLA, STRINGS.FORGED_FORGE.ENEMIES.BOARILLA.ABILITIES)