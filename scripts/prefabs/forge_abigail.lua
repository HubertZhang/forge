--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    Asset("ANIM", "anim/player_ghost_withhat.zip"),
    Asset("ANIM", "anim/ghost_abigail_build.zip"),
    Asset("SOUND", "sound/ghost.fsb"),
}

local prefabs =
{
    "forge_abigail_flower",
}

local brain = require("brains/abigailbrain")

SetSharedLootTable( 'abby',
{
    --{'monstermeat', 1.000},
})

local function Retarget(inst)
    return FindEntity(
        inst,
        20,
        function(guy)
            return inst._playerlink ~= nil
                and inst.components.combat:CanTarget(guy)
                and (guy.components.combat.target == inst._playerlink or
                    inst._playerlink.components.combat.target == guy)
        end,
        { "_combat", "_health" },
        { "INLIMBO", "noauradamage", "_isinheals", "fossilized", "player", "companion" }
    )
end

local function OnAttacked(inst, data)
	if data.attacker and not ((data.attacker:HasTag("player") or data.attacker:HasTag("companion")) or data.attacker:HasTag("_isinheals")) then
		if data.attacker == nil then
			inst.components.combat:SetTarget(nil)
		elseif data.attacker == inst._playerlink then
			inst.components.health:SetVal(0)
		elseif not data.attacker:HasTag("noauradamage") then
			inst.components.combat:SetTarget(data.attacker)
		end
	end	
end

local function OnDeath(inst)
	--local flower = SpawnPrefab("forge_abigail_flower")
	--flower.Transform:SetPosition(inst.Transform:GetWorldPosition())
	--flower._playerlink = inst._playerlink
	--flower.LinkToPlayer(flower, inst._playerlink)
	if inst._playerlink ~= nil then
		inst._playerlink.components.petleash.petprefab = "forge_abigail_flower"
		local pet = inst._playerlink.components.petleash:SpawnPetAt(inst.Transform:GetWorldPosition())
		if inst.buffed then 
			pet.buffed = inst.buffed
			pet.components.cooldown.cooldown_duration = TUNING.FORGE.ABIGAIL.RESPAWN_TIME/inst.buffed
		end
	end

    inst.components.aura:Enable(false)
end

local function auratest(inst, target)
    if target == inst._playerlink then
        return false
    end

    if inst.components.combat.target == target then
        return true
    end

    local leader = inst.components.follower.leader
    if target.components.combat.target ~= nil
        and (target.components.combat.target == inst or
            target.components.combat.target == leader) then
        return true
    end

    if leader ~= nil
        and (leader == target
            or (target.components.follower ~= nil and
                target.components.follower.leader == leader)) then
        return false
    end

    return not target:HasTag("player") and target:HasTag("monster") and not (target.sg and ((target.sg:HasStateTag("sleeping") or target:HasTag("_isinheals")) or target.sg:HasStateTag("fossilized")))
end

local LOOT = { } --we're going to do SICK HACKS to make the flower to keep the link on the player. --Leo: I hate abby so much

local function refreshcontainer(container)
    for i = 1, container:GetNumSlots() do
        local item = container:GetItemInSlot(i)
        if item ~= nil and item.prefab == "forge_abigail_flower" then
            item:Refresh()
        end
    end
end

local function unlink(inst)
    inst._playerlink.abigail = nil
    local inv = inst._playerlink.components.inventory
    refreshcontainer(inv)

    local activeitem = inv:GetActiveItem()
    if activeitem ~= nil and activeitem.prefab == "forge_abigail_flower" then
        activeitem:Refresh()
    end

    for k, v in pairs(inv.opencontainers) do
        refreshcontainer(k.components.container)
    end
end

local function linktoplayer(inst, player)
    inst.components.lootdropper:SetLoot(LOOT)
    inst.persists = false
    inst._playerlink = player
    player.abigail = inst
    player.components.leader:AddFollower(inst)
	player.components.pethealthbar:SetPet(inst, "pet_abigail", TUNING.FORGE.ABIGAIL.HEALTH)

    player:ListenForEvent("onremove", unlink, inst)
end

local SCALE = .5

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddLight()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("ghost")
    inst.AnimState:SetBuild("ghost_abigail_build")
    inst.AnimState:PlayAnimation("idle", true)
    inst.AnimState:SetBloomEffectHandle("shaders/anim_bloom_ghost.ksh")
    inst.AnimState:SetLightOverride(TUNING.GHOST_LIGHT_OVERRIDE)
    --inst.AnimState:SetMultColour(1, 1, 1, .6)

    inst:AddTag("character")
    inst:AddTag("scarytoprey")
    inst:AddTag("girl")
    inst:AddTag("ghost")
    inst:AddTag("noauradamage")
    inst:AddTag("notraptrigger")
    inst:AddTag("abigail")
    inst:AddTag("NOBLOCK")
    inst:AddTag("companion")
    --inst:AddTag("NOCLICK")

    MakeGhostPhysics(inst, 1, .5)

    inst.Light:SetIntensity(.6)
    inst.Light:SetRadius(.5)
    inst.Light:SetFalloff(.6)
    inst.Light:Enable(true)
    inst.Light:SetColour(180 / 255, 195 / 255, 225 / 255)

    --It's a loop that's always on, so we can start this in our pristine state
    inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_girl_howl_LP", "howl")

    --inst:Hide()

    inst.nameoverride = "abigail"

    inst.entity:SetPristine()
	
	inst.ForgeOnSpawn = function(self)
		inst.AnimState:PlayAnimation("appear")
	end
	inst.ForgeOnDespawn = function(self)
		inst.sg:GoToState("dissipate")
	end

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.walkspeed = TUNING.ABIGAIL_SPEED*.5
    inst.components.locomotor.runspeed = TUNING.ABIGAIL_SPEED
	
    inst:SetStateGraph("SGabby_forge")

    inst:SetBrain(brain)
	
    inst:AddComponent("follower")
	inst.components.follower:KeepLeaderOnAttacked()
	inst.components.follower.keepleader = true
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE.ABIGAIL.HEALTH)
	inst.components.health:StartRegen(1, 1)
	
	inst:AddComponent("debuffable")

    inst:AddComponent("combat")
    inst.components.combat.defaultdamage = TUNING.FORGE.ABIGAIL.DAMAGE
    inst.components.combat.playerdamagepercent = TUNING.ABIGAIL_DMG_PLAYER_PERCENT
    inst.components.combat:SetRetargetFunction(3, Retarget)
	
	inst:AddComponent("aura")
    inst.components.aura.radius = 3
    inst.components.aura.tickperiod = 1
    inst.components.aura.ignoreallies = true
    inst.components.aura.auratestfn = auratest	
	
	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('scorpeon')
	
	inst:AddComponent("inspectable")
	
	inst.acidimmune = true
	
	inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("death", OnDeath)

	inst.LinkToPlayer = linktoplayer
	inst:DoTaskInTime(0, function(inst) 
		if inst.components.follower.leader then 
			inst.LinkToPlayer(inst, inst.components.follower.leader)
		end 
	end)
	
	
	
    return inst
end

return Prefab("forge_abigail", fn, assets, prefabs)
