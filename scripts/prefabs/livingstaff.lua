--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/healingstaff.zip"),
    Asset("ANIM", "anim/swap_healingstaff.zip"),
}

local prefabs = {
    "forge_blossom_projectile",
    "forge_blossom_hit_fx",
    "lavaarena_healblooms",
    "reticuleaoe",
    "reticuleaoeping",
    "reticuleaoefriendlytarget",
}

local PROJECTILE_DELAY = 4 * FRAMES

--------------------------------------------------------------------------
local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_healingstaff", "swap_healingstaff")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    local player = ThePlayer
    local ground = TheWorld.Map
    local pos = Vector3()
    --Cast range is 8, leave room for error
    --4 is the aoe range
    --Walk a tiny distance into healing range
    for r = 6, 0, -.25 do
        pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
        if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
            return pos
        end
    end
    return pos
end

local function LifeBlossom(inst, caster, pos)
    inst:DoTaskInTime(0.9, function()
    	local heal_rate = inst.heal_rate
    	local boosted = false
    	if caster.components.buffable then
    		heal_rate = caster.components.buffable:ApplyStatBuffs({"spell_heal_rate", "heal_dealt"}, heal_rate)
    		boosted = heal_rate > inst.heal_rate -- TODO use TUNING.FORGE
    	end
    	local circle = SpawnAt("healingcircle", pos)
    	local cc = circle.components.heal_aura
    	cc.heal_rate = heal_rate
    	cc.caster = caster
    	circle.buffed = boosted
    	Debug:Print("[LifeBlossom] Heal: " .. tostring(heal_rate) .. "/s")
    	inst.components.rechargeable:StartRecharge()
    	TheWorld:PushEvent("healingcircle_spawned")
    	inst.components.aoespell:OnSpellCast(caster) -- TODO anything special here?
    end)
end

local function OnSwing(inst, attacker, target)
	inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/heal_staff")
	local offset = (target:GetPosition() - attacker:GetPosition()):GetNormalized()*1.2
	local particle = SpawnPrefab("forge_blossom_hit_fx")
	particle.Transform:SetPosition((attacker:GetPosition() + offset):Get())
	particle.AnimState:SetScale(0.8,0.8)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	inst.nameoverride = "healingstaff"

    inst.AnimState:SetBank("healingstaff")
    inst.AnimState:SetBuild("healingstaff")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("rangedweapon")
	inst:AddTag("magicweapon")

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting.reticule.reticuleprefab = "reticuleaoe"
    inst.components.aoetargeting.reticule.pingprefab = "reticuleaoeping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.validcolour = { 0, 1, .5, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { 0, .4, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.projectiledelay = PROJECTILE_DELAY
	
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
    inst.castsound = "dontstarve/common/lava_arena/spell/heal"
	
	inst.heal_rate = TUNING.FORGE.LIVINGSTAFF.HEAL_RATE -- per second

	-- Fox: Initializing reticule_spawner before aoespell
	inst:AddComponent("reticule_spawner")
	inst.components.reticule_spawner:Setup(unpack(TUNING.FORGE.RET_DATA.livingstaff))
	
    inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(LifeBlossom)
	inst.components.aoespell:SetSpellType("heal")
	
	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.LIVINGSTAFF.COOLDOWN)
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.LIVINGSTAFF.DAMAGE)
    inst.components.weapon:SetRange(10, 20)
    inst.components.weapon:SetProjectile("forge_blossom_projectile")
	inst.components.weapon:SetDamageType(DAMAGETYPES.MAGIC)
	inst.components.weapon:SetOnProjectileLaunch(OnSwing)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType({"staves", "healers"})
	
	inst:AddComponent("inspectable")
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "healingstaff"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	
	--inst.damage_type = TUNING.FORGE.LIVINGSTAFF.TYPE

    return inst
end

return ForgePrefab("livingstaff", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "healingstaff.tex", nil, TUNING.FORGE.LIVINGSTAFF, STRINGS.FORGED_FORGE.WEAPONS.LIVINGSTAFF.ABILITIES, "swap_healingstaff", "common_hand")