--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local tickrate = 1/10 -- of a second (for acid)
local tickvalue = -8.75 -- per second (for acid)

local assets =
{

}

local prefabs =
{

}

local brain = require("brains/scorpeonbrain")

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }

local function FindPlayerTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
	if player then
		if not inst.components.follower.leader then --Don't attempt to find a target that isn't the leader's target. 
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) and player then
		return target
	else
		return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player and player
	end
end

--[[
local function RetargetFn(inst)
    local player, distsq = inst:GetNearestPlayer()
    return (player and not player.components.health:IsDead() and not player:HasTag("notarget")) and player or FindEntity(inst, 255, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "LA_mob", "battlestandard" })
end

local function KeepTarget(inst, target)
	--Note: lose target if you haven't been hit or can't hit others.
    return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead())
end

local function OnAttacked(inst, data)
    local foe = data.attacker or nil
    local target = inst.components.combat.target or nil
    if foe and not foe:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) then
		inst.aggrotimer = nil
        local hands = foe.components.inventory and foe.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
        local thands = target and target.components.inventory and target.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
        if target and not foe:HasTag("lessaggro") and not (hands and (hands:HasTag("rangedweapon") or hands:HasTag("blowdart"))) then
            if data.stimuli and data.stimuli == "electric" then
				inst.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_STUN
                inst.components.combat:SetTarget(foe)
			elseif foe:HasTag("grabaggro") then
				inst.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_STUN
				inst.components.combat:SetTarget(foe)
			elseif hands and hands:HasTag("hammer") and not target:HasTag("companion") and not (target:HasTag("moreaggro") or foe:HasTag("lessaggro")) and not inst.sg:HasStateTag("attack") then
				----print("OnAttacked melee check PASSED")
				inst.components.combat:SetTarget(foe)
			elseif hands and (hands:HasTag("sharp") or hands:HasTag("pointy") or hands:HasTag("book")) and not (target:HasTag("moreaggro") or foe:HasTag("lessaggro")) and not (thands and thands:HasTag("hammer")) and not inst.sg:HasStateTag("attack") then
				----print("OnAttacked melee check PASSED")
				inst.components.combat:SetTarget(foe)
			elseif not hands and not (target:HasTag("moreaggro") or foe:HasTag("lessaggro")) and not inst.sg:HasStateTag("attack") then	
				inst.components.combat:SetTarget(foe)	
            end 
        elseif target and target:HasTag("moreaggro") and math.random(1, 100) > 90 then
            inst.components.combat:SetTarget(foe)   
        elseif not target then
            inst.components.combat:SetTarget(foe)
        end             
    end
end]]

local function OnDroppedTarget(inst)
	local playertargets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 50, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" }, {"player"} ) or {}
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not (inst.lasttarget and inst.lasttarget == v) then
			table.insert(playertargets, v)		
		end	
	end
	if not inst:HasTag("brainwashed") then
		--I assume first one in the table is the closest
		if #playertargets > 0 then
			inst.components.combat:SetTarget(playertargets[1])
		--Leo: This elseif check is just to make the mob not stop and continue on, making it look like the forge.
		elseif inst.lasttarget and inst.lasttarget.components and inst.lasttarget.components.health and not inst.lasttarget.components.health:IsDead() then
			inst.components.combat:SetTarget(inst.lasttarget)
		end
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
		
	end
end

local function RetargetFn(inst)
    local player, distsq = inst:GetNearestPlayer()
	if inst:HasTag("brainwashed") then
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "player", "companion", "brainwashed" })
	else
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not (inst.lastarget and inst.lasttarget == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
	end	
end

local function KeepTarget(inst, target)
	if inst:HasTag("brainwashed") then
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("player") and not target:HasTag("companion")
	else
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("LAmob") --inst:IsNear(target, 5)
	end
end

local function OnAttacked(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) then	
		if inst:HasTag("brainwashed") and not (data.attacker:HasTag("player") or data.attacker:HasTag("companion")) then
			inst.components.combat:SetTarget(data.attacker)
		elseif data.attacker:HasTag("brainwashed") and not inst:HasTag("brainwashed") then
			inst.components.combat:SetTarget(data.attacker)
		else
			local player, distsq = inst:GetNearestPlayer()	
			if player and player.components.health and not player.components.health:IsDead() and player == data.attacker then
				if inst.components.combat.target and not (data.attacker:HasTag("lessaggro") or inst:IsNear(inst.components.combat.target, 2.5)) then
					inst.components.combat:SetTarget(data.attacker)
				elseif not inst.components.combat.target then
					inst.components.combat:SetTarget(data.attacker)
				end		
			end
		end	
	end
end
-----------------------------
--Proximity based aggro
local function OnHitOther(inst)
	inst.last_attack_landed = GetTime()
end

local function OnMissOther(inst)
	--don't count alt or general aoe attacks. Added isaoeattacking because Boarrior uses isaltattacking for knockback purposes.
	if not inst._isaltattacking and not inst._isaoeattacking then
		----print("Target Debug: Attack missed!")
		inst.attacks_missed = (inst.attacks_missed and inst.attacks_landed <= 9) and inst.attacks_landed + 1 or 1 
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
		
	end
end

local function RetargetFn_Test(inst)
	----print("Target Debug: retarget running, lasttarget is "..inst.lasttarget:GetDisplayName())
    local player, distsq = inst:GetNearestPlayer()
    return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not (inst.lastarget and inst.lasttarget == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
end

local function KeepTarget_Test(inst, target)
	--if inst.last_attack_landed and inst.last_attack_landed < GetTime() then
		--inst.last_attack_landed = nil
		--return 
	--elseif not inst.last_attack_landed then	
		--Note: lose target if you haven't been hit or can't hit others.
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) --inst:IsNear(target, 5)
	--end
end

local function OnAttacked_Test(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) then	
		local player, distsq = inst:GetNearestPlayer()	
		if player and player.components.health and not player.components.health:IsDead() and player == data.attacker then
			if inst.components.combat.target and not (data.attacker:HasTag("lessaggro") or inst:IsNear(inst.components.combat.target, 2.5)) then
				inst.components.combat:SetTarget(data.attacker)
			elseif not inst.components.combat.target then
				inst.components.combat:SetTarget(data.attacker)
			end		
		end
	end
end
----------------------------

local function OnAttack(inst, data)
    if inst.components.combat.target and inst.altattack == true and inst.sg.statemem.wants_to_spit then
		local posx, posy, posz = inst.Transform:GetWorldPosition()
		local angle =	-inst:GetAngleToPoint(inst.components.combat.target:GetPosition():Get()) * DEGREES---inst.Transform:GetRotation() * DEGREES
		local offset = 4.25
		local targetpos = Point(posx + (offset * math.cos(angle)), 0, posz + (offset * math.sin(angle)))
		local spit = SpawnPrefab("scorpeon_projectile")
		spit.Transform:SetPosition(inst:GetPosition():Get())
		spit.thrower = inst
		spit.components.complexprojectile:Launch(targetpos, inst)
    end
end

local function EnterPhase2Trigger(inst)
	inst.components.combat:SetAttackPeriod(TUNING.FORGE.SCORPEON.ATTACK_PERIOD_ENRAGED)
	inst.sg:GoToState("taunt")
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 150, 0.8)
	inst.DynamicShadow:SetSize(3, 1.5)
	inst.Transform:SetScale(0.9, 0.9, 0.9)
    inst.Transform:SetSixFaced()

    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("LA_mob")
	
	inst:AddTag("fossilizable")

    inst.AnimState:SetBank("peghook")
    inst.AnimState:SetBuild("lavaarena_peghook_basic")
    inst.AnimState:PlayAnimation("idle_loop", true)
	
	inst.AnimState:AddOverrideBuild("fossilized")

    inst:AddComponent("spawnfader")
	
	inst.nameoverride = "peghook" --So we don't have to make the describe strings.
	
	if TheWorld.components.lavaarenamobtracker ~= nil then
        TheWorld.components.lavaarenamobtracker:StartTracking(inst)
    end

    inst.entity:SetPristine()
    
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = TUNING.FORGE.SCORPEON.RUNSPEED
	
    inst:AddComponent("fossilizable")
	
    inst:SetStateGraph("SGscorpeon")

    inst:SetBrain(brain)
	
    inst:AddComponent("follower")
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE.SCORPEON.HEALTH)
	
	inst:AddComponent("healthtrigger")
	inst.components.healthtrigger:AddTrigger(TUNING.FORGE.SCORPEON.PHASE2_TRIGGER, EnterPhase2Trigger)
	
	inst.altattack = true
	inst.is_enraged = false --I can't remember why I have this.	

    inst:AddComponent("combat")
	--inst.components.combat.playerdamagepercent = 0.5
	inst.components.combat:SetRange(TUNING.FORGE.SCORPEON.ATTACK_RANGE, TUNING.FORGE.SCORPEON.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(5, 1)
    inst.components.combat:SetDefaultDamage(TUNING.FORGE.SCORPEON.DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.FORGE.SCORPEON.ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(1, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat.battlecryenabled = false
	inst.components.combat:SetDamageType(DAMAGETYPES.PHYSICAL)

	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")
	
    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('scorpeon')
	
	
    inst:AddComponent("inspectable")
	
	inst:AddComponent("armorbreak_debuff")
	inst.components.armorbreak_debuff:SetFollowSymbol("head")
	
	inst:AddComponent("debuffable")
	
	inst:ListenForEvent("onmissother", OnAttack)

    inst:AddComponent("sleeper")
    inst.components.sleeper:SetResistance(3)
    inst.components.sleeper.testperiod = GetRandomWithVariance(6, 2)
	
	MakeLargeBurnableCharacter(inst, "body")

    inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
    --inst:ListenForEvent("onattackother", OnAttackOther)
	--[[
	inst:DoTaskInTime(0.5, function(inst)
		local target = FindPlayerTarget(inst) or nil
		inst.components.combat:SetTarget(target)
	end)]]

    return inst
end

---------------------------------------------------------------------------------------
local function projectile_onhitother(inst, target)
	--local fx = SpawnPrefab("scorpeon_splashfx")
	--fx.Transform:SetPosition(target:GetPosition():Get())
	target.components.combat:GetAttacked(inst.thrower and inst.thrower or inst, TUNING.FORGE.SCORPEON.SPIT_DAMAGE, inst.thrower, "acid")
	--Leo: Added acidimmune to make it easy for modded characters to be immune to acid if they so well please.
	if target.components.health and target.components.debuffable and target.components.debuffable:IsEnabled() and not target.acidimmune and not target.components.debuffable:HasDebuff("healingcircle_regenbuff") and not (target.sg and target.sg:HasStateTag("noattack")) then
		target.components.debuffable:AddDebuff("scorpeon_dot", "scorpeon_dot")
	end
end
local function projectile_onland(inst)
	local splash = SpawnPrefab("scorpeon_splashfx")
	inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/poison_drop")
	splash.Transform:SetPosition(inst:GetPosition():Get())
	
	local x, y, z = inst:GetPosition():Get()
	local ents = TheSim:FindEntities(x,0,z, TUNING.FORGE.SCORPEON.SPIT_ATTACK_RANGE, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" } ) or {}
	for k, v in pairs(ents) do
		if v.components.combat and v.components.health and not v.components.health:IsDead() and v ~= inst.thrower then
			projectile_onhitother(inst, v)
		end
	end
	inst:Remove()
end

local function projectile_fn(inst)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("gooball_fx")
	inst.AnimState:SetBuild("gooball_fx")
	inst.AnimState:PlayAnimation("idle_loop", true)
	inst.AnimState:SetMultColour(.2, 1, 0, 1)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
    end

	inst:AddTag("spat")
	inst:AddTag("projectile")
	
	inst:Show()
    inst.persists = false

	inst:AddComponent("complexprojectile")
	--TODO: Set Tuning variables.
    inst.components.complexprojectile:SetHorizontalSpeed(15)
    inst.components.complexprojectile:SetGravity(-50)
    inst.components.complexprojectile:SetLaunchOffset(Vector3(0.2, 2.5, 0))
    inst.components.complexprojectile:SetOnHit(projectile_onland)
	
	inst:AddComponent("combat")
	inst.components.combat:SetDamageType(DAMAGETYPES.LIQUID)
	
	inst.DoTrail = inst:DoPeriodicTask(0.1, function()
		local pos = inst:GetPosition()
		
		local trail = SpawnPrefab("scorpeon_tail")
		trail.Transform:SetPosition(pos.x, pos.y, pos.z)	
	end)
	
	inst:DoTaskInTime(20, function (inst) inst:Remove() end) --incase projectile gets stuck, remove self.
	
	return inst
end
---------------------------------------------------------------------------------------
local colourtoadd = { 0.1, 0.5, 0.1 }
local colouraddspeed = 1 -- #seconds it takes to reach color
local function OnTick(inst, target)
	inst.tickcount = inst.tickcount and inst.tickcount + 1
    if target.components.health ~= nil and
        not target.components.health:IsDead() and
        not target:HasTag("playerghost") then
		local mult = target.acidmult ~= nil and target.acidmult or 1 --incase modded players want resistances.
        --target.components.health:DoDelta(tickvalue*tickrate, true, "scorpeon_dot")
		target.components.health:DoDelta(TUNING.FORGE.SCORPEON.ACID_DAMAGE_TOTAL / -130.5 * mult, true, "peghook_dot", nil, nil, true)
    else
        inst.components.debuff:Stop()
    end
end

local function OnAttached(inst, target)
    inst.entity:SetParent(target.entity)
	inst.tickcount = 0
    inst.task = inst:DoPeriodicTask(tickrate, OnTick, nil, target)
	
	if target.components.colourfader then
		target.components.colourfader:StartFade(colourtoadd, .25)
	end
	
    inst:ListenForEvent("death", function()
        inst.components.debuff:Stop()
    end, target)
end

local function OnDetached(inst, target)
	if not (target.components.debuffable and target.components.debuffable.debuffs["healingcircle_regenbuff"])
	and target.components.colourfader then
		target.components.colourfader:StartFade({0, 0, 0}, .25)
	end
	
	inst:Remove()
end

local function OnTimerDone(inst, data)
    if data.name == "corrosiveover" then
        inst.components.debuff:Stop()
    end
	----print("DEBUG: Scorp Acid Tick Count is "..inst.tickcount and inst.tickcount or "Error")
end

local function OnExtended(inst, target)
    inst.components.timer:StopTimer("corrosiveover")
    inst.components.timer:StartTimer("corrosiveover", inst.duration)
    inst.task:Cancel()
	
	if target and target.components.colourfader then
		target.components.colourfader:StartFade(colourtoadd, .25)
	end
	
    inst.task = inst:DoPeriodicTask(tickrate, OnTick, nil, target)
end

local function DoT_OnInit(inst)
    local parent = inst.entity:GetParent()
    if parent ~= nil then
        parent:PushEvent("startcorrosivedebuff", inst)
    end
end

local function dot_fn(inst)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

	inst:AddTag("CLASSIFIED")

    inst:DoTaskInTime(0, DoT_OnInit)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
    inst:AddComponent("debuff")
    inst.components.debuff:SetAttachedFn(OnAttached)
    inst.components.debuff:SetDetachedFn(OnDetached)
    inst.components.debuff:SetExtendedFn(OnExtended)
    --inst.components.debuff.keepondespawn = true
	
	inst.duration = TUNING.FORGE.SCORPEON.ACID_WEAROFF_TIME
	
	inst.nameoverride = "peghook_dot" --So we don't have to make the describe strings.

    inst:AddComponent("timer")
	inst:DoTaskInTime(0, function() -- in case we want to change duration
		inst.components.timer:StartTimer("corrosiveover", inst.duration)
	end)
    inst:ListenForEvent("timerdone", OnTimerDone)
	
	return inst
	
end
--------

local function splashfx_fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

    inst.AnimState:SetBank("lavaarena_hits_splash")
    inst.AnimState:SetBuild("lavaarena_hits_splash")
    inst.AnimState:PlayAnimation("aoe_hit")
    inst.AnimState:SetMultColour(.2, 1, 0, 1)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
    inst.AnimState:SetScale(.54, .54)

	
	inst.entity:SetPristine()

	inst:ListenForEvent("animover", inst.Remove)

	return inst
end
----------
local function onremove(inst)
	if inst.TrackHeight then
		inst.TrackHeight:Cancel()
		inst.TrackHeight = nil
		inst.trailtask = nil
	end
end

local function CreateTail()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)
	
	inst.AnimState:SetBank("gooball_fx")
	inst.AnimState:SetBuild("gooball_fx")
	inst.AnimState:PlayAnimation("disappear")
	inst.AnimState:SetMultColour(.2, 1, 0, 1)
	inst.AnimState:SetFinalOffset(-1)
	
	inst.entity:SetPristine()
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false
	
	inst:ListenForEvent("animover", inst.Remove)

	return inst
end

local function OnUpdateProjectileTail(inst, tails)
    local x, y, z = inst.Transform:GetWorldPosition()
    for tail, _ in pairs(tails) do
        tail:ForceFacePoint(x, y, z)
    end
    if inst.entity:IsVisible() then
        local tail = CreateTail()
        local rot = inst.Transform:GetRotation()
        tail.Transform:SetRotation(rot)
        rot = rot * DEGREES
        local offsangle = math.random() * 2 * PI
        local offsradius = math.random() * .2 + .2
        local hoffset = math.cos(offsangle) * offsradius
        local voffset = math.sin(offsangle) * offsradius
        tail.Transform:SetPosition(x + math.sin(rot) * hoffset, y + voffset, z + math.cos(rot) * hoffset)
        --tail.Physics:SetMotorVel(speed * (.2 + math.random() * .3), 0, 0)]]
        tails[tail] = true
        inst:ListenForEvent("onremove", function(tail) tails[tail] = nil end, tail)
        --[[tail:ListenForEvent("onremove", function(inst)
            tail.Transform:SetRotation(tail.Transform:GetRotation() + math.random() * 30 - 15)
        end, inst)]]
    end
end

local function test_fn()
	local inst = fn()
	
	inst.AnimState:SetAddColour(0, 0, 0.5, 0)
	
	if not TheWorld.ismastersim then
        return inst
    end

	inst.components.combat:SetRetargetFunction(1, RetargetFn_Test)
    inst.components.combat:SetKeepTargetFunction(KeepTarget_Test)
	inst.components.combat.keeptargettimeout = 8
	
	inst:ListenForEvent("attacked", OnAttacked_Test)
	inst.components.combat.onhitotherfn = OnHitOther
	
	return inst
end

----------
return ForgePrefab("scorpeon", fn, assets, prefabs, nil, "ENEMIES", false, "images/forged_forge.xml", "scorpeon_icon.tex", nil, TUNING.FORGE.SCORPEON, STRINGS.FORGED_FORGE.ENEMIES.SCORPEON.ABILITIES),
Prefab("scorpeon_projectile", projectile_fn, assets, prefabs),
Prefab("scorpeon_dot", dot_fn),
Prefab("scorpeon_splashfx", splashfx_fn),
Prefab("scorpeon_tail", CreateTail),
Prefab("targettest", test_fn)
