--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    --Asset("ANIM", "anim/lavaarena_trails_basic.zip"),
}

local prefabs =
{

}

------------------------------
--ToDo List--
------------------------------
--Adjust speed and hit/attack range of Boarilla to ensure you need speedboosts to dodge incoming punchs.
--Make small adjustments to CalcJumpSpeed in the SG to better mimic the slam ingame.
-----------------------------

local brain = require("brains/rhinocebrobrain")

SetSharedLootTable( 'rhinocebro',
{
    --{'monstermeat', 1.000},
})

local REPEL_RADIUS = 5
local REPEL_RADIUS_SQ = REPEL_RADIUS * REPEL_RADIUS

local function OnHitOther(inst, other) --knockback
	if inst.sg:HasStateTag("charging") then
	if other.sg and other.sg.sg.events.knockback then
		other:PushEvent("knockback", {knocker = inst, radius = 2})
	else
	--this stuff below is mostly left here for creatures in basegame. For modders that are reading this, use the knockback event above.
	if other ~= nil and not (other:HasTag("epic") or other:HasTag("largecreature")) then
		
		if other:IsValid() and other.entity:IsVisible() and not (other.components.health ~= nil and other.components.health:IsDead()) then
            if other.components.combat ~= nil then
                --other.components.combat:GetAttacked(inst, 10)
                if other.Physics ~= nil then
					local x, y, z = inst.Transform:GetWorldPosition()
                    local distsq = other:GetDistanceSqToPoint(x, 0, z)
					--if distsq < REPEL_RADIUS_SQ then
						if distsq > 0 then
							other:ForceFacePoint(x, 0, z)
						end
						local k = .5 * distsq / REPEL_RADIUS_SQ - 1
						other.speed = 60 * k
						other.dspeed = 2
						other.Physics:ClearMotorVelOverride()
						other.Physics:Stop()
						
						if other.components.inventory and other.components.inventory:ArmorHasTag("heavyarmor") or other:HasTag("heavybody") then 
						--Leo: Need to change this to check for bodyslot for these tags.
						other:DoTaskInTime(0.1, function(inst) 
							other.Physics:SetMotorVelOverride(-TUNING.FORGE.KNOCKBACK_RESIST_SPEED, 0, 0) 
						end)
						else
						other:DoTaskInTime(0, function(inst) 
							other.Physics:SetMotorVelOverride(-TUNING.FORGE.RHINOCEBRO.ATTACK_KNOCKBACK, 0, 0) 
						end)
						end
						other:DoTaskInTime(0.4, function(inst) 
						other.Physics:ClearMotorVelOverride()
						other.Physics:Stop()
						end)
					--end
                end
            end
        end
	end
	end
	end
end
-------------------------------------------------

local function FindPlayerTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
	if player then
		if not inst.components.follower.leader then --Don't attempt to find a target that isn't the leader's target. 
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) and player then
		return target
	else
		return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player and player
	end
end

local function OnAttackOther(inst, data)
    --inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("LA_mob") and dude.components.combat and not dude.components.combat.target and not dude.components.health:IsDead() end, 30)
end

local function GetReturnPos(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local rad = 2
    local angle = math.random() * 2 * PI
    return x + rad * math.cos(angle), y, z - rad * math.sin(angle)
end

local function DoReturn(inst)
    ----print("DoReturn", inst)
    if inst.components.homeseeker ~= nil and inst.components.homeseeker:HasHome() then
        if inst:HasTag("pet_hound") then
            if inst.components.homeseeker.home:IsAsleep() and not inst:IsNear(inst.components.homeseeker.home, HOME_TELEPORT_DIST) then
                inst.Physics:Teleport(GetReturnPos(inst.components.homeseeker.home))
            end
        elseif inst.components.homeseeker.home.components.childspawner ~= nil then
            inst.components.homeseeker.home.components.childspawner:GoHome(inst)
        end
    end
end

local function StartAltAttacking(inst) --Used to reapply alt_attack var when reloaded.
	if inst.is_enraged and inst.is_enraged == true then
		inst.altattack = true
		inst.components.combat:SetRange(TUNING.FORGE.RHINOCEBRO.JUMP_ATTACK_RANGE, 0)
	end
end

local function EnterPhase2Trigger(inst)
    inst.AnimState:AddOverrideBuild("lavaarena_rhinodrill_damaged"..inst.damagedtype)
end

-----------------------------
--Proximity based aggro

local function OnMissOther(inst)
	--don't count alt or general aoe attacks. Added isaoeattacking because Boarrior uses isaltattacking for knockback purposes.
	if not inst._isaltattacking and not inst._isaoeattacking then
		----print("Target Debug: Attack missed!")
		inst.attacks_missed = (inst.attacks_missed and inst.attacks_landed <= 9) and inst.attacks_landed + 1 or 1 
	end
end

local function OnDroppedTarget(inst)
	local playertargets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 50, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" }, {"player"} ) or {}
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not (inst.lasttarget and inst.lasttarget == v) then
			table.insert(playertargets, v)		
		end	
	end
	if not inst:HasTag("brainwashed") then
		--I assume first one in the table is the closest
		if #playertargets > 0 then
			inst.components.combat:SetTarget(playertargets[1])
		--Leo: This elseif check is just to make the mob not stop and continue on, making it look like the forge.
		elseif inst.lasttarget and inst.lasttarget.components and inst.lasttarget.components.health and not inst.lasttarget.components.health:IsDead() then
			inst.components.combat:SetTarget(inst.lasttarget)
		end
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
		
	end
end

local function RetargetFn(inst)
    local player, distsq = inst:GetNearestPlayer()
	if inst:HasTag("brainwashed") then
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "player", "companion", "brainwashed" })
	else
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not (inst.lastarget and inst.lasttarget == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
	end	
end

local function KeepTarget(inst, target)
	if inst:HasTag("brainwashed") then
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("player") and not target:HasTag("companion")
	else
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("LAmob") --inst:IsNear(target, 5)
	end
end

local function OnAttacked(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) then	
		if inst:HasTag("brainwashed") and not (data.attacker:HasTag("player") or data.attacker:HasTag("companion")) then
			inst.components.combat:SetTarget(data.attacker)
		elseif data.attacker:HasTag("brainwashed") and not inst:HasTag("brainwashed") then
			inst.components.combat:SetTarget(data.attacker)
		else
			local player, distsq = inst:GetNearestPlayer()	
			if player and player.components.health and not player.components.health:IsDead() and player == data.attacker then
				if inst.components.combat.target and not (data.attacker:HasTag("lessaggro") or inst:IsNear(inst.components.combat.target, 2.5)) then
					inst.components.combat:SetTarget(data.attacker)
				elseif not inst.components.combat.target then
					inst.components.combat:SetTarget(data.attacker)
				end		
			end
		end	
	end
end
----------------------------
local function CanBeRevivedBy(inst, reviver)
    return not reviver:HasTag("player") and reviver:HasTag("LA_mob")
end

local function fn(alt)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    inst.DynamicShadow:SetSize(2.75, 1.25)
    inst.Transform:SetSixFaced()
    inst.Transform:SetScale(1.15, 1.15, 1.15)
    inst:SetPhysicsRadiusOverride(1)
    MakeCharacterPhysics(inst, 400, inst.physicsradiusoverride)

    inst.AnimState:SetBank("rhinodrill")
    inst.AnimState:SetBuild("lavaarena_rhinodrill_basic")
    inst.AnimState:OverrideSymbol("fx_wipe", "wilson_fx", "fx_wipe")
    if alt and alt == true then
        inst.AnimState:AddOverrideBuild("lavaarena_rhinodrill_clothed_b_build")
    end
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.AnimState:AddOverrideBuild("fossilized")
	
	inst.nameoverride = alt ~= nil and alt == true and "rhinodrill2" or "rhinodrill" --So we don't have to make the describe strings.

    inst:AddTag("LA_mob")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("largecreature")

    --fossilizable (from fossilizable component) added to pristine state for optimization
    inst:AddTag("fossilizable")

    --inst._bufflevel = net_tinybyte(inst.GUID, "rhinodrill._bufflevel", "buffleveldirty")
    --inst._camerafocus = net_bool(inst.GUID, "rhinodrill._camerafocus", "camerafocusdirty")
    --inst._camerafocustask = nil
	
	inst:AddComponent("revivablecorpse")
    inst.components.revivablecorpse:SetCanBeRevivedByFn(CanBeRevivedBy)
	inst.components.revivablecorpse:SetReviveHealthPercent(0.2)
	inst.components.revivablecorpse:SetReviveSpeedMult(0.5)

    ------------------------------------------

	inst.deathevent = "truedeath"
    if TheWorld.components.lavaarenamobtracker ~= nil then
        TheWorld.components.lavaarenamobtracker:StartTracking(inst)
    end

        ------------------------------------------

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        --inst:ListenForEvent("buffleveldirty", OnBuffLevelDirty)
        --inst:ListenForEvent("camerafocusdirty", OnCameraFocusDirty)

        return inst
    end
	
	inst:AddComponent("fossilizable")

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	--inst.components.locomotor.walkspeed = TUNING.FORGE.RHINOCEBRO.WALKSPEED
    inst.components.locomotor.runspeed = TUNING.FORGE.RHINOCEBRO.RUNSPEED
    inst:SetStateGraph("SGrhinocebro")

    inst:SetBrain(brain)

    inst:AddComponent("follower")
	
	inst:AddComponent("bloomer")
	inst:AddComponent("colouradder")
	inst:AddComponent("colourfader")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE.RHINOCEBRO.HEALTH)
	inst.components.health.nofadeout = true

    --inst:AddComponent("sanityaura")
    --inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED
	
	inst.altattack = true
	inst.cancheer = false
	inst.brostacks = 0
	inst.cheer_loop = 0
	inst.rams = 0
	inst.deathevent = "truedeath"
	inst.damagedtype = "" --used to pick a specific build for damaged.
	
	inst:DoTaskInTime(TUNING.FORGE.RHINOCEBRO.CHEER_CD, function(inst) inst.cancheer = true end)
	--inst:DoTaskInTime(0, GetVariation)
	
	
	
	inst:AddComponent("healthtrigger")
	inst.components.healthtrigger:AddTrigger(0.3, EnterPhase2Trigger) 

    inst:AddComponent("combat")
	--inst.components.combat.playerdamagepercent = 0.5
	inst.components.combat:SetRange(TUNING.FORGE.RHINOCEBRO.ATTACK_RANGE, TUNING.FORGE.RHINOCEBRO.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(5, 1)
    inst.components.combat:SetDefaultDamage(TUNING.FORGE.RHINOCEBRO.DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.FORGE.RHINOCEBRO.ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(1, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat.battlecryenabled = false
	inst.components.combat:SetDamageType(DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("armorbreak_debuff")

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('rhinocebro')

    inst:AddComponent("inspectable")

    inst:AddComponent("sleeper")
	
	inst:AddComponent("debuffable")
	
	inst.components.combat.onhitotherfn = OnHitOther
	
    MakeHauntablePanic(inst)
	
	MakeMediumBurnableCharacter(inst, "body")
	
    inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
    --inst:ListenForEvent("onattackother", OnAttackOther)
	inst:DoTaskInTime(0.5, function(inst)
		local target = FindPlayerTarget(inst) or nil
		inst.components.combat:SetTarget(target)
	end)

    return inst
end

local function alt_fn()
	local inst = fn(true)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

local function OnTick(inst, target)
	inst.AnimState:PlayAnimation("attack_fx3")
end

local function OnAttached(inst, target)
    inst.entity:SetParent(target.entity)
	--inst.tickcount = 0
    inst.task = inst:DoPeriodicTask(5, OnTick, nil, target)
	
	local fx = SpawnPrefab("rhinocebro_bufffx")
	if fx then
	--fx.Transform:SetPosition(inst:GetPosition():Get())
	fx.entity:SetParent(target.entity)
    fx.entity:AddFollower()
	fx.Follower:FollowSymbol(target.GUID, "head", 0, 0, 0)
	fx.Transform:SetPosition(0, 0, 0)
	--fx.entity:SetParent(target.entity)
	fx.AnimState:PlayAnimation("in", false)
	fx.AnimState:PushAnimation("idle", false)
	fx.AnimState:PushAnimation("out", false)

	fx:DoTaskInTime(3, function(inst) inst:Remove() end)
	end
	
	if target.components.combat and target.brostacks then
		target.components.combat:AddDamageBuff("rhinocebro_buff", 1 + (0.25 * target.brostacks), false)
	end
	
    inst:ListenForEvent("death", function()
        --inst.components.debuff:Stop()
    end, target)
end

local function OnDetached(inst, target)
	if target.components.combat and target.components.combat.damagemultiplier and target.components.combat.damagemultiplier > 1 then
		target.brostacks = 0
		target.components.combat.damagemultiplier = 1
	end
	
	inst:Remove()
end

local function OnTimerDone(inst, data)
    if data.name == "rhinobuffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)

	local fx = SpawnPrefab("rhinocebro_bufffx")
	if fx then
	--fx.Transform:SetPosition(inst:GetPosition():Get())
	fx.entity:SetParent(target.entity)
    fx.entity:AddFollower()
	fx.Follower:FollowSymbol(target.GUID, "head", 0, 0, 0)
	fx.Transform:SetPosition(0, 0, 0)
	--fx.entity:SetParent(target.entity)
	fx.AnimState:PlayAnimation("in", false)
	fx.AnimState:PushAnimation("idle", false)
	fx.AnimState:PushAnimation("out", false)

	fx:DoTaskInTime(3, function(inst) inst:Remove() end)
	end
	
	target.components.combat:RemoveDamageBuff("rhinocebro_buff", false)
	if target.components.combat and target.brostacks then
		target.components.combat:AddDamageBuff("rhinocebro_buff", 1 + (0.25 * target.brostacks), false)
	end
	
    inst.task:Cancel()
	
    inst.task = inst:DoPeriodicTask(5, OnTick, nil, target)
end

local function fx_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_rhino_buff_effect")
    inst.AnimState:SetBuild("lavaarena_rhino_buff_effect")
    inst.AnimState:PlayAnimation("in")

    inst.Transform:SetSixFaced()

    inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false

	--fx:ListenForEvent("animqueueover", fx:Remove())
    

    return inst
end

local function dot_fn(inst)
	local inst = CreateEntity()
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()

	inst:AddTag("CLASSIFIED")
	inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
	
	

    inst.AnimState:SetBank("lavaarena_battlestandard")
    inst.AnimState:SetBuild("lavaarena_battlestandard")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
    inst:AddComponent("debuff")
    inst.components.debuff:SetAttachedFn(OnAttached)
    inst.components.debuff:SetDetachedFn(OnDetached)
    inst.components.debuff:SetExtendedFn(OnExtended)
	
	inst.nameoverride = "rhinobuff" --So we don't have to make the describe strings.
	
	return inst
	
end

return ForgePrefab("rhinocebro", fn, assets, prefabs, nil, "ENEMIES", false, "images/rhinocebro_icon.xml", "rhinocebro_icon.tex", nil, TUNING.FORGE.RHINOCEBRO, STRINGS.FORGED_FORGE.ENEMIES.RHINOCEBRO.ABILITIES),
ForgePrefab("rhinocebro2", alt_fn, assets, prefabs, nil, "ENEMIES", false, "images/rhinocebro_icon.xml", "rhinocebro_icon.tex", nil, TUNING.FORGE.RHINOCEBRO, STRINGS.FORGED_FORGE.ENEMIES.RHINOCEBRO.ABILITIES),
Prefab("rhinocebro_buff", dot_fn),
Prefab("rhinocebro_bufffx", fx_fn)