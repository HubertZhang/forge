--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA = {
	WILSON = { "forgedarts", "forge_woodarmor" },
	WILLOW = { "forgedarts", "featheredtunic" },
	WENDY = { "forgedarts", "featheredtunic" },
	WOLFGANG = { "forginghammer", "forge_woodarmor" },
	WX78 = { "forginghammer", "forge_woodarmor" },
	WICKERBOTTOM = { "petrifyingtome", "reedtunic" },
	WES = { "forgedarts", "featheredtunic" },
	WAXWELL = { "petrifyingtome", "reedtunic" },
	WOODIE = { "riledlucy", "forge_woodarmor" },
	WATHGRITHR = { "pithpike", "featheredtunic" },
	WEBBER = { "forgedarts", "featheredtunic" },
	WINONA = { "forginghammer", "forge_woodarmor" },
}

AddClassPostConstruct("screens/redux/lobbyscreen", function(self)
	for _,character in pairs(_G.MODCHARACTERLIST) do
		if not TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA[string.upper(character)] then
			local val = (math.abs(_G.hash(character)) % 4) + 1
			local possibleinvs = {[1] = "WATHGRITHR", [2] = "WILLOW", [3] = "WOLFGANG", [4] = "WICKERBOTTOM"}
			TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA[string.upper(character)] = TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA[possibleinvs[val]]
		end
		
		if not TUNING.LAVAARENA_STARTING_HEALTH[string.upper(character)] then
			TUNING.LAVAARENA_STARTING_HEALTH[string.upper(character)] = TUNING.LAVAARENA_STARTING_HEALTH.WILSON
		end
		
		if not TUNING.LAVAARENA_SURVIVOR_DIFFICULTY[string.upper(character)] then
			TUNING.LAVAARENA_SURVIVOR_DIFFICULTY[string.upper(character)] = TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.WILSON
		end

		if not STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS[character] then
			STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS[character] = "*Is a master in all arts.\n\n\n\nExpertise:\nMelee, Darts, Books, Staves, Etc."
		end
	end
end)

if not _G.TheNet:GetIsServer() or _G.TheNet:GetServerGameMode() ~= "lavaarena" then return end

AddPlayerPostInit(function(inst)
	inst:AddComponent("itemtyperestrictions")
	
	-- default is nil wtf make 1
	inst.components.combat.damagemultiplier = 1
	
	inst:AddComponent("buffable")
	
	inst.starting_inventory = TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA[string.upper(inst.prefab)] or TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.WILSON
	
	-- Set characters max health TODO might change this for api, but might not
	inst.components.health:SetMaxHealth(TUNING.LAVAARENA_STARTING_HEALTH[string.upper(inst.prefab)] or TUNING.LAVAARENA_STARTING_HEALTH.WILSON)
	-- Set health on revive
	inst.components.revivablecorpse:SetReviveHealthPercent(TUNING.FORGE.HEALTH_ON_REVIVE)
	-- Make character buffable
	--inst:AddComponent("buffable") -- This is in player_common master_postinit, should I put stat tracker there too?
	if not inst.components.colourfader then 
		inst:AddComponent("colourfader")
	end
	-- Damage Number Event
	--data = { target = self.inst, damage = damage, damageresolved = damageresolved, stimuli = stimuli, weapon = weapon, redirected = damageredirecttarget }
	inst:ListenForEvent("onhitother", function(self, data)
		if _G.Forge_options.damage_number_display == "all" then
			--print("[DamageNumber] All Players")
			for i, player in pairs(_G.AllPlayers) do
				player:SpawnChild("damage_number"):UpdateDamageNumbers(player, data.target, math.floor(data.damageresolved + 0.5), data.weapon and data.weapon.damage_type, data.stimuli, data.weapon and data.weapon.components.weapon.isaltattacking or false, self.playercolour)
			end
		else
			self:SpawnChild("damage_number"):UpdateDamageNumbers(self, data.target, math.floor(data.damageresolved + 0.5), data.weapon and data.weapon.damage_type, data.stimuli, data.weapon and data.weapon.components.weapon.isaltattacking or false)
		end
	end)
	
	local function GetSpawnOffset(index)
		if index == 1 then
			return _G.Vector3()
		else
			local angle = (index-1)/5 * 2 * _G.PI
			local pos = _G.TheWorld.multiplayerportal ~= nil and _G.TheWorld.multiplayerportal:GetPosition() or _G.TheWorld.components.lavaarenaevent:GetArenaCenterPoint()
			return _G.FindWalkableOffset(pos, angle, 1.5, 2, true, true)
		end
	end
	local function getplayerindexinplayertable(player)
		for k, v in pairs(_G.AllPlayers) do
			if v == player then
				return k
			end
		end
		return 1
	end
	local _OnNewSpawn_old = inst.OnNewSpawn
	inst.OnNewSpawn = function(inst)
		_G.TheWorld:PushEvent("lavaarena_portal_activate")
		if _OnNewSpawn_old then
			_OnNewSpawn_old(inst)
		end
		local index = getplayerindexinplayertable(inst)
		inst:Hide()
		inst.DynamicShadow:Enable(false)
		if inst.components.playercontroller ~= nil then
			inst.components.playercontroller:Enable(false)
		end
		local pos = _G.TheWorld.multiplayerportal and _G.TheWorld.multiplayerportal:GetPosition() or inst:GetPosition()
		local offset = GetSpawnOffset(index)
		if offset ~= nil then
			pos.x = pos.x + offset.x
			pos.z = pos.z + offset.z
		end
		inst:DoTaskInTime(0, function() inst.Transform:SetPosition(pos.x, 0, pos.z) end)
		inst:DoTaskInTime(1+0.2*index, function(inst)
			local fire = _G.SpawnPrefab("lavaarena_portal_player_fx")
			fire.Transform:SetPosition(pos.x, 0, pos.z)
			if inst:HasTag("largecreature") or inst:HasTag("epic") then --totally not for PP I swear! --Fid: Hmmmm are you sure?
				fire.Transform:SetScale(2, 2, 2)
			end
			inst:DoTaskInTime(0.3, function(inst)
				inst:Show()
				inst.DynamicShadow:Enable(true)
				
				if inst.forge_onspawnfn then
					--inst.forge_onspawnfn() --Leo: I'll test this when I get up later.
				end
				
				if inst.components.playercontroller ~= nil then
					inst.components.playercontroller:Enable(true)
				end
				if inst == _G.AllPlayers[#_G.AllPlayers] then
					_G.TheWorld:PushEvent("ms_lavaarena_allplayersspawned")
					_G.TheWorld:PushEvent("lavaarena_portal_disactivate")
				end
			end)
		end)
	end
	
	inst:ListenForEvent("death", function(inst)
		_G.TheWorld:PushEvent("ms_playerdied", inst)
	end)
	
	local _oldonspawnpet = inst.components.petleash.onspawnfn
    local _oldondespawnpet = inst.components.petleash.ondespawnfn
	local player = inst
	inst.components.petleash:SetOnSpawnFn(function(inst, pet)
	--Fid: Leo, make this more mod friendly pls n thx
	--Glass: How's this?
		if pet then
			if pet.ForgeOnSpawn then
				pet:ForgeOnSpawn()
			elseif not pet:HasTag("nospawnfx") then
				_oldonspawnpet(inst, pet)			
			end
		end
		
		-- Link damage numbers to pet leader
		pet:ListenForEvent("onhitother", function(inst, data) -- TODO need to add pet dmg type
			player:SpawnChild("damage_number"):UpdateDamageNumbers(player, data.target, math.floor(data.damageresolved + 0.5), nil, stimuli, false)
		end)
		
		-- Add stat tracking for pets
		if _G.TheWorld and _G.TheWorld.components.stat_tracker then
			_G.TheWorld.components.stat_tracker:InitializePetStats(pet)
		end
	end)
	inst.components.petleash:SetOnDespawnFn(function(inst, pet)
        if pet then
			if pet.ForgeOnDespawn then
				pet:ForgeOnDespawn()
			elseif not pet:HasTag("nospawnfx") then
				_oldondespawnpet(inst, pet)
			end
        end
    end)
	
	local function MakeBernieDance(inst, data) --Isn't this a Willow only thing??? --Leo: Yes. --Fid: Then why is it here and not inside the if prefab == "willow"? --Leo: because FUCK YOU DA-- idk good point, maybe leave it for other players that might want to use bernie?
		if inst.components.petleash.numpets > 0 then --Fid: But Leo....... It's a local function.
			local followers = inst.components.petleash:GetPets()
			for i,v in pairs(followers) do 
				if v.sg and not (v.sg:HasStateTag("busy") or v.sg:HasStateTag("deactivated")) and not v.components.health:IsDead() and v.prefab == "forge_bernie" then
					v.sg:GoToState("taunt")
				end			
			end
		end	
	end
	
	
	
	
	if inst.prefab == "wilson" then
		inst.components.itemtyperestrictions:SetRestrictions("books")
		--Revive bonus
		inst:AddComponent("corpsereviver")
		inst.components.corpsereviver:SetReviverSpeedMult(TUNING.FORGE.WILSON.BONUS_REVIVE_SPEED_MULTIPLIER)
		inst.components.corpsereviver:SetAdditionalReviveHealthPercent(TUNING.FORGE.WILSON.BONUS_HEALTH_ON_REVIVE_PERCENT)
	end
	
	
	
	
	if inst.prefab == "willow" then
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "books"})
		inst.components.combat:AddDamageBuff("passive_explosive", {buff = 1.1, stimuli = "explosive"})
		inst.components.combat:AddDamageBuff("passive_fire", {buff = 1.1, stimuli = "fire"})
		--Spawn Bernie
		inst:AddTag("bernie_reviver")
		inst:DoTaskInTime(0, function(inst)
			if inst.components.petleash.numpets == 0 then
				inst.components.petleash.petprefab = "forge_bernie"
				local pos = inst:GetPosition()	
				local pet = inst.components.petleash:SpawnPetAt(pos.x + math.random(-3, 3), 0, pos.z + math.random(-3, 3))
				inst.components.pethealthbar:SetPet(pet, "pet_bernie", 1000)
			end
		end)
		inst:ListenForEvent("attacked", MakeBernieDance)
	end
	
	
	
	
	if inst.prefab == "wendy" then
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "books"})
		--Spawn Abigail
		inst.components.petleash.maxpets = 2 --needed to be 2 to allow transitions between flower stage and ghost stage.
		inst:DoTaskInTime(0, function(inst)
			if not inst.abigail and inst.components.petleash.numpets == 0 then
				inst.components.petleash.petprefab = "forge_abigail"
				local pos = inst:GetPosition()	
				local pet = inst.components.petleash:SpawnPetAt(pos.x + math.random(-3, 3), 0, pos.z + math.random(-3, 3))
				inst.components.pethealthbar:SetPet(pet, "pet_abigail", TUNING.FORGE.ABIGAIL.HEALTH)
			end
		end)
	end
	
	
	
	
	if inst.prefab == "wolfgang" then
		inst.components.itemtyperestrictions:SetRestrictions({"darts", "staves", "books"})
		inst:AddComponent("passive_mighty")
	end
	
	
	
	
	if inst.prefab == "wx78" then
		inst.components.itemtyperestrictions:SetRestrictions({"darts", "staves", "books"})
		inst.components.combat:AddDamageBuff("passive_electric", {buff = 1.5, stimuli = "electric"})
		inst:AddComponent("passive_shock")
	end
	
	
	
	
	if inst.prefab == "wickerbottom" then
		inst.components.itemtyperestrictions:SetRestrictions({"darts", "melees"})
		inst:AddComponent("passive_amplify")
	end
	
	
	
	
	if inst.prefab == "wes" then
		inst.components.itemtyperestrictions:SetRestrictions("books")
		--Revived twice as fast with half health buff
		inst.components.revivablecorpse:SetReviveSpeedMult(0.5)
		inst:AddTag("lessaggro")
		inst:AddTag("moreaggro")
	end
	
	
	
	
	if inst.prefab == "waxwell" then
		inst.components.itemtyperestrictions:SetRestrictions({"darts", "melees"})
		--Program Shadow Duelist code
		inst:AddComponent("passive_shadows")
	end
	
	
	
	
	if inst.prefab == "woodie" then
		inst.components.itemtyperestrictions:SetRestrictions({"darts", "staves", "books"})
	end
	
	
	
	
	if inst.prefab == "wathgrithr" then
		inst.components.itemtyperestrictions:SetRestrictions({"staves", "books"})
		inst:AddComponent("passive_battlecry")
		inst:AddTag("lessaggro")
	end
	
	
	
	
	if inst.prefab == "webber" then
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "staves", "books"})
		--Baby Spiders
		inst.components.petleash.maxpets = 3
		inst.components.petleash.petprefab = "babyspider"
		
		inst:DoTaskInTime(0, function(inst) 
			for i = 1, TUNING.FORGE.BABY_SPIDERS.AMOUNT or 1 do
				local theta = math.random() * 2 * _G.PI
				local pt = inst:GetPosition()
				local radius = math.random(3, 6)
				local offset = _G.FindWalkableOffset(pt, theta, radius, 3, true, true, NoHoles)
					if offset ~= nil then
						pt.x = pt.x + offset.x
						pt.z = pt.z + offset.z
					end
				local pet = inst.components.petleash:SpawnPetAt(pt.x, 0, pt.z)
			end
		end)
	end
	
	
	
	
	if inst.prefab == "winona" then
		inst.components.itemtyperestrictions:SetRestrictions("books")
		inst.components.buffable:AddBuff(inst.prefab .. "_passive", {{name = "cooldown", val = -0.1, type = "add"}})
	end
	
	
	
	
	--For mod characters
	if inst.forge_fn then
		inst:forge_fn()
	end
end) 