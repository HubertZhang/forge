--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    Asset("ANIM", "anim/lavaarena_boaron_basic.zip"),
}

local prefabs =
{

}

local brain = require("brains/boaronbrain")

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }

local function FindPlayerTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
	if player then
		if not inst.components.follower.leader then --Don't attempt to find a target that isn't the leader's target.		
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player
		end
	end
end

local function AttemptNewTarget(inst, target)
	if inst:HasTag("brainwashed") then
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "player", "companion" })
	else
		local player, distsq = inst:GetNearestPlayer()
		if target ~= nil and inst:IsNear(target, 20) and player then
			return target
		else
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player and player
		end
	end
end

local function OnDroppedTarget(inst)
	local playertargets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 50, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" }, {"player"} ) or {}
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not (inst.lasttarget and inst.lasttarget == v) then
			table.insert(playertargets, v)		
		end	
	end
	
	if not inst:HasTag("brainwashed") then
		--I assume first one in the table is the closest
		if #playertargets > 0 then
			inst.components.combat:SetTarget(playertargets[1])
		--Leo: This elseif check is just to make the mob not stop and continue on, making it look like the forge.
		elseif inst.lasttarget and inst.lasttarget.components and inst.lasttarget.components.health and not inst.lasttarget.components.health:IsDead() then
			inst.components.combat:SetTarget(inst.lasttarget)
		end
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
	end
end

local function RetargetFn(inst)
	if inst:HasTag("brainwashed") then
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "player", "companion", "brainwashed"})
	else
		local player, distsq = inst:GetNearestPlayer()
		if inst.components.follower and inst.components.follower.leader then
			local leader = inst.components.follower.leader
			return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and (leader and leader.components.combat and leader.components.combat.target == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
		else
			return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not (inst.lastarget and inst.lasttarget == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
		end
	end
end

local function KeepTarget(inst, target)
	if inst:HasTag("brainwashed") then
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("player") and not target:HasTag("companion")
	else
		if inst.components.follower and inst.components.follower.leader then
			local leader = inst.components.follower.leader
			return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and (leader and leader.components.combat and leader.components.combat.target == target) and not target:HasTag("LAmob")
		else
			return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("LAmob") --inst:IsNear(target, 5)
		end	
	end
end

local function OnAttacked(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) and not inst.components.follower.leader then
		if inst:HasTag("brainwashed") and not (data.attacker:HasTag("player") or data.attacker:HasTag("companion")) then
			inst.components.combat:SetTarget(data.attacker)
		elseif data.attacker:HasTag("brainwashed") and not inst:HasTag("brainwashed") then
			inst.components.combat:SetTarget(data.attacker)
		else
			local player, distsq = inst:GetNearestPlayer()	
			if player and player.components.health and not player.components.health:IsDead() and player == data.attacker then
				if inst.components.combat.target and not (data.attacker:HasTag("lessaggro") or inst:IsNear(inst.components.combat.target, TUNING.FORGE.KEEP_AGGRO_MELEE_DIST)) then
					inst.components.combat:SetTarget(data.attacker)
				elseif not inst.components.combat.target then
					inst.components.combat:SetTarget(data.attacker)
				end		
			end
		end
	end	
end

local function OnAttackOther(inst, data)
    --inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("LA_mob") and dude.components.combat and not dude.components.combat.target and not dude.components.health:IsDead() end, 30)
end

local function GetReturnPos(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local rad = 2
    local angle = math.random() * 2 * PI
    return x + rad * math.cos(angle), y, z - rad * math.sin(angle)
end

local function DoReturn(inst)
    ----print("DoReturn", inst)
    if inst.components.homeseeker ~= nil and inst.components.homeseeker:HasHome() then
        if inst:HasTag("pet_hound") then
            if inst.components.homeseeker.home:IsAsleep() and not inst:IsNear(inst.components.homeseeker.home, HOME_TELEPORT_DIST) then
                inst.Physics:Teleport(GetReturnPos(inst.components.homeseeker.home))
            end
        elseif inst.components.homeseeker.home.components.childspawner ~= nil then
            inst.components.homeseeker.home.components.childspawner:GoHome(inst)
        end
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 50, .5)

    inst.DynamicShadow:SetSize(1.75, 0.75)
	inst.Transform:SetScale(0.8, 0.8, 0.8)
    inst.Transform:SetSixFaced()

    inst:AddTag("scarytoprey")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("LA_mob")
	
	--fossilizable (from fossilizable component) added to pristine state for optimization
    inst:AddTag("fossilizable")
	
    inst.AnimState:SetBank("boaron")
    inst.AnimState:SetBuild("lavaarena_boaron_basic")
    inst.AnimState:PlayAnimation("idle_loop", true)
	
	inst.AnimState:AddOverrideBuild("fossilized")

    inst:AddComponent("spawnfader")
	
	inst.nameoverride = "boaron" --So we don't have to make the describe strings.
	
	if TheWorld.components.lavaarenamobtracker ~= nil then
        TheWorld.components.lavaarenamobtracker:StartTracking(inst)
    end

    inst.entity:SetPristine()
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("fossilizable")
	
	

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = TUNING.FORGE.PITPIG.RUNSPEED
    inst:SetStateGraph("SGpitpig")

    inst:SetBrain(brain)

    inst:AddComponent("follower")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE.PITPIG.HEALTH)

    --inst:AddComponent("sanityaura")
    --inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED
	
	inst.alt_attack = false
	inst:DoTaskInTime(TUNING.FORGE.PITPIG.RAM_CD, function(inst) inst.alt_attack = true end)

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.FORGE.PITPIG.DAMAGE)
	inst.components.combat:SetRange(TUNING.FORGE.PITPIG.ATTACK_RANGE, TUNING.FORGE.PITPIG.HIT_RANGE)
	inst.components.combat.playerdamagepercent = 1
    inst.components.combat:SetAttackPeriod(TUNING.FORGE.PITPIG.ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(2, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat:SetDamageType(DAMAGETYPES.PHYSICAL)
    --inst.components.combat:SetHurtSound("dontstarve/creatures/hound/hurt")
	
	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")
	
	inst:AddComponent("armorbreak_debuff")

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('hound')

    inst:AddComponent("inspectable")

    inst:AddComponent("sleeper")
	
	inst:AddComponent("debuffable")
	
    MakeHauntablePanic(inst)
	
	MakeMediumBurnableCharacter(inst, "bod")

	inst.AttemptNewTarget = AttemptNewTarget

	inst:DoTaskInTime(0.5, function(inst)
		local target = FindPlayerTarget(inst) or nil
		inst.components.combat:SetTarget(target)
	end)
    inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
    --inst:ListenForEvent("onattackother", OnAttackOther)

    return inst
end

return ForgePrefab("pitpig", fn, assets, prefabs, nil, "ENEMIES", false, "images/forged_forge.xml", "pitpig_icon.tex", nil, TUNING.FORGE.PITPIG, STRINGS.FORGED_FORGE.ENEMIES.PITPIG.ABILITIES)