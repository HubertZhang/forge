--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    
}

local prefabs =
{

}

local brain = require("brains/boarriorbrain")

SetSharedLootTable( 'boarrior',
{

})

local WAKE_TO_FOLLOW_DISTANCE = 8
local SLEEP_NEAR_HOME_DISTANCE = 10
local SHARE_TARGET_DIST = 30
local HOME_TELEPORT_DIST = 30

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }

local function OnNewTarget(inst, data)
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local function FindPlayerTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
	if player then
		if not inst.components.follower.leader then --Don't attempt to find a target that isn't the leader's target. 
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) and player then
		return target
	else
		return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player and player
	end
end

local function retargetfn(inst)
    local player, distsq = inst:GetNearestPlayer()
    return (player and not player.components.health:IsDead()) and player or FindEntity(inst, 255, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and inst.components.follower.leader ~= guy end, nil, { "wall", "LA_mob", "battlestandard" })
end

-------------------------------------------------------
--Knockback--
local REPEL_RADIUS = 5
local REPEL_RADIUS_SQ = REPEL_RADIUS * REPEL_RADIUS

local function OnHitOther(inst, other) --knockback
	if not inst.is_doing_special then
	if other.sg and other.sg.sg.events.knockback then
	other:PushEvent("knockback", {knocker = inst, radius = 5})
	else
	--this stuff below is mostly left here for creatures in basegame. For modders that are reading this, use the knockback event above.
	if other ~= nil and not (other:HasTag("epic") or other:HasTag("largecreature")) then
		
		if other:IsValid() and other.entity:IsVisible() and not (other.components.health ~= nil and other.components.health:IsDead()) then
            if other.components.combat ~= nil then
                --other.components.combat:GetAttacked(inst, 10)
                if other.Physics ~= nil then
					local x, y, z = inst.Transform:GetWorldPosition()
                    local distsq = other:GetDistanceSqToPoint(x, 0, z)
					--if distsq < REPEL_RADIUS_SQ then
						if distsq > 0 then
							other:ForceFacePoint(x, 0, z)
						end
						local k = .5 * distsq / REPEL_RADIUS_SQ - 1
						other.speed = 60 * k
						other.dspeed = 2
						other.Physics:ClearMotorVelOverride()
						other.Physics:Stop()
						
						if other.components.inventory and other.components.inventory:ArmorHasTag("heavyarmor") or other:HasTag("heavybody") then 
						--Leo: Need to change this to check for bodyslot for these tags.
						other:DoTaskInTime(0.1, function(inst) 
							other.Physics:SetMotorVelOverride(-TUNING.FORGE.KNOCKBACK_RESIST_SPEED, 0, 0) 
						end)
						else
						other:DoTaskInTime(0, function(inst) 
							other.Physics:SetMotorVelOverride(-TUNING.FORGE.BOARILLA.ATTACK_KNOCKBACK, 0, 0) 
						end)
						end
						other:DoTaskInTime(0.4, function(inst) 
						other.Physics:ClearMotorVelOverride()
						other.Physics:Stop()
						end)
					--end
                end
            end
        end
	end
	end
	end
end

-----------------------------------------------------

local function EnterPhase2Trigger(inst)
	inst.level = 1
	inst.altattack = true
	inst.sg:GoToState("taunt")
end

local function EnterPhase3Trigger(inst)
	if inst.level < 2 then
	inst.level = 2
	inst.altattack = true
	inst.sg.statemem.wants_to_banner = true 
	inst.sg:GoToState("banner_pre")
	end
end

local function EnterPhase4Trigger(inst)
	inst.level = 3
	inst.components.combat:SetAttackPeriod(3)
end

-------------------------------------------------------
--Slam Attack Functions--

local groundlifts =
{
}

function DoTrail(inst, targetposx, targetposz, trailend)
	inst.stopslam = nil --might fix no slam bug?
	local startingpos = inst:GetPosition()
	inst:ForceFacePoint(targetposx, 0, targetposz)
	--if TheWorld.Map:IsAboveGroundAtPoint(targetposx, 0, targetposz) and TheWorld.Pathfinder:IsClear(startingpos.x, startingpos.y, startingpos.z, targetposx, 0, targetposz, {ignorewalls = false, ignorecreep = true}) then
		local targetpos = {x = targetposx, y = 0, z = targetposz}
		local found_players = {}
		
		local angle = -inst.Transform:GetRotation() * DEGREES
		local angled_offset = {x = 1.25 * math.cos(angle+90), y = 0, z = 1.25 * math.sin(angle+90)}
		local angled_offset2 = {x = 1.25 * math.cos(angle-90), y = 0, z = 1.25 * math.sin(angle-90)}
		local impact_distance = 36
					
		local maxtrails = 12 + 5
		for i = 1, maxtrails do
			inst:DoTaskInTime(FRAMES*math.ceil(1+i/ (trailend and 1.5 or 3.5)), function()
				local offset = (targetpos - startingpos):GetNormalized()*(i)
				local x, y, z = (startingpos + offset):Get()
				if TheWorld.Map:IsAboveGroundAtPoint((startingpos+offset):Get()) and not inst.stopslam then
					if i > 6 then
						SpawnPrefab(trailend ~= nil and "lavaarena_groundlift" or "lavaarena_groundliftrocks").Transform:SetPosition((startingpos+offset+angled_offset):Get())
						SpawnPrefab(trailend ~= nil and "lavaarena_groundlift" or "lavaarena_groundliftrocks").Transform:SetPosition((startingpos+offset+angled_offset2):Get())
						if not inst:HasTag("groundspike") then
							inst:AddTag("groundspike")
						end
					end
					local ents = TheSim:FindEntities(x, y, z, 2, { "locomotor" }, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or { "LA_mob", "fossil", "shadow", "playerghost", "INLIMBO" })
					for _,ent in ipairs(ents) do
						if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and not ent.hasbeenhit then
							inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
							--Leo:Temporary fix for preventing multiple hits.
							ent.components.combat:GetAttacked(inst, i < 6 and inst.components.combat:CalcDamage(ent) or (inst.components.combat:CalcDamage(ent) * 0.75 * (not trailend and 0.5 or 1)))
							ent.hasbeenhit = true
							ent:DoTaskInTime(0.25, function(inst) inst.hasbeenhit = nil end)
						--SpawnPrefab("forgespear_fx"):SetTarget(ent)
						end
					end
				else
					inst.stopslam = true
				end	
				if i == maxtrails then
					inst.stopslam = nil
				end
			end)
		end
	--end
end
--[[
inst:DoTaskInTime(FRAMES*math.ceil(1+i/ (trailend and 1.5 or 3.5)), function()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local angled_offset = {x = 1.25 * math.cos(angle+90), y = 0, z = 1.25 * math.sin(angle+90)}
	local angled_offset2 = {x = 1.25 * math.cos(angle-90), y = 0, z = 1.25 * math.sin(angle-90)}
	local offset = (targetpos - startingpos):GetNormalized()*(6+i*1.1)
	if TheWorld.Map:IsAboveGroundAtPoint((startingpos+offset):Get()) and not inst.stopslam then
		SpawnPrefab(trailend ~= nil and "lavaarena_groundlift" or "lavaarena_groundliftrocks").Transform:SetPosition((startingpos+offset+angled_offset):Get()) 
		SpawnPrefab(trailend ~= nil and "lavaarena_groundlift" or "lavaarena_groundliftrocks").Transform:SetPosition((startingpos+offset+angled_offset2):Get()) 
		local x, y, z = (startingpos + offset):Get()
		local ents = TheSim:FindEntities(x, y, z, 2.5, { "locomotor" }, { "LA_mob", "shadow", "playerghost", "INLIMBO" })
		for _,ent in ipairs(ents) do
			if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health then
				inst:PushEvent("onareaattackother", { target = ent--, weapon = self.inst, stimuli = self.stimuli })
				--Leo:Temporary fix for preventing multiple hits.
				if not ent.hasbeenhit then
				ent.components.combat:GetAttacked(inst, 200)
				ent.hasbeenhit = true
				ent:DoTaskInTime(0.25, function(inst) inst.hasbeenhit = nil end)
				end
			--SpawnPrefab("forgespear_fx"):SetTarget(ent)
			end
		end


--]]
-------------------------------------------------------


local function OnDroppedTarget(inst)
	local playertargets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 50, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" }, {"player"} ) or {}
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not (inst.lasttarget and inst.lasttarget == v) then
			table.insert(playertargets, v)		
		end	
	end
	if not inst:HasTag("brainwashed") then
		--I assume first one in the table is the closest
		if #playertargets > 0 then
			inst.components.combat:SetTarget(playertargets[1])
		--Leo: This elseif check is just to make the mob not stop and continue on, making it look like the forge.
		elseif inst.lasttarget and inst.lasttarget.components and inst.lasttarget.components.health and not inst.lasttarget.components.health:IsDead() then
			inst.components.combat:SetTarget(inst.lasttarget)
		end
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	----print("DEBUG: OnNewTarget is running")
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
		local x, y, z = data.target.Transform:GetWorldPosition()
		local distsq = inst:GetDistanceSqToPoint(x, y, z)
		if distsq and distsq > 30 and distsq < 130 and inst.components.health:GetPercent() <= TUNING.FORGE.BOARRIOR.PHASE2_TRIGGER and not inst.slamhardcooldown then
			inst.wants_to_slam = data.target
			--print(inst.wants_to_slam.prefab)
		end
	end
end

local function RetargetFn(inst)
    local player, distsq = inst:GetNearestPlayer()
	if inst:HasTag("brainwashed") then
		return FindEntity(inst, 150, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "player", "companion", "brainwashed" })
	else
		return FindEntity(inst, 150, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not (inst.lastarget and inst.lasttarget == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
	end	
end

local function KeepTarget(inst, target)
	if inst:HasTag("brainwashed") then
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("player") and not target:HasTag("companion")
	else
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("LA_mob") --inst:IsNear(target, 5)
	end
end

local function OnAttacked(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) then	
		if inst:HasTag("brainwashed") and not (data.attacker:HasTag("player") or data.attacker:HasTag("companion")) then
			inst.components.combat:SetTarget(data.attacker)
		elseif data.attacker:HasTag("brainwashed") and not inst:HasTag("brainwashed") then
			inst.components.combat:SetTarget(data.attacker)
		else
			local player, distsq = inst:GetNearestPlayer()	
			if player and player.components.health and not player.components.health:IsDead() and player == data.attacker then
				if inst.components.combat.target and not (data.attacker:HasTag("lessaggro") or inst:IsNear(inst.components.combat.target, 2.5)) then
					inst.components.combat:SetTarget(data.attacker)
				elseif not inst.components.combat.target then
					inst.components.combat:SetTarget(data.attacker)
				end		
			end
		end	
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 500, 1.5)

	inst.DynamicShadow:SetSize(5.25, 1.75)
    inst.Transform:SetFourFaced()

    inst:AddTag("hostile")
    inst:AddTag("epic")
    inst:AddTag("LA_mob")
	
    inst:AddTag("fossilizable")

    inst.AnimState:SetBank("boarrior")
    inst.AnimState:SetBuild("lavaarena_boarrior_basic")
    inst.AnimState:PlayAnimation("idle_loop", true)
	
	inst.AnimState:AddOverrideBuild("fossilized")
	
	inst.nameoverride = "boarrior" --So we don't have to make the describe strings.
	
	if TheWorld.components.lavaarenamobtracker ~= nil then
        TheWorld.components.lavaarenamobtracker:StartTracking(inst)
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("fossilizable")
	
	

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.walkspeed = TUNING.FORGE.BOARRIOR.WALKSPEED
    inst.components.locomotor.runspeed = TUNING.FORGE.BOARRIOR.RUNSPEED
    inst:SetStateGraph("SGboarrior")

    inst:SetBrain(brain)

    inst:AddComponent("follower")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE.BOARRIOR.HEALTH)
	if TheWorld.components.lavaarenaevent:GetCurrentRound() == #TheWorld.components.lavaarenaevent.rounds_data then
		inst.components.health.nofadeout = true
	else
		inst.components.health.destroytime = 5
	end

    --inst:AddComponent("sanityaura")
    --inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED
	
	inst.damagetaken = 0
	inst.altattack = true
	inst.altattack2 = true
	inst.altattack3 = true
	inst.level = 0
	inst.knockback = TUNING.FORGE.BOARRIOR.ATTACK_KNOCKBACK

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.FORGE.BOARRIOR.DAMAGE)
	inst.components.combat:SetRange(TUNING.FORGE.BOARRIOR.ATTACK_RANGE, TUNING.FORGE.BOARRIOR.HIT_RANGE)
	inst.components.combat.playerdamagepercent = 1
    inst.components.combat:SetAttackPeriod(TUNING.FORGE.BOARRIOR.ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(2, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat.battlecryenabled = false
	inst.components.combat:SetDamageType(DAMAGETYPES.PHYSICAL)
    --inst.components.combat:SetHurtSound("dontstarve/creatures/hound/hurt")
	
	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")
	
	inst:AddComponent("armorbreak_debuff")
	inst.components.armorbreak_debuff:SetFollowSymbol("head")
	
    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('hound')

    inst:AddComponent("inspectable")

    inst:AddComponent("sleeper")
	
	inst:AddComponent("debuffable")
	
    MakeHauntablePanic(inst)
	
	MakeMediumBurnableCharacter(inst, "bod")

	--inst.StartTrail = StartTrail
	--inst.EndTrail = EndTrail
	inst.DoTrail = DoTrail
	
	inst.components.combat.onhitotherfn = OnHitOther
	
	inst:AddComponent("healthtrigger")
	inst.components.healthtrigger:AddTrigger(TUNING.FORGE.BOARRIOR.PHASE2_TRIGGER, EnterPhase2Trigger)
	inst.components.healthtrigger:AddTrigger(TUNING.FORGE.BOARRIOR.PHASE3_TRIGGER, EnterPhase3Trigger)
	inst.components.healthtrigger:AddTrigger(TUNING.FORGE.BOARRIOR.PHASE4_TRIGGER, EnterPhase4Trigger)
	
	inst.hit_recovery = 0.75
	
	--inst.AttemptNewTarget = AttemptNewTarget

    inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
    --inst:ListenForEvent("onattackother", OnAttackOther)

    return inst
end

return ForgePrefab("boarrior", fn, assets, prefabs, nil, "ENEMIES", false, "images/forged_forge.xml", "boarrior_icon.tex", nil, TUNING.FORGE.BOARRIOR, STRINGS.FORGED_FORGE.ENEMIES.BOARRIOR.ABILITIES)