--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{

}

local prefabs =
{

}

local brain = require("brains/snortoisebrain")

SetSharedLootTable( 'snortoise',
{

})

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }
local LAUNCH_SPEED = .2

local function OnNewTarget(inst, data)
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local function FindPlayerTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
	if player then
		if not inst.components.follower.leader then --Don't attempt to find a target that isn't the leader's target. 
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player
		end
	end
end

local function OnDroppedTarget(inst)
	local playertargets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 50, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" }, {"player"} ) or {}
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not (inst.lasttarget and inst.lasttarget == v) then
			table.insert(playertargets, v)		
		end	
	end
	if not inst:HasTag("brainwashed") then
		--I assume first one in the table is the closest
		if #playertargets > 0 then
			inst.components.combat:SetTarget(playertargets[1])
		--Leo: This elseif check is just to make the mob not stop and continue on, making it look like the forge.
		elseif inst.lasttarget and inst.lasttarget.components and inst.lasttarget.components.health and not inst.lasttarget.components.health:IsDead() then
			inst.components.combat:SetTarget(inst.lasttarget)
		end
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
		
	end
end

local function RetargetFn(inst)
    local player, distsq = inst:GetNearestPlayer()
	if inst:HasTag("brainwashed") then
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "player", "companion", "brainwashed" })
	else
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not (inst.lastarget and inst.lasttarget == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
	end	
end

local function KeepTarget(inst, target)
	if inst:HasTag("brainwashed") then
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("player") and not target:HasTag("companion")
	else
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("LAmob") --inst:IsNear(target, 5)
	end
end

local function OnAttacked(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) then	
		if inst:HasTag("brainwashed") and not (data.attacker:HasTag("player") or data.attacker:HasTag("companion")) then
			inst.components.combat:SetTarget(data.attacker)
		elseif data.attacker:HasTag("brainwashed") and not inst:HasTag("brainwashed") then
			inst.components.combat:SetTarget(data.attacker)
		else
			local player, distsq = inst:GetNearestPlayer()	
			if player and player.components.health and not player.components.health:IsDead() and player == data.attacker then
				if inst.components.combat.target and not (data.attacker:HasTag("lessaggro") or inst:IsNear(inst.components.combat.target, 2.5)) then
					inst.components.combat:SetTarget(data.attacker)
				elseif not inst.components.combat.target then
					inst.components.combat:SetTarget(data.attacker)
				end		
			end
		end	
	end
end

local function OnAttackOther(inst, data)
    
end

-------------------------------------
--Shell Attack Stuff--
-------------------------------------
local function FindTarget(inst)
	if inst.components.combat.target ~= nil then
		return inst.components.combat.target
	else
		local player, distsq = inst:GetNearestPlayer()
		if player then
			return distsq ~= nil and distsq < 225 and not (player:HasTag("notarget") or player:HasTag("LA_mob")) and player or 
			FindEntity(inst, 60, function(guy) return inst.components.combat:CanTarget(guy) end, nil, { "wall", "LA_mob", "battlestandard" })
		end
	end
end

local function SetShellPhysics(inst)
	ToggleOffCharacterCollisions(inst)
end

local function SetNormalPhysics(inst)
	ToggleOnCharacterCollisions(inst)
end

-------------------------------------


local function OnEntitySleep(inst)
    ----print("OnEntitySleep", inst)
    if not TheWorld.state.isday then
        DoReturn(inst)
    end
end

local function OnStopDay(inst)
    ----print("OnStopDay", inst)
    if inst:IsAsleep() then
        DoReturn(inst)
    end
end

local function OnSpawnedFromHaunt(inst)
    if inst.components.hauntable ~= nil then
        inst.components.hauntable:Panic()
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()
	inst:SetPhysicsRadiusOverride(0.8)
    MakeCharacterPhysics(inst, 150, 0.8)
	inst.DynamicShadow:SetSize(2.5, 1.75)
	inst.Transform:SetScale(0.8, 0.8, 0.8)
    inst.Transform:SetSixFaced()
	
	inst:AddTag("flippable")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("LA_mob")

    --fossilizable (from fossilizable component) added to pristine state for optimization
    inst:AddTag("fossilizable")

    inst.AnimState:SetBank("turtillus")
    inst.AnimState:SetBuild("lavaarena_turtillus_basic")
    inst.AnimState:PlayAnimation("idle_loop", true)
	
	inst.AnimState:AddOverrideBuild("fossilized")

    inst:AddComponent("spawnfader")
	
	inst.nameoverride = "turtillus"
	
	if TheWorld.components.lavaarenamobtracker ~= nil then
        TheWorld.components.lavaarenamobtracker:StartTracking(inst)
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
    --inst.Physics:SetCollisionCallback(oncollide)

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = TUNING.FORGE.SNORTOISE.RUNSPEED
	
    inst:AddComponent("fossilizable")
	
    inst:SetStateGraph("SGsnortoise")
	inst.sg.mem.radius = inst.physicsradiusoverride

    inst:SetBrain(brain)

	inst.SetShellPhysics = SetShellPhysics
    inst.SetNormalPhysics = SetNormalPhysics
	inst.FindTarget = FindTarget
	
    inst:AddComponent("follower")

	inst:AddComponent("item_launcher")
	--inst:AddComponent("fossilizable")
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE.SNORTOISE.HEALTH)
	
	inst.altattack = true
	inst.is_enraged = false
	inst.canshield = nil
	inst.should_taunt = true
	
	inst.ram_attempt = 0 --this is used for the placeholder shell attack.
	
	inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)

    inst:AddComponent("combat")
	--inst.components.combat.playerdamagepercent = 0.5
	inst.components.combat:SetRange(TUNING.FORGE.SNORTOISE.ATTACK_RANGE, TUNING.FORGE.SNORTOISE.HIT_RANGE)
	inst.components.combat:SetAreaDamage(TUNING.FORGE.SNORTOISE.HIT_RANGE, 1)
    inst.components.combat:SetDefaultDamage(TUNING.FORGE.SNORTOISE.DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.FORGE.SNORTOISE.ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(2, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat:SetDamageType(DAMAGETYPES.PHYSICAL)
	--I know this looks horrifyingly disgusting but we're running out of ideas here....
	local _oldGetAttacked = inst.components.combat.GetAttacked
	inst.components.combat.GetAttacked = function(self, attacker, damage, weapon, stimuli)
		local absorb = 0
		if self.inst.sg:HasStateTag("hiding") or self.inst.sg:HasStateTag("spinning") then
			if not (stimuli == "explosive" or stimuli == "electric" or stimuli == "acid") then
				absorb = 1
			end
		end
		if self.inst.components.health.absorb ~= absorb then self.inst.components.health:SetAbsorptionAmount(absorb) end
		return _oldGetAttacked(self, attacker, damage, weapon, stimuli)
	end
	
	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")
	
	inst:AddComponent("armorbreak_debuff")

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('snortoise')

    inst:AddComponent("inspectable")
	

    inst:AddComponent("sleeper")
    inst.components.sleeper:SetResistance(3)
    inst.components.sleeper.testperiod = GetRandomWithVariance(6, 2)
    --inst.components.sleeper:SetSleepTest(ShouldSleep)
    --inst.components.sleeper:SetWakeTest(ShouldWakeUp)
    inst:ListenForEvent("newcombattarget", OnNewTarget)
	
	inst:AddComponent("debuffable")
	
    MakeHauntablePanic(inst)
	
	MakeLargeBurnableCharacter(inst, "body")

    inst:WatchWorldState("stopday", OnStopDay)
    inst.OnEntitySleep = OnEntitySleep

    inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
    --inst:ListenForEvent("onattackother", OnAttackOther)
	--[[
	inst:DoTaskInTime(0.5, function(inst)
		local target = FindPlayerTarget(inst) or nil
		inst.components.combat:SetTarget(target)
	end)]]

    return inst
end

return ForgePrefab("snortoise", fn, assets, prefabs, nil, "ENEMIES", false, "images/forged_forge.xml", "snortoise_icon.tex", nil, TUNING.FORGE.SNORTOISE, STRINGS.FORGED_FORGE.ENEMIES.SNORTOISE.ABILITIES)