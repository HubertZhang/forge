--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    Asset("ANIM", "anim/abigail_flower.zip"),
}

local prefabs =
{
    "lavaarena_abigail",
    "redpouch_unwrap",
}

local function getstatus(inst)
    if inst._chargestate == 3 and inst.components.inventory then
        return inst.components.inventoryitem.owner ~= nil
            and "HAUNTED_POCKET"
            or "HAUNTED_GROUND"
    end
    local time_charge = inst.components.cooldown:GetTimeToCharged()
    return (time_charge < TUNING.FORGE.ABIGAIL.RESPAWN_TIME * .5 and "SOON")
        or (time_charge < TUNING.FORGE.ABIGAIL.RESPAWN_TIME * 2 and "MEDIUM")
        or "LONG"
end

local function activate(inst)
    --inst.SoundEmitter:PlaySound("dontstarve/common/haunted_flower_LP", "loop")
    --inst:ListenForEvent("entity_death", inst._onentitydeath, TheWorld)
	SpawnPrefab("small_puff").Transform:SetPosition(inst.Transform:GetWorldPosition())
	if inst._playerlink then
		inst._playerlink.components.petleash.petprefab = "forge_abigail"
		local pet = inst._playerlink.components.petleash:SpawnPetAt(inst.Transform:GetWorldPosition())
        if pet ~= nil then
            pet.Transform:SetPosition(inst.Transform:GetWorldPosition())
            pet.SoundEmitter:PlaySound("dontstarve/common/ghost_spawn")
            pet:LinkToPlayer(inst._playerlink)
			if inst.buffed then pet:PushEvent("buffpets", {buff = inst.buffed}) end
        end
		inst:Remove()
	end
	--local abigail = SpawnPrefab("forge_abigail")
    --if abigail ~= nil then
       -- abigail.Transform:SetPosition(inst.Transform:GetWorldPosition())
        --abigail.SoundEmitter:PlaySound("dontstarve/common/ghost_spawn")
        --abigail:LinkToPlayer(inst._playerlink)
        --inst:Remove()
   -- end
end

local function deactivate(inst)
    inst.SoundEmitter:KillAllSounds()
    inst:RemoveEventCallback("entity_death", inst._onentitydeath, TheWorld)
end

local function IsValidLink(inst, player)
    return player:HasTag("ghostlyfriend") and player.abigail == nil
end

local function updatestate(inst)
    if inst._playerlink ~= nil and inst.components.cooldown:IsCharged() then
        if inst._chargestate ~= 4 then
            inst._chargestate = 4
            inst.AnimState:PlayAnimation("haunted_pre")
            inst.AnimState:PushAnimation("idle_haunted_loop", true)
            inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
            --if inst.components.inventoryitem.owner == nil then
            activate(inst)
            --end
        end
    else
        if inst._chargestate == 4 then
            inst.AnimState:ClearBloomEffectHandle()
            deactivate(inst)
        end
        if inst._playerlink ~= nil and inst.components.cooldown:GetTimeToCharged() < TUNING.FORGE.ABIGAIL.RESPAWN_TIME * 0.25 then
            if inst._chargestate ~= 3 then
                inst._chargestate = 3
                inst.AnimState:PlayAnimation("haunted_pre")
				inst.AnimState:PushAnimation("idle_haunted_loop", true)
				inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
            end
        elseif inst._playerlink ~= nil and inst.components.cooldown:GetTimeToCharged() < TUNING.FORGE.ABIGAIL.RESPAWN_TIME * 0.5 then
			if inst._chargestate ~= 2 then
            inst._chargestate = 2
            inst.AnimState:PlayAnimation("idle_2")
			end
		elseif 	inst._playerlink ~= nil and inst.components.cooldown:GetTimeToCharged() < TUNING.FORGE.ABIGAIL.RESPAWN_TIME then
			if inst._chargestate ~= 1 then
            inst._chargestate = 1
            inst.AnimState:PlayAnimation("idle_1")
			--print("DEBUG IDLE 1 RAN")
			end
        end
    end
end

local function ondeath(inst, deadthing, killer)
    if inst._chargestate == 3 and
        inst._playerlink ~= nil and
        inst._playerlink.abigail == nil and
        inst._playerlink.components.leader ~= nil and
        inst.components.inventoryitem.owner == nil and
        deadthing ~= nil and
        not (deadthing:HasTag("wall") or deadthing:HasTag("smashable")) then

        if deadthing:IsValid() then
            if not inst:IsNear(deadthing, 16) then
                return
            end
        elseif killer == nil or not inst:IsNear(killer, 16) then
            return
        end

        inst._playerlink.components.sanity:DoDelta(-TUNING.SANITY_HUGE)
		inst._playerlink.components.petleash.petprefab = "forge_abigail"
        --local abigail = SpawnPrefab("forge_abigail")
		local pet = inst._playerlink.components.petleash:SpawnPetAt(inst.Transform:GetWorldPosition())
        if pet ~= nil then
            pet.Transform:SetPosition(inst.Transform:GetWorldPosition())
            pet.SoundEmitter:PlaySound("dontstarve/common/ghost_spawn")
            pet:LinkToPlayer(inst._playerlink)
			if inst.buffed then pet:PushEvent("buffpets", inst.buffed) end
			inst:Remove()
        end
    end
end

local function unlink(inst)
    if inst._playerlink ~= nil then
        
        inst._playerlink.abigail = nil
        inst._playerlink = nil
    end
end

local function linktoplayer(inst, player)
    if player ~= nil then
        inst._playerlink = player
		player.abigail = inst
		player.components.leader:AddFollower(inst)
		player:ListenForEvent("onremove", unlink, inst)
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("abigail_flower")
    inst.AnimState:SetBuild("abigail_flower")
    inst.AnimState:PlayAnimation("idle_1")

	inst:AddTag("nospawnfx") --Leo: a common tag made for modders if they need it to disable the spawnfx, like for pets transitioning between prefabs and such.
    --inst:Hide()

    inst.nameoverride = "abigail_flower"

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst._chargestate = nil
    --inst._playerlink = nil

    inst._onremoveplayer = function()
        unlink(inst)
        updatestate(inst)
    end
	
	inst._onentitydeath = function(world, data)
        ondeath(inst, data.inst)
    end
    -----------------------------------

    inst:AddComponent("inspectable")
    --inst.components.inspectable.getstatus = getstatus

    inst:AddComponent("cooldown")
    inst.components.cooldown.cooldown_duration = TUNING.FORGE.ABIGAIL.RESPAWN_TIME
    inst.components.cooldown.onchargedfn = updatestate
    inst.components.cooldown.startchargingfn = updatestate
    inst.components.cooldown:StartCharging()
	
	inst:AddComponent("follower")
	inst.components.follower.keepleader = true
	
	inst.LinkToPlayer = linktoplayer
	inst:DoTaskInTime(0, function(inst) 
		if inst.components.follower.leader then 
			inst.LinkToPlayer(inst, inst.components.follower.leader)
		end 
	end)
	
	inst:ListenForEvent("buffpets", function(inst, data) 
		inst.buffed = data.buff
		inst.components.cooldown.cooldown_duration = TUNING.FORGE.ABIGAIL.RESPAWN_TIME/data.buff
	end)
	
	inst:ListenForEvent("debuffpets", function(inst, data) 
		inst.buffed = nil
		inst.components.cooldown.cooldown_duration = TUNING.FORGE.ABIGAIL.RESPAWN_TIME
	end)
	
	inst.charge_task = inst:DoPeriodicTask(1, updatestate)

    --inst.OnRemoveEntity = unlink
    --inst.Refresh = refresh

    return inst
end

return Prefab("forge_abigail_flower", fn, assets, prefabs)
