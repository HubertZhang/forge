--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/swap_lucy_axe.zip"),
    Asset("ANIM", "anim/lavaarena_lucy.zip"),
    Asset("INV_IMAGE", "lucy"),
}

local assets_fx = {
    Asset("ANIM", "anim/lavaarena_lucy.zip"),
}

local assets_lavasplash = {
	Asset("ANIM", "anim/splash_ocean.zip"),
}

local prefabs = {
    "reticulelong",
    "reticulelongping",
	"weaponsparks_fx",
    "lucy_transform_fx",
    "splash_lavafx",
    "lucyspin_fx",
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_lucy_axe", "swap_lucy_axe")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    return Vector3(ThePlayer.entity:LocalToWorldSpace(6.5, 0, 0))
end

local function ReticuleMouseTargetFn(inst, mousepos)
    if mousepos ~= nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        local dx = mousepos.x - x
        local dz = mousepos.z - z
        local l = dx * dx + dz * dz
        if l <= 0 then
            return inst.components.reticule.targetpos
        end
        l = 6.5 / math.sqrt(l)
        return Vector3(x + dx * l, 0, z + dz * l)
    end
end

local function ReticuleUpdatePositionFn(inst, pos, reticule, ease, smoothing, dt)
    local x, y, z = inst.Transform:GetWorldPosition()
    reticule.Transform:SetPosition(x, 0, z)
    local rot = -math.atan2(pos.z - z, pos.x - x) / DEGREES
    if ease and dt ~= nil then
        local rot0 = reticule.Transform:GetRotation()
        local drot = rot - rot0
        rot = Lerp((drot > 180 and rot0 + 360) or (drot < -180 and rot0 - 360) or rot0, rot, dt * smoothing)
    end
    reticule.Transform:SetRotation(rot)
end

local function ChuckLucy(inst, caster, pos)
	inst.components.aimedprojectile:Throw(inst, caster, pos)
end

local function MakeNormal(inst)
	inst:RemoveTag("NOCLICK")
	inst.ismoving = false
	inst.Physics:Stop()
	inst.Physics:SetCollisionGroup(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
	inst.components.inventoryitem.canbepickedup = true
	inst.AnimState:SetMultColour(1,1,1,1)
end

local function Return(inst, attacker, inocean)
	attacker.components.combat.ignorehitrange = false
	if not inocean then inst.Physics:SetMotorVel(5, 0, 0) end
	inst:DoTaskInTime(FRAMES*15, function()
		if (inocean and not inst:IsOnValidGround()) or inst:IsOnValidGround() then
			if attacker and attacker.prefab == "woodie" then
				inst.Physics:Stop()
				inst.AnimState:SetMultColour(0,0,0,0)
				inst.returnfrompos = inst:GetPosition()
				if not inocean then SpawnPrefab("lucy_transform_fx").Transform:SetPosition(inst:GetPosition():Get()) end
				inst.projectileowner = nil
				inst:DoTaskInTime(FRAMES*12, function()
                    if (inocean and not inst:IsOnValidGround()) or inst:IsOnValidGround() then
                        if attacker and attacker.entity and attacker.entity:IsValid() and attacker.entity:IsVisible() then
                            if attacker.sg:HasStateTag("idle") then inst.projectileowner = attacker end
                            attacker.components.inventory:Equip(inst)
                            local origin_pos = inst.returnfrompos or inst:GetPosition() or Vector3(0,0,0)
                            attacker:SpawnChild("lucyspin_fx"):SetOrigin(origin_pos:Get())
                            if not attacker.sg.statemem.playedfx then
                                SpawnPrefab("lucy_transform_fx").entity:AddFollower():FollowSymbol(attacker.GUID, "swap_object", 50, -25, -1)
                            end
                        else
                            if inocean then
                                inst:DoTaskInTime(2, function(inst)
                                    inst:PutBackOnGround()
                                    local pos = inst:GetPosition()
                                    inst.Transform:SetPosition(pos.x, 12, pos.z)
                                    MakeNormal(inst)
                                end)
                            else
                                SpawnPrefab("lucy_transform_fx").Transform:SetPosition(inst:GetPosition():Get())
                            end
                        end
                        MakeNormal(inst)
                    end
                    inst.returnfrompos = nil
                end)
			else
				if inocean then
					inst:DoTaskInTime(3, function(inst)
						inst:PutBackOnGround()
						local pos = inst:GetPosition()
						inst.Transform:SetPosition(pos.x, 12, pos.z)
						MakeNormal(inst)
					end)
				else
					inst:DoTaskInTime(FRAMES*12, function() MakeNormal(inst) end)
				end
			end
		end
	end)
end

local function OnUpdate(inst, attacker)
	inst:DoTaskInTime(0, function(inst)
		if not inst:IsOnValidGround() then
			inst.ismoving = false
			inst.Physics:Stop()
			inst.components.aimedprojectile:Stop()
			inst.AnimState:SetMultColour(0,0,0,0)
			inst.AnimState:PlayAnimation("idle")
			--SpawnPrefab("splash_lavafx").Transform:SetPosition(inst:GetPosition():Get())
			Return(inst, attacker, true)
		end
		if inst.ismoving then OnUpdate(inst, attacker) end
	end)
end

local function OnThrown(inst, owner, attacker, targetpos)
	attacker.components.inventory:DropItem(inst)
	inst:AddTag("NOCLICK")
	inst.Physics:ClearCollisionMask()
	inst.components.inventoryitem.canbepickedup = false
	inst.AnimState:PlayAnimation("spin_loop", true)
	inst.ismoving = true
	attacker.components.combat.ignorehitrange = true
	OnUpdate(inst, attacker)
end

local function OnHit(inst, owner, attacker, target)
	SpawnPrefab("weaponsparks_fx"):SetPiercing(inst, target)
	inst.AnimState:PlayAnimation("bounce")
	inst.AnimState:PushAnimation("idle")
	
	if target.components.combat and attacker ~= nil and not (target.aggrotimer and target.components.combat.lastwasattackedtime <= target.aggrotimer ) then 
		local oldtarget = target.components.combat.target or nil
		target:PushEvent("newcombattarget", {target=attacker, oldtarget=oldtarget})
		--desperate attempt to force slam on whoever chucked lucy
		if target.prefab == "boarrior" then
			local x, y, z = attacker.Transform:GetWorldPosition()
			local distsq = target:GetDistanceSqToPoint(x, y, z)
			if distsq and distsq > 30 and distsq < 300 and target.components.health:GetPercent() <= TUNING.FORGE.BOARRIOR.PHASE2_TRIGGER and not target.slamhardcooldown then
				target.wants_to_slam = attacker
			end
		end
		target.components.combat:SetTarget(attacker) 
		target.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_LUCY 
	end
	inst.components.aimedprojectile:RotateToTarget(attacker:GetPosition())
	if inst:IsOnValidGround() then Return(inst, attacker) end
	
	if target and target.components.armorbreak_debuff and not (attacker and attacker.components.passive_shock and attacker.components.passive_shock.shock) then
		target.components.armorbreak_debuff:ApplyDebuff()
	end
end

local function OnMiss(inst, owner, attacker)
	SpawnPrefab("weaponsparks_fx"):SetBounce(inst)
	inst.AnimState:PlayAnimation("bounce")
	inst.AnimState:PushAnimation("idle")
	if inst:IsOnValidGround() then Return(inst, attacker) end
end

local function OnAttack(inst, attacker, target)
	SpawnPrefab("weaponsparks_fx"):SetPosition(attacker, target)
	if target and target.components.armorbreak_debuff and not (attacker and attacker.components.passive_shock and attacker.components.passive_shock.shock) then
		target.components.armorbreak_debuff:ApplyDebuff()
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	inst.nameoverride = "lavaarena_lucy"
	
    inst.AnimState:SetBank("lavaarena_lucy")
    inst.AnimState:SetBuild("lavaarena_lucy")
    inst.AnimState:PlayAnimation("idle")

    inst.Transform:SetSixFaced()

    inst:AddTag("sharp")
    inst:AddTag("throw_line")
    inst:AddTag("chop_attack")
	inst:AddTag("irreplaceable") --temp fix for the disappearing on the new RoT ocean.

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting:SetAlwaysValid(true)
    inst.components.aoetargeting.reticule.reticuleprefab = "reticulelong"
    inst.components.aoetargeting.reticule.pingprefab = "reticulelongping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.mousetargetfn = ReticuleMouseTargetFn
    inst.components.aoetargeting.reticule.updatepositionfn = ReticuleUpdatePositionFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(ChuckLucy)
	
	inst:AddComponent("rechargeable")
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.RILEDLUCY.DAMAGE)
	inst.components.weapon:SetOnAttack(OnAttack)
	inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)
	--inst.components.weapon:SetAltDamage(TUNING.FORGE.RILEDLUCY.ALT_DAMAGE)
	inst.components.weapon:SetAltAttack(TUNING.FORGE.RILEDLUCY.ALT_DAMAGE, 10, nil, DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("aimedprojectile")
	--inst.components.aimedprojectile:SetRange(12)
	inst.components.aimedprojectile:SetHitDistance(3)
	inst.components.aimedprojectile:SetStimuli("strong")
	--inst.components.aimedprojectile:SetDamage(TUNING.FORGE.RILEDLUCY.ALT_DAMAGE)
	inst.components.aimedprojectile:SetOnThrownFn(OnThrown)
    inst.components.aimedprojectile:SetOnHitFn(OnHit)
	inst.components.aimedprojectile:SetOnMissFn(OnMiss)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("melees")
	
	inst:AddComponent("inspectable")
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "lucy"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	
	--inst.damage_type = TUNING.FORGE.RILEDLUCY.TYPE

    return inst
end

--------------------------------------------------------------------------

local function lavasplashfn()
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
	inst.AnimState:SetMultColour(.5,.5,.5,1)
	inst.AnimState:SetAddColour(1,.7,.2,.9)
	inst.AnimState:SetBank("splash")
	inst.AnimState:SetBuild("splash_ocean")
	inst.AnimState:PlayAnimation("idle")
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	
	inst:ListenForEvent("animover", inst.Remove)
	
	return inst
end

--------------------------------------------------------------------------

local function CreateSpinFX()
    local inst = CreateEntity()

    inst:AddTag("FX")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("lavaarena_lucy")
    inst.AnimState:SetBuild("lavaarena_lucy")
    inst.AnimState:PlayAnimation("return")
    inst.AnimState:SetMultColour(.2, .2, .2, .2)

    inst.Transform:SetSixFaced()

    inst:DoTaskInTime(13 * FRAMES, inst.Remove)

    return inst
end

local function OnUpdateSpin(fx, inst)
    local parent = fx.owner.entity:GetParent()
    if fx.alpha >= .6 and (parent == nil or not (parent.AnimState:IsCurrentAnimation("catch_pre") or parent.AnimState:IsCurrentAnimation("catch"))) then
        fx.dalpha = -.1
    end
    local x, y, z = inst.Transform:GetWorldPosition()
    local x1, y1, z1 = fx.Transform:GetWorldPosition()
    local dx = x1 - x
    local dz = z1 - z
    local dist = math.sqrt(dx * dx + dz * dz)
    fx.offset = fx.offset * .8 + .2
    fx.vy = fx.vy + fx.ay
    fx.height = fx.height + fx.vy
    fx.Transform:SetPosition(x + dx * fx.offset / dist, fx.height, z + dz * fx.offset / dist)
    if fx.alpha ~= 0 then
        fx.alpha = fx.alpha + fx.dalpha
        if fx.alpha >= 1 then
            fx.dalpha = 0
            fx.alpha = 1
        elseif fx.alpha <= 0 then
            fx:Remove()
        end
        fx.AnimState:SetMultColour(fx.alpha, fx.alpha, fx.alpha, fx.alpha)
    end
end

local function OnOriginDirty(inst)
    local parent = inst.entity:GetParent()
    if parent ~= nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        local dx = inst._originx:value() - x
        local dz = inst._originz:value() - z
        local distsq = dx * dx + dz * dz
        local dist = math.sqrt(distsq)
        local fx = CreateSpinFX()
        fx.owner = inst
        fx.offset = math.min(3, dist)
        fx.height = 2
        fx.vy = .2
        fx.ay = -.05
        fx.alpha = .2
        fx.dalpha = .2
        fx.Transform:SetPosition(x + dx * fx.offset / dist, fx.height, z + dz * fx.offset / dist)
        fx:ForceFacePoint(inst._originx:value(), 0, inst._originz:value())
        fx:ListenForEvent("onremove", function() fx:Remove() end, inst)
        fx:DoPeriodicTask(0, OnUpdateSpin, nil, inst)
    end
end

local function SetOrigin(inst, x, y, z)
    inst._originx:set(x)
    inst._originz:set(z)
    if not TheNet:IsDedicated() then
        OnOriginDirty(inst)
    end
end

local function fxfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddNetwork()

    inst:AddTag("FX")

    inst._originx = net_float(inst.GUID, "lavaarena_lucy_spin._originx", "origindirty")
    inst._originz = net_float(inst.GUID, "lavaarena_lucy_spin._originz", "origindirty")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        inst:ListenForEvent("origindirty", OnOriginDirty)

        return inst
    end

    inst.persists = false
    inst.SetOrigin = SetOrigin
    inst:DoTaskInTime(.5, inst.Remove)

    return inst
end

--------------------------------------------------------------------------

--[[
local function fxfn()
    local inst = CreateEntity()
	
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddPhysics()
	inst.entity:AddNetwork()
	
    inst:AddTag("FX")
	
	inst.AnimState:SetBank("lavaarena_lucy")
    inst.AnimState:SetBuild("lavaarena_lucy")
    inst.AnimState:PlayAnimation("return")
	inst.AnimState:SetMultColour(.2, .2, .2, .2)
	
	inst.Transform:SetSixFaced()
	
    inst.entity:SetPristine()
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.colormult = .2
	inst.dist = 4
	
	inst.Throw = function(inst, startpos, player)
		inst.player = player
		inst.savedposition = inst.player:GetPosition() --Used incase the player disconnects
		local offset = (startpos - inst.player:GetPosition()):GetNormalized()*inst.dist
		inst.Transform:SetPosition((inst.player:GetPosition() + offset):Get())
		inst:FacePoint(inst.player:GetPosition())
		inst:DoPeriodicTask(0, function(inst)
			inst.dist = inst.dist - 0.31
			inst.colormult = inst.colormult == 1 and inst.colormult or inst.colormult + .05
			inst.AnimState:SetMultColour(inst.colormult, inst.colormult, inst.colormult, inst.colormult)
			if inst.player and inst.player.entity and inst.player.entity:IsValid() and inst.player.entity:IsVisible() then
				local offset = (inst:GetPosition() - inst.player:GetPosition()):GetNormalized()*inst.dist
				inst.Transform:SetPosition((inst.player:GetPosition() + offset):Get())
				inst:FacePoint(inst.player:GetPosition())
			else
				local offset = (inst:GetPosition() - inst.savedposition):GetNormalized()*inst.dist
				inst.Transform:SetPosition((inst.savedposition + offset):Get())
				inst:FacePoint(inst.savedposition)
			end
		end)
	end
	
    inst:DoTaskInTime(13 * FRAMES, inst.Remove)
	
    return inst
end]]

return ForgePrefab("riledlucy", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "lucy.tex", nil, TUNING.FORGE.RILEDLUCY, STRINGS.FORGED_FORGE.WEAPONS.RILEDLUCY.ABILITIES, "swap_lucy_axe", "common_hand"),
    Prefab("lucyspin_fx", fxfn, assets_fx),
	Prefab("splash_lavafx", lavasplashfn, assets_lavasplash)