--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/fireballstaff.zip"),
    Asset("ANIM", "anim/swap_fireballstaff.zip"),
}

local prefabs = {
    "forge_fireball_projectile",
    "forge_fireball_hit_fx",
    "infernalstaff_meteor",
    "reticuleaoe",
    "reticuleaoeping",
    "reticuleaoehostiletarget",
}

local PROJECTILE_DELAY = 4 * FRAMES

--------------------------------------------------------------------------

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_fireballstaff", "swap_fireballstaff")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    local player = ThePlayer
    local ground = TheWorld.Map
    local pos = Vector3()
    --Cast range is 8, leave room for error
    --4 is the aoe range
    for r = 7, 0, -.25 do
        pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
        if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
            return pos
        end
    end
    return pos
end

local function Cataclysm(inst, caster, pos)
	SpawnPrefab("infernalstaff_meteor"):AttackArea(caster, inst, pos)
	inst.components.rechargeable:StartRecharge()
end

local function OnSwing(inst, attacker, target)
	local offset = (target:GetPosition() - attacker:GetPosition()):GetNormalized()*1.2
	local particle = SpawnPrefab("forge_fireball_hit_fx")
	particle.Transform:SetPosition((attacker:GetPosition() + offset):Get())
	particle.AnimState:SetScale(0.8,0.8)
end

local function CalcAltDamage(inst, attacker, target)
    local centerpos = inst.meteor:GetPosition()
    local base_damage = TUNING.FORGE.INFERNALSTAFF.ALT_DAMAGE.base
    local center_mult = TUNING.FORGE.INFERNALSTAFF.ALT_DAMAGE.center_mult
    local base_dist = 16
    local dist = distsq(centerpos, target:GetPosition())
    local dist_ratio = math.max(0, 1 - dist / base_dist)
    local dmg = base_damage*(1 + Lerp(0, center_mult, dist_ratio))
    return dmg
end

local function OnAttack(inst, attacker, target)
	if inst.components.weapon.isaltattacking then
		SpawnPrefab("infernalstaff_meteor_splashhit"):SetTarget(target)
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	inst.nameoverride = "fireballstaff"

    inst.AnimState:SetBank("fireballstaff")
    inst.AnimState:SetBuild("fireballstaff")
    inst.AnimState:PlayAnimation("idle")
	
	inst:AddTag("magicweapon")
    inst:AddTag("rangedweapon")
    inst:AddTag("firestaff")
    inst:AddTag("pyroweapon")

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting.reticule.reticuleprefab = "reticuleaoe"
    inst.components.aoetargeting.reticule.pingprefab = "reticuleaoeping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.projectiledelay = PROJECTILE_DELAY

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.castsound = "dontstarve/common/lava_arena/spell/meteor"
	
	-- Fox: Initializing reticule_spawner before aoespell
	inst:AddComponent("reticule_spawner")
	inst.components.reticule_spawner:Setup(unpack(TUNING.FORGE.RET_DATA.infernalstaff))
	
	inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(Cataclysm)
	inst.components.aoespell:SetSpellType("damage")
	
	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.INFERNALSTAFF.COOLDOWN)
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.INFERNALSTAFF.DAMAGE)
	inst.components.weapon:SetOnAttack(OnAttack)
    inst.components.weapon:SetRange(10, 20)
    inst.components.weapon:SetProjectile("forge_fireball_projectile")
	inst.components.weapon:SetOnProjectileLaunch(OnSwing)
	inst.components.weapon:SetDamageType(DAMAGETYPES.MAGIC)
	--inst.components.weapon:SetAltType("spell") --Fid: Glass, This functionality should be moved to aoespell
	inst.components.weapon:SetStimuli("fire")
	inst.components.weapon:SetAltAttack(TUNING.FORGE.INFERNALSTAFF.ALT_DAMAGE.minimum, {10, 20}, nil, DAMAGETYPES.MAGIC, CalcAltDamage)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("staves")
	
	inst:AddComponent("inspectable")
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "fireballstaff"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	
	--inst.damage_type = TUNING.FORGE.INFERNALSTAFF.TYPE

    return inst
end

return ForgePrefab("infernalstaff", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "fireballstaff.tex", nil, TUNING.FORGE.INFERNALSTAFF, STRINGS.FORGED_FORGE.WEAPONS.INFERNALSTAFF.ABILITIES, "swap_fireballstaff", "common_hand")