--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
	crocommander = {},
	crocommander_rapidfire = {
		Asset("ANIM", "anim/lavaarena_snapper_rapidfire.zip"),	
	}
}

local prefabs = {
	crocommander = {},
	crocommander_rapidfire = {}
}

local brain = require("brains/crocommanderbrain")

SetSharedLootTable( 'crocommander',
{

})

local WAKE_TO_FOLLOW_DISTANCE = 8
local SLEEP_NEAR_HOME_DISTANCE = 10
local SHARE_TARGET_DIST = 30
local HOME_TELEPORT_DIST = 30

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }

local function FindPlayerTarget(inst)
	local player, distsq = inst:GetNearestPlayer()
	if player then
		if not inst.components.follower.leader then --Don't attempt to find a target that isn't the leader's target. 
			return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) and player then
		return target
	else
		return not (player:HasTag("notarget") or player:HasTag("LA_mob")) and (player and not player.components.health:IsDead()) and player and player
	end
end

local function OnDroppedTarget(inst)
	local playertargets = {}
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 50, nil, {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" }, {"player"} ) or {}
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and not (inst.lasttarget and inst.lasttarget == v) then
			table.insert(playertargets, v)		
		end	
	end
	if not inst:HasTag("brainwashed") then
		--I assume first one in the table is the closest
		if #playertargets > 0 then
			inst.components.combat:SetTarget(playertargets[1])
		--Leo: This elseif check is just to make the mob not stop and continue on, making it look like the forge.
		elseif inst.lasttarget and inst.lasttarget.components and inst.lasttarget.components.health and not inst.lasttarget.components.health:IsDead() then
			inst.components.combat:SetTarget(inst.lasttarget)
		end
	end
end

local function OnNewTarget(inst, data)
	--inst.last_attack_landed = GetTime() + 8
	if data and data.target then
		inst.lasttarget = inst.components.combat.target
		
	end
end

local function RetargetFn(inst)
    local player, distsq = inst:GetNearestPlayer()
	if inst:HasTag("brainwashed") then
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "wall", "player", "companion", "brainwashed" })
	else
		return FindEntity(inst, 50, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not (inst.lastarget and inst.lasttarget == guy) end, nil, { "wall", "LA_mob", "battlestandard" })
	end	
end

local function KeepTarget(inst, target)
	if inst:HasTag("brainwashed") then
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("player") and not target:HasTag("companion")
	else
		return inst.components.combat:CanTarget(target) and (target.components.health and not target.components.health:IsDead()) and not target:HasTag("LAmob") --inst:IsNear(target, 5)
	end
end

local function OnAttacked(inst, data)
	if data and data.attacker and not data.attacker:HasTag("notarget") and not (inst.aggrotimer and inst.components.combat.lastwasattackedtime <= inst.aggrotimer) then	
		if inst:HasTag("brainwashed") and not (data.attacker:HasTag("player") or data.attacker:HasTag("companion")) then
			inst.components.combat:SetTarget(data.attacker)
		elseif data.attacker:HasTag("brainwashed") and not inst:HasTag("brainwashed") then
			inst.components.combat:SetTarget(data.attacker)
		else
			local player, distsq = inst:GetNearestPlayer()	
			if player and player.components.health and not player.components.health:IsDead() and player == data.attacker then
				if inst.components.combat.target and not (data.attacker:HasTag("lessaggro") or inst:IsNear(inst.components.combat.target, 2.5)) then
					inst.components.combat:SetTarget(data.attacker)
				elseif not inst.components.combat.target then
					inst.components.combat:SetTarget(data.attacker)
				end		
			end
		end	
	end
end

local function MakeWeapon(inst)
    if inst.components.inventory ~= nil then
        local weapon = CreateEntity()
        weapon.entity:AddTransform()
        MakeInventoryPhysics(weapon)
        weapon:AddComponent("weapon")
        weapon.components.weapon:SetDamage(TUNING.FORGE.CROCOMMANDER.SPIT_DAMAGE)
        weapon.components.weapon:SetRange(inst.components.combat.attackrange, inst.components.combat.attackrange + 40)
        weapon.components.weapon:SetProjectile("forge_gooball_projectile")
		weapon.components.weapon:SetDamageType(DAMAGETYPES.LIQUID)
        weapon:AddComponent("inventoryitem")
        weapon.persists = false
		weapon.attackperiod = 3
        weapon.components.inventoryitem:SetOnDroppedFn(weapon.Remove)
        weapon:AddComponent("equippable")
        inst.weapon = weapon
        inst.components.inventory:Equip(inst.weapon)
        inst.components.inventory:Unequip(EQUIPSLOTS.HANDS)
    end
end

local function MakeFireWeapon(inst)
	----print("DEBUG: FireWeapon is making")
    if inst.components.inventory ~= nil then
        local weapon = CreateEntity()
        weapon.entity:AddTransform()
        MakeInventoryPhysics(weapon)
        weapon:AddComponent("weapon")
        weapon.components.weapon:SetDamage(TUNING.FORGE.CROCOMMANDER.SPIT_DAMAGE)
        weapon.components.weapon:SetRange(inst.components.combat.attackrange, inst.components.combat.attackrange + 40)
        weapon.components.weapon:SetProjectile("forge_fireball_projectile_fast")
		weapon.components.weapon:SetDamageType(DAMAGETYPES.MAGIC)
        weapon:AddComponent("inventoryitem")
        weapon.persists = false
        weapon.components.inventoryitem:SetOnDroppedFn(weapon.Remove)
		weapon.attackperiod = 0.2
        weapon:AddComponent("equippable")
        inst.weapon = weapon
        inst.components.inventory:Equip(inst.weapon)
        inst.components.inventory:Unequip(EQUIPSLOTS.HANDS)
    end
end

local function SetBanner(inst)
	local wave = TheWorld.components.lavaarenaevent
	local currentround = wave ~= nil and wave:GetCurrentRound()
	----print("DEBUG: Crocommander got currentround as "..currentround or "nil")
	if wave.rounds_data[currentround] ~= nil and wave.rounds_data[currentround].banner ~= nil then
		return wave.rounds_data[currentround].banner
	else
		--print("ERROR: Something went wrong with SetBanner in crocomander.lua, setting banner to heal")
		return "battlestandard_heal"
	end
end

local function AttemptBanner(inst)
	if inst.components.health and not inst.components.health:IsDead() and not (inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("nointerrupt") or inst.sg:HasStateTag("nocancel")) then
		if inst.components.combat and inst.components.combat.target and inst.wantstobanner and inst.wantstobanner == true and inst.components.entitytracker:GetEntity("mybanner") == nil and not (inst.neverbanner and inst.neverbanner == true) and not inst:HasTag("brainwashed") then
			inst.sg:GoToState("build")
		end
	end
end

local function SwapWeapon(inst)
	if inst.components.combat.target and inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) and inst:IsNear(inst.components.combat.target, TUNING.FORGE.CROCOMMANDER.MELEE_RANGE) then
		inst.components.inventory:Unequip(EQUIPSLOTS.HANDS)
		inst.components.combat:SetRange(TUNING.FORGE.CROCOMMANDER.ATTACK_RANGE, TUNING.FORGE.CROCOMMANDER.HIT_RANGE)
		inst.components.combat:SetAttackPeriod(TUNING.FORGE.CROCOMMANDER.ATTACK_PERIOD)
	elseif inst.components.combat.target and inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) == nil and not inst:IsNear(inst.components.combat.target, TUNING.FORGE.CROCOMMANDER.MELEE_RANGE) then
		inst.components.combat:SetRange(TUNING.FORGE.CROCOMMANDER.SPIT_ATTACK_RANGE, inst.components.combat.attackrange + 40)
		inst.components.inventory:Equip(inst.weapon)	
		inst.components.locomotor:Stop() --else will continue the chase
	end
end


local function common_fn(tuning)
	local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()
	
	inst.DynamicShadow:SetSize(2, .75)
    inst.Transform:SetFourFaced()

    MakeCharacterPhysics(inst, 100, .75)

    inst.AnimState:SetBank("snapper")
    inst.AnimState:SetBuild("lavaarena_snapper_basic")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.AnimState:AddOverrideBuild("fossilized")

    inst:AddTag("LA_mob")
    inst:AddTag("monster")
    inst:AddTag("hostile")

    --fossilizable (from fossilizable component) added to pristine state for optimization
    inst:AddTag("fossilizable")
	
    if TheWorld.components.lavaarenamobtracker ~= nil then
        TheWorld.components.lavaarenamobtracker:StartTracking(inst)
    end
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = TUNING.FORGE[tuning].RUNSPEED
	
    inst:AddComponent("fossilizable")
	
    inst:SetStateGraph("SGcrocommander")

    inst:SetBrain(brain)
	
	inst:AddComponent("leader")
    inst:AddComponent("follower")
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.FORGE[tuning].HEALTH)

    inst:AddComponent("combat")
	--inst.components.combat.playerdamagepercent = 0.5
	inst.components.combat:SetRange(TUNING.FORGE[tuning].ATTACK_RANGE, TUNING.FORGE[tuning].HIT_RANGE)
	--inst.components.combat:SetAreaDamage(5, 1)
    inst.components.combat:SetDefaultDamage(TUNING.FORGE[tuning].DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.FORGE[tuning].ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(2, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	inst.components.combat:SetDamageType(DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")
	
	inst:AddComponent("armorbreak_debuff")
	inst.components.armorbreak_debuff:SetFollowSymbol("head")

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('snapper')

    inst:AddComponent("inspectable")
	
	inst:AddComponent("inventory")
	
	inst:AddComponent("sleeper")
	
	inst:AddComponent("debuffable")
	
	inst:AddComponent("entitytracker")
	
    MakeHauntablePanic(inst)
	
	MakeLargeBurnableCharacter(inst, "body")

    inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
    --inst:ListenForEvent("onattackother", OnAttackOther)
	
	--[[
	inst:DoTaskInTime(0.1, function(inst)
		local target = FindPlayerTarget(inst) or nil
		inst.components.combat:SetTarget(target)
	end)]]
	
	return inst
end

local fns = {}
fns.crocommander = function()
	local inst = common_fn("CROCOMMANDER")
	
	inst.nameoverride = "snapper"
	
	if not TheWorld.ismastersim then
		return inst
	end
	
	inst.mybanner = SetBanner()
	inst.wantstobanner = true
	
	inst:DoTaskInTime(0, MakeWeapon)
	inst:DoPeriodicTask(1, AttemptBanner)
	inst:DoPeriodicTask(0.5, SwapWeapon)
	
	return inst
end

fns.crocommander_rapidfire = function()
	local inst = common_fn("CROCOMMANDER_RAPIDFIRE")
	
	inst.AnimState:SetBuild("lavaarena_snapper_rapidfire")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.mybanner = "battlestandard_speed"
	inst.wantstobanner = true
	inst.israngedforever = true
	--inst.neverbanner = true
	
	inst:DoTaskInTime(0.5, MakeFireWeapon)
	inst:DoPeriodicTask(1, AttemptBanner)
	inst:DoPeriodicTask(0.5, SwapWeapon)
	
	return inst
end

local variants = {"crocommander", "crocommander_rapidfire"}
local function MakeVariant(prefab)
	return ForgePrefab(prefab, fns[prefab], assets[prefab], prefabs[prefab], nil, "ENEMIES", false, "images/forged_forge.xml", prefab.."_icon.tex", nil, TUNING.FORGE[string.upper(prefab)], STRINGS.FORGED_FORGE.ENEMIES[string.upper(prefab)].ABILITIES)
end
local prefs = {}
for k,v in pairs(variants) do
	table.insert(prefs, MakeVariant(v))
end
return unpack(prefs)