--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/lavaarena_firebomb.zip"),
    Asset("ANIM", "anim/swap_lavaarena_firebomb.zip"),
}

local assets_fx =
{
    Asset("ANIM", "anim/lavaarena_firebomb.zip"),
}

local assets_sparks =
{
    Asset("ANIM", "anim/sparks_molotov.zip"),
}

local prefabs =
{
    "firebomb_projectile",
    "firebomb_proc_fx",
    "firebomb_sparks",
    "reticuleaoesmall",
    "reticuleaoesmallping",
    "reticuleaoesmallhostiletarget",
}

local prefabs_projectile =
{
    "firebomb_explosion",
    "firehit",
}

--------------------------------------------------------------------------
local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_lavaarena_firebomb", "swap_lavaarena_firebomb")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    local player = ThePlayer
    local ground = TheWorld.Map
    local pos = Vector3()
    --Cast range is 8, leave room for error
    --2 is the aoe range
    for r = 5, 0, -.25 do
        pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
        if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
            return pos
        end
    end
    return pos
end

local function onthrown(inst, owner)
    inst:AddTag("NOCLICK")
    inst.persists = false
	
	inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_fuse_LP", "hiss")

    --inst.AnimState:PlayAnimation("spin_loop", true)

	--inst.owner = owner
	
    inst.Physics:SetMass(1)
    inst.Physics:SetCapsule(0.2, 0.2)
    inst.Physics:SetFriction(0)
    inst.Physics:SetDamping(0)
    inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.ITEMS)
end

local range = TUNING.FORGE.FIREBOMB.ALT_RANGE

local function OnHitFire(inst, attacker, target)
	inst.SoundEmitter:KillSound("hiss")
	SpawnPrefab("firebomb_explosion").Transform:SetPosition(inst.Transform:GetWorldPosition())
	local found_mobs = {}
	local x, y, z = inst:GetPosition():Get()
	local ents = TheSim:FindEntities(x, y, z, range, nil, {"player", "companion"})
	for _,ent in ipairs(ents) do
		if inst.attacker ~= nil and ent ~= inst.attacker and ent.entity:IsValid() and ent.entity:IsVisible() and ent.components.health and not ent.components.health:IsDead() and ent.components.combat then
			table.insert(found_mobs, ent)
		end
	end
	if inst.owner.components.weapon then
		inst.owner.components.weapon:DoAltAttack(inst.attacker, found_mobs, nil, "explosive")
	end
    inst:Remove()
end

local function ResetCharge(inst)
	inst.charge = 0
	if inst.charge_fx then
		inst.charge_fx:Remove()
		inst.charge_fx = nil
	end
	if inst.chargetask then
		inst.chargetask:Cancel()
		inst.chargetask = nil
	end
	inst.SoundEmitter:KillSound("hiss")
end


local function FirebombToss(inst, caster, pos)
	local projectile = SpawnPrefab("firebomb_projectile")
	projectile.Transform:SetPosition(inst:GetPosition():Get())
	projectile.owner = caster
	projectile.components.complexprojectile:Launch(pos, caster)
	projectile:AttackArea(caster, inst, pos)
	ResetCharge(inst)
	inst.components.rechargeable:StartRecharge()
end

--Leo: Since I'm still looking for proof that firebombs lose charge overtime and not just fizzle, just gonna do it the easy way for now.
--Charge just fizzles if not attacking for 5 seconds.
local function AddChargeLvL(inst, level)
	if not inst.charge_fx  and inst.components.inventoryitem.owner then
		inst.charge_fx = SpawnPrefab("firebomb_sparks")
		inst.charge_fx.entity:AddFollower()
		inst.charge_fx.Follower:FollowSymbol(inst.components.inventoryitem.owner.GUID, "swap_object", 3, 2, 0)
		inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_fuse_LP", "hiss")
	end
	inst.charge_fx.SetSparkLevel(inst.charge_fx, level)
end

local function DoProcAlt(inst, caster ,other)
	local pos = other:GetPosition()
	local fx = SpawnPrefab("firebomb_proc_fx")
	--Leo: For some reason doing the proc bomb here makes it attack twice, making the fx do it for now.
	fx.Transform:SetPosition(pos:Get())
	fx.owner = inst
	fx.attacker = caster
	fx.target = other
	ResetCharge(inst)
end

local function OnHitOther(inst, caster , other) 
	----print("DEBUG: Firebomb is charging")
	if inst.components.inventoryitem.owner then
		inst.charge = (inst.charge or 0) + 1
		if inst.chargetask then
			inst.chargetask:Cancel()
			inst.chargetask = nil
		end
		inst.chargetask = inst:DoTaskInTime(TUNING.FORGE.FIREBOMB.CHARGE_DECAY_TIME, ResetCharge)
		if inst.charge >= TUNING.FORGE.FIREBOMB.CHARGE_HITS_1 and inst.charge < TUNING.FORGE.FIREBOMB.CHARGE_HITS_2 then
			AddChargeLvL(inst, 1)
		elseif inst.charge >= TUNING.FORGE.FIREBOMB.CHARGE_HITS_2 and inst.charge < TUNING.FORGE.FIREBOMB.MAXIMUM_CHARGE_HITS then
			AddChargeLvL(inst, 2)
		elseif inst.charge >= TUNING.FORGE.FIREBOMB.MAXIMUM_CHARGE_HITS then
			DoProcAlt(inst, caster, other )
		end
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("lavaarena_firebomb")
    inst.AnimState:SetBuild("lavaarena_firebomb")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("throw_line")

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting.reticule.reticuleprefab = "reticuleaoesmall"
    inst.components.aoetargeting.reticule.pingprefab = "reticuleaoesmallping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true
	
	inst.nameoverride = "lavaarena_firebomb" --So we don't have to make the describe strings.

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.charge = 0
	inst.charge_level = 0
	
	inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(FirebombToss)
	inst.components.aoespell:SetSpellType("damage")
	
	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.FIREBOMB.COOLDOWN)
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.FIREBOMB.DAMAGE)
	inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)
	inst.components.weapon.onattack = OnHitOther

	inst.components.weapon:SetStimuli("fire")
	inst.components.weapon:SetAltAttack(TUNING.FORGE.FIREBOMB.ALT_DAMAGE, {10, 20}, nil, DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("darts")
	
	inst:AddComponent("inspectable")
	inst.nameoverride = "lavaarena_firebomb" --So we don't have to make the describe strings.
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "lavaarena_firebomb"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	
	inst.charge = 0

    return inst
end

local function CreateProjectileAnim()
    local inst = CreateEntity()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.Transform:SetSixFaced()

    inst.AnimState:SetBank("lavaarena_firebomb")
    inst.AnimState:SetBuild("lavaarena_firebomb")
    inst.AnimState:PlayAnimation("spin_loop", true)
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    return inst
end

local function OnDirectionDirty(inst)
    inst.animent.Transform:SetRotation(inst.direction:value())
end

local function projectilefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddPhysics()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.Physics:SetMass(1)
    inst.Physics:SetFriction(0)
    inst.Physics:SetDamping(0)
    inst.Physics:SetRestitution(.5)
    inst.Physics:SetCollisionGroup(COLLISION.ITEMS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.GROUND)
    inst.Physics:SetCapsule(.2, .2)

    inst:AddTag("NOCLICK")

    inst:AddTag("projectile")

    inst.direction = net_float(inst.GUID, "lavaarena_firebomb_projectile.direction", "directiondirty")

    --Dedicated server does not need to spawn the local animation
    if not TheNet:IsDedicated() then
        inst.animent = CreateProjectileAnim()
        inst.animent.entity:SetParent(inst.entity)

        if not TheWorld.ismastersim then
            inst:ListenForEvent("directiondirty", OnDirectionDirty)
        end
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.AttackArea = function(inst, attacker, weapon, pos)
		weapon.firebomb = inst
		inst.attacker = attacker
		inst.owner = weapon
	end
	
	inst:AddComponent("complexprojectile")
	inst.components.complexprojectile:SetHorizontalSpeed(TUNING.FORGE.FIREBOMB.HORIZONTAL_SPEED)
    inst.components.complexprojectile:SetGravity(TUNING.FORGE.FIREBOMB.GRAVITY)
    inst.components.complexprojectile:SetLaunchOffset(Vector3(.25, 1, 0))
    inst.components.complexprojectile:SetOnLaunch(onthrown)
    inst.components.complexprojectile:SetOnHit(OnHitFire)

    return inst
end

local function DoExplosiveAoe(inst)
	--temporary proc alt
	inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_explo")
	local found_mobs = {}
	local x, y, z = inst:GetPosition():Get()
	local ents = TheSim:FindEntities(x, y, z, 0.5, nil, {"player", "companion"})
	for _,ent in ipairs(ents) do
		if inst.attacker ~= nil and ent ~= inst.attacker and ent.entity:IsValid() and ent.entity:IsVisible() and ent.components.health and not ent.components.health:IsDead() then
			table.insert(found_mobs, ent)
		end
	end
	if inst.owner.components.weapon then
		inst.owner.components.weapon:DoAltAttack(inst.attacker, found_mobs, nil, "explosive")
	end
	--inst:ListenForEvent("animqueueover", inst:Remove())
end

local function explosionfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_firebomb")
    inst.AnimState:SetBuild("lavaarena_firebomb")
    inst.AnimState:PlayAnimation("used")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetLightOverride(1)
    inst.AnimState:SetFinalOffset(-1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false
	
	inst.AttackArea = function(inst, attacker, weapon, pos)
		weapon.meteor = inst
		inst.attacker = attacker
		inst.owner = weapon
		inst.Transform:SetPosition(pos:Get())
	end
	
	inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_explo")
	--inst:AddComponent("weapon")
	--inst.components.weapon:SetAltAttack(TUNING.FORGE.FIREBOMB.ALT_DAMAGE, {90, 20}, nil, DAMAGETYPES.PHYSICAL)
	
	--inst:DoTaskInTime(0, DoExplosiveAoe)
	
    return inst
end

local function procfxfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_firebomb")
    inst.AnimState:SetBuild("lavaarena_firebomb")
    inst.AnimState:PlayAnimation("hitfx")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetLightOverride(1)
    inst.AnimState:SetFinalOffset(-1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	inst.persists = false

	inst.AttackOther = function(weapon, caster, target)
		inst.target = target
		inst.attacker = caster
		--inst.owner = weapon
	end

	inst:DoTaskInTime(0, DoExplosiveAoe)
	
    return inst
end

local function SetSparkLevel(inst, level)
    inst.AnimState:PlayAnimation(tostring(level), true)
end

local function sparksfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("sparks_molotov")
    inst.AnimState:SetBuild("sparks_molotov")
    inst.AnimState:PlayAnimation("1", true)
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetLightOverride(1)
    inst.AnimState:SetFinalOffset(1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false
    inst.SetSparkLevel = SetSparkLevel

    return inst
end

return ForgePrefab("firebomb", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "lavaarena_firebomb.tex", nil, TUNING.FORGE.FIREBOMB, STRINGS.FORGED_FORGE.WEAPONS.FIREBOMB.ABILITIES, "swap_lavaarena_firebomb", "common_hand"),
    Prefab("firebomb_projectile", projectilefn, assets_fx, prefabs_projectile),
    Prefab("firebomb_explosion", explosionfn, assets_fx),
    Prefab("firebomb_proc_fx", procfxfn, assets_fx),
    Prefab("firebomb_sparks", sparksfn, assets_sparks)