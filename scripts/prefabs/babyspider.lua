--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    --Asset("ANIM", "anim/lavaarena_trails_basic.zip"),
}

local prefabs =
{
	"die_fx",
}

------------------------------
--ToDo List--
------------------------------
--Rework shellspin
-----------------------------

local brain = require("brains/babyspiderbrain")

SetSharedLootTable( 'babyspider',
{
    --{'monstermeat', 1.000},
})


local function ShouldWakeUp(inst)
    return (inst.components.follower.leader and not inst.components.follower.leader.components.health:IsDead()) or (inst.daddy and not inst.daddy.components.health:IsDead())
end

local function ShouldSleep(inst)
    return (inst.daddy and inst.daddy.components.health:IsDead()) or (inst.components.follower.leader and inst.components.follower.leader.components.health:IsDead())
end


local function RetargetFn(inst)
    if (inst.components.follower.leader and inst.components.follower.leader.components.health:IsDead()) or (inst.daddy and inst.daddy.components.health:IsDead()) then
		return nil
	elseif inst.components.follower.leader and inst.components.follower.leader.components.combat.target then	
		return inst.components.follower.leader.components.combat.target
	end
end

local function KeepTarget(inst, target)
	--Note: we only have interest in webber's target!
    return target:IsValid() and 
		target.sg ~= nil and not 
		((target.sg:HasStateTag("sleeping") or target:HasTag("_isinheals")) or target.sg:HasStateTag("fossilized")) and 
		inst.components.follower.leader and not inst.components.follower.leader.components.health:IsDead() and not 
		(inst.components.follower.leader.components.combat.target ~= nil and inst.components.follower.leader.components.combat.target ~= target)
end

local SCALE = .5

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

	MakeCharacterPhysics(inst, 10, .2)

    inst.DynamicShadow:SetSize(1.5 * SCALE, .25 * SCALE)

    inst.Transform:SetScale(SCALE, SCALE, SCALE)
    inst.Transform:SetFourFaced()

    inst.AnimState:SetBank("spider")
    inst.AnimState:SetBuild("spider_build")
    inst.AnimState:PlayAnimation("idle", true)
	
	inst:AddTag("companion")
	inst:AddTag("spider")
	inst:AddTag("notarget")

    inst.nameoverride = "webber_spider_minion" --So we don't have to make the describe strings.
	
	inst.entity:SetPristine()
	
	inst.ForgeOnSpawn = function(self)
		local fx = _G.SpawnPrefab("die_fx")
		fx.Transform:SetScale(0.5, 0.5, 0.5)
		fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
	end

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = TUNING.SPIDER_RUN_SPEED * 2
    inst:SetStateGraph("SGbabyspider")

    inst:SetBrain(brain)
	
    inst:AddComponent("follower")
	inst.components.follower.keepleader = true
	inst:DoTaskInTime(0, function (inst)
		if inst.components.follower.leader then
			inst.daddy = inst.components.follower.leader
		end
	end)
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(99999)
	inst.components.health:SetInvincible(true)
	
	

    inst:AddComponent("combat")
	inst.components.combat:SetRange(3 * SCALE)
    inst.components.combat:SetDefaultDamage(9)
    inst.components.combat:SetAttackPeriod(TUNING.SPIDER_ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(1, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
	
	inst:AddComponent("colouradder")

    inst:AddComponent("lootdropper")

    inst:AddComponent("inspectable")

    inst:AddComponent("sleeper")
    inst.components.sleeper.testperiod = GetRandomWithVariance(2, 1)
    inst.components.sleeper:SetSleepTest(ShouldSleep)
    inst.components.sleeper:SetWakeTest(ShouldWakeUp)
	
    MakeHauntablePanic(inst)
	
	MakeLargeBurnableCharacter(inst, "body")

    return inst
end

return Prefab("babyspider", fn, assets, prefabs)
