--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local prefabs_fossil = {
    "lavaarena_fossilizing",
    "reticuleaoe",
    "reticuleaoeping",
    "reticuleaoecctarget",
}

local prefabs_elemental = {
    "golem",
    "reticuleaoesummon",
    "reticuleaoesummonping",
    "reticuleaoesummontarget",
}
--------------------------------------------------------------------------

local function ReticuleTargetFn()
    local player = ThePlayer
    local ground = TheWorld.Map
    local pos = Vector3()
    --Cast range is 8, leave room for error
    --4 is the aoe range
    for r = 7, 0, -.25 do
        pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
        if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
            return pos
        end
    end
    return pos
end

local CastSpell = {
	fossil = function(inst, caster, pos)
		if caster.components.buffable then
			inst.components.fossilizer:SetDuration(caster.components.buffable:ApplyStatBuffs({"spell_duration"}, TUNING.FORGE.PETRIFYINGTOME.DURATION))
		else
			inst.components.fossilizer:SetDuration(TUNING.FORGE.PETRIFYINGTOME.DURATION)
		end
		inst.components.fossilizer:Fossilize(pos, caster)
		
		inst.components.rechargeable:StartRecharge()
		inst.components.aoespell:OnSpellCast(caster)
	end,
	
	elemental = function(inst, caster, pos)
		local golem = SpawnPrefab("golem")
		golem.Transform:SetPosition(pos:Get())
		inst.components.rechargeable:StartRecharge()
		if caster.components.buffable then
			--function self:AddDamageBuff(buffname, data, recieved)
			local mults, adds, flats = caster.components.buffable:GetStatBuffs({"spell_dmg"})
			--print("mults: " .. tostring(mults))
			--print("adds: " .. tostring(adds))
			--print("flats: " .. tostring(flats))
			golem.components.combat:AddDamageBuff("spell_dmg_mult", {buff = mults, addtype = "mult"})
			golem.components.combat:AddDamageBuff("spell_dmg_add", {buff = (adds - 1), addtype = "add"})
			golem.components.combat:AddDamageBuff("spell_dmg_flat", {buff = flats, addtype = "flat"})
			--golem.components.combat.damagemultiplier = caster.components.buffable:GetStatBuffs("spell_dmg", 1)
			golem.ischarged = caster.components.buffable:HasBuff("amplify") -- TODO only buff on amplify? should it be based on damage increase (mainly for modded support)?
		end
		if caster then
			--if caster.components.combat then caster.components.combat:CopyBuffsTo(golem) end
			if TheWorld.components.stat_tracker then
				TheWorld.components.stat_tracker:InitializePetStats(golem, caster.userid)
			end
			golem:ListenForEvent("onhitother", function(inst, data) -- TODO need to add pet dmg type
				caster:SpawnChild("damage_number"):UpdateDamageNumbers(caster, data.target, math.floor(data.damageresolved + 0.5), nil, data.stimuli, false)
			end)
			golem.components.follower:SetLeader(caster)
		end
		inst.components.aoespell:OnSpellCast(caster)
	end
}

local GetSpellType = {
	fossil = "cc",
	elemental = "pet",
}

local function MakeBook(booktype, reticule, prefabs)
    local name = "book_"..booktype
    local assets = {
        Asset("ANIM", "anim/"..name..".zip"),
        Asset("ANIM", "anim/swap_"..name..".zip"),
    }
	
	local function onequip(inst, owner)
		owner.AnimState:ClearOverrideSymbol("swap_object")
		owner.AnimState:OverrideSymbol("book_closed", "swap_"..name, "book_closed")
	end
	
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)
	
		inst.nameoverride = name

        inst.AnimState:SetBank(name)
        inst.AnimState:SetBuild(name)
        inst.AnimState:PlayAnimation(name)

        inst:AddTag("book")

        --rechargeable (from rechargeable component) added to pristine state for optimization
        inst:AddTag("rechargeable")

        inst:AddComponent("aoetargeting")
        inst.components.aoetargeting.reticule.reticuleprefab = reticule
        inst.components.aoetargeting.reticule.pingprefab = reticule.."ping"
        inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
        inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
        inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
        inst.components.aoetargeting.reticule.ease = true
        inst.components.aoetargeting.reticule.mouseenabled = true

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		inst.castsound = "dontstarve/common/lava_arena/spell/fossilized"

		-- Fox: Initializing reticule_spawner before aoespell
		inst:AddComponent("reticule_spawner")
		inst.components.reticule_spawner:Setup(unpack(TUNING.FORGE.RET_DATA[booktype]))
		
		inst:AddComponent("aoespell")
		inst.components.aoespell:SetAOESpell(CastSpell[booktype])
		inst.components.aoespell:SetSpellType(GetSpellType[booktype])
		
		inst:AddComponent("rechargeable")
		inst.components.rechargeable:SetRechargeTime(18)
		
		inst:AddComponent("weapon")
		inst.components.weapon:SetDamage(TUNING.FORGE[string.upper(booktype == "fossil" and "petrifyingtome" or "bacontome")].DAMAGE)
		inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)
		
		inst:AddComponent("itemtype")
		inst.components.itemtype:SetType("books")
		
		inst:AddComponent("inspectable")
		
		inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.imagename = name

		inst:AddComponent("equippable")
		inst.components.equippable:SetOnEquip(onequip)
		
		if booktype == "fossil" then
			inst:AddComponent("fossilizer")
		end
		
		--inst.damage_type = TUNING.FORGE[string.upper(booktype == "fossil" and "petrifyingtome" or "bacontome")].TYPE
		
        return inst
    end

    return ForgePrefab(booktype == "fossil" and "petrifyingtome" or "bacontome", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", name .. ".tex", nil, TUNING.FORGE[string.upper(booktype == "fossil" and "petrifyingtome" or "bacontome")], STRINGS.FORGED_FORGE.WEAPONS[string.upper(booktype == "fossil" and "petrifyingtome" or "bacontome")].ABILITIES)
end

return MakeBook("fossil", "reticuleaoe", prefabs_fossil),
    MakeBook("elemental", "reticuleaoesummon", prefabs_elemental)