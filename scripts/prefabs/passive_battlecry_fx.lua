--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]
require "admin_commands"

local function MakeBattleCry(name, build, scale, offset)
	
    local assets =
    {
        Asset("ANIM", "anim/"..build..".zip"),
    }

    local function fn()
        local inst = CreateEntity()
		
        inst.entity:AddTransform()
        inst.entity:AddFollower()
		inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        inst.AnimState:SetBank(build)
        inst.AnimState:SetBuild(build)
        inst.AnimState:PlayAnimation("in")
        inst.AnimState:SetMultColour(.5, .5, .5, .5)

        inst.Transform:SetScale(scale, scale, scale)

        inst:AddTag("DECOR")
        inst:AddTag("NOCLICK")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		-- Create Battle Cry icon for the given target
		inst.SetTarget = function(inst_tar, target, damage_mult, store_time)
			-- Create Battle Cry icon
			inst_tar.Follower:FollowSymbol(target.GUID, "torso", offset, offset, 1)
			target.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/battle_cry")		
			
			-- Buff damage for next hit
			if target.components.combat --[[buffable]] then
				--target.components.buffable:AddBuff("battlecry", {dmg_mult = TUNING.FORGE.BATTLECRY.DAMAGE_MULTIPLIER_INCREASE})
				target.components.combat:AddDamageBuff("battlecry", {buff = damage_mult}, false)
			end
			-- Reset when used
			if target.prefab ~= "wathgrithr" then -- TODO need to have way to check for spells, there is the spell complete event, so in attack complete have it check for alt, if spell then dont remove? in spell complete remove it? better way? would need an event check for alt then that aren't spells...hmmm
				local function RemoveBattleCryBuff(self, data)
					if self.components.combat --[[buffable]] then
						--self.components.buffable:RemoveBuff("battlecry")
						self.components.combat:RemoveDamageBuff("battlecry")
					end
					if self.battlecry_fx then
						self.battlecry_fx:RemoveBattleCryFX()
						self.battlecry_fx = nil
						Debug:Print("[BattleCryFX] Battle Cry has been removed for " .. tostring(self.name) .. " (" .. tostring(self.userid) .. ")")
					end
				end
				target:ListenForEvent("onhitother", function(self, data)
					if not (data.weapon and data.weapon.components.weapon.isaltattacking) then
						RemoveBattleCryBuff(self, data)
					end
				end)
				target:ListenForEvent("spell_complete", function(self, data)
					if data.spell_type == "damage" then
						RemoveBattleCryBuff(self, data)
					end
				end)
				target:ListenForEvent("alt_attack_complete", function(self, data) RemoveBattleCryBuff(self, data) end)
			end
			
			-- Check for special weapon buffs
			local item = target.replica.inventory:GetEquippedItem(_G.EQUIPSLOTS.HANDS) or nil
			if item then
                if item.components.multithruster then
                    item.components.multithruster.ready = true
                end
                if item.components.helmsplitter then
                    item.components.helmsplitter.ready = true
                end
			end
			
			Debug:Print("[BattleCryFX] Battle Cry created for " .. tostring(target.name) .. " (" .. tostring(target.userid) .. ")")
			
			-- Create timer for Battle Cry duration
			inst:DoTaskInTime(store_time, function() 
				if target.battlecry_fx then
					if target.prefab == "wathgrithr" then
						target.components.passive_battlecry:ResetBattleCry()
					else
						target.battlecry_fx:RemoveBattleCryFX()
						target.battlecry_fx = nil
						if target.components.combat --[[buffable]] then
							--target.components.buffable:RemoveBuff("battlecry")
							target.components.combat:RemoveDamageBuff("battlecry")
						end
					end
				end
				Debug:Print("[BattleCryFX] Battle Cry has expired for " .. tostring(target.name) .. " (" .. tostring(target.userid) .. ")")
			end)
		end
		
		-- Remove the Battle Cry
		inst.RemoveBattleCryFX = function()		
			-- Play ending animation of the Battle Cry
			inst.AnimState:PlayAnimation("out")
		end
		
		-- Update animation at the end of each animation
		inst:ListenForEvent("animover", function(inst)
			-- idle animation until the "out" animation
			if inst.AnimState:IsCurrentAnimation("out") then
				inst:Remove()
			else
				inst.AnimState:PlayAnimation("idle")
			end
		end)
        return inst
    end
	
	-- TODO this is for admin commands, change how this is done
	if name == "passive_battlecry_fx_self" then
		return ForgePrefab(name, fn, assets, nil, nil, "BUFFS", false, "images/forged_forge.xml", "boarilla_icon.tex", nil, TUNING.FORGE.BATTLECRY, nil, nil, nil, function(self, data) SpawnBattleCry(data.player) end)
	else
		return Prefab(name, fn, assets)
	end
end

return MakeBattleCry("passive_battlecry_fx_self", "lavaarena_attack_buff_effect", 1.4, 0), MakeBattleCry("passive_battlecry_fx_other", "lavaarena_attack_buff_effect2", 1, 1)