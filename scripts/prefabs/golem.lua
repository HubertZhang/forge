--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local brain = require("brains/golembrain")

local function OnNewTarget(inst, data)

end

local function RetargetFn(inst)
		return FindEntity(
            inst,
            TUNING.FORGE.GOLEM.HIT_RANGE,
            function(target)
				return inst.components.combat:CanTarget(target) and target.sg and not (target.sg:HasStateTag("sleeping") or target.sg:HasStateTag("fossilized")) and (target:HasTag("_isinheals") and target.components.combat.lastattacker ~= inst or not target:HasTag("_isinheals"))
            end,
            nil,
            { "player", "companion" },
			{"monster", "hostile", "LA_mob"}
        )
end

local function KeepTarget(inst, target)
	--Note: lose target if you haven't been hit or can't hit others.
    return inst.components.combat:CanTarget(target) and inst:IsNear(target, TUNING.FORGE.GOLEM.HIT_RANGE) and not (target.sg and (target.sg:HasStateTag("sleeping") or target.sg:HasStateTag("fossilized"))) and (target:HasTag("_isinheals") and target.components.combat.lastattacker ~= inst or not target:HasTag("_isinheals"))
end

local function EquipWeapon(inst)
    if inst.components.inventory ~= nil and not inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
        local weapon = CreateEntity()
        --[[Non-networked entity]]
        weapon.entity:AddTransform()
        weapon:AddComponent("weapon")
        weapon.components.weapon:SetDamage(inst.components.combat.defaultdamage)
        weapon.components.weapon:SetRange(inst.components.combat.attackrange)
        weapon.components.weapon:SetProjectile("forge_fireball_projectile")
		weapon.components.weapon:SetDamageType(DAMAGETYPES.MAGIC)
        weapon:AddComponent("inventoryitem")
        weapon.persists = false
        weapon.components.inventoryitem:SetOnDroppedFn(inst.Remove)
        weapon:AddComponent("equippable")
        
        inst.components.inventory:Equip(weapon)
    end
end

local function SetCharged(inst)
	if inst.ischarged then
		inst.AnimState:Show("head_spikes")
	end
end

local function OnAttack(inst)
	SpawnPrefab("fireball_cast_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
end

local function Die(inst)
	if inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("attack") then
		inst.sg.statemem.wants_to_die = true
	else
		inst.sg:GoToState("death")
	end
end

local function SetStayTime(inst)
	if not inst.canstay then
		inst:DoTaskInTime(TUNING.FORGE.GOLEM.LIFE_TIME, Die)
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
	inst.entity:AddPhysics()
    inst.entity:AddNetwork()

	inst.DynamicShadow:SetSize(1.1, .7)

    inst.Transform:SetFourFaced()

    inst:AddTag("character")
    inst:AddTag("scarytoprey")
    inst:AddTag("elemental")
    inst:AddTag("companion")
    inst:AddTag("flying")
    inst:AddTag("notraptrigger")
    inst:AddTag("NOCLICK")
	inst:AddTag("notarget")
	
    inst.AnimState:SetBank("lavaarena_elemental_basic")
    inst.AnimState:SetBuild("lavaarena_elemental_basic")
    inst.AnimState:Hide("head_spikes")
    inst.AnimState:PlayAnimation("idle", true)
	
	inst:SetPhysicsRadiusOverride(.65)
    inst.Physics:SetMass(450)
    inst.Physics:SetFriction(10)
    inst.Physics:SetDamping(5)
    inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:CollidesWith(COLLISION.GIANTS)
    inst.Physics:SetCapsule(inst.physicsradiusoverride, 1)

    inst.nameoverride = "lavaarena_elemental" --So we don't have to make the describe strings.
	
	inst.entity:SetPristine()
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	--inst:AddComponent("fossilizable")

    inst:AddComponent("locomotor") -- Pretty sure locomotor is needed
    inst.components.locomotor.runspeed = 0
	inst.components.locomotor.walkspeed = 0
    inst:SetStateGraph("SGgolem")

    inst:SetBrain(brain)

    inst:AddComponent("follower")
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(1)

    --inst:AddComponent("sanityaura")
    --inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED
	
	inst.no_knockback = true

	

    inst:AddComponent("combat")
	inst.components.combat.playerdamagepercent = 1
	inst.components.combat:SetRange(TUNING.FORGE.GOLEM.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(5, 1)
    inst.components.combat:SetDefaultDamage(TUNING.FORGE.GOLEM.DAMAGE)
    inst.components.combat:SetAttackPeriod(0)
    inst.components.combat:SetRetargetFunction(1, RetargetFn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    --inst.components.combat:SetHurtSound("dontstarve/creatures/hound/hurt")

	inst:DoTaskInTime(0, SetCharged)
	
	SetStayTime(inst)

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('boarilla')
	
	inst:AddComponent("inventory")

    inst:AddComponent("inspectable")
	
    -- inst.OnEntitySleep = OnEntitySleep

	EquipWeapon(inst)

    return inst
end

return Prefab("golem", fn)
