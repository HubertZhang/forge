--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/blowdart_lava.zip"),
    Asset("ANIM", "anim/swap_blowdart_lava.zip"),
}

local assets_projectile = {
    Asset("ANIM", "anim/lavaarena_blowdart_attacks.zip"),
}

local prefabs = {
    "forgedarts_projectile",
    "forgedarts_projectile_alt",
    "reticulelongmulti",
    "reticulelongmultiping",
}

local prefabs_projectile = {
    "weaponsparks_piercing",
}

local PROJECTILE_DELAY = 4 * FRAMES

--------------------------------------------------------------------------

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_blowdart_lava", "swap_blowdart_lava")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    return Vector3(ThePlayer.entity:LocalToWorldSpace(6.5, 0, 0))
end

local function ReticuleMouseTargetFn(inst, mousepos)
    if mousepos ~= nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        local dx = mousepos.x - x
        local dz = mousepos.z - z
        local l = dx * dx + dz * dz
        if l <= 0 then
            return inst.components.reticule.targetpos
        end
        l = 6.5 / math.sqrt(l)
        return Vector3(x + dx * l, 0, z + dz * l)
    end
end

local function ReticuleUpdatePositionFn(inst, pos, reticule, ease, smoothing, dt)
    local x, y, z = inst.Transform:GetWorldPosition()
    reticule.Transform:SetPosition(x, 0, z)
    local rot = -math.atan2(pos.z - z, pos.x - x) / DEGREES
    if ease and dt ~= nil then
        local rot0 = reticule.Transform:GetRotation()
        local drot = rot - rot0
        rot = Lerp((drot > 180 and rot0 + 360) or (drot < -180 and rot0 - 360) or rot0, rot, dt * smoothing)
    end
    reticule.Transform:SetRotation(rot)
end

local function Barrage(inst, caster, pos)
	for i = 1,6 do
		inst:DoTaskInTime(0.08 * i, function()
			local scaler = math.random()*2.5 - 1.25
			local offset = Vector3(math.random(), 0, math.random()):Normalize()*scaler
			local dart = SpawnPrefab("forgedarts_projectile_alt")
			if i == 1 then dart.components.aimedprojectile:SetStimuli("strong") end
			dart.Transform:SetPosition((inst:GetPosition() + offset):Get())
			dart.components.aimedprojectile:Throw(inst, caster, pos + offset)
			dart.components.aimedprojectile:DelayVisibility(inst.projectiledelay)
		end)
	end
	caster.SoundEmitter:PlaySound("dontstarve/common/lava_arena/blow_dart_spread")
	inst.components.rechargeable:StartRecharge()
	inst.components.aoespell:OnSpellCast(caster)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	inst.nameoverride = "blowdart_lava"

    inst.AnimState:SetBank("blowdart_lava")
    inst.AnimState:SetBuild("blowdart_lava")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("blowdart")
    inst:AddTag("aoeblowdart_long")
    inst:AddTag("sharp")

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting:SetAlwaysValid(true)
    inst.components.aoetargeting.reticule.reticuleprefab = "reticulelongmulti"
    inst.components.aoetargeting.reticule.pingprefab = "reticulelongmultiping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.mousetargetfn = ReticuleMouseTargetFn
    inst.components.aoetargeting.reticule.updatepositionfn = ReticuleUpdatePositionFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.projectiledelay = PROJECTILE_DELAY

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

   	inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(Barrage)
	inst.components.aoespell:SetSpellType("darts") -- TODO this is hack for barrage to count as 1 alt attack
	
	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.FORGEDARTS.COOLDOWN)
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.FORGEDARTS.DAMAGE)
    inst.components.weapon:SetRange(10, 20)
    inst.components.weapon:SetProjectile("forgedarts_projectile")
	inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)
	--inst.components.weapon:SetAltDamage(TUNING.FORGE.FORGEDARTS.DAMAGE)
	inst.components.weapon:SetAltAttack(TUNING.FORGE.FORGEDARTS.FORGEDARTS, {10, 20}, nil, DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("darts")
	
	inst:AddComponent("inspectable")
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "blowdart_lava"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

	--inst.damage_type = TUNING.FORGE.FORGEDARTS.TYPE
	
    return inst
end

--------------------------------------------------------------------------

local FADE_FRAMES = 5

local function CreateTail()
    local inst = CreateEntity()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("lavaarena_blowdart_attacks")
    inst.AnimState:SetBuild("lavaarena_blowdart_attacks")
    inst.AnimState:PlayAnimation("tail_1")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)

    inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function OnUpdateProjectileTail(inst)
    local c = (not inst.entity:IsVisible() and 0) or (inst._fade ~= nil and (FADE_FRAMES - inst._fade:value() + 1) / FADE_FRAMES) or 1
    if c > 0 then
        local tail = CreateTail()
        tail.Transform:SetPosition(inst.Transform:GetWorldPosition())
        tail.Transform:SetRotation(inst.Transform:GetRotation())
        if c < 1 then
            tail.AnimState:SetTime(c * tail.AnimState:GetCurrentAnimationLength())
        end
    end
end

local function OnHit(inst, target, owner)
	SpawnPrefab("weaponsparks_fx"):SetPiercing(inst, target)
	if target then
		target.dartalt_hits = target.dartalt_hits and (target.dartalt_hits + 1) or 1
		if not target.dartalt_task then
			target.dartalt_task = target:DoTaskInTime(1, function(inst) inst.dartalt_hits = nil end)
		end
	end
	inst:Remove()
end

local function OnMiss(inst, owner)
	inst:Remove()
end

local function commonprojectilefn(alt)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    RemovePhysicsColliders(inst)

    inst.AnimState:SetBank("lavaarena_blowdart_attacks")
    inst.AnimState:SetBuild("lavaarena_blowdart_attacks")
    inst.AnimState:PlayAnimation("attack_3", true)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetAddColour(1, 1, 0, 0)
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    --projectile (from projectile component) added to pristine state for optimization
    inst:AddTag("projectile")

    if not TheNet:IsDedicated() then
        inst:DoPeriodicTask(0, OnUpdateProjectileTail)
    end

    if alt then
        inst._fade = net_tinybyte(inst.GUID, "blowdart_lava_projectile_alt._fade")
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	if not alt then
		inst:AddComponent("projectile")
		inst.components.projectile:SetSpeed(35)
		inst.components.projectile:SetRange(20)
		inst.components.projectile:SetHitDist(0.5)
		inst.components.projectile:SetOnHitFn(function(inst, attacker, target) OnHit(inst, target) end)
		inst.components.projectile:SetOnMissFn(inst.Remove)
		inst.components.projectile:SetLaunchOffset(Vector3(-2, 1, 0))
	else
		inst:AddComponent("aimedprojectile")
		inst.components.aimedprojectile:SetSpeed(35)
		inst.components.aimedprojectile:SetRange(30)
		inst.components.aimedprojectile:SetHitDistance(0.5)
		inst.components.aimedprojectile:SetOnHitFn(function(inst, owner, attacker, target) OnHit(inst, target, owner) end)
		inst.components.aimedprojectile:SetOnMissFn(OnMiss)
		--inst.components.aimedprojectile.launchoffset = Vector3(-2, 1, 0) --We can't use this because distsq is not ignoring the Y.
	end
	
	inst:DoTaskInTime(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/blow_dart") end)
	
    return inst
end

local function projectilefn()
    return commonprojectilefn(false)
end

local function projectilealtfn()
    return commonprojectilefn(true)
end

return ForgePrefab("forgedarts", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "blowdart_lava.tex", nil, TUNING.FORGE.FORGEDARTS, STRINGS.FORGED_FORGE.WEAPONS.FORGEDARTS.ABILITIES, "swap_blowdart_lava", "common_hand"),
    Prefab("forgedarts_projectile", projectilefn, assets_projectile, prefabs_projectile),
    Prefab("forgedarts_projectile_alt", projectilealtfn, assets_projectile, prefabs_projectile)
