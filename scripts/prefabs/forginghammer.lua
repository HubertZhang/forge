--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets = {
    Asset("ANIM", "anim/hammer_mjolnir.zip"),
    Asset("ANIM", "anim/swap_hammer_mjolnir.zip"),
}

local assets_crackle = {
    Asset("ANIM", "anim/lavaarena_hammer_attack_fx.zip"),
}

local prefabs = {
    "forginghammer_crackle_fx",
    "forgeelectricute_fx",
    "reticuleaoe",
    "reticuleaoeping",
    "reticuleaoehostiletarget",
    "weaponsparks_fx",
}

local prefabs_crackle = {
    "forginghammer_cracklebase_fx",
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_hammer_mjolnir", "swap_hammer_mjolnir")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    local player = ThePlayer
    local ground = TheWorld.Map
    local pos = Vector3()
    --Cast range is 8, leave room for error
    --4 is the aoe range
    for r = 7, 0, -.25 do
        pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
        if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
            return pos
        end
    end
    return pos
end

local function AnvilStrike(inst, caster, pos)
	caster:PushEvent("combat_leap", {
		targetpos = pos,
		weapon = inst
	})
end

local function OnLeap(inst)
	SpawnPrefab("forginghammer_crackle_fx"):SetTarget(inst)
	inst.components.rechargeable:StartRecharge()
end

local function OnAttack(inst, attacker, target)
	if not inst.components.weapon.isaltattacking then
		SpawnPrefab("weaponsparks_fx"):SetPosition(attacker, target)
		if target and target.components.armorbreak_debuff and not (attacker and attacker.components.passive_shock and attacker.components.passive_shock.shock) then
			target.components.armorbreak_debuff:ApplyDebuff()
		end
	else
		SpawnPrefab("forgeelectricute_fx"):SetTarget(target, true)
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	inst.nameoverride = "hammer_mjolnir"
	
    inst.AnimState:SetBank("hammer_mjolnir")
    inst.AnimState:SetBuild("hammer_mjolnir")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("hammer")

    --aoeweapon_leap (from aoeweapon_leap component) added to pristine state for optimization
    inst:AddTag("aoeweapon_leap")

    --rechargeable (from rechargeable component) added to pristine state for optimization
    inst:AddTag("rechargeable")

    inst:AddComponent("aoetargeting")
    inst.components.aoetargeting.reticule.reticuleprefab = "reticuleaoe"
    inst.components.aoetargeting.reticule.pingprefab = "reticuleaoeping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	-- Fox: Initializing reticule_spawner before aoespell
	inst:AddComponent("reticule_spawner")
	inst.components.reticule_spawner:Setup(unpack(TUNING.FORGE.RET_DATA.hammer))
	
    inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(AnvilStrike)
	
	inst:AddComponent("rechargeable")
	inst.components.rechargeable:SetRechargeTime(TUNING.FORGE.FORGINGHAMMER.COOLDOWN)
	
	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.FORGE.FORGINGHAMMER.DAMAGE)
	inst.components.weapon:SetOnAttack(OnAttack)
	inst.components.weapon:SetDamageType(DAMAGETYPES.PHYSICAL)
	--inst.components.weapon:SetAltDamage(TUNING.FORGE.FORGINGHAMMER.ALT_DAMAGE)
	inst.components.weapon:SetAltAttack(TUNING.FORGE.FORGINGHAMMER.ALT_DAMAGE, TUNING.FORGE.FORGINGHAMMER.ALT_RADIUS, nil, DAMAGETYPES.PHYSICAL)
	
	inst:AddComponent("aoeweapon_leap")
	--inst.components.aoeweapon_leap:SetDamage(TUNING.FORGE.FORGINGHAMMER.ALT_DAMAGE)
	inst.components.aoeweapon_leap:SetStimuli("electric")
	inst.components.aoeweapon_leap:SetOnLeapFn(OnLeap)
	
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("melees")
	
	inst:AddComponent("inspectable")
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "hammer_mjolnir"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	
	--inst.damage_type = TUNING.FORGE.FORGINGHAMMER.TYPE

    return inst
end

local function cracklefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_hammer_attack_fx")
    inst.AnimState:SetBuild("lavaarena_hammer_attack_fx")
    inst.AnimState:PlayAnimation("crackle_hit")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetFinalOffset(1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.SetTarget = function(inst, target)
		inst.Transform:SetPosition(target:GetPosition():Get())
		inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/hammer")
		SpawnPrefab("forginghammer_cracklebase_fx"):SetTarget(inst)
	end
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function cracklebasefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_hammer_attack_fx")
    inst.AnimState:SetBuild("lavaarena_hammer_attack_fx")
    inst.AnimState:PlayAnimation("crackle_projection")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
    inst.AnimState:SetScale(1.5, 1.5)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.SetTarget = function(inst, target)
		inst.Transform:SetPosition(target:GetPosition():Get())
	end
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function electric_fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("lavaarena_hammer_attack_fx")
	inst.AnimState:SetBuild("lavaarena_hammer_attack_fx")
	inst.AnimState:PlayAnimation("crackle_loop")
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	inst.AnimState:SetFinalOffset(1)
	inst.AnimState:SetScale(1.5, 1.5)

	inst:AddTag("FX")
	inst:AddTag("NOCLICK")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst.SetTarget = function(inst, target, sound)
		inst.Transform:SetPosition(target:GetPosition():Get())
		if sound then inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/electric") end
		if target:HasTag("largecreature") or target:HasTag("epic") then inst.AnimState:SetScale(2, 2) end
	end
	
	inst:ListenForEvent("animover", inst.Remove)
	
	return inst
end

return ForgePrefab("forginghammer", fn, assets, prefabs, nil, "WEAPONS", false, "images/inventoryimages.xml", "hammer_mjolnir.tex", nil, TUNING.FORGE.FORGINGHAMMER, STRINGS.FORGED_FORGE.WEAPONS.FORGINGHAMMER.ABILITIES, "swap_hammer_mjolnir", "common_hand"),
    Prefab("forginghammer_crackle_fx", cracklefn, assets_crackle, prefabs_crackle),
    Prefab("forginghammer_cracklebase_fx", cracklebasefn, assets_crackle),
	Prefab("forgeelectricute_fx", electric_fn, assets_crackle)