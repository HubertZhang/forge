--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets_fireballhit =
{
    Asset("ANIM", "anim/fireball_2_fx.zip"),
    Asset("ANIM", "anim/deer_fire_charge.zip"),
}

local assets_blossomhit =
{
    Asset("ANIM", "anim/lavaarena_heal_projectile.zip"),
}

local assets_gooballhit =
{
    Asset("ANIM", "anim/gooball_fx.zip"),
}

--------------------------------------------------------------------------
local function OnHit_Fire(inst, owner, target)
	SpawnPrefab("forge_fireball_hit_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst:Remove()
end
--------------------------------------------------------------------------
local function OnThrown(inst, owner, target)
	SpawnPrefab("fireball_cast_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
end

--------------------------------------------------------------------------

local function CreateTail(bank, build, lightoverride, addcolour, multcolour)
    local inst = CreateEntity()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    MakeInventoryPhysics(inst)
    inst.Physics:ClearCollisionMask()

    inst.AnimState:SetBank(bank)
    inst.AnimState:SetBuild(build)
    inst.AnimState:PlayAnimation("disappear")
    if addcolour ~= nil then
        inst.AnimState:SetAddColour(unpack(addcolour))
    end
    if multcolour ~= nil then
        inst.AnimState:SetMultColour(unpack(multcolour))
    end
    if lightoverride > 0 then
        inst.AnimState:SetLightOverride(lightoverride)
    end
    inst.AnimState:SetFinalOffset(-1)

    inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function OnUpdateProjectileTail(inst, bank, build, speed, lightoverride, addcolour, multcolour, hitfx, tails)
    local x, y, z = inst.Transform:GetWorldPosition()
    for tail, _ in pairs(tails) do
        tail:ForceFacePoint(x, y, z)
    end
    if inst.entity:IsVisible() then
        local tail = CreateTail(bank, build, lightoverride, addcolour, multcolour)
        local rot = inst.Transform:GetRotation()
        tail.Transform:SetRotation(rot)
        rot = rot * DEGREES
        local offsangle = math.random() * 2 * PI
        local offsradius = math.random() * .2 + .2
        local hoffset = math.cos(offsangle) * offsradius
        local voffset = math.sin(offsangle) * offsradius
        tail.Transform:SetPosition(x + math.sin(rot) * hoffset, y + voffset, z + math.cos(rot) * hoffset)
        tail.Physics:SetMotorVel(speed * (.2 + math.random() * .3), 0, 0)
        tails[tail] = true
        inst:ListenForEvent("onremove", function(tail) tails[tail] = nil end, tail)
        tail:ListenForEvent("onremove", function(inst)
            tail.Transform:SetRotation(tail.Transform:GetRotation() + math.random() * 30 - 15)
        end, inst)
    end
end

local function MakeProjectile(name, bank, build, speed, lightoverride, addcolour, multcolour, hitfx)
    local assets =
    {
        Asset("ANIM", "anim/"..build..".zip"),
    }

    local prefabs = hitfx ~= nil and { hitfx } or nil

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)
        RemovePhysicsColliders(inst)

        inst.AnimState:SetBank(bank)
        inst.AnimState:SetBuild(build)
        inst.AnimState:PlayAnimation("idle_loop", true)
        if addcolour ~= nil then
            inst.AnimState:SetAddColour(unpack(addcolour))
        end
        if multcolour ~= nil then
            inst.AnimState:SetMultColour(unpack(multcolour))
        end
        if lightoverride > 0 then
            inst.AnimState:SetLightOverride(lightoverride)
        end
        inst.AnimState:SetFinalOffset(-1)

        --projectile (from projectile component) added to pristine state for optimization
        inst:AddTag("projectile")

        if not TheNet:IsDedicated() then
            inst:DoPeriodicTask(0, OnUpdateProjectileTail, nil, bank, build, speed, lightoverride, addcolour, multcolour, hitfx, {})
        end

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		inst.persists = false

        inst:AddComponent("projectile")
		inst.components.projectile:SetSpeed(speed)
		inst.components.projectile:SetHoming(true)
		inst.components.projectile:SetHitDist(0.5) --Leo: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		inst.components.projectile.onhit = function(inst, owner, target)
			SpawnPrefab(hitfx).Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst:Remove()
		end
		inst.components.projectile:SetOnMissFn(inst.Remove)
		inst.components.projectile:SetStimuli("fire")
		--inst.components.projectile:SetOnThrownFn(OnThrown)

        return inst
    end

    return Prefab(name, fn, assets, prefabs)
end

--------------------------------------------------------------------------

local function fireballhit_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("fireball_fx")
    inst.AnimState:SetBuild("deer_fire_charge")
    inst.AnimState:PlayAnimation("blast")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetLightOverride(1)
    inst.AnimState:SetFinalOffset(-1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()
	
	inst.persists = false
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

--------------------------------------------------------------------------

local function blossomhit_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_heal_projectile")
    inst.AnimState:SetBuild("lavaarena_heal_projectile")
    inst.AnimState:PlayAnimation("hit")
    inst.AnimState:SetAddColour(0, .1, .05, 0)
    inst.AnimState:SetFinalOffset(-1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()
	
	inst.persists = false
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

--------------------------------------------------------------------------

local function gooballhit_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("gooball_fx")
    inst.AnimState:SetBuild("gooball_fx")
    inst.AnimState:PlayAnimation("blast")
    inst.AnimState:SetMultColour(.2, 1, 0, 1)
    inst.AnimState:SetFinalOffset(-1)
	
	inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/poison_hit")

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()
	
	inst.persists = false
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

--------------------------------------------------------------------------

return MakeProjectile("forge_fireball_projectile", "fireball_fx", "fireball_2_fx", 15, 1, nil, nil, "forge_fireball_hit_fx"),
    MakeProjectile("forge_blossom_projectile", "lavaarena_heal_projectile", "lavaarena_heal_projectile", 15, 0, { 0, .2, .1, 0 }, nil, "forge_blossom_hit_fx"),
    MakeProjectile("forge_gooball_projectile", "gooball_fx", "gooball_fx", 20, 0, nil, { .2, 1, 0, 1 }, "forge_gooball_hit_fx"),
    Prefab("forge_fireball_hit_fx", fireballhit_fn, assets_fireballhit),
    Prefab("forge_blossom_hit_fx", blossomhit_fn, assets_blossomhit),
    Prefab("forge_gooball_hit_fx", gooballhit_fn, assets_gooballhit),
    --Leo: Blame klei for not allowing to edit projectiles spawned via weapon component. 
    MakeProjectile("forge_fireball_projectile_fast", "fireball_fx", "fireball_2_fx", 30, 1, nil, nil, "forge_fireball_hit_fx")