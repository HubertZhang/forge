--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local assets =
{
    Asset("ANIM", "anim/bernie.zip"),
    Asset("ANIM", "anim/bernie_build.zip"),
}

local prefabs =
{
    "small_puff",
}

local brain = require("brains/bernie_forgebrain")

local function CanBeRevivedBy(inst, reviver)
    return reviver:HasTag("bernie_reviver")
end

local function GetLeader(inst)
	if inst.components.follower.leader ~= nil then
		inst._leader = inst.components.follower.leader
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    inst:SetPhysicsRadiusOverride(.35)
    MakeCharacterPhysics(inst, 50, inst.physicsradiusoverride)
    inst.DynamicShadow:SetSize(1.1, .55)

    inst.Transform:SetScale(1.4, 1.4, 1.4)
    inst.Transform:SetFourFaced()

    inst.AnimState:SetBank("bernie")
    inst.AnimState:SetBuild("bernie_build")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst:AddTag("character")
    inst:AddTag("smallcreature")
    inst:AddTag("companion")

	--For some reason Klei has this on their clientside???
    inst:AddComponent("revivablecorpse")
    inst.components.revivablecorpse:SetCanBeRevivedByFn(CanBeRevivedBy)
	inst.components.revivablecorpse:SetReviveHealthPercent(1)
	inst.components.revivablecorpse:SetReviveSpeedMult(0.5)

	inst.nameoverride = "lavaarena_bernie" --So we don't have to make the describe strings.
	
    inst.entity:SetPristine()
	
	inst.ForgeOnSpawn = function(self)
		local fx = _G.SpawnPrefab("collapse_small") --its either this or smallpuff
		fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
	end
	
    if not TheWorld.ismastersim then
        return inst
    end
	
    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.walkspeed = TUNING.BERNIE_SPEED
    inst:SetStateGraph("SGbernie_forge")

    inst:SetBrain(brain)
	
    inst:AddComponent("follower")
	inst.components.follower:KeepLeaderOnAttacked()
	inst.components.follower.keepdeadleader = true
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(1000)
	inst.components.health.nofadeout = true

    inst:AddComponent("combat")
	
	inst:AddComponent("timer")
	
	inst:AddComponent("colouradder")
	
	inst:AddComponent("colourfader")
	
	inst:AddComponent("bloomer")

    inst:AddComponent("lootdropper")
	
	inst:AddComponent("debuffable")
	
	inst:AddComponent("inspectable")
	
	inst:DoTaskInTime(0, GetLeader)

    return inst
end

return Prefab("forge_bernie", fn, assets, prefabs)
