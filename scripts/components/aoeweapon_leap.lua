--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local AOEWeapon_Leap = Class(function(self, inst)
    self.inst = inst
	self.range = 4
	self.damage = nil
	self.stimuli = nil
	self.onleap = nil
end)

function AOEWeapon_Leap:SetRange(range)
	self.range = range
end

--[[function AOEWeapon_Leap:SetDamage(damage)
	self.damage = damage
end]]

function AOEWeapon_Leap:SetStimuli(stimuli)
	self.stimuli = stimuli
end

function AOEWeapon_Leap:SetOnLeapFn(fn)
	self.onleap = fn
end

function AOEWeapon_Leap:DoLeap(leaper, startingpos, targetpos)
	local found_mobs = {}
	local x, y, z = targetpos:Get()
    local ents = TheSim:FindEntities(x, y, z, self.range, nil, {"player", "companion"})
	for _,ent in ipairs(ents) do
        if leaper ~= nil and ent ~= leaper and leaper.components.combat:IsValidTarget(ent) and ent.components.health then
			table.insert(found_mobs, ent)
            --leaper:PushEvent("onareaattackother", { target = ent, weapon = self.inst, stimuli = self.stimuli })
			--leaper.components.combat:DoAttack(ent, self.inst, nil, self.stimuli, nil, self.damage, true)
        end
    end
	if self.inst.components.weapon and self.inst.components.weapon:HasAltAttack() then
		self.inst.components.weapon:DoAltAttack(leaper, found_mobs, nil, self.stimuli)
	end
	if self.onleap then self.onleap(self.inst, leaper, startingpos, targetpos) end
end

return AOEWeapon_Leap
