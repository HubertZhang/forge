--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details
The source codes does not come with any warranty including
the implied warranty of merchandise.
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local HealAura = Class(function(self, inst)
	self.inst = inst
	self.range = TUNING.FORGE.LIVINGSTAFF.RANGE
	self.duration = TUNING.FORGE.LIVINGSTAFF.DURATION
	self.heal_rate = TUNING.FORGE.LIVINGSTAFF.HEAL_RATE

	self.cache = {}
	self.pos = nil
	self.start_time = GetTime()
	self.current_time = 0
	self.attack_cooldown = 2
	self.oversleep_duration = 2 -- sleep time after heal ends
	self.sleep_duration = self.duration + self.oversleep_duration -- total sleep time

	self.inst:DoTaskInTime(0, inst.StartUpdatingComponent, self)
	self.onunfossilize = function(ent) self:OnUnfossilize(ent) end
	self.onwakeupstat = function(ent) self:OnWakeUpStat(ent) end
end)

local function IsDebuffable(ent)
	return ent.components.debuffable and ent.components.debuffable:IsEnabled()
end

local function IsEntityAlive(ent)
	return ent.components.health and not ent.components.health:IsDead()
end

local function IsPlayerOrAlly(ent)
	return ent:HasTag("player") or ent:HasTag("companion")
end

-- to prevent skipping states with epic mobs unfossilizing and mobs in bunkered, attack, fossilized, or burning states from being slept
local function IsIgnoreState(ent)
	return ent:HasTag("fossilized") or ent:HasTag("fire") or
		ent.sg and (ent.sg:HasStateTag("hiding") or ent.sg.HasStateTag("attack") or ent.sg:HasStateTag("nosleep") or
		(ent:HasTag("epic") and ent.sg:HasStateTag("caninterrupt")))
end

-- we don't want the mob falling asleep while we are attacking them or attacked too recently
function HealAura:IsRecentlyAttacked(ent)
	return self.current_time - ent.components.combat.lastwasattackedtime < (ent.random_attack_cooldown or self.attack_cooldown)
end

function HealAura:AddMobSleep(ent)
	local sleeper = ent.components.sleeper
	if sleeper and not sleeper.isasleep and not self:IsRecentlyAttacked(ent) and not IsIgnoreState(ent) then
		sleeper:AddSleepiness(10, self.sleep_duration - (self.current_time - self.start_time))
		if not ent.stat_sleep_start then
			ent.stat_sleep_start = self.current_time
			-- Update numcc stat
			if self.caster and TheWorld and TheWorld.components.stat_tracker then
				TheWorld.components.stat_tracker:AdjustStat("numcc", self.caster, 1)
			end
		end
	end
end

function HealAura:InRange(ent)
	local pos = ent:GetPosition()
	return distsq(pos.x, pos.z, self.pos.x, self.pos.z) < self.range * self.range
end

function HealAura:OnUpdate(dt)
	self.current_time = GetTime()
	if not self.pos then self.pos = self.inst:GetPosition() end
	local ents = TheSim:FindEntities(self.pos.x, 0, self.pos.z, self.range, {"locomotor"})
	for _, ent in pairs(ents) do
		if not self.cache[ent] and IsEntityAlive(ent) then
			self.cache[ent] = true
			self:OnEntEnter(ent)
		end
	end
	for ent in pairs(self.cache) do
		if not IsEntityAlive(ent) or not self:InRange(ent) then
			self:OnEntLeave(ent)
		end
		if self.cache[ent] and not IsPlayerOrAlly(ent) then
			self:AddMobSleep(ent)
		end
	end
end

function HealAura:OnEntEnter(ent)
	ent:AddTag("_isinheals") -- for targeting and shield behaviours
	if ent.components.colourfader then
		ent.components.colourfader:StartFade({0, 0.3, 0.1}, .35)
	end
	if IsPlayerOrAlly(ent) and IsDebuffable(ent) then
		local debuffable = ent.components.debuffable
		debuffable:AddDebuff("healingcircle_regenbuff", "healingcircle_regenbuff")
		debuffable.debuffs["healingcircle_regenbuff"].inst.heal_value = self.heal_rate * debuffable.debuffs["healingcircle_regenbuff"].inst.tick_rate
		debuffable.debuffs["healingcircle_regenbuff"].inst.caster = self.caster
		if debuffable:HasDebuff("scorpeon_dot") then
			debuffable:RemoveDebuff("scorpeon_dot")
		end
	elseif ent.components.sleeper and not IsPlayerOrAlly(ent) then
		ent.random_attack_cooldown = self.attack_cooldown + math.random()
		if ent:HasTag("epic") then
			ent:ListenForEvent("unfossilize", self.onunfossilize)
		end
		ent:ListenForEvent("onwakeup", self.onwakeupstat)
	end
end

function HealAura:OnEntLeave(ent)
	if not self.cache[ent] then
		print("ERROR: Tried to remove non-exsisting ent!")
		return
	end
	if ent:HasTag("_isinheals") then
		ent:RemoveTag("_isinheals")
	end
	if ent.components.colourfader then
		ent.components.colourfader:StartFade({0, 0, 0}, .35)
	end
	if IsDebuffable(ent) and ent.components.debuffable:HasDebuff("healingcircle_regenbuff") then
		ent.components.debuffable:RemoveDebuff("healingcircle_regenbuff")
	elseif not IsPlayerOrAlly(ent) then
		ent:DoTaskInTime(self.oversleep_duration, function()
			-- prevent mobs waking up if they were pushed out of heal and pushed back in before waking up
			if ent:HasTag("_isinheals") then return end
			-- prevent mobs sleeping instantly if they were pushed out of heal and walked back in
			ent.components.combat.lastwasattackedtime = self.current_time
			if ent.components.sleeper then
				ent.components.sleeper:WakeUp()
				self:OnWakeUpStat(ent)
			end
		end)
		if ent:HasTag("epic") then
			ent:RemoveEventCallback("unfossilize", self.onunfossilize)
		end
		ent:RemoveEventCallback("onwakeup", self.onwakeupstat)
	end
	self.cache[ent] = nil
end

-- this will prevent boarrior and boarilla from sleeping instantly after you fossilize
-- them inside of a heal if they were previously asleep
-- this also fixes a bug where they would not sleep again after being awakened with fossilization
function HealAura:OnUnfossilize(ent)
	if self.current_time - ent.components.sleeper.lasttransitiontime < (ent.random_attack_cooldown or self.attack_cooldown) then
		ent.components.combat.lastwasattackedtime = self.current_time
		ent.components.sleeper:WakeUp() -- fixes a delay with brain not activating
	end
end

-- Update cctime stat
function HealAura:OnWakeUpStat(ent)
	if ent.stat_sleep_start and self.caster and TheWorld and TheWorld.components.stat_tracker then
		TheWorld.components.stat_tracker:AdjustStat("cctime", self.caster, self.current_time - ent.stat_sleep_start)
		ent.stat_sleep_start = nil
	end
end

function HealAura:Stop()
	self.inst:StopUpdatingComponent(self)
	for ent in pairs(self.cache) do
		self:OnEntLeave(ent)
	end
end

HealAura.OnRemoveEntity = HealAura.Stop
HealAura.OnRemoveFromEntity = HealAura.Stop

return HealAura
