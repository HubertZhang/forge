--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local AOEWeapon_Lunge = Class(function(self, inst)
    self.inst = inst
	self.width = 3
	self.damage = nil
	self.stimuli = nil
	self.onlunge = nil
end)

function AOEWeapon_Lunge:SetWidth(width)
	self.width = width
end

--[[function AOEWeapon_Lunge:SetDamage(damage)
	self.damage = damage
end]]

function AOEWeapon_Lunge:SetStimuli(stimuli)
	self.stimuli = stimuli
end

function AOEWeapon_Lunge:SetOnLungeFn(fn)
	self.onlunge = fn
end

function AOEWeapon_Lunge:DoLunge(lunger, startingpos, targetpos)
	local mob_index = {}
	local found_mobs = {}
	for i = 0,10 do
		lunger:DoTaskInTime(FRAMES*math.ceil(1+i/3.5), function()
			local offset = (targetpos - startingpos):GetNormalized()*(i*0.6)
			SpawnPrefab("spear_gungnir_lungefx").Transform:SetPosition((startingpos+offset):Get())
			
			local x, y, z = (startingpos + offset):Get()
			local ents = TheSim:FindEntities(x, y, z, self.width/2, nil, {"player", "companion"})
			for _,ent in ipairs(ents) do
				if not mob_index[ent] and ent ~= lunger and lunger.components.combat:IsValidTarget(ent) and ent.components.health then
					mob_index[ent] = true
					table.insert(found_mobs, ent)
					--lunger:PushEvent("onareaattackother", { target = ent, weapon = self.inst, stimuli = self.stimuli })
					--lunger.components.combat:DoAttack(ent, self.inst, nil, self.stimuli, nil, self.damage, true)
					--SpawnPrefab("forgespear_fx"):SetTarget(ent)
				end
			end
			if i == 10 then
				if self.inst.components.weapon and self.inst.components.weapon:HasAltAttack() then
					self.inst.components.weapon:DoAltAttack(lunger, found_mobs, nil, self.stimuli)
				end
			end
		end)
	end
	
	if self.onlunge ~= nil then self.onlunge(self.inst, lunger, startingpos, targetpos) end
	return true
end

return AOEWeapon_Lunge
