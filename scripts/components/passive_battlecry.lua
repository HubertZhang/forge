--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local Queue = require "util/queue"

local Passive_BattleCry = Class(function(self, inst)
    self.player = inst
	self.battle_cry = false
	self.battle_cry_from_alt = false
	self.hit_stack = List.new()
	self.hit_count = 0
	self.hit_count_trigger = TUNING.FORGE.BATTLECRY.TRIGGER
	self.radius = TUNING.FORGE.BATTLECRY.RADIUS
	self.decay_time = TUNING.FORGE.BATTLECRY.DECAY_TIME
	self.damage_mult = TUNING.FORGE.BATTLECRY.DAMAGE_MULTIPLIER_INCREASE
	self.store_time = TUNING.FORGE.BATTLECRY.STORE_TIME
	
	--[[
	TODO: Test battlecry
		spells may or may not work when triggering battlecry from a spell, not sure if isaltattacking applies to spells.
		Darts do not work perfectly?
	--]]
	self.player:ListenForEvent("onhitother", function(inst, data) 
		if not (data.weapon and data.weapon.components.weapon.isaltattacking and self.battle_cry) then
			self:BattleCryCheck(inst, data)
		end
	end)
	--[[self.player:ListenForEvent("spell_complete", function(inst, data)
		if data.spell_type == "damage" and self.battle_cry and not self.battle_cry_from_alt then
			self:ResetBattleCry()
		end
		self.battle_cry_from_alt = false
	end)--]]
	self.player:ListenForEvent("alt_attack_complete", function(inst, data)
		if self.battle_cry_from_alt then
			self:ApplyBattleCry()
			self.battle_cry_from_alt = false
		elseif self.battle_cry then
			self:ResetBattleCry()
		end
	end)
	
	Debug:Print("[Battle Cry] Initializing for " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ")...")
end)

-- Updates status of wigfrids Battle Cry ability, triggers on 8 consecutive hits
-- Note: for hits to count as consecutive they must hit a target within a certain timeframe
function Passive_BattleCry:BattleCryCheck(inst, data)
	-- Reset Battle Cry
	if self.battle_cry then
		self:ResetBattleCry()
	else
		local current_time = GetTime()
		-- Add current hit
		List.pushright(self.hit_stack, current_time)
		self.hit_count = self.hit_count + 1
		
		Debug:Print("[BattleCry] " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ") hit at " .. tostring(self.hit_stack[self.hit_stack.first]) .. ", adding hit to the stack")
		
		-- Check hit stack
		self:HitStackUpdate(current_time)
		
		-- Check for Battle Cry trigger
		if self.hit_count >= self.hit_count_trigger then
			if data.weapon and data.weapon.components.weapon.isaltattacking then
				self.battle_cry_from_alt = true
			else
				self:ApplyBattleCry()
				self.battle_cry_from_alt = false
			end
			
		end
	end
end


-- Apply the Battle Cry to players
function Passive_BattleCry:ApplyBattleCry()
	Debug:Print("[BattleCry] Applying Battle Cry for " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ")...")
	if self.player.battlecry_fx == nil then
		self.player.battlecry_fx = SpawnPrefab("passive_battlecry_fx_self")
		self.player.battlecry_fx:SetTarget(self.player, self.damage_mult, self.store_time)
		self:ApplyBattleCryToNearbyPlayers()
		self.battle_cry = true
	else
		Debug:Print("[BattleCry] FX already exists for " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ")")
	end
end

-- Resets Battle Cry
function Passive_BattleCry:ResetBattleCry()
	Debug:Print("[BattleCry] Remove and Reset Battle Cry for " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ")")
	self.battle_cry = false
	-- Remove Battle Cry fx
	if self.player.battlecry_fx then
		self.player.battlecry_fx:RemoveBattleCryFX()
		self.player.battlecry_fx = nil
	end
	--self.player.components.buffable:RemoveBuff("battlecry")
	if self.player.components.combat then
		self.player.components.combat:RemoveDamageBuff("battlecry")
	end
	-- Reset hit stack
	self.hit_stack = List.new()
	self.hit_count = 0
end

-- Update the hit stack by removing any expired hits
function Passive_BattleCry:HitStackUpdate(current_time)
	Debug:Print("[BattleCry] " .. tostring(self.player.name) .. "'s (" .. tostring(self.player.userid) .. ") first hit of current stack is " .. tostring(current_time - self.hit_stack[self.hit_stack.first]) .. " (" .. tostring(current_time) .. " - " .. tostring(self.hit_stack[self.hit_stack.first]) .. ") seconds old.")
	
	-- Remove first time entry if expired
	if current_time - self.hit_stack[self.hit_stack.first] > self.decay_time then
		Debug:Print("[BattleCry] Removing first hit of " .. tostring(self.player.name) .. "'s (" .. tostring(self.player.userid) .. ") hit stack")
		List.popleft(self.hit_stack)
		self.hit_count = self.hit_count - 1
		Debug:Print("[BattleCry] " .. tostring(self.player.name) .. "'s (" .. tostring(self.player.userid) .. ") hit stack is at " .. self.hit_count .. " hit(s)")
		if self.hit_count > 0 then
			self:HitStackUpdate(current_time)
		end
	end
end

-- Check for players within BATTLECRY_RADIUS of Wigfrid and give them the battle cry
function Passive_BattleCry:ApplyBattleCryToNearbyPlayers()
	-- Check all players coordinates
	for i,player in pairs(AllPlayers) do
		if self.player.userid ~= player.userid and not player.battlecry_fx and self:PlayerRadiusCheck(Vector3(player.Transform:GetWorldPosition())) and not player.components.health:IsDead() then
			player.battlecry_fx = SpawnPrefab("passive_battlecry_fx_other")
			player.battlecry_fx:SetTarget(player, self.damage_mult, self.store_time)
		end
	end
end

-- Check if given player is within BATTLECRY_RADIUS of Wigfrid
function Passive_BattleCry:PlayerRadiusCheck(player_coord)
	local battlecry_origin = Vector3(self.player.Transform:GetWorldPosition())
	local x_distance = battlecry_origin.x - player_coord.x
	local z_distance = battlecry_origin.z - player_coord.z
	local true_distance = math.sqrt(x_distance^2 + z_distance^2)
	
	return true_distance <= self.radius
end

function Passive_BattleCry:SetHitCountTrigger(val)
	self.hit_count_trigger = val
end

function Passive_BattleCry:SetDecayTime(val)
	self.decay_time = val
end

function Passive_BattleCry:SetRadius(val)
	self.radius = val
end

function Passive_BattleCry:SetDamageMult(val)
	self.damage_mult = val
end

function Passive_BattleCry:SetStoreTime(val)
	self.store_time = val
end

function Passive_BattleCry:GetDebugString()
    return string.format("Status: %s, Hit Count: %d", tostring(self.battle_cry), self.hit_count)
end

return Passive_BattleCry