--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local SHADOW_DISTANCE = 5
local MAX_SHADOW_SPAWN = 5
local angle_per_shadow = 2*math.pi/MAX_SHADOW_SPAWN

local Passive_Shadows = Class(function(self, player)
    self.player = player
	self.damage_dealt = 0
	self.current_target = nil
	self.shadow_fx = {}
	self.passive_attack = false
	self.damage_threshold = TUNING.FORGE.SHADOWS.DAMAGE_DEALT_THRESHOLD
	
	self.offsets = {}
	for i=1, MAX_SHADOW_SPAWN, 1 do
		local angle = angle_per_shadow * i * 2 -- mutliply by 2 gives it the star pattern
		self.offsets[i] = {x = SHADOW_DISTANCE * math.sin(angle), y = 0, z = SHADOW_DISTANCE * math.cos(angle)}
	end
		
	self.player:ListenForEvent("onhitother", function(inst, data) 
		if not (data.weapon and data.weapon.components.weapon.isaltattacking) then
			self:OnDamageDealt(inst, data) 
		else
			print("[Shadows] passive detected, no damage added towards next shadow proc.")
			self.passive_attack = false
		end
	end)
end)

-- TODO triggers on spell cast!!!!!!
-- Update the Shadows status every time damage is dealt
function Passive_Shadows:OnDamageDealt(inst, data)
	-- Reset damage if new target
	if data.target ~= self.current_target then
		self.current_target = data.target
		self.damage_dealt = 0
	end
	if data.damageresolved then
		self.damage_dealt = self.damage_dealt + data.damageresolved
	end
	Debug:Print("[Shadows] DamageUpdate: " .. tostring(self.damage_dealt))
	-- Shadows trigger check
	if self.damage_dealt >= self.damage_threshold then
		self:ApplyShadows()
	end
end

-- Attack the current target with Shadows
function Passive_Shadows:ApplyShadows()
	Debug:Print("[Shadows] Attacking with Shadows...")
	
	self.damage_dealt = 0 -- TODO does the damage start counting immediately after hitting the threshold but before the shadows finish their attack?
	
	for i=1, MAX_SHADOW_SPAWN, 1 do
		self.player:DoTaskInTime(0.1*i, function()
			local target_pos = Point(self.current_target.Transform:GetWorldPosition())
			self.shadow_fx[i] = SpawnPrefab("passive_shadow_fx")
			self.shadow_fx[i]:SetPlayer(self.player)
			self.shadow_fx[i]:SetPosition(target_pos, self.offsets[i])
			self.shadow_fx[i]:SetTarget(self.current_target)
		end)		
	end	
end

-- Remove Shadows and reset it
function Passive_Shadows:RemoveShadows()
	Debug:Print("[Shadows] Removing Shadows...")
--	self.damage_dealt = 0
	
	self.shadow_fx:Remove()
	self.shadow_fx = {}
end

function Passive_Shadows:SetDamageThreshold(val)
	self.damage_threshold = val
end

function Passive_Shadows:GetDebugString()
    return string.format("Target: %s, Damage Dealt: %d", tostring(self.current_target), self.damage_dealt)
end

return Passive_Shadows