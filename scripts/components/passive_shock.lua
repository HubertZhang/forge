--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local Queue = require "util/queue"

local Passive_Shock = Class(function(self, player)
    self.player = player
	self.shock = false
	self.hit_stack = List.new()
	self.hit_count = 0
	self.hit_count_trigger = TUNING.FORGE.SHOCK.TRIGGER
	self.duration = TUNING.FORGE.SHOCK.DURATION
	self.decay_time = TUNING.FORGE.SHOCK.DECAY_TIME
	self.damage = TUNING.FORGE.SHOCK.DAMAGE
	
	self.player:ListenForEvent("attacked", function(inst, data) self:ShockCheck(inst, data) end)
end)

-- Update and check the status of Shock
function Passive_Shock:ShockCheck(inst, data)
	--Don't do anything if the source of damage is a projectile
	if data and data.weapon and (data.weapon.components.projectile or data.weapon.components.complexprojectile or (data.weapon.components.weapon and data.weapon.components.weapon:CanRangedAttack())) then
		return
	end
	
	-- Only check if attacked when Shock is not active
	if not self.shock then
		local current_time = GetTime()
		-- Add current hit
		List.pushright(self.hit_stack, current_time)
		self.hit_count = self.hit_count + 1
		
		-- Check hit stack, only need to check if we have more than 1 hit
		if self.hit_count > 1 then
			self:HitStackUpdate(current_time)
		end
		
		Debug:Print("[Shock] " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ") hit at " .. tostring(self.hit_stack[self.hit_stack.first]) .. ", adding hit to the stack (current count: " .. tostring(self.hit_count) .. ")")
		
		-- Check hit stack
		self:HitStackUpdate(current_time)
		
		-- Check for Shock trigger
		if self.hit_count >= self.hit_count_trigger then
			self:ApplyShock()
			self:Shock(data.attacker)
			
			-- Reset hit stack
			self.hit_stack = List.new()
			self.hit_count = 0
		end
	else
		self:Shock(data.attacker)
	end
end

-- Update the hit stack by removing any expired hits
function Passive_Shock:HitStackUpdate(current_time)
	Debug:Print("[Shock] " .. tostring(self.player.name) .. "'s (" .. tostring(self.player.userid) .. ") last hit of current stack is " .. tostring(current_time - self.hit_stack[self.hit_stack.last]) .. " (" .. tostring(current_time) .. " - " .. tostring(self.hit_stack[self.hit_stack.last]) .. ") seconds old.")
	
	-- Remove last time entry if expired
	local time_diff = current_time - self.hit_stack[self.hit_stack.last]
	if time_diff > self.decay_time then
		-- Remove last time entry time_diff / DECAY_TIME
		while time_diff > self.decay_time and self.hit_count > 0 do
			Debug:Print("[Shock] Removing last hit of " .. tostring(self.player.name) .. "'s (" .. tostring(self.player.userid) .. ") hit stack")
			List.popright(self.hit_stack)
			self.hit_count = self.hit_count - 1
			Debug:Print("[Shock] " .. tostring(self.player.name) .. "'s (" .. tostring(self.player.userid) .. ") hit stack is at " .. self.hit_count .. " hit(s)")
			time_diff = time_diff - self.decay_time
		end
	end
end

-- Apply Shock
function Passive_Shock:ApplyShock()
	self.shock = true
	
	self.player:DoTaskInTime(self.duration, function()
		self:RemoveShock()
	end)	
end

-- Shock the attacker
function Passive_Shock:Shock(attacker)
	if attacker ~= nil then
		--local add, mult = self.player.components.combat:GetBuffMults(false, self.player, attacker, nil, "electric")
		SpawnPrefab("forgeelectricute_fx"):SetTarget(attacker)
		self.player.components.combat:DoSpecialAttack(self.damage, attacker, "electric")
		--attacker.components.combat:GetAttacked(self.player, self.damage * (mult + add), nil, "electric")
		--self.player.components.bloomer:PushBloom("electrocute", "shaders/anim.ksh", -2)
		--self.player.Light:Enable(true)
	end
end

-- Remove Shock
function Passive_Shock:RemoveShock()
	self.shock = false
end

function Passive_Shock:GetDebugString()
    return string.format("Status: %s, Hit Count: %d", tostring(self.shock), self.hit_count)
end

return Passive_Shock