--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details
The source codes does not come with any warranty including
the implied warranty of merchandise.
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local StatTracker = Class(function(self, inst)
	self:InitialSetup()

	-- TODO listener for player reconnect OR have player check if TheWorld.components.stat_tracker exists and then call the AddStatTrackerToPlayer method?
	inst:ListenForEvent("ms_playerspawn", function(inst, player)
		if self.started then
			print("SPAWNED " .. tostring(player.userid))
			self:AddStatTrackerToPlayer(player)
		end
	end)
	inst:ListenForEvent("ms_lavaarena_allplayersspawned", function(inst)
		self:PlayerSetup()
		self.started = true
	end)
	-- TODO need to add field order? then have the values saved at the corresponding indexes so you can return the stats in the correct format for match results
end)

-- Initialize
function StatTracker:InitialSetup()
	self.stat_order = {}
	self.stats = {}
	self.debug_string_list = {} -- is there a way to update this string when adjusting stats and then have GetDebugString just return this? would this work? or same issue?
	self.debug_key_to_index = {}
	self.stat_count = 0
	self.debug_string = ""
	self.nonuser_count = 0
	self.started = false
	self:InitializeForgeStats()
	--self:PlayerSetup()
end

-- Setup each player to start tracking stats
function StatTracker:PlayerSetup()
	self:GetAllPlayersData()
	for i, player in pairs(AllPlayers) do
		if player.userid == nil then
			self.nonuser_count = self.nonuser_count + 1
			player.userid = "AI-" .. tostring(self.nonuser_count)
		end
		self:AddStatTrackerToPlayer(player)
	end
end

-- Get all players user info on game start
function StatTracker:GetAllPlayersData()
	local function GetPlayersClientTable()
		local clients = TheNet:GetClientTable() or {}
		if not TheNet:GetServerIsClientHosted() then
			for i, v in ipairs(clients) do
				if v.performance ~= nil then
					table.remove(clients, i) -- remove "host" object
					break
				end
			end
		end
		return clients
	end
	for i,player in pairs(GetPlayersClientTable()) do
		local base, clothing, portrait = GetSkinsDataFromClientTableData(player)
		self.stats[player.userid] = {
			stat_values = {},
			user_data = {
				user = {
					name = player.name,
					prefab = player.prefab,
					userid = player.userid,
					netid = player.netid,
					base = base,
					body = clothing.body,
					hands = clothing.hands,
					legs = clothing.legs,
					feet = clothing.feet,
					colour = player.colour,
					performance = player.performance,
					portrait = portrait,
				},
				beststat = {"none", nil},
				participation = true,
			},
		}
	end
end

-- Resets everything and reinitializes
function StatTracker:Reset()

	self:InitialSetup()
end

-- Adds the given stat to the tracker
function StatTracker:AddStat(stat)
	self.stats[stat] = {}
end

-- Adjust given stat by given number, if given stat is not already tracked then it is created and set to given value.
function StatTracker:AdjustStat(stat, player, inc_value)
	local userid = player.userid
	if userid == nil then
		print("ERROR [StatTracker] tried to update " .. tostring(stat) .. " by " .. tostring(inc_value) .. " but userid is nil.")
		return
	elseif userid == "" then
		print("AI found, adding to player list...")
		self:AddNonUserPlayer(player)
		userid = player.userid -- update userid for ai
	end
	if self.stats[userid] == nil then
		print("NEW USER " .. tostring(userid))
		self.stats[userid] = {stat_values = {}}
	end
	if self.stats[userid].stat_values == nil then
		self.stats[userid].stat_values = {}
	end
	if self.stat_order[stat] == nil then
		print("ERROR [StatTracker] tried to adjust " .. tostring(stat) .. " a nil stat")
	else
		self.stats[userid].stat_values[self.stat_order[stat]] = (self.stats[userid].stat_values[self.stat_order[stat]] or 0) + (inc_value or 0)
	end
end

--[[
Returns the value of the given stat.
If stat does not exist for the given user or in general then returns 0.
If no userid is nil then it will return the total stat count amoung all players.
--]]
function StatTracker:GetStatTotal(stat, userid)
	if userid == nil and self.stat_order[stat] ~= nil then
		local stat_total = 0
		for id, _ in pairs(self.stats) do
			stat_total = stat_total + (self.stats[id].stat_values[self.stat_order[stat]] or 0)
		end
		return stat_total
	else
		return self.stat_order[stat] and (userid and (self.stats[userid].stat_values[self.stat_order[stat]] or 0)) or 0
	end
end

-- Adds all the default forge stats to the stat tracker, sets them to 0, and sets up their triggers
--[[
Notes:
numcc, cctime, ccbroken
	sleep adjusted in healingcircle
		ccbroken down below
	petrification adjusted in fossilizable
turtillusflips, ccbroken
	SGsnortoise
]]
function StatTracker:InitializeForgeStats()
	for i,stat in pairs(TUNING.FORGE.DEFAULT_FORGE_STATS) do
		self.stat_order[stat] = i
		self.stat_count = self.stat_count + 1
	end
end

-- TODO change player name here?
-- Adds stat tracker to players who are not controlled by users aka AI.
function StatTracker:AddNonUserPlayer(player)
	if player.userid == nil or player.userid == "" then
		print("Adding nonuser " .. tostring(player.prefab))
		self.nonuser_count = self.nonuser_count + 1
		player.userid = "AI-" .. tostring(self.nonuser_count)
		local clothing = player.components.skinner.clothing
		self.stats[player.userid] = {
			stat_values = {},
			user_data = {
				user = {
					name = "AI", -- TODO randomize names?
					prefab = player.prefab,
					userid = player.userid,
					netid = 100,
					base = player.components.skinner.skin_name, -- could force it/randomize it?
					body = clothing.body,
					hands = clothing.hands,
					legs = clothing.legs,
					feet = clothing.feet,
					colour = {0.5, 0.5, 0.5, 1}, -- TODO What colour? currently gray
					performance = 0,
					portrait = "playerportrait_bg_flames",
				},
				beststat = {"none", nil},
				participation = true,
			},
		}
		-- This is needed because player.prefab is not set on spawn
		-- TODO base is not updating until it changes aka mighty/death
		player:DoTaskInTime(1, function(inst)
			print("Getting prefab for " .. tostring(inst.prefab))
			self.stats[player.userid].user_data.user.prefab = inst.prefab
			print("Before " .. tostring(self.stats[player.userid].user_data.user.base))
			self.stats[player.userid].user_data.user.base = inst.prefab .. "_none" -- TODO force it?
			print("After " .. tostring(self.stats[player.userid].user_data.user.base))
		end)
		--self:AddStatTrackerToPlayer(player)
	end
end

function StatTracker:AddStatTrackerToPlayer(player)
	--print("Adding stat tracker to " .. tostring(player).. " id: " .. tostring(player.userid))
	-- combat
	--[[
	player:ListenForEvent("killed", function(inst, data)
		if data.victim.prefab == "battlestandard_damager" or data.victim.prefab == "battlestandard_shield" or data.victim.prefab == "battlestandard_heal" then
			self:AdjustStat("standards", player.userid, 1)
		else
			self:AdjustStat("kills", player, 1)
			--self:AdjustStat(tostring(data.victim.prefab) .. "_kills", 1) -- TODO track specific mob kills
		end
	end)--]]
	player:ListenForEvent("attacked", function(inst, data)
		self:AdjustStat("player_damagetaken", player, data.damageresolved)
		self:AdjustStat("total_damagetaken", player, data.damageresolved)
	end)
	player:ListenForEvent("death", function(inst, data)
		if data.cause ~= "file_load" then
			self:AdjustStat("deaths", player, 1)
		end
	end)
	player:ListenForEvent("alt_attack_complete", function(inst, data)
		if data.weapon and (data.weapon.components.aoespell == nil or data.weapon.components.aoespell and data.weapon.components.aoespell.spell_type == nil) then
			self:AdjustStat("altattacks", player, 1)
		end
	end)
	player:ListenForEvent("spell_complete", function(inst, data)
		if data.spell_type == "darts" then
			self:AdjustStat("altattacks", player, 1)
		else
			self:AdjustStat("spellscast", player, 1)
		end
	end)
	player:ListenForEvent("healthdelta", function(inst, data)
		if data.oldpercent > 0  and data.amount > 0 then -- player was not rez'd
			self:AdjustStat("healingreceived", player, data.amount)
			if data.afflicter then
				self:AdjustStat("healingdone", data.afflicter, data.amount)
			end
		end
	end)
	player:ListenForEvent("onattackother", function(inst, data)
		local ent = data.target
		if ent.components.sleeper and ent.components.sleeper:IsAsleep() then
			-- last_ccbroken_time is to deal with onattackother being fired twice and the mob being asleep for both
			local current_time = GetTime()
			if not ent.last_ccbroken_time or current_time - ent.last_ccbroken_time > 1 then
				self:AdjustStat("ccbroken", player, 1)
				ent.last_ccbroken_time = current_time
			end
		end

		-- Check if guard break is coming -- TODO this might not be accurate?
		if ent.sg and ent.sg:HasStateTag("hiding") and (data.stimuli == "electric" or data.stimuli == "explosive") then
			self:AdjustStat("guardsbroken", player, 1)
		end
	end)
	player:ListenForEvent("respawnfromcorpse", function(inst, data)
		if data.source and data.source then
			self:AdjustStat("corpsesrevived", data.source, 1)
		end
	end)
	-- TODO this may or may not be accurate
	-- ex. what if they never stop moving, not an issue right?
	player:ListenForEvent("locomote", function(inst, data)
		if inst.sg:HasStateTag("busy") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		if is_moving and not should_move then
			self:AdjustStat("stepcounter", player, inst.sg.mem.footsteps)
		end
	end)
	--self.player:ListenForEvent("", function(inst,data) end)
end

--[[ TODO
	Possible bug on reconnect with webber, need further testing
	golem work?
		do we give golem the follower component?
]]
-- Initialize all stats and start tracking stats for the given pet and link those stats to the pets leader. Note if the pets leader changes then the link will automatically change to the new leader and track stats unless new leader is not a player.
function StatTracker:InitializePetStats(pet, userid_override)
	local function AdjustPetStat(stat, amount)
		local player = userid_override or pet.components.follower.leader or nil

		if player ~= nil then
			self:AdjustStat(stat, player, amount)
		end
	end

	-- combat
	--[[
	pet:ListenForEvent("onhitother", function(inst, data)
		AdjustPetStat("pet_damagedealt", data.damageresolved)
		AdjustPetStat("total_damagedealt", data.damageresolved)
	end)--]]

	pet:ListenForEvent("attacked", function(inst, data)
		AdjustPetStat("pet_damagetaken", data.damageresolved)
		AdjustPetStat("total_damagetaken", data.damageresolved)
	end)

	pet:ListenForEvent("death", function(inst, data)
		AdjustPetStat("petdeaths", 1)
	end)
end

-- Track stats for the given mob
function StatTracker:TrackMobStats(mob)
	mob:ListenForEvent("healthdelta", function(inst, data)
		if data.oldpercent > 0 and data.afflicter ~= nil then
			local is_pet = data.afflicter.components.follower ~= nil
			local is_shadows = not is_pet and data.afflicter and data.afflicter.player ~= nil
			local is_weapon = data.afflicter.components.inventoryitem ~= nil
			local player = data.afflicter and (is_pet and data.afflicter.components.follower.leader or is_weapon and data.afflicter.components.inventoryitem.owner or (is_shadows and data.afflicter.player) or data.afflicter)
			if player ~= nil then
				-- Pet Damage
				if is_pet or is_shadows then
					self:AdjustStat("pet_damagedealt", player, math.abs(data.amount))
				-- Player Damage
				else
					self:AdjustStat("player_damagedealt", player, math.abs(data.amount))

					-- Weapon Stats
					local weapon = is_weapon and not data.afflicter.components.weapon.isaltattacking or  player.components.inventory and player.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
					if weapon then
						-- Update Dart Stat
						if (weapon.prefab == "moltendarts" or weapon.prefab == "forgedarts") then -- TODO add tag dart to weapons so custom dart weapons track? mayber track if ammo then track ammo type?
							self:AdjustStat("blowdarts", player, 1)
						-- Update Attack Stat
						elseif not weapon.components.weapon.isaltattacking then
							self:AdjustStat("attacks", player, 1)
						end
					end
				end
				self:AdjustStat("total_damagedealt", player, math.abs(data.amount))

			end

			if mob.components.health:IsDead() then
				if mob.prefab == "battlestandard_damager" or mob.prefab == "battlestandard_shield" or mob.prefab == "battlestandard_heal" then
					self:AdjustStat("standards", player, 1)
				else
					self:AdjustStat("kills", player, 1)
				end
			end
		end
	end)
end

-- Returns all the stat values for the given user
function StatTracker:GetPlayerStats(userid)
	return self.stats[userid].stat_values
end

-- Returns the saved users data
function StatTracker:GetUserData(userid)
	return self.stats[userid].user_data
end

-- Returns the field order of the stats -- TODO might need to change this when adding custom stats since I use a default table
function StatTracker:GetStatFieldOrder()
	return TUNING.FORGE.DEFAULT_FORGE_STATS
end

-- Returns the best stat for each user
function StatTracker:FindBestStats()
	local function GetStatTier(stat, value)
		return TUNING.FORGE.DEFAULT_FORGE_TITLES[stat] and TUNING.FORGE.DEFAULT_FORGE_TITLES[stat].tier2 and (TUNING.FORGE.DEFAULT_FORGE_TITLES[stat].tier2 <= (value or 0)) and 2 or 1
	end

	-- Rank all players for each stat based on their value
	local stat_ranks = {}
	--print("Ranking Stats...")
	for stat,title_info in pairs(TUNING.FORGE.DEFAULT_FORGE_TITLES) do
		--print("- " .. tostring(stat))
		local current_stat_ranks = {}
		for userid,user_info in pairs(self.stats) do
			--print("- - userid: " .. tostring(userid) .. ", value: " .. tostring((user_info.stat_values[self.stat_order[stat]] or 0)))
			table.insert(current_stat_ranks, {userid = userid, value = (user_info.stat_values[self.stat_order[stat]] or 0)})
		end
		table.sort(current_stat_ranks, function(a, b) return a.value > b.value end)
		stat_ranks[stat] = current_stat_ranks
	end

	-- userid: stat, tier, place, value
	-- Find the best stat for each player
	local best_stat = {}
	for stat, values in pairs(stat_ranks) do
		--print("[BestStats] stat: " .. tostring(stat))
		for place, val in ipairs(values) do
			--print(tostring(place) .. ": " .. tostring(val.userid) .. " - " .. tostring(val.value) .. " (" .. tostring(GetStatTier(stat, val.value)) .. ")")
			if (val.value or 0) > 0 and (best_stat[val.userid] == nil or (best_stat[val.userid].place > place or ((best_stat[val.userid].place == place) and ((best_stat[val.userid].tier < GetStatTier(stat, val.value)) or (TUNING.FORGE.DEFAULT_FORGE_TITLES[best_stat[val.userid].stat].priority > TUNING.FORGE.DEFAULT_FORGE_TITLES[stat].priority))))) then
				best_stat[val.userid] = {stat = stat, tier = GetStatTier(stat, val.value), place = place, value = (val.value or 0)}
				self.stats[val.userid].user_data.participation = place > 1
				local stat_str = stat .. (not self.stats[val.userid].user_data.participation and (GetStatTier(stat, val.value) == 2) and "2" or "")
				self.stats[val.userid].user_data.beststat = {stat_str, math.floor((val.value) or 0)}
				--print(tostring(stat) .. " is the new best stat for " .. tostring(val.userid))
			end
		end
	end
	-- best_stat should have the stat that should be displayed
	return best_stat
end

function StatTracker:GetDebugString()
	local STAT_LIMIT_PER_ROW = 3
	local stat_str = "Stats: "
	local count = 1
	--[[
	for stat, value in pairs(self.stats) do
		stat_str = stat_str .. string.format("%s: %d, ", tostring(stat), math.floor(value + 0.5))
		--stat_str = stat_str .. tostring(stat) .. ": " .. tostring(value) .. ", "
		count = count + 1
		if count > STAT_LIMIT_PER_ROW then
			count = 1
			stat_str = stat_str .. "\n"
		end
	end
	--stat_str = string.sub(stat_str,1,-3)
	--]]
    --return stat_str--string.format("%s", stat_str)
	return self.debug_string
end

return StatTracker
