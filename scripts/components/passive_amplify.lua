--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local Passive_Amplify = Class(function(self, player)
    self.player = player
	self.amplify_active = false
	self.damage_dealt = 0
	self.damage_threshold = TUNING.FORGE.AMPLIFY.DAMAGE_DEALT_THRESHOLD
	self.spell_dmg_mult = TUNING.FORGE.AMPLIFY.DAMAGE_MULTIPLIER_INCREASE
	self.spell_heal_rate = TUNING.FORGE.AMPLIFY.HEAL_RATE_INCREASE
	self.spell_duration_mult = TUNING.FORGE.AMPLIFY.DURATION_MULT
	
	self.player:ListenForEvent("onhitother", function(inst, data) self:OnDamageDealt(inst, data) end)
	self.player:ListenForEvent("spell_complete", function(inst, data) self:RemoveAmplify(inst, data) end)
	self.player:ListenForEvent("ms_clientdisconnected", function(inst, data) self:OnDisconnect(inst, data)	end)
	self.player:ListenForEvent("death", function(inst, data) 
		if self.amplify_active then 
			self:RemoveAmplify()
		else
			self:Reset()
		end
	end)
end)

-- Update the Amplify status every time damage is dealt
function Passive_Amplify:OnDamageDealt(inst, data)
	if not self.amplify_active and not (data.weapon and data.weapon.components.weapon.isaltattacking) then
		self.damage_dealt = self.damage_dealt + data.damageresolved
		Debug:Print("[Amplify] DamageUpdate: " .. tostring(self.damage_dealt))
		if self.damage_dealt >= self.damage_threshold then
			self:ApplyAmplify()
		end
	end
end

-- Apply Amplify
local function SetDirty(netvar, val)
	netvar:set_local(val)
	netvar:set(val)
end

function Passive_Amplify:ApplyAmplify()
	Debug:Print("[Amplify] Applying Amplify...")
	self.amplify_active = true
	
	-- Add Buff
	if self.player.components.buffable then
		self.player.components.buffable:AddBuff("amplify", {
			{name = "spell_dmg", val = self.spell_dmg_mult, type = "mult"},-- TODO this is for golem buff atm, need to find a better way of handling this
			{name = "spell_heal_rate", val = self.spell_heal_rate, type = "mult"},
			{name = "spell_duration", val = self.spell_duration_mult, type = "mult"}
		})
		self.player.components.combat:AddDamageBuff("amplify", {buff = function(attacker, victim, weapon, stimuli)
			if weapon and weapon.components.aoespell and weapon.components.weapon.isaltattacking then
				return self.spell_dmg_mult
			else
				return 1
			end
		end}, false)
	end
	
	self.amplify_fx = SpawnPrefab("passive_amplify_fx") -- TODO change prefab names
	self.amplify_fx:SetTarget(self.player)
	
	if self.player.player_classified then
		SetDirty(self.player.player_classified.net_buffs, json.encode({name = "wick_buff", removed = false}))
	end
end

-- Remove Amplify and reset it
function Passive_Amplify:RemoveAmplify()
	if self.amplify_active then
		Debug:Print("[Amplify] Removing Amplify...")		
		self.amplify_fx:RemoveAmplifyFX()
		self.amplify_fx = nil
		if self.player.components.buffable then
			self.player.components.buffable:RemoveBuff("amplify")
			self.player.components.combat:RemoveDamageBuff("amplify")
		end
		self:Reset()
		if self.player.player_classified then
			SetDirty(self.player.player_classified.net_buffs, json.encode({name = "wick_buff", removed = true}))
		end
	end
end

function Passive_Amplify:SetDamageThreshold(val)
	self.damage_threshold = val
end

function Passive_Amplify:SetSpellDamageMult(val)
	self.spell_dmg_mult = val
end

function Passive_Amplify:SetDurationMult(val)
	self.spell_duration_mult = val
end

function Passive_Amplify:SetSpellHeatRate(val)
	self.spell_heal_rate = val
end

function Passive_Amplify:Reset()
	self.amplify_active = false
	self.damage_dealt = 0
end

function Passive_Amplify:OnDisconnect(inst, data)
	if self.player.userid == data.userid then
		self.RemoveAmplify()
	end
end

function Passive_Amplify:GetDebugString()
    return string.format("Status: %s, Damage Dealt: %.1d", tostring(self.amplify_active), self.damage_dealt)
end

return Passive_Amplify