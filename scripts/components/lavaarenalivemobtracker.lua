--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local LavaarenaLiveMobTracker = Class(function(self, inst)

	self.inst = inst
	assert(inst.components.lavaarenamobtracker ~=nil, "LavaarenaMobTracker not found! Can't keep going now can we?")
	local lavaarenamobtracker_StartTracking_old = inst.components.lavaarenamobtracker.StartTracking
	inst.components.lavaarenamobtracker.StartTracking = function(mt, ent) if not ent:HasTag("companion") then
		lavaarenamobtracker_StartTracking_old(mt, ent)
		self._registermob(ent)
		ent:ListenForEvent(ent.deathevent or "death", self._unregistermob)
		ent:ListenForEvent("forceunregister", self._unregistermob)
		end
	end
	
	self.pendingitems_random = {}
	self.pendingitems_final = {}

    self.currentround = 1
    self.currentwave = 1
    self.currentportal = ""

    local function tablesetget2(t, k)
        rawset(t, k, {})
        return rawget(t, k)
    end

    local function tablesetget1(t, k)
        rawset(t, k, setmetatable({}, {__index = tablesetget2}))
        return rawget(t, k)
    end

    local function tablesetget(t, k)
        rawset(t, k, setmetatable({}, {__index = tablesetget1}))
        return rawget(t, k)
    end

	self.livemobs = setmetatable({}, {__index = tablesetget})
	self._registermob = function(mob) self.livemobs[self.currentround][self.currentwave][self.currentportal][mob] = true self.livemobs[self.currentround][self.currentwave]["all"][mob] = true end
	self._unregistermob = function(mob)
        for k, v in pairs(self.livemobs[self.currentround]) do
            for k1, v1 in pairs(v) do
                if v1[mob] then
                    v1[mob] = nil
                end
            end
        end
		if #self:GetAllLiveMobs() == 0 then
			TheWorld:PushEvent("ms_lavaarena_allmobsdied")
		end
	end
end)

local function DropItem(inst, prefab)
	local item = SpawnPrefab(prefab)
	if item == nil then
		return
	end
	local x, y, z = inst:GetPosition():Get()
	item.Transform:SetPosition(x, y, z)
	if item.components.inventoryitem then
		item.components.inventoryitem:DoDropPhysics(x, y, z, true, 1.2)
		Launch(item, inst, 4)
		local lootbeacon = SpawnPrefab("lavaarena_lootbeacon")
		lootbeacon:SetTarget(item)
		
		item:DoTaskInTime(1.5, function(item)
			if not item:IsOnValidGround() then
				local dest = FindNearbyLand(item:GetPosition(), 7)
				if dest ~= nil then
					SpawnAt("spawn_fx_tiny", item)
					item:DoTaskInTime(.5, function()
						SpawnAt("spawn_fx_tiny", dest)
						
						if item.Physics ~= nil then
							item.Physics:Stop()
							item.Physics:Teleport(dest:Get())
						elseif item.Transform ~= nil then
							item.Transform:SetPosition(dest:Get())
						end
					end)
				end
			end
		end)
	end
end

LavaarenaLiveMobTracker.tst = DropItem --for i=1, 30 do TheWorld.components.lavaarenalivemobtracker.tst(ThePlayer, "poop") end

local function ondeath_lootdropper(inst, wave)
	local self = TheWorld.components.lavaarenalivemobtracker
    self.pendingitems_final[wave] = self.pendingitems_final[wave] or {}
    for k, v in pairs(self.pendingitems_final[wave]) do
    	local moblist = self:GetLiveMobs(wave, k)
    	if #moblist == 0 then
    		inst.lootdropqueue = inst.lootdropqueue or {}
    		for k1, v1 in pairs(v) do
    			table.insert(inst.lootdropqueue, v1)
    			v[k1] = nil
    		end
    	end
    end
    inst.lootdropqueue = inst.lootdropqueue or {}
    TheWorld:PushEvent("collectlootformob", {loot = inst.lootdropqueue, ent = inst})
    if inst.lootdropqueue ~= nil then
        for k1, v1 in pairs(inst.lootdropqueue) do
            DropItem(inst, v1)
        end
    end
end

function LavaarenaLiveMobTracker:GetLiveMobs(wave, side)
    local ret = {}
    for k, v in pairs(self.livemobs[self.currentround][wave][side]) do
        table.insert(ret, k)
    end
    return ret
end

function LavaarenaLiveMobTracker:GetAllLiveMobs()
    local ret = {}
    for k, v in pairs(self.livemobs[self.currentround]) do
        for k1, v1 in pairs(v["all"]) do
            if v1 then
                table.insert(ret, k1)
            end
        end
    end
    return ret
end

function LavaarenaLiveMobTracker:GetNumMobsWithItems()
	local count = 0
    for k, v in pairs(self.livemobs) do
        for k1, v1 in pairs(v) do
            for k2, v2 in pairs(v1["all"]) do
                if k and k.lootdropqueue ~= nil then
                    count = count + 1
                end
            end
        end
    end
	return count
end

local function ApplyDropsForSide(self, side, hasitem)
    for ent, _ in pairs(self.livemobs[self.currentround][self.currentwave][side]) do
        if not hasitem[ent] and math.random() > 0.7 and #self.pendingitems_random[side] ~= 0 then
            ent.lootdropqueue = {}
            local itemindex = math.random(#self.pendingitems_random[side])
            table.insert(ent.lootdropqueue, self.pendingitems_random[side][itemindex])
            table.remove(self.pendingitems_random[side], itemindex)
            hasitem[ent] = true
        end
    end
end


function LavaarenaLiveMobTracker:ApplyDrops()
    local hasitem = {}
    if not (#self.pendingitems_random.all + #self.pendingitems_random.left + #self.pendingitems_random.middle + #self.pendingitems_random.right > #self:GetLiveMobs(self.currentwave, "all")) then -- shouldnt have more items than the mobs can hold
        while #self.pendingitems_random.all + #self.pendingitems_random.left + #self.pendingitems_random.middle + #self.pendingitems_random.right > 0 do
            ApplyDropsForSide(self, "left", hasitem)
            ApplyDropsForSide(self, "right", hasitem)
            ApplyDropsForSide(self, "middle", hasitem)
            --do all last, cause we dont want to take possibly limited spaces from left right and middle
            ApplyDropsForSide(self, "all", hasitem)
        end
    end
    for ent, _ in pairs(self.livemobs[self.currentround][self.currentwave]["all"]) do
        local wave = self.currentwave
        ent:ListenForEvent("death", function(inst) ondeath_lootdropper(inst, wave) end)
    end
end

function LavaarenaLiveMobTracker:SetRound(round)
    self.currentround = round
end

function LavaarenaLiveMobTracker:SetWave(wave)
    self.currentwave = wave
end

function LavaarenaLiveMobTracker:SetPortal(portal)
    self.currentportal = portal
end

return LavaarenaLiveMobTracker