--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local Fossilizer = Class(function(self, inst)
	self.inst = inst
	self.duration = TUNING.FORGE.PETRIFYINGTOME.DURATION
	self.range = TUNING.FORGE.PETRIFYINGTOME.ALT_RADIUS
end)

function Fossilizer:SetDuration(duration)
	self.duration = duration
end

function Fossilizer:SpawnFosilFx(pos)
	local positions = {}
	
	--Creating positions to choose where to spawn fx
	for i = 1, 20 do 
		local s = i/5
		local a = math.sqrt(s*512) 
		local b = math.sqrt(s)
		
		local test = Vector3(math.sin(a)*b*1.2, 0, math.cos(a)*b*1.2)
		
		--We don't want to spawn on water, right?
		local tile = TheWorld.Map:GetTileAtPoint((pos + test):Get())
		if tile ~= 1 and tile ~= 255 then
			table.insert(positions, test) 
		end
	end 
	
	local i = 0
	self.spawn_task = self.inst:DoPeriodicTask(.15, function()
		i = i + 1
		
		if i >= 13 and self.spawn_task then -- 2 seconds
			self.spawn_task:Cancel()
			self.spawn_task = nil
			
			return
		end
		
		SpawnAt("fossilizing_fx", pos + positions[math.random(#positions)]).Transform:SetScale(.65, .65, .65)
	end)
end

function Fossilizer:Fossilize(pos, caster)
	self:SpawnFosilFx(pos)
	
	local i = 0
	self.check_task = self.inst:DoPeriodicTask(.25, function()
		local targets = TheSim:FindEntities(pos.x, 0, pos.z, self.range, {"fossilizable"}, {"fossil_immune"}) 
		for _, target in ipairs(targets) do
			if TheWorld and TheWorld.components.stat_tracker and target.sg and target.sg:HasStateTag("hiding") then
				TheWorld.components.stat_tracker:AdjustStat("guardsbroken", caster, 1)
			end
			target:PushEvent("fossilize", {duration = self.duration, doer = caster})
		end
		
		i = i + 1
		if i >= 11 and self.check_task then
			self.check_task:Cancel()
			self.check_task = nil
		end
	end)
end

function Fossilizer:OnRemoveEntity()
	for _, task in ipairs({"spawn_task", "check_task"}) do
		if self[task] ~= nil then
			self[task]:Cancel()
			self[task] = nil
		end
	end
	
	if self.inst.components.reticule_spawner then
		self.inst.components.reticule_spawner:KillRet()
	end
end

Fossilizer.OnRemoveFromEntity = Fossilizer.OnRemoveEntity

function Fossilizer:GetDebugString()
    return string.format("Duration: %2.2f", self.duration)
end

return Fossilizer