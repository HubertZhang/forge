--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local defaultwavemanager = {
    onenter = function(self, mem)
        mem.wavesfinished = 1
        self:PushWave(mem.wavesfinished)
    end,
    onallmobsdied = function(self, mem)
        mem.wavesfinished = mem.wavesfinished + 1
        if mem.wavesfinished <= #mem.round_data.waves then
            self:PushWave(mem.wavesfinished)
            return false
        end
        return true
    end,
}

local function RegisterCenterpoint(inst, center)
	print("REGISTER CENTERPOINT", inst, center)
	inst:DoTaskInTime(0, function()
		inst.center = center
	end)
end

local function RegisterBoarlord(inst, boarlord)
	inst:DoTaskInTime(0, function()
		inst.boarlord = boarlord
	end)
end

local function RegisterStageInfo(inst, stageinfo)
	local self = inst.components.lavaaarenaevent
	if self.stage_info ~= nil then
		self.stage_info:Remove()
	end
	self.stage_info = stageinfo
end

local function OnPlayerDied(world)
	local self = world.components.lavaarenaevent
	local alive_count = 0
	for k, v in pairs(AllPlayers) do
		if not v.components.health:IsDead() then
			alive_count = alive_count + 1
		end
	end
	if alive_count == 0 then
		self:End(false)
	else
		-- CunningFox: Makes pugna laugh
		local target = self:GetBoarlord()
		if target then
			target:PushEvent("laugh")
		end
	end
end

local LavaarenaEvent = Class(function(self, inst)
	self.inst = inst
	
	self.enabled = true
	self.wave_queued = false
	self.start_time = 0
	
	self.current_round = 0
	self.current_wave = 1
	self.boarlord = nil
	self.stage_info = nil
	--self.center = nil
	
    --we can edit this after the AddComponent
    self.loadrounddata = "forge_rounds/"..Forge_options.waveset
	
	-- add register events
	local function self_start()
		TheWorld:SpawnDoyDoy() -- CunningFox: Spawning our doydoy first
							   -- Fid: His name is Carl
							   -- CunningFox: CarlZalph(c)?
							   -- Fid: CarlZalph(c) Jr. is his full name
		self:Start()
	end
	
	self._onallmobsdead = function() self:AllMobsDied() end
    inst:ListenForEvent("spawningfinished", function() if self.wavemanager.onspawningfinished then self.wavemanager.onspawningfinished(self, self.mem) end end)
	inst:ListenForEvent("ms_register_lavaarenacenter", RegisterCenterpoint)
	inst:ListenForEvent("ms_register_lavaarenaboarlord", RegisterBoarlord)
	inst:ListenForEvent("ms_register_lavaarenastageinfo", RegisterStageInfo)
	inst:ListenForEvent("ms_lavaarena_endofstage", self.Progress)
	inst:ListenForEvent("ms_lavaarena_allplayersspawned", self_start)
	inst:ListenForEvent("ms_playerdied", OnPlayerDied)
	inst:ListenForEvent("ms_lavaarena_allmobsdied", self._onallmobsdead)
end)

function LavaarenaEvent:GetBoarlord()
	return self.inst.boarlord
end

function LavaarenaEvent:GetCurrentRound()
	return self.current_round
end

function LavaarenaEvent:GetArenaCenterPoint()
	return self.inst.center ~= nil and self.inst.center:GetPosition() or Vector3()
end

function LavaarenaEvent:GetStageInfo()
	return self.stage_info
end

function LavaarenaEvent:Progress()
 -- fillerm, ill make an actual function later
end

function LavaarenaEvent:DoSpeech(dialog, banter_line)
	local is_banter = string.find(string.upper(dialog), "BANTER", 1) ~= nil
	dialog = STRINGS[string.upper(dialog)] or STRINGS["BOARLORD_"..string.upper(dialog)]
	if dialog ~= nil then
		if is_banter then
			dialog = { dialog[banter_line or math.random(#dialog)] }
		end
		
		local lines = {}
		for i,v in ipairs(dialog) do
			table.insert(lines, {message=v, lineduration=self.rounds_data[self.current_round].lineduration or 3.983333, noanim=false})
		end
		
		local target = self:GetBoarlord()
		if target then
			-- CunningFox: This one is needed for pugna to play proper animations
			target:PushEvent("lines_changed", {lines = #lines})
			
			target.components.talker:Say(lines)
		end
	end
end

function LavaarenaEvent:StartRound(roundnum)
    if self.wavemanager and self.wavemanager.onexit then self.wavemanager.onexit(self, self.mem) end
    self.inst:StopUpdatingComponent(self)

    self.current_round = roundnum
    local round_data = self.rounds_data[roundnum]
    if not round_data then return end

    self.wavemanager = round_data.wavemanager or defaultwavemanager

    self.mem = {round_data = round_data}
    self.wavemanager.onenter(self, self.mem)
    if type(self.wavemanager.onupdate) == "function" then
        self.inst:StartUpdatingComponent(self)
    end
end

function LavaarenaEvent:PushWave(wavenum)
    print("[ForgeRounds] Starting Round: "..self.current_round.." Wave: "..wavenum)

    local round_data = self.rounds_data[self.current_round]
    if round_data == nil then return end
	local wave = round_data.waves[wavenum]
    if wave == nil then return end

    local livetracker = self.inst.components.lavaarenalivemobtracker
    livetracker:SetRound(self.current_round)
    livetracker:SetWave(wavenum)

	local function fn(delay)
		local spawners = self.inst.spawners

		if #wave == 1 then
			spawners.left:SpawnMobs(wave[1], delay)
			spawners.right:SpawnMobs(wave[1], delay)
			spawners.middle:SpawnMobs(wave[1], delay)
		elseif #wave == 2 then
            spawners.left:SpawnMobs(wave[1], delay)
            spawners.right:SpawnMobs(wave[1], delay)
            spawners.middle:SpawnMobs(wave[2], delay)
		elseif #wave == 3 then
            spawners.left:SpawnMobs(wave[1], delay)
            spawners.right:SpawnMobs(wave[3], delay)
            spawners.middle:SpawnMobs(wave[2], delay)
		end

		local function registeritems()
			if round_data.randomitems[wavenum] then
				livetracker.pendingitems_random = round_data.randomitems[wavenum]
			end
			if round_data.finalitems[wavenum] then
				livetracker.pendingitems_final[wavenum] = round_data.finalitems[wavenum]
			end
			livetracker:ApplyDrops()
            self.inst:RemoveEventCallback("spawningfinished", registeritems)
		end

        self.inst:ListenForEvent("spawningfinished", registeritems)

        if round_data.spawningfinished and round_data.spawningfinished[wavenum] then
            local function spawningfinishedcallback()
                self.inst:RemoveEventCallback("spawningfinished", spawningfinishedcallback)
                round_data.spawningfinished[wavenum](self, TheWorld.components.lavaarenalivemobtracker.livemobs[self.current_round][wavenum])
            end
            self.inst:ListenForEvent("spawningfinished", spawningfinishedcallback)
        end
	end

	if round_data.wave_introspeech[wavenum] then
        local pre_delay = round_data.talk_pre_delay and round_data.talk_pre_delay[wavenum] or 3
        local pst_delay = round_data.talk_post_delay and round_data.talk_post_delay[wavenum] or 1
        if pre_delay == 0 then
            self:DoSpeech(round_data.wave_introspeech[wavenum])
            self:QueueBoarlordDoneTalking(function()
                fn(pst_delay)
            end)
        else
            self.inst:DoTaskInTime(pre_delay, function()
                self:DoSpeech(round_data.wave_introspeech[wavenum])
                self:QueueBoarlordDoneTalking(function()
                    fn(pst_delay)
                end)
            end)
        end
	else
        fn(round_data.no_talk_delay and round_data.no_talk_delay[wavenum] or 2.33)
	end
end

function LavaarenaEvent:QueueBoarlordDoneTalking(fn)
	local function spawnafterdonetalking(boarlord)
		fn()
		boarlord:RemoveEventCallback("donetalking", spawnafterdonetalking)
	end
	self:GetBoarlord():ListenForEvent("donetalking", spawnafterdonetalking)
end

function LavaarenaEvent:Start()
    --delaying load here so we can do calculations based on chosen characters
    self.rounds_data = require(self.loadrounddata)  
    self.start_time = GetTime()
    self:StartRound(1)
end

function LavaarenaEvent:AllMobsDied()
	local pt = self:GetArenaCenterPoint()
    local ents = TheSim:FindEntities(pt.x, 0, pt.z, 300, {"NOCLICK"}, {"LA_mob", "corpse"})
    for i, ent in ipairs(ents) do
		if ent.tempnoclick then
			ent:RemoveTag("NOCLICK")
			ent.tempnoclick = nil
		end
    end
	if self.enabled then
        local startnextround = self.wavemanager.onallmobsdied(self, self.mem)
        if startnextround then
            if self.rounds_data[self.current_round].roundend then
                self.rounds_data[self.current_round].roundend(self)
            end
            if self.current_round < #self.rounds_data then
                self:StartRound(self.current_round + 1)
            else
                self.inst:DoTaskInTime(self.rounds_data.victory_wait or 3, function()
                    self:End(true)
                end)
            end
        end
	end
end

--[[ TODO

--]]
function LavaarenaEvent:End(victory)
	local field_order = {"userid","netid","character","cardstat"}
	
	local function PrintMatchStats()
		if TheNet:IsDedicated() then
			local player_stats = Settings.match_results.player_stats or TheFrontEnd.match_results.player_stats
			if player_stats ~= nil and #player_stats.data > 0 then
				local str = "\nstats_type,".. tostring(player_stats.gametype)
				str = str .. "\nsession," .. tostring(player_stats.session)
				str = str .. "\nserver_date," .. os.date("%c")
				
				local outcome = Settings.match_results.outcome or TheFrontEnd.match_results.outcome
				if outcome ~= nil then
					str = str .. "\nlb_submit," .. tostring(outcome.lb_submit) .. ", " .. tostring(outcome.lb_response)
					str = str .. "\nwon," .. (outcome.won and "true" or "false") 
					str = str .. "\nround," .. tostring(outcome.round)
					str = str .. "\ntime," .. tostring(math.floor(outcome.time))
					if TheNet:GetServerGameMode() == "quagmire" then
						str = str .. "\nscore," .. tostring(outcome.score)
						str = str .. "\ntributes_success," .. tostring(outcome.tributes_success)
						str = str .. "\ntributes_failed," .. tostring(outcome.tributes_failed)
					end
				end
				
				local userid_index = 0
				str = str .. "\nfields"
				for i, v in ipairs(player_stats.fields) do
					str = str .. "," .. v
				end
				
				for j, player in ipairs(player_stats.data) do
					str = str .. "\nplayer"..j
					for i, v in ipairs(player) do
						str = str .. "," .. v
					end
				end
				print(str)

				str = str .. "\nendofmatch"

				print("Logging Match Statistics") -- TODO put gametype instead so if playing double trouble? Or add gamemode to what prints?
				local stats_file = "event_match_stats/".. GetActiveFestivalEventStatsFilePrefix() .. string.gsub(os.date("%x"), "/", "-") .. ".csv"
				TheSim:GetPersistentString(stats_file, function(load_success, old_str) 
					if old_str ~= nil then
						str = str .. "\n" .. old_str
					end
					TheSim:SetPersistentString(stats_file, str, false, function() print("Done Logging Match Statistics") end)
				end)
				
			end
		end
	end
	
	local function SetMatchResults()
		--TheFrontEnd.match_results.wxp_data = {}
		--TheFrontEnd.match_results.wxp_data[TheNet:GetUserID()] = { new_xp = 0, match_xp = 0, earned_boxes = 0, details = {}}
		
		local player_stats = {}
		local users = {}
		local highest_stats = {}
		local players_best_stats = {}
		local death_count = 0
		local field_order_established = false
		
		TheWorld.components.stat_tracker:FindBestStats()
		for userid, user_info in pairs(TheWorld.components.stat_tracker.stats) do
			local current_player_stats = {}
			table.insert(current_player_stats, userid)
			table.insert(current_player_stats, user_info.user_data.user.netid)
			table.insert(current_player_stats, user_info.user_data.user.prefab)
			table.insert(current_player_stats, user_info.user_data.beststat[1])
			
			for i, stat in pairs (TheWorld.components.stat_tracker:GetStatFieldOrder()) do
				table.insert(current_player_stats, math.floor(user_info.stat_values[i] or 0)) -- TODO round down or up?
				if not field_order_established then
					table.insert(field_order, stat)
				end
			end
			field_order_established = true			
			table.insert(player_stats, current_player_stats)
			
			table.insert(users, user_info.user_data)
			death_count = death_count + TheWorld.components.stat_tracker:GetStatTotal("deaths", userid)
		end
		
		local users_test = json.encode(users)
		TheFrontEnd.match_results.mvp_cards = json.decode(users_test)
		TheFrontEnd.match_results.player_stats = {gametype = "TheForgedForge", session = TheWorld.meta.session_identifier, data = player_stats, fields = field_order}
		TheFrontEnd.match_results.outcome = {won = victory, time = math.floor(GetTime() - self.start_time + 0.5), total_deaths = death_count}
		PrintMatchStats()
		
		-- Reset stat tracker for next game
		TheWorld.components.stat_tracker:Reset()
	end
	
	local function ResetForge()
		local function doreset()
			StartNextInstance({
				reset_action = RESET_ACTION.LOAD_SLOT,
				save_slot = SaveGameIndex:GetCurrentSaveSlot()
			})
		end
		if TheWorld ~= nil and TheWorld.ismastersim then
			SaveGameIndex:DeleteSlot(
				SaveGameIndex:GetCurrentSaveSlot(),
				doreset,
				true
			)
		end
	end
	
	self.inst.net.components.lavaarenaeventstate:PushPopup(victory)
	if self.rounds_data then
		self.inst:DoTaskInTime(self.rounds_data.endspeechwait or 0.5, function()
			self:GetBoarlord().components.talker:ShutUp() --Fid: Because RLGL mode, there's a chance he'll be talking
			if not victory then
				self.lost = true -- CunningFox: This is for crowd not to change their state from "boo"
				self:DoSpeech(self.rounds_data.player_loss)
			else
				self:DoSpeech(self.rounds_data.player_victory)
			end
			self:QueueBoarlordDoneTalking(ResetForge)
		end)
	end
	
	TheWorld:PushEvent("ms_lavaarenaended", victory)
	
	-- 1 second delay to make sure all stats are updated note without this deaths would not count at the end
	self.inst:DoTaskInTime(1, function()
		SetMatchResults()
	end)
end

-- Forces the selected round and wave to immediately start
function LavaarenaEvent:ForceStartRound(round, wave)
	local round = round or 1
	local wave = wave or 1
	-- TODO Kill all mobs?
	-- if wave 1 then do normal procedure
	if wave == 1 then
		self:StartRound(round)
	-- if not wave 1 then use default wave manager, this means that waves will only trigger on all mobs dying from previous waves
	else
		if self.wavemanager and self.wavemanager.onexit then self.wavemanager.onexit(self, self.mem) end
		self.inst:StopUpdatingComponent(self)

		self.current_round = round
		local round_data = self.rounds_data[round]
		if not round_data then return end

		self.wavemanager = defaultwavemanager

		self.mem = {round_data = round_data}
		if type(self.wavemanager.onupdate) == "function" then
			self.inst:StartUpdatingComponent(self)
		end
		self.mem.wavesfinished = wave
		self:PushWave(wave)
	end
end

-- Forces the current round and current wave to restart
function LavaarenaEvent:ForceRestartCurrentRound()
	self:ForceStartRoundAndWave(self.current_round)
end

-- Resets all players with original equipment and health and despawns all equipment earned and all enemies left on the map and restarts the forge
function LavaarenaEvent:ForceRestart()
	
end

-- TODO should on enable restart forge or continue? currently continues
function LavaarenaEvent:Enable()
	self.enabled = true
    if self.wasupdatingcomponent then
        self.inst:StartUpdatingComponent(self)
        self.wasupdatingcomponent = false
    end
end

-- TODO need to find a way to pause? somehow pause enemies?
function LavaarenaEvent:Disable()
	self.enabled = false
    self.wasupdatingcomponent = self.inst.updatecomponents[self] ~= nil
    self.inst:StopUpdatingComponent(self)
end

local spawnsfinished = 0

function LavaarenaEvent:SpawnFinishedForPortal()
    spawnsfinished = spawnsfinished + 1
    if spawnsfinished == 3 then
        self.inst:PushEvent("spawningfinished")
        local pt = self:GetArenaCenterPoint()
		local ents = TheSim:FindEntities(pt.x, 0, pt.z, 300, nil, {"NOCLICK", "LA_mob", "corpse"})
        for i, ent in ipairs(ents) do
			if not ent.components.inventoryitem then
				ent:AddTag("NOCLICK")
				ent.tempnoclick = true
			end
        end
        spawnsfinished = 0
    end
end

function LavaarenaEvent:OnUpdate(dt)
    self.wavemanager.onupdate(self, self.mem, dt)
end

function LavaarenaEvent:IsIntermission()
	return #TheWorld.components.lavaarenalivemobtracker:GetAllLiveMobs() == 0
end

return LavaarenaEvent