--[[
Copyright (C) 2019 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local ParryWeapon = Class(function(self, inst)
    self.inst = inst
    self.onparrystart = nil
    self.duration = 2

    --Zarklord: V2C: Recommended to explicitly add tag to prefab pristine state
    inst:AddTag("parryweapon")
end)

function ParryWeapon:SetOnParryStartFn(fn)
    self.onparrystart = fn
end

function ParryWeapon:OnPreParry(player)
    if player.sg then
        player.sg:PushEvent("start_parry")
    end
    if self.onparrystart then
        self.onparrystart(self.inst, player)
    end
end

function ParryWeapon:TryParry(player, target, damage, weapon, stimuli)
    if player.sg then
        player.sg:PushEvent("try_parry")
    end
    if self.ontryparry then
        return self.ontryparry(player, target, damage, weapon, stimuli)
    end
    --Zarklord: I think this is good, if im missing something let me know.
    local ent = target or weapon

    local anglediff = player.Transform:GetRotation() - player:GetAngleToPoint(ent.Transform:GetWorldPosition())
    if not (math.abs(anglediff) <= 70) then
        return false
    end
    
    local damagetype = ent.components.weapon and ent.components.weapon.damagetype or ent.components.combat and ent.components.combat.damagetype or nil
    if not (damagetype == DAMAGETYPES.PHYSICAL) then
        return false
    end

    return true
end

return ParryWeapon