--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local Passive_Mighty = Class(function(self, inst)
    self.player = inst
	self.mighty_ready = false
	self.health_threshold = TUNING.FORGE.MIGHTY.HEALTH_THRESHOLD
	self.health_trigger = TUNING.FORGE.MIGHTY.HEALTH_TRIGGER
	self.damage_mult = TUNING.FORGE.MIGHTY.DAMAGE_MULTIPLIER_INCREASE
	self.resistance_mult = TUNING.FORGE.MIGHTY.RESISTANCE_MULTIPLIER_INCREASE
	self.scale = TUNING.FORGE.MIGHTY.SCALE
	self.duration = TUNING.FORGE.MIGHTY.DURATION
	
	self.player:ListenForEvent("healthdelta", function(inst, data) self:OnHealthChange(inst, data) end)
	self.player:ListenForEvent("attacked", function(inst, data) self:OnAttacked(inst, data) end)
	self.player:ListenForEvent("respawnfromcorpse", function(inst, data) self:OnRevived(inst, data) end)
	
	Debug:Print("[Mighty] Initializing for " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ")...")
end)

-- Check and update Mighty status when health changes
function Passive_Mighty:OnHealthChange(inst, data)
	if self.player.sg:HasStateTag("nomorph") or
        self.player:HasTag("playerghost") or
        self.player.components.health:IsDead() then
        return
    end
	
	-- Check if Mighty should be reset (Wolfgangs health is above the threshold)
	if self.player.components.health.currenthealth > self.health_threshold and not self.mighty_ready then
		Debug:Print("[Mighty] " .. tostring(self.player.name) .. "'s (" .. tostring(self.player.userid) .. ")" .. " health is above " .. tostring(self.health_threshold))
		self.mighty_ready = true
	end
end

-- Check if Mighty should be triggered (Wolfgangs health is below the trigger) when attacked
function Passive_Mighty:OnAttacked(inst, data)
	if self.player.components.health.currenthealth < self.health_trigger and self.mighty_ready then
		Debug:Print("[Mighty] " .. tostring(self.player.name) .. "'s (" .. tostring(self.player.userid) .. ")" .. " health is below " .. tostring(self.health_trigger))
		self:BecomeMighty(inst)
		self.mighty_ready = false
	end
end

-- Update Wolfgangs stats based on current mightiness
function Passive_Mighty:ApplyMightiness()
    local wimpy_scale = .9
	
	Debug:Print("[Mighty] Adjusting Mightiness to " .. tostring(self.player.strength) .. " for " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ")")
	
	-- Mighty
    if self.player.strength == "mighty" then
        self.player._mightiness_scale = self.scale
		
		-- Update Wolfgangs stats from Normal to Mighty stats
		self.player:ApplyScale("mightiness", self.player._mightiness_scale)
		self.player.components.combat.damagemultiplier = self.player.components.combat.damagemultiplier + self.damage_mult
		self.player.components.health.absorb = self.player.components.health.absorb + self.resistance_mult
		self.player:AddTag("heavybody")
		
		-- Create timer for Mighty duration
		self.player:DoTaskInTime(self.duration, function() 
				if not self.player.components.health:IsDead() then
					if self.player.strength == "mighty" then
						self:BecomeNormal()
					end
					Debug:Print("[Mighty] Mighty has expired for " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ")")
				end
			end)
	-- Normal
    else
        self.player._mightiness_scale = 1
		
		-- Update Wolfgangs stats from Mighty to Normal stats
		self.player:ApplyScale("mightiness", self.player._mightiness_scale)
		self.player.components.combat.damagemultiplier = self.player.components.combat.damagemultiplier - self.damage_mult
		self.player.components.health.absorb = self.player.components.health.absorb - self.resistance_mult
		self.player:RemoveTag("heavybody")
    end
	
	Debug:Print("[Mighty] Updated stats for " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ")" .. ": dmg: " .. tostring(self.player.components.combat.damagemultiplier) .. " absorb: " .. tostring(self.player.components.health.absorb))
	
    if self.player.components.rider:IsRiding() then
        OnMounted(self.player)
    end
end

-- Make Wolfgang Wimpy
function Passive_Mighty:BecomeWimpy()
    if self.player.strength == "wimpy" then
        return
    end

	Debug:Print("[Mighty] " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ") is Becoming Wimpy")
	
    self.player.components.skinner:SetSkinMode("wimpy_skin", "wolfgang_skinny")

	self.player.sg:PushEvent("powerdown")
	self.player.components.talker:Say(GetString(self.player, "ANNOUNCE_NORMALTOWIMPY"))
	self.player.SoundEmitter:PlaySound("dontstarve/characters/wolfgang/shrink_medtosml")

    self.player.talksoundoverride = "dontstarve/characters/wolfgang/talk_small_LP"
    self.player.hurtsoundoverride = "dontstarve/characters/wolfgang/hurt_small"
    self.player.strength = "wimpy"
	
	-- Update Mightiness
	self:ApplyMightiness()
end

-- Make Wolfgang Normal
function Passive_Mighty:BecomeNormal()
    if self.player.strength == "normal" then
        return
    end
	
	Debug:Print("[Mighty] " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ") is Becoming Normal")
	
    self.player.components.skinner:SetSkinMode("normal_skin", "wolfgang")

	if self.player.strength == "mighty" then
		--self.player.components.talker:Say(GetString(self.player, "ANNOUNCE_MIGHTYTONORMAL"))
		self.player.sg:AddStateTag("nointerrupt")
		self.player.sg:PushEvent("powerdown")
		self.player.SoundEmitter:PlaySound("dontstarve/characters/wolfgang/shrink_lrgtomed")
	elseif self.player.strength == "wimpy" then
		self.player.components.talker:Say(GetString(self.player, "ANNOUNCE_WIMPYTONORMAL"))
		self.player.sg:PushEvent("powerup")
		self.player.SoundEmitter:PlaySound("dontstarve/characters/wolfgang/grow_smtomed")
	end

    self.player.talksoundoverride = nil
    self.player.hurtsoundoverride = nil
    self.player.strength = "normal"
	
	-- Update Mightiness
	self:ApplyMightiness()
end

-- Make Wolfgang Mighty
function Passive_Mighty:BecomeMighty()
    if self.player.strength == "mighty" then
        return
    end
	
	Debug:Print("[Mighty] " .. tostring(self.player.name) .. " (" .. tostring(self.player.userid) .. ") is Becoming Mighty")
	
    self.player.components.skinner:SetSkinMode("mighty_skin", "wolfgang_mighty")

	--self.player.components.talker:Say(GetString(self.player, "ANNOUNCE_NORMALTOMIGHTY"))
	self.player.sg:AddStateTag("nointerrupt")
	self.player.sg:PushEvent("powerup")
	self.player.SoundEmitter:PlaySound("dontstarve/characters/wolfgang/grow_medtolrg")

    self.player.talksoundoverride = "dontstarve/characters/wolfgang/talk_large_LP"
    self.player.hurtsoundoverride = "dontstarve/characters/wolfgang/hurt_large"
    self.player.strength = "mighty"
	
	-- Update Mightiness
	self:ApplyMightiness()
end

function Passive_Mighty:OnRevived(inst, data)
	if self.player.strength == "mighty" then
		self:BecomeNormal()
	end
	self.mighty_ready = false
	self:OnHealthChange()
end

function Passive_Mighty:SetDamageMult(val)
	self.damage_mult = val
end

function Passive_Mighty:SetResistanceMult(val)
	self.resistance_mult = val
end

function Passive_Mighty:SetDuration(val)
	self.duration = val
end

function Passive_Mighty:SetHealthThreshold(val)
	self.health_threshold = val
end

function Passive_Mighty:SetHealthTrigger(val)
	self.health_trigger = val
end

function Passive_Mighty:SetScale(val)
	self.scale = val
end

function Passive_Mighty:GetDebugString()
    return string.format("Status: %s, Hit Count: %s", tostring(self.mighty_ready), self.player.strength)
end

return Passive_Mighty