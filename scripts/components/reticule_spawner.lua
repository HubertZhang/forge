--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

-- Fox: Never thought that it'll be so complicated lol
local function KillTasks(self)
	for _, tsk in ipairs({"task", "timeout_task"}) do
		if self[tsk] then
			self[tsk]:Cancel()
			self[tsk] = nil
		end
	end
end

local function OnCasted(inst, data)
	local self = inst.components.reticule_spawner
	
	KillTasks(self)
	
	self.task = self.ping:DoTaskInTime(self.time or 2, function()
		self:KillRet()
		self.task = nil
	end)
end

local ReticuleSpawner = Class(function(self, inst)
    self.inst = inst
	
	self.time = 2
	self.type = "aoe"
	self.ping = nil
	self.task = nil
end)

function ReticuleSpawner:Setup(type, time)
	self.type = type or "aoe"
	self.time = time or 2
end

function ReticuleSpawner:Spawn(pos)
	if self.task then
		self.task:Cancel()
		self.task = nil
		
		self:KillRet()
	end
	
	self.ping = SpawnAt("reticule"..self.type, pos)
	
	self.inst:ListenForEvent("aoe_casted", OnCasted)
	
	-- In case something went very wrong...
	self.timeout_task = self.inst:DoTaskInTime(4, function()
		print("ReticuleSpawner: Timeouted!")
		self:Interrupt()
	end)
end

function ReticuleSpawner:KillRet()
	if self.ping then
		self.ping:KillFX()
		self.ping = nil
	end
	
	self.inst:RemoveEventCallback("aoe_casted", OnCasted)
end

function ReticuleSpawner:Interrupt()
	KillTasks(self)
	
	self:KillRet()
end

return ReticuleSpawner
