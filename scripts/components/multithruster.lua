--[[
Copyright (C) 2018, 2019 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local function onready(self, ready, _ready)
    if _ready == true and ready == true then
        self.inst:AddTag("multithruster")
    elseif _ready == true and ready == false then
        self.inst:RemoveTag("multithruster")
    end
end

local Multithruster = Class(function(self, inst)
    self.inst = inst
    self.ready = true
end,
nil,
{
    ready = onready
})

-- Starts the thrust attack
function Multithruster:StartThrusting(player)
	if player.sg then
		player.sg:PushEvent("start_multithrust")
		return true
	end
	
	return false
end

-- Creates a thrust that hits the given target
function Multithruster:DoThrust(player, target)
    if player.sg then
        player.sg:PushEvent("do_multithrust")
    end

	local damage_mult = player.components.combat.damagemultiplier
	
	-- Each thrust should have the bonus from Battle Cry, add the bonus if it was removed
	if not player.battlecry_fx then
		damage_mult = damage_mult + TUNING.FORGE.BATTLECRY.DAMAGE_MULTIPLIER_INCREASE - 1
	end
	player.components.combat:DoAttack(target, nil, nil, "strong", damage_mult)
end

-- Stop thrusting and reset it
function Multithruster:StopThrusting(player)
    if player.sg then
        player.sg:PushEvent("stop_multithrust")
    end

	self.ready = false
end

return Multithruster