--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local Rechargeable = Class(function(self, inst)
	self.inst = inst
	self.recharge = 255
	self.rechargetime = -2
	self.maxrechargetime = 30
	self.cooldownrate = 1
	self.isready = true
	self.updatetask = nil
	self.onready = nil
	self.pickup = false
	self.onequip = function(inst, data)
        self:RecalculateRate()
    end
	-- when a player equips this weapon
	self.inst:ListenForEvent("equipped", function(inst, data)
		self.owner = data.owner
		self:RecalculateRate()
		-- check if less than 1 sec remaining on CD? then have a 1 sec equip?
		-- if not on cooldown, then add 1 sec equip CD
		if self.updatetask == nil then
			self.pickup = true
			self:StartRecharge()
		end
		-- when the player who equipped this wep equips/unequipes something else
		self.inst:ListenForEvent("equip", self.onequip, self.owner)
		self.inst:ListenForEvent("unequip", self.onequip, self.owner)
	end)
	-- when a player unequips this weapon
	self.inst:ListenForEvent("unequipped", function(inst, data)
		self.inst:RemoveEventCallback("equip", self.onequip, self.owner)
		self.inst:RemoveEventCallback("unequip", self.onequip, self.owner)
		-- if dropping it when 1 sec pickup cd active, remove it
		if self.pickup and self.updatetask ~= nil then
			self.pickup = false
			self.updatetask:Cancel()
			self.updatetask = nil
		end
		-- if dropped, then remove CDR bonuses
		self.cooldownrate = 1
		self.owner = nil
	end)
end)

function Rechargeable:SetRechargeTime(rechargetime)
	self.maxrechargetime = rechargetime
end

function Rechargeable:SetOnReadyFn(fn)
	self.onready = fn
end

function Rechargeable:RecalculateRate()
	if self.owner ~= nil then
		self.cooldownrate = self.owner.components.buffable:ApplyStatBuffs({"cooldown"}, 1)
		-- if we still have a recharge going, need to update client info with new rechargetime
		if self.updatetask ~= nil then
			self.inst.replica.inventoryitem:SetChargeTime(self:GetRechargeTime())
		end
	end
end

function Rechargeable:FinishRecharge()
	if self.updatetask ~= nil then
		self.updatetask:Cancel()
		self.updatetask = nil
	end
	self.isready = true
	if self.inst.components.aoetargeting then
		self.inst.components.aoetargeting:SetEnabled(true)
	end
	if self.onready then
		self.onready(self.inst)
	end
	self.pickup = false
	self.recharge = 255
	self.inst:PushEvent("rechargechange", { percent = self.recharge and self.recharge / 180, overtime = false })
end

function Rechargeable:Update()
	self.recharge = self.recharge + 180 * FRAMES / (self.rechargetime * (self.pickup and 1 or self.cooldownrate))
	if self.recharge >= 180 then
		self:FinishRecharge()
	end
end

function Rechargeable:StartRecharge()
	self.isready = false
	if self.inst.components.aoetargeting then
		self.inst.components.aoetargeting:SetEnabled(false)
	end
	self.rechargetime = self.pickup and 1 or self.maxrechargetime
	self.recharge = 0
	self:RecalculateRate()
	self.inst:DoTaskInTime(0, function()
		self.inst.replica.inventoryitem:SetChargeTime(self:GetRechargeTime())
		self.inst:PushEvent("rechargechange", { percent = self.recharge and self.recharge / 180, overtime = false })
		self.updatetask = self.inst:DoPeriodicTask(FRAMES, function() self:Update() end)
	end)
end

function Rechargeable:GetPercent()
	return self.recharge and self.recharge / 180, false
end

function Rechargeable:GetRechargeTime()
	return (self.pickup and 1) or self.maxrechargetime * self.cooldownrate
end

function Rechargeable:GetDebugString()
    return string.format("recharge: %2.2f, rechargetime: %2.2f, cooldownrate: %2.2f", self.recharge, self:GetRechargeTime(), self.cooldownrate)
end

return Rechargeable