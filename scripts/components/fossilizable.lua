--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local function OnAttacked(inst, data)
	local self = inst.components.fossilizable
	
	if self:IsFossilized() then
		inst:PushEvent("unfossilize")
		if TheWorld and TheWorld.components.stat_tracker then
			-- ...
			-- update stat: ccbroken
			if data.attacker then
				TheWorld.components.stat_tracker:AdjustStat("ccbroken", data.attacker, 1)
			end
		end
	end
end

local Fossilizable = Class(function(self, inst)
	self.inst = inst
	
	self.duration = 0
	self.start_time = 0
	self.cd_timer = 3
	
	self.inst:ListenForEvent("attacked", OnAttacked)
	self.inst:ListenForEvent("unfossilize", function()
		
	end)
end)

function Fossilizable:StartCD()
	if self.cd_task then self.cd_task:Cancel() self.cd_task = nil end
	self.inst:AddTag("fossil_immune")
	self.cd_task = self.inst:DoTaskInTime(self.cd_timer, function(inst) inst:RemoveTag("fossil_immune") end)
end

function Fossilizable:OnSpawnFX()
	if self.inst.SoundEmitter then
		self.inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/fossilized")
	end
	
	SpawnAt("fossilizing_fx", self.inst)
end

function Fossilizable:OnFossilize(duration, caster)
	self.inst:AddTag("fossilized")
	self:StartCD()
	
	-- Caster
	-- update stat: ccnum
	if caster and TheWorld.components.stat_tracker then
		TheWorld.components.stat_tracker:AdjustStat("numcc", caster, 1)
	end
	
	self.duration = duration or 10
	self.start_time = GetTime()
	self.caster = caster
	self.inst:StartUpdatingComponent(self)
end

function Fossilizable:OnUnfossilize()
	self.inst:StopUpdatingComponent(self)
	self.duration = 0
	self.inst:RemoveTag("fossilized")
	self.inst:PushEvent("unfossilize")
	-- Caster
	-- update stat: cctime
	if self.caster and self.start_time then
		TheWorld.components.stat_tracker:AdjustStat("cctime", self.caster, GetTime() - self.start_time)
		self.start_time = nil
	end
end

function Fossilizable:OnExtend(duration)
	self:StartCD()
	
	self.inst:StopUpdatingComponent(self)
	self.duration = duration
	self.inst:StartUpdatingComponent(self)
	
	-- TODO does this increase "ccnum" if they are already cc'd?
	-- Caster
	-- update stat: ccnum
	if self.caster and TheWorld.components.stat_tracker then
		TheWorld.components.stat_tracker:AdjustStat("numcc", self.caster, 1)
	end
end

function Fossilizable:IsFossilized()
	return self.duration > 0
end

function Fossilizable:OnUpdate(dt)
	self.duration = self.duration - dt
	
	if self.duration <= 0 then
		self.duration = 0
		self.inst:StopUpdatingComponent(self)
		
		self.inst:PushEvent("unfossilize")
	end
end

function Fossilizable:GetDebugString()
    return string.format("Duration: %2.2f", self.duration)
end

return Fossilizable