--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

-- CunningFox: We need to add nice heal fade
-- Addapted from colourtweener.lua

local function UnpackColour(clr)
	return clr[1] or 0, clr[2] or 0, clr[3] or 0, 0
end

local ColourFader = Class(function(self, inst)
	self.inst = inst

	--initial values
	self.current_colour = {0,0,0,0}

	--target values
	self.target_colour = {0,0,0,0}

	--function
	self.callback = nil --call @ end of tween

	self.time = nil
	self.timepassed = 0
end)

function ColourFader:EndFade()
	self.inst:StopUpdatingComponent(self)
	
	--Set all values to final values
	self.current_colour = self.target_colour
	self.inst.AnimState:SetAddColour(UnpackColour(self.target_colour))
	
	self.time = nil

	if self.callback then
		self.callback(self.inst)
	end
end

function ColourFader:StartFade(colour, time, callback)
	self.callback = callback
	self.target_colour = colour

	self.time = time
	self.timepassed = 0
	
	if self.time > 0 then
		self.inst:StartUpdatingComponent(self)
	else
		self:EndFade()
	end
end

function ColourFader:SetCurrentFade(colour)
	self.current_colour = colour
end
	
function ColourFader:OnUpdate(dt)	
	self.timepassed = self.timepassed + dt
	
	local t = self.timepassed/self.time
	if t > 1 then
		t = 1
	end
	
	local trgt = {}
	
	for i = 1, 3 do 
		table.insert(trgt, Lerp(self.current_colour[i], self.target_colour[i], t))
	end
	
	self.inst.AnimState:SetAddColour(UnpackColour(trgt))

	if self.timepassed >= self.time then
		self:EndFade()
	end
end

function ColourFader:GetDebugString()
	if not self.time then return "Disabled!" end
	
	local t = self.timepassed/self.time
	if t > 1 then
		t = 1
	end
	
    return string.format(
		"%.2f, %.2f, %.2f",
		Lerp(self.current_colour[1], self.target_colour[1], t),
		Lerp(self.current_colour[2], self.target_colour[2], t),
		Lerp(self.current_colour[3], self.target_colour[3], t)
	)
end

--ThePlayer:AddComponent("colourfader"); ThePlayer.components.colourfader:StartFade({0, .3, .1, 0}, 1)
return ColourFader