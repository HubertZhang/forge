--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

--[[
	Eat when nothing happends
	Cheer when mobs spawned
	Boo when player dies
]]

local REACTIONS = {
	EAT = "eat",
	CHEER = "cheer",
	BOO = "boo",
}

local STATES = {
	CALM = 0,
	CHEER_MOB = 1,
	CHEER_HUMAN = 2,
}

local Crowd = Class(function(self, inst)
	self.inst = inst
	self.is_boo = false
	
	local _ondied = function() self:StartBoo() end
	self.inst:ListenForEvent("ms_playerdied", _ondied, TheWorld)
	
	self.react_task = self.inst:DoPeriodicTask(math.random() + .5, function() self:SendReaction() end)
end)

function Crowd:SendReaction()
	if self.is_boo then return end
	
	for _, player in ipairs(AllPlayers) do
		if player.player_classified then
			local react = self:GetReaction()
			player.player_classified.crowd_state:set(react == REACTIONS.CHEER and STATES.CHEER_MOB or STATES.CALM)
		end
	end
end

function Crowd:StartBoo()
	if self.boo_task then
		self.boo_task:Cancel()
		self.boo_task = nil
	end
	
	self.is_boo = true
	
	if TheWorld.components.lavaarenalivemobtracker and not TheWorld.components.lavaarenalivemobtracker.lost then
		self.boo_task = self.inst:DoTaskInTime(8 + math.random()*3, function()
			self.is_boo = false
			self:SendReaction()
		end)	
	end
	
	self.inst:DoTaskInTime(math.random()+math.random(), function()
		for _, player in ipairs(AllPlayers) do
			if player.player_classified then
				player.player_classified.crowd_state:set(STATES.CHEER_HUMAN)
			end
		end
	end)
end

function Crowd:GetReaction()
	if self.is_boo then
		return REACTIONS.BOO
	elseif TheWorld.components.lavaarenalivemobtracker and #TheWorld.components.lavaarenalivemobtracker:GetAllLiveMobs() > 0 then
		return REACTIONS.CHEER
	end
	
	return REACTIONS.EAT
end

return Crowd