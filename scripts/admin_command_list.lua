--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("admin_command")
require("admin_commands")

-- name, type, admin_fn, atlas, image

-- GENERAL
AdminCommand("change_character", "GENERAL", function(self, data) ChangeCharacter(data.player) end, "images/avatars.xml", "avatar_unknown.tex")
AdminCommand("super_god_mode", "GENERAL", function(self, data) SuperGodMode(data.player) end, "images/inventoryimages.xml", "amulet.tex")
AdminCommand("revive", "GENERAL", function(self, data) RevivePlayer(data.player) end, "images/inventoryimages.xml", "amulet_red_heart.tex")
AdminCommand("kill_all_mobs", "GENERAL", function(self) KillAllMobs() end, "images/inventoryimages.xml", "nightmarefuel.tex")
AdminCommand("petrify_mobs", "GENERAL", function(self, data) SpawnCircle(self, data.player) end, "images/inventoryimages.xml", "bluegem.tex") -- TODO fossil type icon?
AdminCommand("over_9000", "GENERAL", function(self, data) Over9000(data.player) end, "images/inventoryimages.xml", "redgem.tex") -- TODO scouter icon?
AdminCommand("reset_to_save", "GENERAL", function(self) ResetToSave() end, "images/inventoryimages.xml", "book_brimstone.tex") -- TODO scouter icon?
AdminCommand("debug_select", "GENERAL", function(self, data) DebugSelect(data.player) end, "images/inventoryimages.xml", "blueprint.tex")
AdminCommand("scorpeon_dot", "BUFFS", function(self, data) SpawnPoison(data.player) end, "images/forged_forge.xml", "scorpeon_icon.tex")

-- FORGE
AdminCommand("force_round", "FORGE", function(self, data) SelectRoundAndWave(data.round, data.wave) end, nil, nil)
AdminCommand("restart_forge", "FORGE", function(self) RestartForge() end, nil, nil)
AdminCommand("restart_current_round", "FORGE", function(self) RestartCurrentWave() end, nil, nil)
AdminCommand("enable_rounds", "FORGE", function(self) EnableWaves() end, nil, nil)
AdminCommand("disable_rounds", "FORGE", function(self) DisableWaves() end, nil, nil)
AdminCommand("end_forge", "FORGE", function(self) EndForge() end, nil, nil)


print("[AllAdminCommands]")
for _, admin_command in ipairs(AllAdminCommands) do
	print(" - " .. tostring(admin_command.name))
end