--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require "class"
require "util"

local num = 0
AllAdminCommands = {}
AdminCommand = Class(function(self, name, category, admin_fn, atlas, image)

    self.name 			= name
    self.type 			= category

    self.atlas	 		= (atlas and resolvefilepath(atlas)) or resolvefilepath("images/inventoryimages.xml")
    self.imagefn 		= type(image) == "function" and image or nil
    self.image 			= self.imagefn == nil and image or ("torch.tex")
	
	self.admin_fn = admin_fn
	
    self.sortkey 		= num

    num = num + 1
    AllAdminCommands[name] = self
end)

function GetValidAdminCommand(name)
    return AllAdminCommands[name]
end

function IsAdminCommandValid(name)
    return GetValidAdminCommand(name) ~= nil
end

function RemoveAllAdminCommands()
    AllAdmincommands = {}
    num = 0
end
