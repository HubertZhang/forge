--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("stategraphs/commonstates")

local function GetExcludeTags(inst)
	if inst:HasTag("brainwashed") then
		return {"player", "companion", "brainwashed", "notarget", "battlestandard", "INLIMBO"}
	else	
		return {"LA_mob", "notarget", "battlestandard", "INLIMBO"}
	end
end

local function DoFrontAoe(inst)
local posx, posy, posz = inst.Transform:GetWorldPosition()
local angle = -inst.Transform:GetRotation() * DEGREES
local offset = 2
local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
local ents = inst:HasTag("brainwashed") and TheSim:FindEntities(targetpos.x, 0, targetpos.z, TUNING.FORGE.BOARILLA.AOE_HIT_RANGE, { "locomotor"}, { "player", "companion", "brainwashed" , "fossil", "shadow", "playerghost", "INLIMBO" }) or TheSim:FindEntities(targetpos.x, 0, targetpos.z, TUNING.FORGE.BOARILLA.AOE_HIT_RANGE, { "locomotor"}, { "LA_mob", "fossil", "shadow", "playerghost", "INLIMBO" })
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst.components.combat.target then
			inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(ent))
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) then
		return target
	else
		return distsq ~= nil and distsq < 225 and not player:HasTag("notarget") and player --Leo: Removed the LA_mob check because we should always want them to attack players in forge.
	end
end


local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .2, inst, 30)
end

local function ShakePound(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
    ShakeAllCameras(CAMERASHAKE.FULL, 1.2, .03, .7, inst, 30)
end

local function ShakeRoar(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 0.8, .03, .5, inst, 30)
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(0))
        if dist > 0 then
            return math.min(inst.jump_range, dist) / (10 * FRAMES)
        end
    end
    return 0
end

local function DoRollAoE(inst)
	if inst.sg:HasStateTag("rolling") then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x,0,pos.z, 3, nil, GetExcludeTags(inst))
		if ents and #ents > 0 then
			for i, v in ipairs(ents) do
				if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and not v.ischargehit and v ~= inst then
					if inst.components.combat.target == v then
						inst._hashittarget = true
					end
					v.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(v))
					v.ischargehit = true
					v:DoTaskInTime(1, function(inst) inst.ischargehit = nil end)
				end
			end
		end	
    --inst.components.combat:DoAreaAttack(inst, TUNING.FORGE.SNORTOISE.SPIN_HIT_RANGE, nil, nil, nil, { "INLIMBO", "notarget", "playerghost", "battlestandard", "LA_mob" })
	end
end

local function SetRollPhysics(inst)
	ToggleOffCharacterCollisions(inst)
end

local function SetNormalPhysics(inst)
	ToggleOnCharacterCollisions(inst)
end


local REPEL_RADIUS = 5
local REPEL_RADIUS_SQ = REPEL_RADIUS * REPEL_RADIUS

function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, "action"),
	ActionHandler(ACTIONS.ATTACK, function(inst) 
		local health = inst.components.health:GetPercent()
		if inst.altattack and health <= TUNING.FORGE.BOARILLA.PHASE2_TRIGGER and inst.altattack == true then
			return "attack2"
		else
			return "attack"
		end	
	end),
}

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("hiding") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("hiding") and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + TUNING.FORGE.HIT_RECOVERY < GetTime())  then 
			inst.sg:GoToState("hit") 
		elseif not inst.components.health:IsDead() and inst.sg:HasStateTag("hiding") then
			inst.sg:GoToState("hide_hit")
		end 
	end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
    EventHandler("doattack", function(inst, data) 
		if not inst.components.health:IsDead() and not (inst.sg:HasStateTag("hit") or inst.sg:HasStateTag("busy")) then 
			if inst.components.health:GetPercent() <= TUNING.FORGE.BOARILLA.ROLL_TRIGGER and inst.altattack2 == true and not inst.classic then
				inst.sg:GoToState("attack3", data.target) 
			elseif inst.components.health:GetPercent() <= TUNING.FORGE.BOARILLA.PHASE2_TRIGGER and inst.altattack == true then
				inst.sg:GoToState("attack2", data.target) 
			else
				inst.sg:GoToState("attack", data.target) 
			end			
		end 
	end),
    --CommonHandlers.OnSleep(),
	EventHandler("gotosleep", function(inst) 
		if inst.sg:HasStateTag("rolling") then
			inst.sg.statemem.wants_to_sleep = true	
		else
			inst.sg:GoToState(inst.sg:HasStateTag("sleeping") and "sleeping" or "sleep")
		end 
	end),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	EventHandler("entershield", function(inst) 
		if not inst.sg:HasStateTag("busy") and not inst.components.health:IsDead() then 
			inst.sg:GoToState("hide_pre") 
		end	
	end),
    EventHandler("exitshield", function(inst) 
		if not inst.sg:HasStateTag("stunned") and not inst.sg:HasStateTag("fossilized") then
		inst.sg:GoToState("hide_pst") 
		end
	end),
	EventHandler("knockback", function(inst, data) if not inst.components.health:IsDead() and not (inst.sg:HasStateTag("hiding") or inst.sg:HasStateTag("rolling")) and (data.knocker and data.knocker.epicknockback) then inst.sg:GoToState("knockback", data) end end),
	CommonHandlers.OnFossilize(),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --punch
        tags = {"attack", "busy"},
		
		onenter = function(inst)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/grunt")
		--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
        inst.AnimState:PlayAnimation("attack2")
	end,
	
    timeline=
    {

			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/swish") 
			--inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_whoosh")
			end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/attack2")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/infection_attack_pre")	
			end),
            TimeEvent(11*FRAMES, function(inst) 
				inst.components.combat:DoAttack()
				DoFrontAoe(inst)
				--inst.components.combat:DoAreaAttack(inst.components.combat.target ~= nil and inst.components.combat.target or inst, TUNING.FORGE.BOARILLA.AOE_HIT_RANGE, nil, nil, nil, {"LA_mob", "battlestandard", "INLIMBO", "notarget"})	
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
			inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
	State{
        name = "attack2", --slam
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		inst.altattack = false
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		SetRollPhysics(inst)
        inst.AnimState:PlayAnimation("attack1")
        if inst.components.combat.target ~= nil then
			target = inst.components.combat.target
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.components.combat.target = target
				
            end
        end
		
		if inst.brain ~= nil then
			inst.brain:Stop()
		end
	end,
	
	onupdate = function(inst)
        if inst.sg.statemem.jump then
            inst.Physics:SetMotorVel(inst.sg.statemem.speed and inst.sg.statemem.speed or 1, 0, 0)
        end
    end,
	
	onexit = function(inst)
		inst.components.item_launcher:Enable(false)
		if inst.brain ~= nil then
			inst.brain:Start()
		end
		SetNormalPhysics(inst)
		inst.components.combat:SetRange(TUNING.FORGE.BOARILLA.ATTACK_RANGE, TUNING.FORGE.BOARILLA.HIT_RANGE)
		if inst.jumpcd_task then
			inst.jumpcd_task:Cancel()
			inst.jumpcd_task = nil
		end
		inst.jumpcd_task = inst:DoTaskInTime(TUNING.FORGE.BOARILLA.SLAM_CD, function(inst) inst.altattack = true end)
    end,
	
    timeline=
    {

			TimeEvent(3*FRAMES, function(inst) 
				inst.sg.statemem.jump = true
				if inst.components.combat.target and inst.components.combat.target:IsValid() then
					inst.sg.statemem.speed = CalcJumpSpeed(inst, inst.components.combat.target)		
				end
			end),
			TimeEvent(10*FRAMES, function(inst) inst.sg:AddStateTag("nointerrupt") end),
			TimeEvent(12*FRAMES, function(inst) 
				
				if inst.components.combat.target and inst.components.combat.target:IsValid() then 
					inst:FacePoint(inst.components.combat.target:GetPosition()) 
				end 
			end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/attack1") end),
			TimeEvent(20*FRAMES, ShakePound),
            TimeEvent(23*FRAMES, function(inst)
				--inst.components.combat.areahitdamagepercent = TUNING.FORGE.BOARILLA.SLAM_MULT 
				-- Glass: areahitdamagepercent should not be needed here
				inst.components.item_launcher:Enable(true)
				inst.components.combat:DoAreaAttack(inst, TUNING.FORGE.BOARILLA.JUMP_HIT_RANGE, nil, nil, nil, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or {"INLIMBO", "playerghost", "notarget", "battlestandard", "LA_mob"})
				inst.components.combat.areahitdamagepercent = 1
			end),
			TimeEvent(18*FRAMES, function(inst) 
				inst.Physics:ClearMotorVelOverride()
				inst.components.locomotor:Stop()
				inst.sg.statemem.jump = false 
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
			inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
	State{
        name = "attack3", --roll
        tags = {"busy"},
		
		onenter = function(inst, target)
		inst._hashittarget = nil
		inst.components.locomotor:StopMoving()
		if inst.brain ~= nil then
			inst.brain:Stop()
		end
		
		if inst.components.combat.target and not inst._hashittarget then
            if inst.components.combat.target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.components.combat.target = inst.components.combat.target
            end
		else
			--we hit our target, time to stop
			if inst.rams < 30 then
				inst.rams = 30
			end
        end
		
		SetRollPhysics(inst)
		inst.altattack2 = false
        inst.components.combat:StartAttack()
        
        inst.AnimState:PlayAnimation("roll_pre")
	end,
	
	onexit = function(inst)
		inst:DoTaskInTime(TUNING.FORGE.BOARILLA.ROLL_CD, function(inst) inst.altattack2 = true inst.components.combat:SetRange(8, 0) end)
		inst.components.combat:SetRange(TUNING.FORGE.BOARILLA.ATTACK_RANGE, TUNING.FORGE.BOARILLA.HIT_RANGE)
		SetNormalPhysics(inst)
		if inst.brain ~= nil then
			inst.brain:Start()
		end
		inst.components.item_launcher:Enable(false)
    end,
	
    timeline=
    {

    },

        events=
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("attack3_loop", inst.components.combat.target)  
			end),
        },
	
    },
	
	State{
        name = "attack3_loop", --ram
        tags = {"attack", "busy", "rolling", "nosleep", "nofreeze"},
		
		onenter = function(inst, target)
		if inst.brain ~= nil then
			inst.brain:Stop()
		end
		if inst.components.combat.target and not inst._hashittarget then
            if inst.components.combat.target:IsValid() then
                inst.components.combat.target = inst.components.combat.target
            end
		else
			--we hit our target, time to stop
			if inst.rams < TUNING.FORGE.BOARILLA.MAXROLLS then
				inst.rams = TUNING.FORGE.BOARILLA.MAXROLLS
			end
        end
		
		SetRollPhysics(inst)
		inst.rams = inst.rams + 1
		
		if inst._chargetask == nil then
			inst._chargetask = inst:DoPeriodicTask(0.15, DoRollAoE)
		end
		inst.Physics:SetMotorVelOverride(inst.components.locomotor.walkspeed*1.2, 0, 0)
		inst.components.item_launcher:Enable(true)

        inst.AnimState:PlayAnimation("roll_loop")        
	end,
	
	onupdate = function(inst)				
		if not inst.sg.statemem.charged_target then		
			local target = inst.components.combat.target
			if target == nil or target.components.health:IsDead() or inst._hashittarget then
				if not target or target.components.health:IsDead() then
					if inst.rams < TUNING.FORGE.BOARILLA.MAXROLLS then
						inst.rams = TUNING.FORGE.BOARILLA.MAXROLLS
					end
				end
			else
				
			local angle = inst.Transform:GetRotation() - inst:GetAngleToPoint(target:GetPosition())
			if angle > 180 then 
				angle = 360 - angle 
			elseif angle < -180 then 
				angle = 360 + angle 
			end
				
			--if angle > 45 or angle < -45 then
				--inst.sg.statemem.speed = inst.sg.statemem.speed <= 0 and inst.sg.statemem.speed or inst.sg.statemem.speed - accel
			--else
				--inst.sg.statemem.speed = inst.sg.statemem.speed >= maxspeed and inst.sg.statemem.speed or inst.sg.statemem.speed + accel
			--end
				
			local rot = 0
			local maxrott = 4
				
			if angle >= maxrott then
				rot = maxrott
			elseif angle <= -maxrott then
				rot = -maxrott
			else
				rot = angle
			end
				
			inst.Transform:SetRotation(inst.Transform:GetRotation() - rot)
			end
		end	
		inst.Physics:SetMotorVelOverride(inst.components.locomotor.walkspeed*1.2, 0, 0)
	end,
	
	onexit = function(inst)
		--inst.components.combat:SetRange(TUNING.FORGE.BOARILLA.ATTACK_RANGE, TUNING.FORGE.BOARILLA.HIT_RANGE)
		--inst.components.combat:SetRange(3, 5)
		SetNormalPhysics(inst)
		--inst.components.combat.target = nil
		if inst._chargetask ~= nil then
			inst._chargetask:Cancel()
			inst._chargetask = nil
		end
		if inst.brain ~= nil then
			inst.brain:Start()
		end
		inst.components.item_launcher:Enable(false)
    end,
	
    timeline=
    {
		TimeEvent(0*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(3*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
		
		TimeEvent(6*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(9*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
		
		TimeEvent(12*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(15*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
    },
        events=
        {
            EventHandler("animover", function(inst) 
				if inst.rams < TUNING.FORGE.BOARILLA.MAXROLLS then
					inst.sg:GoToState("attack3_loop")
				else
					inst.rams = 0
					if inst._chargetask ~= nil then
						inst._chargetask:Cancel()
						inst._chargetask = nil
					end
					if inst.components.health:GetPercent() <= 0.5 and inst.components.combat.target and inst.components.combat.target.components.health and not inst.components.combat.target.components.health:IsDead() and not inst.sg.statemem.wants_to_sleep then
						inst.sg:GoToState("attack2", inst.components.combat.target)
					else	
						inst.sg:GoToState("attack3_pst") 							
					end 
				end	
			end),
			EventHandler("attacked", function(inst, data) 
				if not (data and data.stimuli) then
					inst.rams = inst.rams + 2
				end
			end),
			EventHandler("onareaattackother", function(inst, data) 
				if data.target == inst.components.combat.target then
					inst.rams = (TUNING.FORGE.BOARILLA.MAXROLLS)
					inst.sg.statemem.charged_target = true
				end
			end),
        },
	
    },
	
	State{
        name = "attack3_pst", 
        tags = {"busy"},
		
		onenter = function(inst, target)
			inst.components.combat:SetRange(TUNING.FORGE.BOARILLA.ATTACK_RANGE, TUNING.FORGE.BOARILLA.HIT_RANGE)
			inst.Physics:Stop()
			inst.Physics:SetMotorVelOverride(inst.components.locomotor.walkspeed*1.2, 0, 0)
			inst.AnimState:PlayAnimation("roll_pst")
			
		end,
	
		timeline=
		{
			TimeEvent(0*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(3*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
		
		TimeEvent(6*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
		end),
		
		--TimeEvent(9*FRAMES, function(inst) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			--ShakeIfClose(inst)
		--end),
		
		TimeEvent(11*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
			ShakeIfClose(inst)
			inst.components.locomotor:Stop()
		end),
		},

        events=
        {
            EventHandler("animover", function(inst) 
				if inst.sg.statemem.wants_to_sleep and inst.sg.statemem.wants_to_sleep == true then
					inst.sg.statemem.wants_to_sleep = nil
					inst.components.sleeper:AddSleepiness(5,3)
					inst.sg:GoToState("idle")
				elseif inst.sg.mem.jumptarget and inst.sg.mem.jumptarget.components.health and not inst.sg.mem.jumptarget.components.health:IsDead() then
					inst.sg:GoToState("attack2", inst.sg.mem.jumptarget)
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
	
    },
	
	State{
		name = "hide_pre",
        tags = {"busy", "nointerrupt"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_pre")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_pre") 
				--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp") 
				end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hiding",
        tags = {"busy", "hiding", "nobuff"},

        onenter = function(inst, cb)
			inst.components.sleeper:SetResistance(9999)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_loop")
			inst.components.debuffable:RemoveDebuff("shield_buff", "shield_buff")
            inst.components.health:SetAbsorptionAmount(1)
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hide_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_pst")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_swell") end),
        },
		
		onexit = function(inst)
			inst.components.sleeper:SetResistance(1)
            inst.components.health:SetAbsorptionAmount(0) --Leo: Don't delete this incase something goes wrong with buffsystem: (-(0.02*inst.components.armorbreak_debuff.debufflevel))
			inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.BOARILLA.SHEILD_CD, function(inst) inst.canshield = true end)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hide_hit",
        tags = {"hiding", "busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_hit")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_dull") 
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit", "nofreeze"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit")
			
			inst.sg.mem.last_hit_time = GetTime()
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) 
					inst.sg:GoToState("idle") 			
			end),
        },
    },
	
	State{
		name = "spawn",
        tags = {"busy", "canattack"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,
		
		onexit = function(inst)
			--inst.sg.statemem.stun_taunted = nil
		end,

		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") end),
			TimeEvent(20*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar_phase")
			end),
			TimeEvent(20*FRAMES, ShakeRoar),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "taunt",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,
		
		onexit = function(inst)
			--inst.sg.statemem.stun_taunted = nil
		end,

		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") end),
			TimeEvent(20*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar_phase")
			end),
			TimeEvent(20*FRAMES, ShakeRoar),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("run_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/death_roar")
            inst.AnimState:PlayAnimation("death")
			inst.components.armorbreak_debuff:RemoveDebuff()
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
        end,
		
		timeline=
        {
			TimeEvent(11*FRAMES, ShakeIfClose),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hide_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
			--TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then

                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/bodyfall") inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/sleep_in")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		end,
		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/sleep_out") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/sleep")
			end),
        },

        events =
        {
			--Leo: Temp fix for perma sleeping bug:
			EventHandler("attacked", function(inst) inst.sg:GoToState("wake") end ),
			
			---
			EventHandler("animover", function(inst) 
				if inst.components.sleeper.sleepiness <= 0 then
					inst.sg:GoToState("wake") 
				else
					inst.sg:GoToState("sleeping")
				end
			end),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.components.combat:SetTarget(AttemptNewTarget(inst, inst.components.combat.target))  inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {

			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") 
				end),
			TimeEvent(0*FRAMES, ShakeIfClose),
			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step") end),
			TimeEvent(2*FRAMES, ShakeIfClose),
			TimeEvent(10*FRAMES, function(inst) inst.components.locomotor:WalkForward() end),
			
			--TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/walk_spiderqueen") inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy", "stunned", "nofreeze"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			inst.components.sleeper:SetResistance(1)
			inst.sg.statemem.stimuli = data.stimuli
			if data.stimuli and data.stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/grunt")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(TUNING.FORGE.STUN_TIME)
				inst.components.combat:SetTarget(data.attacker and data.attacker or nil)
				inst.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_STUN
			else
				inst.lasttarget = nil --should allow previous target to be targeted again if hes closest.
				inst.components.combat:SetTarget(nil)
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/grunt")
			end
			inst.sg.statemem.flash = 0
			
			inst.components.health:SetAbsorptionAmount(0)

            inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.BOARILLA.SHEILD_CD, function(inst) inst.canshield = true end)
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit") end),
        },
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			inst.components.health:SetAbsorptionAmount(0)
			inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.BOARILLA.SHEILD_CD, function(inst) inst.canshield = true end)            
			--inst.components.bloomer:PopBloom("leap")
            --inst.components.colouradder:PopColour("leap")
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,
		
		

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli and stimuli == "explosive" and (not inst.last_taunt_time or (inst.last_taunt_time + TUNING.FORGE.TAUNT_CD < GetTime())) then
				inst.last_taunt_time = GetTime()
				inst.sg.statemem.needstotaunt = true
			end
			inst.components.health:SetAbsorptionAmount(0)
            inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.BOARILLA.SHEILD_CD, function(inst) inst.canshield = true end)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.sg.statemem.needstotaunt then
					inst.sg.statemem.needstotaunt = nil
					inst.sg:GoToState("taunt")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	
	--Fossilized States--
	State
    {
        name = "fossilized",
        tags = { "busy", "fossilized", "caninterrupt" },

        onenter = function(inst, data)
            --ClearStatusAilments(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("fossilized")
			inst.AnimState:PushAnimation("fossilized_loop", true)
            inst.components.fossilizable:OnFossilize(TUNING.FORGE.BOARILLA.FOSSIL_TIME)
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_pre_2") end),
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/sleep_out") end),
			TimeEvent(39*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/sleep_out") end),
			TimeEvent(71*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/sleep_out") end),
		},

        events =
        {
            EventHandler("fossilize", function(inst, data)
                inst.components.fossilizable:OnExtend(TUNING.FORGE.BOARILLA.FOSSIL_TIME)
            end),
            EventHandler("unfossilize", function(inst)
                inst.sg.statemem.unfossilizing = true
                inst.sg:GoToState("unfossilizing")
            end),
        },

        onexit = function(inst)
            inst.components.fossilizable:OnUnfossilize()
            if not inst.sg.statemem.unfossilizing then
                --Interrupted
                inst.components.fossilizable:OnSpawnFX()
            end
        end,
    },
	
	State
    {
        name = "unfossilizing",
        tags = { "busy", "caninterrupt", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("fossilized_shake")
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_shake_LP", "shakeloop") end),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.unfossilized = true
                    inst.sg:GoToState("unfossilized")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.unfossilized then
                --Interrupted
                inst.components.fossilizable:OnSpawnFX()
            end
            inst.SoundEmitter:KillSound("shakeloop")
        end,
    },
	
	State
    {
        name = "unfossilized",
        tags = { "busy", "caninterrupt", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("fossilized_pst")
            inst.components.fossilizable:OnSpawnFX()
        end,

        timeline = 
		{ 
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end)
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "knockback",
        tags = { "busy", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation("stun_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/hit")
			--inst.AnimState:SetTime(10* FRAMES)

            if data ~= nil and data.radius ~= nil and data.knocker ~= nil and data.knocker:IsValid() then
                local x, y, z = data.knocker.Transform:GetWorldPosition()
                local distsq = inst:GetDistanceSqToPoint(x, y, z)
                local rangesq = data.radius/2 * data.radius/2
                local rot = inst.Transform:GetRotation()
                local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                local drot = math.abs(rot - rot1)
                while drot > 180 do
                    drot = math.abs(drot - 360)
                end
                local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                inst.sg.statemem.speed = (data.strengthmult or 1) * 12 * k
                inst.sg.statemem.dspeed = 0
                if drot > 90 then
                    inst.sg.statemem.reverse = true
                    inst.Transform:SetRotation(rot1 + 180)
                    inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                else
                    inst.Transform:SetRotation(rot1)
                    inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                end
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.dspeed
                if inst.sg.statemem.speed < 0 then
                    inst.sg.statemem.dspeed = inst.sg.statemem.dspeed + .075
                    inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
                else
					inst.AnimState:PlayAnimation("stun_pst")
                    inst.sg.statemem.speed = nil
                    inst.sg.statemem.dspeed = nil
                    inst.Physics:Stop()
                end
            end
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                --inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.Physics:Stop()
            end
        end,
    },
	
}

--CommonStates.AddFrozenStates(states)
--CommonStates.AddFossilizedStates(states)
    
return StateGraph("boarilla", states, events, "taunt", actionhandlers)

