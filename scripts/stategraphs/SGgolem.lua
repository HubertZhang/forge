--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("stategraphs/commonstates")

local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, "action"),
	ActionHandler(ACTIONS.ATTACK, "attack"),
}

local events=
{
    EventHandler("attacked", function(inst, data) 
		if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
    EventHandler("doattack", function(inst, data) 
		if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("attack", data.target) 
		end 
	end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(false,false),
    CommonHandlers.OnFreeze(),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/idle_LP", "idle_LP")
        end,

    },

    State{
        name = "attack", --swipe
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
        --inst.components.combat:StartAttack()
		if inst.components.combat.target ~= nil then
			inst:ForceFacePoint(inst.components.combat.target:GetPosition())
		end
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack")
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {

			TimeEvent(5*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/attack")
			if inst.components.combat.target ~= nil then
			inst:ForceFacePoint(inst.components.combat.target:GetPosition())
			end
			inst.components.combat:DoAttack(inst.components.combat.target)
			end),
            TimeEvent(16*FRAMES, function(inst) 
			if inst.components.combat.target ~= nil then
			inst:ForceFacePoint(inst.components.combat.target:GetPosition())
			end
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/attack")
			inst.components.combat:DoAttack(inst.components.combat.target)
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
			local target = inst.components.combat.target
			if inst.sg.statemem.wants_to_die then
			inst.sg:GoToState("death")
			elseif target ~= nil and not target.components.health:IsDead() and target:IsValid() and not (target.sg and (target.sg:HasStateTag("sleeping") or target.sg:HasStateTag("fosillized"))) then
			inst.sg:GoToState("attack", target) 
			else
			inst.sg:GoToState("idle") 
			end
			end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/enter")
			--inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "spawn",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("spawn")
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/enter")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("idle")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/spell/elemental/death")
			inst.SoundEmitter:KillSound("idle_LP")
			if inst.sg.statemem.wants_to_die then
				inst.SoundEmitter:KillSound("dontstarve/common/lava_arena/spell/elemental/attack")
            end
			inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			--inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst:Remove()
                end
            end),
        },

    },	
}

--CommonStates.AddFrozenStates(states)

return StateGraph("golem", states, events, "spawn", actionhandlers)

