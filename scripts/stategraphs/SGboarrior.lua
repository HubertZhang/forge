--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("stategraphs/commonstates")

local function CalcDashSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(0))
        if dist > 0 then
            return math.min(20, dist) / (10 * FRAMES)
        end
    end
    return 0
end

local function WantsToCharge(inst)
	if inst.level >= 2 and inst.components.combat.target then
		local x, y, z = inst.components.combat.target.Transform:GetWorldPosition()
		local distsq = inst:GetDistanceSqToPoint(x, y, z)
		--print("target check is running")
		--print(distsq)
		if distsq > 300 and distsq < 400 then
			return true
		else
			return false
		end
	else
		return false
	end	
end

local function DoFrontAoe(inst)
local posx, posy, posz = inst.Transform:GetWorldPosition()
local angle = -inst.Transform:GetRotation() * DEGREES
local offset = 2
local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, TUNING.FORGE.BOARRIOR.AOE_HIT_RANGE, { "locomotor"}, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or { "LA_mob", "fossil", "shadow", "playerghost", "INLIMBO" })
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst.components.combat.target then
			--inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(ent))
		end
	end
end

local function AttemptSlam(inst, target)
	if inst.components.combat.target and inst.altattack and inst.altattack == true and inst.components.combat.canattack == true and inst.components.health and inst.components.health:GetPercent() <= 0.9 then
		local x, y, z = inst.components.combat.target.Transform:GetWorldPosition()
		local distsq = inst:GetDistanceSqToPoint(x, y, z)
		--print("[MidRangeTarget] dist is "..distsq)
		if distsq and distsq > 15 and distsq < 130 then
			inst.sg:GoToState("attack_special", inst.components.combat.target)
		end
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) then
		return target
	else
		return distsq ~= nil and distsq < 225 and not player:HasTag("notarget") and player --Leo: Removed the LA_mob check because we should always want them to attack players in forge.
	end
end

local BANNER_LOOPS = 20

local function GetCD(inst)
	local health = inst.components.health:GetPercent()
	if health > TUNING.FORGE.BOARRIOR.PHASE2_TRIGGER then
		return 8
	elseif health <= TUNING.FORGE.BOARRIOR.PHASE2_TRIGGER and health > TUNING.FORGE.BOARRIOR.PHASE3_TRIGGER then
		return  8
	elseif health <= TUNING.FORGE.BOARRIOR.PHASE3_TRIGGER and health > 0.25 then --Phase4 Trigger?
		return 8
	end	
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .2, inst, 30)
end

local function ShakeIfCloseSlam(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .7, .04, .2, inst, 30)
end

local function ShakePound(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
    ShakeAllCameras(CAMERASHAKE.FULL, 1.2, .03, .7, inst, 30)
end

local function ShakeRoar(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 0.7, .03, .5, inst, 30)
end

local function FindMidRangeTarget(inst)
	local targets = {}
	if not inst:HasTag("brainwashed") then
		local x, y, z = inst.Transform:GetWorldPosition()
		local ents = TheSim:FindEntities(x, y, z, TUNING.FORGE.BOARRIOR.ATTACK2_RANGE, { "locomotor", "player" }, { "INLIMBO", "playerghost"})
		
		for i, v in ipairs(ents) do
			if v:IsValid() and (v.components.health and not v.components.health:IsDead()) then
				local x, y, z = v.Transform:GetWorldPosition()
				local distsq = inst:GetDistanceSqToPoint(x, y, z)
				--print("[MidRangeTarget] dist is "..distsq)
				if distsq and distsq > 25 and distsq < 130 then
					table.insert(targets, v)
				end
			end		
		end
	end
	return targets
end

--don't use this
local function FindMagicCaster(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x, y, z, TUNING.FORGE.BOARRIOR.ATTACK2_RANGE, { "locomotor", "player" }, { "INLIMBO", "playerghost"})
	local targets = {}
	for i, v in ipairs(ents) do
		if v:IsValid() and (v.components.health and not v.components.health:IsDead()) then
			local hands = v.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			if hands and hands:HasTag("magicweapon") then
				table.insert(targets, v)
			end
		end		
	end
	--print("[FindMagicCaster] "..#targets)
	return targets
end

local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, "action"),
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst.altattack and inst.altattack == true then
			return "attack_special"
		else
			return "attack"
		end	
	end),	
}

local function SetBanner(inst)
	local wave = TheWorld.components.lavaarenaevent
	local currentround = wave ~= nil and wave:GetCurrentRound()
	--print("DEBUG: Crocommander got currentround as "..currentround or "nil")
	if wave.rounds_data[currentround] ~= nil and wave.rounds_data[currentround].banner ~= nil then
		return wave.rounds_data[currentround].banner
	else
		--print("ERROR: Something went wrong with SetBanner in SGboarrior.lua, setting banner to heal")
		return "battlestandard_heal"
	end
end

local function SpawnBanners(inst)
	local pos = TheWorld.components.lavaarenaevent:GetArenaCenterPoint() or inst.Transform:GetPosition()
	local offset = 9.5
	local banner = SetBanner(inst)
	
	SpawnPrefab(banner).Transform:SetPosition(pos.x + offset, 0, pos.z + offset)
	SpawnPrefab(banner).Transform:SetPosition(pos.x + offset, 0, pos.z - offset)
	SpawnPrefab(banner).Transform:SetPosition(pos.x - offset, 0, pos.z - offset)
	SpawnPrefab(banner).Transform:SetPosition(pos.x - offset, 0, pos.z + offset)
end

local function IsSwarmed(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x, y, z, TUNING.FORGE.BOARRIOR.ATTACK_RANGE + 1, { "locomotor", "player" }, { "fossil", "shadow", "playerghost", "INLIMBO", "epic"})
	--print("DEBUG: ISSWARMED RETURNED "..#ents or nil)
	return #ents or 0
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not inst.sg:HasStateTag("nocancel") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data)
		elseif not inst.components.health:IsDead() and not (inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("nointerrupt")) and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + TUNING.FORGE.HIT_RECOVERY < GetTime()) then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
    EventHandler("doattack", function(inst, data) 
		if inst.ischarging then inst.ischarging = nil end --stop changing when trying to attack
		if not inst.components.health:IsDead() and not (inst.sg:HasStateTag("hit") or inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("nointerrupt")) then 
			if inst.level > 0 and inst.altattack2 and inst.altattack2 == true then
				inst.sg:GoToState("attack_special2")	
			elseif inst.components.health and inst.components.health:GetPercent() <= 0.9 and #FindMidRangeTarget(inst) > 0 and inst.altattack and inst.altattack == true then
				inst.sg:GoToState("attack_special", data.target) 
			else
				inst.sg:GoToState("attack", data.target) 
			end
		end 
	end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		
        if is_moving and not should_move then
			if not inst.ischarging then --safe to assume doattack will stop this?
				inst.sg:GoToState("run_stop")
			end
        elseif not is_moving and should_move then
			if inst.ischarging then
				inst.sg:GoToState("dash")
			else
				inst.sg:GoToState("run_start")
			end
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
    end),
	CommonHandlers.OnFossilize(),
    --CommonHandlers.OnFreeze(),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
			if inst.components.combat.target then
				AttemptSlam(inst, inst.components.combat.target)
			end
        end,
		
		events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    }, 
	
	State{
		name = "stun",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			inst.components.sleeper:SetResistance(1) --incase banner state gets interrupted at the exact moment the last banner call ends.
			if data.stimuli ~= nil and data.stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(TUNING.FORGE.STUN_TIME)
				inst.components.combat:SetTarget(data.attacker and data.attacker or nil)
				inst.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_STUN
			else
				inst.lasttarget = nil --should allow previous target to be targeted again if hes closest.
				inst.components.combat:SetTarget(nil)	
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/hit")
			end
			inst.sg.stun_stimuli = data.stimuli
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
        },
		
		onexit = function(inst)
            
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.stun_stimuli or nil)
		end,

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.stun_stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli and stimuli == "explosive" and (not inst.last_taunt_time or (inst.last_taunt_time + TUNING.FORGE.TAUNT_CD < GetTime())) then
				inst.last_taunt_time = GetTime()
				inst.sg.statemem.needstotaunt = true
			end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {

        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.banner_calls and inst.banner_calls < BANNER_LOOPS then
					inst.sg:GoToState("banner_pre")
				else
					if inst.sg.statemem.needstotaunt then
						inst.sg.statemem.needstotaunt = nil
						inst.sg:GoToState("taunt")
					else
						inst.sg:GoToState("idle")
					end
				end	
				inst.sg.stun_stimuli = nil
			end),
        },
    },
	
	State{
		name = "banner_pre",
        tags = {"busy", "nointerrupt"},

        onenter = function(inst, cb)
			inst.components.sleeper:SetResistance(9999)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("banner_pre")
			if not inst.banner_calls then
				inst.banner_calls = 0
				inst:DoTaskInTime(2, SpawnBanners)
			end
			inst.sg.statemem.wants_to_banner = true --might use to force finish the banner calling.
        end,

		timeline=
        {
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("banner_loop") end),
        },
    },
	
	State{
		name = "banner_loop",
        tags = {"busy", "nointerrupt", "nofreeze"},

        onenter = function(inst, cb)
			inst.components.sleeper:SetResistance(9999)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("banner_loop", true)
			inst.banner_calls = inst.banner_calls ~= nil and inst.banner_calls + 1 or 1
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/banner_call_a") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.banner_calls and inst.banner_calls < BANNER_LOOPS then
					inst.sg:GoToState("banner_loop") 
				else
					--inst.sg.statemem.wants_to_banner = nil
					inst.sg:GoToState("banner_pst")
				end
			end),
        },
    },
	
	State{
		name = "banner_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("banner_pst")
			
        end,

		timeline=
        {
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/stun") end),
        },
		
		onexit = function(inst)
            inst.components.sleeper:SetResistance(1)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "attack", --swipe
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()		
        if inst.components.combat.target ~= nil then
            if inst.components.combat.target:IsValid() then
                inst:ForceFacePoint(inst.components.combat.target:GetPosition())
            end
        end
		inst.sg.statemem.attacktarget = inst.components.combat.target
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe_pre")
        inst.AnimState:PlayAnimation("attack1")
		if inst.components.health:GetPercent() > TUNING.FORGE.BOARRIOR.PHASE3_TRIGGER or inst.altattack3 == false then
			inst.AnimState:PushAnimation("attack1_pst", false)
		elseif not inst.sg.statemem.attacktarget or inst.sg.statemem.attacktarget.components.health:IsDead() then
			inst.sg.statemem.attacktarget = nil
			inst.AnimState:PushAnimation("attack1_pst", false)
		end
	end,

    timeline=
    {

			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/swipe")
			end),
            TimeEvent(11*FRAMES, function(inst)
				inst.components.combat:DoAttack()
				DoFrontAoe(inst)
				--inst.components.combat:DoAreaAttack(inst.components.combat.target or inst, 3, nil, nil, nil, {"LA_mob", "shadow", "INLIMBO", "playerghost", "battlestandard"}) 
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
				if inst.components.health:GetPercent() <= TUNING.FORGE.BOARRIOR.PHASE3_TRIGGER and inst.sg.statemem.attacktarget and not inst.sg.statemem.attacktarget.components.health:IsDead() and inst.altattack3 and inst.altattack3 == true then
					inst.altattack3 = false
					inst.sg:GoToState("attack2", inst.sg.statemem.attacktarget)
				else
					inst.components.combat:TryRetarget()
					inst.sg:GoToState("idle")  
				end
			end),
        },
	
    },
	
	State{
        name = "attack2", --swipe
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		if target then
			inst.sg.statemem.attacktarget = target
		end
		if inst.sg.statemem.attacktarget then  
			inst:ForceFacePoint(inst.sg.statemem.attacktarget:GetPosition())
		end
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe_pre")
        inst.AnimState:PlayAnimation("attack2")
		if not inst.sg.statemem.attacktarget or inst.sg.statemem.attacktarget.components.health:IsDead() then
			inst.sg.statemem.attacktarget = nil
			inst.AnimState:PushAnimation("attack2_pst", false)	
		end
		
		if inst.brain ~= nil then
		inst.brain:Stop()
		end
        
		inst.Physics:SetMotorVelOverride(10,0,0)
	end,
	
	onexit = function(inst)
		if inst.brain ~= nil then
			inst.brain:Start()
		end
    end,

    timeline=
    {

            TimeEvent(4*FRAMES, function(inst) 
				inst.components.combat:DoAttack(inst.sg.statemem.attacktarget)
				DoFrontAoe(inst)
				--inst.components.combat:DoAreaAttack(inst.sg.statemem.attacktarget or inst, 3, nil, nil, nil, {"LA_mob", "shadow", "INLIMBO", "playerghost", "battlestandard"}) 
			end),
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/swipe")
			end),
			TimeEvent(6*FRAMES, function(inst) inst.Physics:ClearMotorVelOverride()
					inst.components.locomotor:Stop() end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
				if inst.sg.statemem.attacktarget and not inst.sg.statemem.attacktarget.components.health:IsDead() then
					inst.sg:GoToState("attack3", inst.sg.statemem.attacktarget)
				else
					inst.sg:GoToState("idle")  
				end 
			end),
        },
	
    },
	
	State{
        name = "attack3", --final swipe
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		if target then
			inst.sg.statemem.attacktarget = target
		end
        --inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe_pre")
        inst.AnimState:PlayAnimation("attack3")
        if inst.sg.statemem.attacktarget then  
			inst:ForceFacePoint(inst.sg.statemem.attacktarget:GetPosition())
		end
		
		if inst.brain ~= nil then
		inst.brain:Stop()
		end
		inst.Physics:SetMotorVelOverride(10,0,0)
	end,
	
	onexit = function(inst)
		if inst.brain ~= nil then
			inst.brain:Start()
		end
		inst.sg.statemem.attacktarget = nil
    end,

    timeline=
    {

			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe")
				--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/swipe")
			end),
			TimeEvent(6*FRAMES, function(inst) inst.Physics:ClearMotorVelOverride()
					inst.components.locomotor:Stop() end),
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/taunt_2")
			--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt_short")
			end),
			TimeEvent(6*FRAMES, ShakeIfClose),
            TimeEvent(6*FRAMES, function(inst) 
				inst.components.combat:DoAttack(inst.sg.statemem.attacktarget)
				DoFrontAoe(inst)
				--inst.components.combat:DoAreaAttack(inst.sg.statemem.attacktarget or inst, 3, nil, nil, nil, {"LA_mob", "shadow", "INLIMBO", "playerghost", "battlestandard"}) 
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("idle")
			end),
        },
	
    },
	
	--TODO: clean up
	State{
        name = "attack_special", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target, forcedtarget)
		inst.slamhardcooldown = true
		inst:DoTaskInTime(2, function(inst) inst.slamhardcooldown = nil end)

		inst.is_doing_special = true --To prevent knockback effects
		inst.Transform:SetEightFaced()

		inst.components.combat:StartAttack()
		
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/grunt")
		
        inst.AnimState:PlayAnimation("attack5")
		local targets = FindMidRangeTarget(inst)
		if #targets > 0 and not inst.wants_to_slam then 
			inst.components.combat:SetTarget(targets[math.random(1, #targets)])
		elseif inst.wants_to_slam and inst.wants_to_slam:IsValid() then
			inst.components.combat:SetTarget(inst.wants_to_slam) -- this is pretty redudant but whatever.
		end
		inst.sg.statemem.attacktarget = inst.components.combat.target
        if inst.sg.statemem.attacktarget ~= nil then
            if inst.sg.statemem.attacktarget:IsValid() then
                inst:ForceFacePoint(inst.components.combat.target:GetPosition())
				inst.targetx, inst.targety, inst.targetz = inst.components.combat.target.Transform:GetWorldPosition()
            end
        end
	end,
	
	onexit = function(inst)
		inst:RemoveTag("groundspike")
		--inst:RemoveTag("object")
        --inst:RemoveTag("stone")
		inst.Transform:SetFourFaced()
		--Leo: TODO: simply just make the trails do no knockback on their own.
		inst:DoTaskInTime(0.25, function(inst) inst.is_doing_special = nil end) --Leo: Give a little bit of time after the state ends before setting it to nil.
		inst.targetx = nil
		inst.targety = nil
		inst.targetz = nil
		inst.sg.statemem.attacktarget = nil
		inst.altattack = false
		inst.wants_to_slam = nil
		inst.components.combat:SetRange(TUNING.FORGE.BOARRIOR.ATTACK_RANGE, TUNING.FORGE.BOARRIOR.HIT_RANGE)
		if not inst.altattack or inst.altattack == false then
			inst:DoTaskInTime(TUNING.FORGE.BOARRIOR.ATTACK2_CD, function(inst) inst.altattack = true end)
		end	
    end,
	
    timeline=
		{

			TimeEvent(15*FRAMES, function(inst)  inst.sg:AddStateTag("nocancel") end),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/attack_5") 
			
			--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")	
			end),
            --TimeEvent(10*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 5, nil, nil, nil, {"LA_mob", "battlestandard", "shadow", "INLIMBO", "playerghost"})  end),
			TimeEvent(15*FRAMES, ShakeIfClose),
			TimeEvent(15*FRAMES, function(inst) 
				if inst.sg.statemem.attacktarget  then				
					inst.targetx, inst.targety, inst.targetz = inst.sg.statemem.attacktarget.Transform:GetWorldPosition()
				end
				if inst.targetx and inst.targetz then	
					inst:FacePoint(inst.targetx, 0, inst.targetz)
					inst:DoTrail(inst.targetx, inst.targetz)
				end	
			end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/attack_5_fire_1") end),
			TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/attack_5_fire_2") end),
			TimeEvent(35*FRAMES, ShakeIfClose),
			TimeEvent(35*FRAMES, function(inst) 
				--inst.components.combat:DoAreaAttack(inst, 5, nil, nil, nil, {"LA_mob", "battlestandard", "shadow", "INLIMBO", "playerghost"}) 
				if inst.targetx and inst.targetz then	 
					inst:DoTrail(inst.targetx, inst.targetz, true)
				end	
				inst.sg:RemoveStateTag("nocancel")
			end),
		},

        events=
        {
            EventHandler("animqueueover", function(inst) 
				--inst.components.combat:TryRetarget() --who put this here?
				inst.sg:GoToState("idle")
			end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/hit")
			inst.sg.mem.last_hit_time = GetTime()
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.banner_calls and inst.banner_calls < BANNER_LOOPS then
					inst.sg:GoToState("banner_pre")
				elseif inst.wants_to_slam then
					inst.sg:GoToState("attack_special", nil, inst.wants_to_slam)
				else
					inst.sg:GoToState("idle") 
				end	
			end),
        },
    },
	
	
	State{
		name = "taunt",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/taunt")	
			end),
			TimeEvent(15*FRAMES, ShakeRoar)
        },

        events=
        {
			EventHandler("animover", function(inst)
				if inst.banner_calls and inst.banner_calls < BANNER_LOOPS then
					inst.sg:GoToState("banner_pre") 
				else
					inst.sg:GoToState("idle") 
				end
			end),
        },
    },
	
	State{
		name = "attack_special2",
        tags = {"busy"},

        onenter = function(inst, cb)
			inst.altattack2 = false
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack4")
			inst.components.combat:StartAttack()
			--inst.components.combat.areahitdamagepercent = 0.8
			-- Glass: I removed these, not sure if they were needed, they were causing incorrect damage as far as I know
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/spin") end),
			TimeEvent(13*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6, nil, nil, nil, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or {"battlestandard", "LA_mob", "shadow", "playerghost", "FX", "DECOR"}) end),
			--TimeEvent(17*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6.2, nil, nil, nil, {"battlestandard", "LA_mob", "shadow", "playerghost", "FX", "DECOR"}) end), 
			--TimeEvent(26*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6.2, nil, nil, nil, {"battlestandard", "LA_mob", "shadow", "playerghost", "FX", "DECOR"}) end), 
			TimeEvent(31*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6, nil, nil, nil, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or {"battlestandard", "LA_mob", "shadow", "playerghost", "FX", "DECOR"}) end), 
			--TimeEvent(41*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6.2, nil, nil, nil, {"battlestandard", "LA_mob", "shadow", "playerghost", "FX", "DECOR"}) end), 
        },
		
		onexit = function(inst)
			--inst.components.combat.areahitdamagepercent = 0.8
			if inst.level >= 2 then
				inst.altattack3 = true
			end
			inst:DoTaskInTime(TUNING.FORGE.BOARRIOR.SPIN_CD, function(inst) inst.altattack2 = true end)
		end,
		
        events=
        {
			EventHandler("onhitother", function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/swipe") end),
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step")		
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.components.armorbreak_debuff:RemoveDebuff()
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/death")
            inst.AnimState:PlayAnimation("death2")
            inst.Physics:Stop()
            --RemovePhysicsColliders(inst)
			--MakeObstaclePhysics(inst, 1.5)
			inst:AddTag("NOCLICK")
			ChangeToObstaclePhysics(inst)
        end,

		timeline=
        {
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit1") end),
			TimeEvent(50*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit1") end),
			TimeEvent(55*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/death_bodyfall") end),
			TimeEvent(70*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bonehit2") end),
			TimeEvent(55*FRAMES, ShakeRoar),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bone_drop") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
			TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bone_drop") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/sleep_out") end),
        },

        events =
        {
            --Leo: Temp fix for perma sleeping bug:
			EventHandler("attacked", function(inst) inst.sg:GoToState("wake") end ),
			
			---
			EventHandler("animover", function(inst) 
				if inst.components.sleeper.sleepiness <= 0 then
					inst.sg:GoToState("wake") 
				else
					inst.sg:GoToState("sleeping")
				end
			end),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/grunt")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/bone_drop") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/bone_drop") 
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			local wantstocharge = WantsToCharge(inst)
			if wantstocharge == true then
				inst.ischarging = true
				inst.sg:GoToState("dash")
			end	
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
			if inst.wants_to_slam then
				inst.sg:GoToState("attack_special", nil, inst.wants_to_slam)
			end	
			if inst.components.combat.target then
				AttemptSlam(inst, inst.components.combat.target)
			end
			local wantstocharge = WantsToCharge(inst)
			if wantstocharge == true then
				inst.ischarging = true
				inst.sg:GoToState("dash")
			end	
        end,
		
		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour", nil , 8)
			end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/grunt") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour", nil , 8)
			end),
			TimeEvent(0*FRAMES, ShakeIfClose),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour", nil , 8)
			end),
			TimeEvent(20*FRAMES, ShakeIfClose),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour", nil , 8)          
        end,
		
		timeline=
        {
			TimeEvent(0*FRAMES, ShakeIfClose),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "dash",
        tags = { "nointerrupt", "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("dash")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/grunt") 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step")		
        end,
		
		onexit = function(inst)
			inst.ischarging = nil
		end,
		
		timeline = 
		{
			TimeEvent(2*FRAMES, function(inst) inst.components.locomotor:RunForward() 
			--inst.components.locomotor.runspeed = CalcDashSpeed(inst, inst.components.combat.target) 
			end),  
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boarrior/step")  
			end),
			TimeEvent(13*FRAMES, ShakeIfClose),
		},
		
        events =
        {
            EventHandler("animover", function(inst) 
				inst.components.locomotor:StopMoving() 
				if inst.components.combat.target then
					inst.sg:GoToState("attack_special", nil, inst.components.combat.target) 
				else
					inst.sg:GoToState("idle")
				end
			end ),
        },
    },
	
	--Fossilized States--
	State
    {
        name = "fossilized",
        tags = { "busy", "fossilized", "caninterrupt" },

        onenter = function(inst, data)
            --ClearStatusAilments(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("fossilized")
			inst.AnimState:PushAnimation("fossilized_loop", true)
            inst.components.fossilizable:OnFossilize(TUNING.FORGE.BOARRIOR.FOSSIL_TIME)
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_pre_2") end),
			TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(44*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(55*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(66*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(77*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(88*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(99*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(110*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
			TimeEvent(121*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_hit") end),
		},

        events =
        {
            EventHandler("fossilize", function(inst, data)
                inst.components.fossilizable:OnExtend(TUNING.FORGE.BOARRIOR.FOSSIL_TIME)
            end),
            EventHandler("unfossilize", function(inst)
                inst.sg.statemem.unfossilizing = true
                inst.sg:GoToState("unfossilizing")
            end),
        },

        onexit = function(inst)
            inst.components.fossilizable:OnUnfossilize()
            if not inst.sg.statemem.unfossilizing then
                --Interrupted
                inst.components.fossilizable:OnSpawnFX()
            end
        end,
    },
	

	
	State
    {
        name = "unfossilizing",
        tags = { "busy", "caninterrupt", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("fossilized_shake")
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_shake_LP", "shakeloop") end),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.unfossilized = true
                    inst.sg:GoToState("unfossilized")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.unfossilized then
                --Interrupted
                inst.components.fossilizable:OnSpawnFX()
            end
            inst.SoundEmitter:KillSound("shakeloop")
        end,
    },
	
	State
    {
        name = "unfossilized",
        tags = { "busy", "caninterrupt", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("fossilized_pst")
            inst.components.fossilizable:OnSpawnFX()
        end,

        timeline = 
		{ 
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end)
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")					
                end
            end),
        },
    },
	
	
}

--CommonStates.AddFrozenStates(states)



    
return StateGraph("boarrior", states, events, "taunt", actionhandlers)

