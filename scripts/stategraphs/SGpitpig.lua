--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("stategraphs/commonstates")

function DoDash(inst, startingpos, targetpos)
	if startingpos and targetpos then
		local player_index = {}
		local found_players = {}
		for i = 0,15 do
			inst:DoTaskInTime(FRAMES*math.ceil(1+i/3), function()
				local offset = (targetpos - startingpos):GetNormalized()*(i*0.6)
				--SpawnPrefab("spear_gungnir_lungefx").Transform:SetPosition((startingpos+offset):Get())
				
				local x, y, z = inst:GetPosition():Get()--(startingpos + offset):Get()
				
				local ents = inst:HasTag("brainwashed") and TheSim:FindEntities(x, y, z, 2, nil, {"player", "companion", "brainwashed", "notarget"}) or TheSim:FindEntities(x, y, z, 2, nil, {"LA_mob", "notarget"})
				for _,ent in ipairs(ents) do
					if not player_index[ent] and ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health then
						player_index[ent] = true
						ent.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(ent))
					end
				end
			end)
		end
	end
end

local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, "action"),
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst.altattack and inst.altattack == true then
			return "attack2"
		else
			return "attack"
		end	
	end),
}

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data)
		elseif not inst.components.health:IsDead() and data.stimuli and data.stimuli == "strong" then 
			inst.sg:GoToState("hit") 
		elseif not inst.components.health:IsDead() and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + TUNING.FORGE.HIT_RECOVERY < GetTime()) and not inst.sg:HasStateTag("busy") then 
			inst.sg:GoToState("hit")
		end 
	end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
    EventHandler("doattack", function(inst, data) 
		if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then 
			if inst.altattack == false then
			inst.sg:GoToState("attack", data.target) 
			else
			inst.sg:GoToState("attack2", data.target) 
			end
		end 
	end),
	EventHandler("knockback", function(inst, data) if not inst.components.health:IsDead() then inst.sg:GoToState("knockback", data) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnFossilize(),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },
	
	

    State{
        name = "attack", --swipe
        tags = {"attack", "busy"},
		
		  onenter = function(inst, target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack1")
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {

			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/attack_1")
			--inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_weapon")
			end),
            TimeEvent(12*FRAMES, function(inst) inst.components.combat:DoAttack(inst.components.combat.target) end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "attack2", --dash
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		inst.altattack = false
        inst.components.combat:StartAttack()
		if inst.components.combat.target then
			inst.sg.statemem.attacktarget = inst.components.combat.target
			inst.sg.statemem.attackpos = inst.components.combat.target:GetPosition()
			inst:ForceFacePoint(inst.sg.statemem.attackpos)
		end
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("attack2")
		if inst.brain ~= nil then
		inst.brain:Stop()
		end
	end,
	
	onexit = function(inst)
		if inst.brain ~= nil then
			inst.brain:Start()
		end
		inst.sg.statemem.attacktarget = nil
		inst.sg.statemem.attackpos = nil
		inst:DoTaskInTime(TUNING.FORGE.PITPIG.RAM_CD, function(inst) inst.altattack = true end)
    end,
	
    timeline=
    {
			
			TimeEvent(10*FRAMES, function(inst) 
				if inst.components.combat.target then
					inst.sg.statemem.attackpos = inst.components.combat.target:GetPosition()
					inst:ForceFacePoint(inst.sg.statemem.attackpos)
				end	
			end),
			TimeEvent(18*FRAMES, function(inst) 
				inst.Physics:SetMotorVel(35, 0, 0)
			end),
			TimeEvent(19*FRAMES, function(inst) 
				if inst.components.combat.target then
					--inst.sg.statemem.attackpos = inst.components.combat.target:GetPosition()
					--inst:ForceFacePoint(inst.sg.statemem.attackpos)
					DoDash(inst, inst:GetPosition(), inst.sg.statemem.attackpos) 
				end	
			end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/attack_2")	end),
			TimeEvent(26*FRAMES, function(inst) 
				inst.Physics:ClearMotorVelOverride()
				inst.components.locomotor:Stop() 
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit", "nofreeze"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/hit")
			--inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
			inst.sg.mem.last_hit_time = GetTime()
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },	
	
	State{
		name = "taunt",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
			--if inst.brain ~= nil then
				--inst.brain:Stop()
			--end
        end,

		timeline=
        {
			--Leo: Something wonky is going on with this, disabling it for right now. I'll deal with it later.
			--TimeEvent(7*FRAMES, function(inst) inst.Physics:SetMotorVel(math.random(-1,1), 0,math.random(-1,1)) end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve/pig/oink")
			end),
			TimeEvent(18*FRAMES, function(inst) 	
				inst.Physics:ClearMotorVelOverride()
				inst.components.locomotor:Stop() 
			end),
			
        },
		
		onexit = function(inst)
			--if inst.brain ~= nil then
				--inst.brain:Start()
			--end
		end,

        events=
        {
			EventHandler("animover", function(inst)
				--Leo: Pretty sure they don't lose target on taunt, if they do its definetly not when they're in melee range of their current target. 
				--if inst.components.follower and not inst.components.follower.leader and math.random(1, 3) == 1 then
					--inst.components.combat:SetTarget(nil)
					--inst.components.combat:TryRetarget()
				--end
				inst.sg:GoToState("idle") 
			end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("run_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")
			inst.components.armorbreak_debuff:RemoveDebuff()
			--inst.SoundEmitter:PlaySound("dontstarve/pig/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			--inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then

                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            --Leo: Temp fix for perma sleeping bug:
			EventHandler("attacked", function(inst) inst.sg:GoToState("wake") end ),
			
			---
			EventHandler("animover", function(inst) 
				if inst.components.sleeper.sleepiness <= 0 then
					inst.sg:GoToState("wake") 
				else
					inst.sg:GoToState("sleeping")
				end
			end),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			inst.sg.statemem.stimuli = data.stimuli or nil
			if data.stimuli and data.stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/stun")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(TUNING.FORGE.STUN_TIME)
				inst.components.combat:SetTarget(data.attacker and data.attacker or nil)
				inst.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_STUN
			else
				inst.lasttarget = nil --should allow previous target to be targeted again if hes closest.
				inst.components.combat:SetTarget(nil)				
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/hit")
			end
        end,

		timeline=
        {

        },
		
		onexit = function(inst)
            
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,
		
		

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
            inst.Physics:Stop()
			if stimuli and stimuli == "explosive" and (not inst.last_taunt_time or (inst.last_taunt_time + TUNING.FORGE.TAUNT_CD < GetTime())) then
				inst.last_taunt_time = GetTime()
				inst.sg.statemem.needstotaunt = true
			end
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.sg.statemem.needstotaunt then
					inst.sg.statemem.needstotaunt = nil
					inst.sg:GoToState("taunt")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	
	State{
        name = "knockback",
        tags = { "busy", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation("stun_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/hit")
			--inst.AnimState:SetTime(10* FRAMES)

            if data ~= nil and data.radius ~= nil and data.knocker ~= nil and data.knocker:IsValid() then
                local x, y, z = data.knocker.Transform:GetWorldPosition()
                local distsq = inst:GetDistanceSqToPoint(x, y, z)
                local rangesq = data.radius * data.radius
                local rot = inst.Transform:GetRotation()
                local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                local drot = math.abs(rot - rot1)
                while drot > 180 do
                    drot = math.abs(drot - 360)
                end
                local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                inst.sg.statemem.speed = (data.strengthmult or 1) * 12 * k
                inst.sg.statemem.dspeed = 0
                if drot > 90 then
                    inst.sg.statemem.reverse = true
                    inst.Transform:SetRotation(rot1 + 180)
                    inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                else
                    inst.Transform:SetRotation(rot1)
                    inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                end
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.dspeed
                if inst.sg.statemem.speed < 0 then
                    inst.sg.statemem.dspeed = inst.sg.statemem.dspeed + .075
                    inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
                else
					inst.AnimState:PlayAnimation("stun_pst")
                    inst.sg.statemem.speed = nil
                    inst.sg.statemem.dspeed = nil
                    inst.Physics:Stop()
                end
            end
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                --inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.Physics:Stop()
            end
        end,
    },
	
	
}

--CommonStates.AddFrozenStates(states)
CommonStates.AddFossilizedStates(states,
{
	fossilizedtimeline =
	{
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_pre_1") end)
	},
	
    unfossilizingtimeline = 
    { 
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_shake_LP", "shakeloop") end),
    },
	
	unfossilizedtimeline = 
    { 
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end)
    },
}, 
{

	unfossilizing_onexit = function(inst) inst.SoundEmitter:KillSound("shakeloop") end

}
)



    
return StateGraph("pitpig", states, events, "taunt", actionhandlers)

