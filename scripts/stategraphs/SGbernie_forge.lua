--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require "stategraphs/commonstates"

local TAUNT_DIST = 10

local function ondeathfn(inst, data)
    if not inst.sg:HasStateTag("deactivating") then
        inst.sg:GoToState("death", data)
    end
end

local function DoBuff(inst, data)
	if data and data.buff then
		local percent = inst.components.health:GetPercent()
		inst.components.health:SetMaxHealth(inst.components.health.maxhealth * data.buff)
		inst.components.health:SetPercent(percent)
		local s = 1 + (0.4 * data.buff)
		inst.Transform:SetScale(s, s, s)
		inst.components.revivablecorpse:SetReviveSpeedMult(0.5/data.buff)
	end
end

local function UnBuff(inst, data)
	if data and data.buff then
		local percent = inst.components.health:GetPercent()
		inst.components.health:SetMaxHealth(1000)
		inst.components.health:SetPercent(percent)
		local s = 1.4
		inst.Transform:SetScale(s, s, s)
		inst.components.revivablecorpse:SetReviveSpeedMult(0.5)
	end
end

local events =
{
    CommonHandlers.OnLocomote(false, true),
	 EventHandler("attacked", function(inst, data) 
		if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then
			inst.sg:GoToState("hit")
		end 
	end),
	EventHandler("knockback", function(inst, data) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("inactive") then inst.sg:GoToState("knockback", data) end end),
    EventHandler("death", ondeathfn),
	EventHandler("buffpets", function(inst, data) inst.sg:GoToState("buff", data) end),
	EventHandler("debuffpets", function(inst, data) inst.sg:GoToState("unbuff", data) end),
	EventHandler("respawnfromcorpse", function(inst, reviver) if inst.sg:HasStateTag("death") and reviver then inst.sg:GoToState("corpse_rebirth", reviver) end end),
}

local function IsTauntable(inst, target)
    return target.components.combat ~= nil
        and not target.components.combat:TargetIs(inst)
        and target.components.combat:CanTarget(inst)
end

local function FindForgeCreatures(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, TAUNT_DIST, { "LA_mob", "_combat", "locomotor" })
    for i = #ents, 1, -1 do
        if not IsTauntable(inst, ents[i]) then
            table.remove(ents, i)
        end
    end
    return #ents > 0 and ents or nil
end

local function TauntCreatures(inst)
	--print("DEBUG")
    if FindForgeCreatures(inst) ~= nil then
		--print("DEBUG 2")
        for i, v in pairs(FindForgeCreatures(inst)) do
			if v.components.combat.target == inst.components.follower.leader then
                v.components.combat:SetTarget(inst)
			end
        end
    end
end

local states =
{
    State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst)
            inst.Physics:Stop()
            if not inst.AnimState:IsCurrentAnimation("idle_loop") then
                inst.AnimState:PlayAnimation("idle_loop", true)
            end
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/idle")
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,
    },
	
	State{
        name = "knockback",
        tags = { "busy", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation("activate")
			inst.AnimState:SetTime(10* FRAMES)

            if data ~= nil and data.radius ~= nil and data.knocker ~= nil and data.knocker:IsValid() then
                local x, y, z = data.knocker.Transform:GetWorldPosition()
                local distsq = inst:GetDistanceSqToPoint(x, y, z)
                local rangesq = data.radius * data.radius
                local rot = inst.Transform:GetRotation()
                local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                local drot = math.abs(rot - rot1)
                while drot > 180 do
                    drot = math.abs(drot - 360)
                end
                local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                inst.sg.statemem.speed = (data.strengthmult or 1) * 12 * k
                inst.sg.statemem.dspeed = 0
                if drot > 90 then
                    inst.sg.statemem.reverse = true
                    inst.Transform:SetRotation(rot1 + 180)
                    inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                else
                    inst.Transform:SetRotation(rot1)
                    inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                end
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.dspeed
                if inst.sg.statemem.speed < 0 then
                    inst.sg.statemem.dspeed = inst.sg.statemem.dspeed + .075
                    inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
                else
                    inst.sg.statemem.speed = nil
                    inst.sg.statemem.dspeed = nil
                    inst.Physics:Stop()
                end
            end
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                --inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.Physics:Stop()
            end
        end,
    },

    State{
        name = "taunt",
        tags = { "busy", "taunting"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            --inst.components.timer:StartTimer("taunt_cd", 4)
			if inst.components.follower.leader and FindForgeCreatures(inst) then
				TauntCreatures(inst)			
			end
        end,

        timeline =
        {
            --3, 12, 21, 30
            TimeEvent(FRAMES*3, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*12, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*21, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*30, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            --10, 20, 28, 36
            TimeEvent(FRAMES*10, PlayFootstep),
            TimeEvent(FRAMES*20, PlayFootstep),
            TimeEvent(FRAMES*28, PlayFootstep),
            TimeEvent(FRAMES*36, PlayFootstep),
            
            TimeEvent(FRAMES*20, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "hit",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/hit")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = { "busy", "deactivating", "death" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("death")
			inst.Physics:ClearCollisionMask()
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/death")
			inst:AddTag("corpse")
			inst:RemoveTag("NOCLICK")
        end,

        events =
        {
            --EventHandler("animover", function(inst) inst:GoInactive() end),
        },
    },
	
	State{
        name = "buff",
        tags = { "busy", "nointerrupt" },

        onenter = function(inst, data)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("activate")
			inst:RemoveTag("notarget")
			DoBuff(inst, data)
        end,

        timeline =
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/sit_up")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State{
        name = "unbuff",
        tags = { "busy", "nointerrupt" },

        onenter = function(inst, data)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("activate")
			inst:RemoveTag("notarget")
			UnBuff(inst, data)
        end,

        timeline =
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/sit_up")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "activate",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("activate")
			inst:RemoveTag("notarget")
        end,

        timeline =
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/sit_up")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "deactivate",
        tags = { "busy", "deactivating", "inactive" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("deactivate")
			inst:AddTag("notarget")
        end,

        timeline =
        {
            TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/sit_down")
            end),
        },

        events =
        {
            --EventHandler("animover", function(inst) inst:GoInactive() end),
        },
    },
	
	State{
        name = "corpse_rebirth",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
			inst:RemoveTag("corpse")
            inst.components.health:SetInvincible(true)
			inst.AnimState:PlayAnimation("activate")
			inst:RemoveTag("notarget")
            if TheWorld.components.lavaarenaevent ~= nil and not TheWorld.components.lavaarenaevent:IsIntermission() then
                inst:AddTag("NOCLICK")
            end
			--inst.sg:SetTimeout(2)
			
        end,

        timeline =
        {
			TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/sit_up")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
			inst.components.health:SetPercent(1)
			if inst._leader then
				inst.components.follower.leader = inst._leader
			end
			
			inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.WORLD)
                inst.Physics:CollidesWith(COLLISION.OBSTACLES)
                inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
                inst.Physics:CollidesWith(COLLISION.CHARACTERS)
                inst.Physics:CollidesWith(COLLISION.GIANTS)
        end,
    },
}

CommonStates.AddWalkStates(states,
{
    walktimeline = {
        TimeEvent(10*FRAMES, function(inst)
            PlayFootstep(inst) 
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/walk")
        end),
        TimeEvent(30*FRAMES, function(inst)
            PlayFootstep(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/walk")
        end),
    },
    endtimeline = {
        TimeEvent(3*FRAMES, function(inst)
            PlayFootstep(inst) 
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/walk")
        end),
    },
})

return StateGraph("bernie", states, events, "activate")
