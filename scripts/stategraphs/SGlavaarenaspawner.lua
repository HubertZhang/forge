local events = {
    EventHandler("spawnlamobs", function(inst, data)
        inst:DoTaskInTime(data.delay - FRAMES*38, function(inst)
            inst.sg:GoToState("prespawnfx", data)
        end)
    end),
}

local DECOR_RADIUS = 3.3 
local NUM_DECOR = 6 

local function GetDecorPos(index) 
    local angle = 240 * DEGREES
    local start_angle = -.5 * angle
    local delta_angle = angle / (NUM_DECOR - 1)
    return DECOR_RADIUS * math.sin(start_angle + index * delta_angle),
        0,
        -DECOR_RADIUS * math.cos(start_angle + index * delta_angle)
end

local function SpawnDecorFX(inst, index)
    local fx = SpawnPrefab("lavaarena_spawnerdecor_fx_"..tostring(math.random(3)))
    local x, y, z = GetDecorPos(index)
    fx.Transform:SetPosition(x, y, z)
    fx.entity:SetParent(inst.entity)
end

local states = {
    State{
        name = "idle",
        tags = {"idle"},
    },

    State{
        name = "prespawnfx",
        tags = {"busy", "prefx"},

        onenter = function(inst, data)
            inst.sg.statemem.spawns = data

            if not (type(data.prefabtable) ~= "table" or #data.prefabtable == 0) then
                for i = 0, NUM_DECOR-1 do
                    local fx = SpawnPrefab("lavaarena_spawnerdecor_fx_small")
                    local x, y, z = GetDecorPos(i)
                    fx.Transform:SetPosition(x, y, z)
                    fx.entity:SetParent(inst.entity)
                end
            end

            inst.sg:SetTimeout(FRAMES * 38)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("spawn", inst.sg.statemem.spawns)
        end
    },

    State{
        name = "spawn",
        tags = {"busy", "spawning"},

        onenter = function(inst, data)
            TheWorld.components.lavaarenalivemobtracker:SetPortal(inst:WhichPortalAmI())
            if not (type(data.prefabtable) ~= "table" or #data.prefabtable == 0) then
                local spawnedmobs = {}
                local emptyslots = {}
                for k, v in pairs(data.prefabtable) do
                    if v == "empty_slot" then
                        table.insert(spawnedmobs, v) --added to pad spawn positions
                        table.insert(emptyslots, v)
                    else
                        local mob = SpawnPrefab(v)
                        if TheWorld.components.stat_tracker then
                            TheWorld.components.stat_tracker:TrackMobStats(mob)
                        end
                        table.insert(spawnedmobs, mob)
                    end
                    
                end
                if #spawnedmobs == 1 then
                    spawnedmobs[1].Transform:SetPosition(inst.Transform:GetWorldPosition())
                    SpawnPrefab("lavaarena_creature_teleport_small_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
                else
                    local angleoffset = inst.Transform:GetRotation()*DEGREES
                    if #spawnedmobs == 3 then
                        angleoffset = angleoffset + 33*DEGREES
                    end
                    local distfromcenter = 2
                    if #spawnedmobs >= 3 then
                        distfromcenter = 1.5
                    end
                    local pt = inst:GetPosition()
                    for i = 1, #spawnedmobs do 
                        if spawnedmobs[i] and spawnedmobs[i] ~= "empty_slot" then --Leo: I added this check because I used nil to force spawn crocos in the middle right now.
                            --if spawned mobs have 5 slots, make offset work for 4, to give the + formation.
                            local offset = FindWalkableOffset(pt, (((i/(#spawnedmobs >= 5 and (#spawnedmobs - #emptyslots)  or #spawnedmobs)) * 2 * PI)+angleoffset), distfromcenter, 2, true, true)
                            if offset ~= nil then
                                offset.x = offset.x + pt.x
                                offset.z = offset.z + pt.z
                                if i == 5 then --5th mob will always spawn in the center. Even if padded with empty_slots
                                    spawnedmobs[i].Transform:SetPosition(inst.Transform:GetWorldPosition())
                                else
                                    spawnedmobs[i].Transform:SetPosition(offset.x, 0, offset.z)
                                end
                                if spawnedmobs[i].components.combat ~= nil then
                                    spawnedmobs[i].components.combat:BattleCry()
                                end
                            else
                                spawnedmobs[i]:Remove()
                            end                 
                        end
                    end
                    SpawnPrefab("lavaarena_creature_teleport_medium_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
                end
                inst.sg:SetTimeout(FRAMES * 30)
            end
            TheWorld.components.lavaarenaevent:SpawnFinishedForPortal()
            if type(data.prefabtable) ~= "table" or #data.prefabtable == 0 then
                inst.sg:GoToState("idle")
            end
        end,

        ontimeout = function(inst)
            local postfxstate = GetRandomItem(inst.postfxstates)
            inst.sg:GoToState(postfxstate)
        end
    },

    State{
        name = "doublefizzle",
        tags = {"busy", "postfx"},

        onenter = function(inst)
            for i = 0, NUM_DECOR-1 do
                SpawnDecorFX(inst, i)
            end
            inst.sg:SetTimeout(5)
        end,

        timeline = {
            TimeEvent(22 * FRAMES, function(inst)
                for i = 0, NUM_DECOR-1 do
                    SpawnDecorFX(inst, i)
                end
            end),
        },

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end
    },

    State{
        name = "convergetothemiddle",
        tags = {"busy", "postfx"},

        onenter = function(inst)
            SpawnDecorFX(inst, 0)
            SpawnDecorFX(inst, NUM_DECOR-1)
            inst.sg:SetTimeout(5)
        end,

        timeline = {
            TimeEvent(8 * FRAMES, function(inst)
                SpawnDecorFX(inst, 1)
                SpawnDecorFX(inst, NUM_DECOR-2)
            end),
            TimeEvent(19 * FRAMES, function(inst)
                SpawnDecorFX(inst, 2)
                SpawnDecorFX(inst, NUM_DECOR-3)
            end),
            TimeEvent(28 * FRAMES, function(inst)
                SpawnDecorFX(inst, 1)
                SpawnDecorFX(inst, NUM_DECOR-2)
            end),
            TimeEvent(39 * FRAMES, function(inst)
                SpawnDecorFX(inst, 0)
                SpawnDecorFX(inst, NUM_DECOR-1)
            end),
        },

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end
    },

    State{
        name = "righttoleftandback",
        tags = {"busy", "postfx"},

        onenter = function(inst)
            SpawnDecorFX(inst, 0)
            SpawnDecorFX(inst, 1)
            inst.sg:SetTimeout(5)
        end,

        timeline = {
            TimeEvent(4 * FRAMES, function(inst)
                SpawnDecorFX(inst, 2)
            end),
            TimeEvent(7 * FRAMES, function(inst)
                SpawnDecorFX(inst, 3)
            end),
            TimeEvent(11 * FRAMES, function(inst)
                SpawnDecorFX(inst, 4)
            end),
            TimeEvent(14 * FRAMES, function(inst)
                SpawnDecorFX(inst, 5)
            end),
            TimeEvent(20 * FRAMES, function(inst)
                SpawnDecorFX(inst, 4)
            end),
            TimeEvent(25 * FRAMES, function(inst)
                SpawnDecorFX(inst, 3)
            end),
            TimeEvent(28 * FRAMES, function(inst)
                SpawnDecorFX(inst, 2)
            end),
            TimeEvent(32 * FRAMES, function(inst)
                SpawnDecorFX(inst, 1)
            end),
            TimeEvent(34 * FRAMES, function(inst)
                SpawnDecorFX(inst, 0)
            end),
        },

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end
    },

    State{
        name = "randomfizzle",
        tags = {"busy", "postfx"},

        onenter = function(inst)
            SpawnDecorFX(inst, 3)
            inst.sg:SetTimeout(5)
        end,

        timeline = {
            TimeEvent(3 * FRAMES, function(inst)
                SpawnDecorFX(inst, 5)
            end),
            TimeEvent(5 * FRAMES, function(inst)
                SpawnDecorFX(inst, 2)
            end),
            TimeEvent(10 * FRAMES, function(inst)
                SpawnDecorFX(inst, 4)
            end),
            TimeEvent(12 * FRAMES, function(inst)
                SpawnDecorFX(inst, 0)
            end),
            TimeEvent(17 * FRAMES, function(inst)
                SpawnDecorFX(inst, 1)
            end),
            TimeEvent(19 * FRAMES, function(inst)
                SpawnDecorFX(inst, 3)
            end),
            TimeEvent(24 * FRAMES, function(inst)
                SpawnDecorFX(inst, 5)
            end),
            TimeEvent(26 * FRAMES, function(inst)
                SpawnDecorFX(inst, 2)
            end),
            TimeEvent(31 * FRAMES, function(inst)
                SpawnDecorFX(inst, 4)
            end),
            TimeEvent(33 * FRAMES, function(inst)
                SpawnDecorFX(inst, 0)
            end),
            TimeEvent(38 * FRAMES, function(inst)
                SpawnDecorFX(inst, 1)
            end),
        },

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end
    },
}

return StateGraph("lavaarenaspawner", states, events, "idle")