--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("stategraphs/commonstates")

local actionhandlers = 
{ 
	
	--ActionHandler(ACTIONS.FAN, "action"), 
    ActionHandler(ACTIONS.GOHOME, "gohome"),
	ActionHandler(ACTIONS.ATTACK, function(inst)
		local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
		if hands ~= nil and hands:HasTag("monsterwpn")  then
			return "attack2"
		else
			return "attack"
		end	
	end),
}

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data)			
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + 1 < GetTime())  then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
    --EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
	EventHandler("doattack", function(inst, data)
        if not inst.components.health:IsDead() and (inst.sg:HasStateTag("canattack") or not inst.sg:HasStateTag("busy")) and data then -- TODO canattack tag
            if inst.components.combat.target and not (inst.weapon and inst.weapon.components.equippable:IsEquipped()) then
                inst.sg:GoToState("attack", data.target)
            else
                inst.sg:GoToState("spit", data.target)
            end
        end
    end),
    CommonHandlers.OnSleep(),
    EventHandler("locomote", function(inst)
        local is_moving = inst.sg:HasStateTag("moving")
        local is_running = inst.sg:HasStateTag("running")
        local is_idling = inst.sg:HasStateTag("idle")

        local should_move = inst.components.locomotor:WantsToMoveForward()
        local should_run = inst.components.locomotor:WantsToRun()

        if is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif (is_idling and should_move) then
			-- Do not run if currently in ranged (spit) mode unless an attack is ready.
            if (inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) and not inst.components.combat:InCooldown() and inst.components.combat.target) or not inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
				inst.sg:GoToState("run_start")
            end
        end
    end),

	EventHandler("knockback", function(inst, data) if not inst.components.health:IsDead() then inst.sg:GoToState("knockback", data) end end),
    CommonHandlers.OnFreeze(),
    CommonHandlers.OnFossilize(),
}

local Banners = {"lavaarena_battlestandard_damager", "lavaarena_battlestandard_heal", "lavaarena_battlestandard_shield"}

local function SpawnBanner(inst)
	local pos = inst:GetPosition()
	local offset = math.random(-2, 2)
	local banner = SpawnPrefab(inst.mybanner)
	banner.Transform:SetPosition(pos.x + offset, 0, pos.z + offset)
	inst.components.entitytracker:TrackEntity("mybanner", banner)
	if TheWorld.components.stat_tracker then
		TheWorld.components.stat_tracker:TrackMobStats(banner)
	end
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
	local leader = inst.components.follower.leader or nil
	if player then
		if target ~= nil and inst:IsNear(target, 20) and not inst.components.follower.leader then
			return target
		elseif leader and leader.components.combat and leader.components.combat.target then
			return leader.components.combat.target
		else
			return distsq ~= nil and distsq < 225 and not player:HasTag("notarget") and player --Leo: Removed the LA_mob check because we should always want them to attack players in forge.
		end
	else
		return nil
	end
end

 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --bite
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack")
        if inst.components.combat.target ~= nil then
            if inst.components.combat.target:IsValid() then
                inst:FacePoint(inst.components.combat.target:GetPosition())
            end
        end
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {

			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/attack") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/attack") 
			end),
            TimeEvent(10*FRAMES, function(inst) inst.components.combat:DoAttack() end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
        name = "spit", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)		
		if inst.weapon and inst.components.inventory then
            inst.components.inventory:Equip(inst.weapon)
			inst.components.combat:SetAttackPeriod(inst.weapon.attackperiod)
        end
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("spit")
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/spit")
        if inst.components.combat.target and inst.components.combat.target:IsValid() then
            inst:FacePoint(inst.components.combat.target:GetPosition())
        end
	end,
	
	onexit = function(inst)
        
    end,
	
    timeline=
    {

			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/spit2")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/hurt") 
			end),
            TimeEvent(10*FRAMES, function(inst) 
				inst.components.combat:DoAttack()
				inst.altattack = false				
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
		name = "stun",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			inst.sg.statemem.stimuli = data.stimuli or nil
			if data.stimuli and data.stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(TUNING.FORGE.STUN_TIME)
				inst.components.combat:SetTarget(data.attacker and data.attacker or nil)
				inst.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_STUN
			else
				inst.lasttarget = nil --should allow previous target to be targeted again if hes closest.
				inst.components.combat:SetTarget(nil)
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/hit")
			end
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/stun") end),
        },
		
		onexit = function(inst)
            
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,
		
		

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli and stimuli == "explosive" and (not inst.last_taunt_time or (inst.last_taunt_time + TUNING.FORGE.TAUNT_CD < GetTime())) then
				inst.last_taunt_time = GetTime()
				inst.sg.statemem.needstotaunt = true
			end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.sg.statemem.needstotaunt then
					inst.sg.statemem.needstotaunt = nil
					inst.sg:GoToState("taunt")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit", "nofreeze"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/hit")
			inst.sg.mem.last_hit_time = GetTime()
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/hurt") 
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "taunt",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/taunt")	end),
        },

        events=
        {
			EventHandler("animover", function(inst)
				if inst.components.combat.target ~= nil and inst.components.entitytracker:GetEntity("mybanner") == nil and inst.wantstobanner and inst.wantstobanner == true and not (inst.neverbanner and inst.neverbanner == true) and not inst:HasTag("brainwashed") then
					inst.sg:GoToState("build")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	
	State{
		name = "build",
        tags = {"busy"},

        onenter = function(inst, desired_banner, allbanners) --other params not used yet.
			inst.wantstobanner = false
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("banner_summon")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/taunt_2")
        end,

		timeline=
        {
			TimeEvent(20*FRAMES, PlayFootstep),
			TimeEvent(20*FRAMES, SpawnBanner)
        },

		onexit = function(inst)
			inst:DoTaskInTime(TUNING.FORGE.CROCOMMANDER.BANNER_CD, function(inst) inst.wantstobanner = true end)
		end,
		
        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("run_pst")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/death")
			inst.components.armorbreak_debuff:RemoveDebuff()
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/death") 
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then

                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            --Leo: Temp fix for perma sleeping bug:
			EventHandler("attacked", function(inst) inst.sg:GoToState("wake") end ),
			
			---
			EventHandler("animover", function(inst) 
				if inst.components.sleeper.sleepiness <= 0 then
					inst.sg:GoToState("wake") 
				else
					inst.sg:GoToState("sleeping")
				end
			end),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.components.combat:SetTarget(AttemptNewTarget(inst, inst.components.combat.target)) inst.sg:GoToState("idle") end),
        },
    },

	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, PlayFootstep ),
			TimeEvent(10*FRAMES, PlayFootstep ),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
        name = "knockback",
        tags = { "busy", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation("stun_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snapper/hit")
			--inst.AnimState:SetTime(10* FRAMES)

            if data ~= nil and data.radius ~= nil and data.knocker ~= nil and data.knocker:IsValid() then
                local x, y, z = data.knocker.Transform:GetWorldPosition()
                local distsq = inst:GetDistanceSqToPoint(x, y, z)
                local rangesq = data.radius * data.radius
                local rot = inst.Transform:GetRotation()
                local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                local drot = math.abs(rot - rot1)
                while drot > 180 do
                    drot = math.abs(drot - 360)
                end
                local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                inst.sg.statemem.speed = (data.strengthmult or 1) * 12 * k
                inst.sg.statemem.dspeed = 0
                if drot > 90 then
                    inst.sg.statemem.reverse = true
                    inst.Transform:SetRotation(rot1 + 180)
                    inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                else
                    inst.Transform:SetRotation(rot1)
                    inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                end
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.dspeed
                if inst.sg.statemem.speed < 0 then
                    inst.sg.statemem.dspeed = inst.sg.statemem.dspeed + .075
                    inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
                else
					inst.AnimState:PlayAnimation("stun_pst")
                    inst.sg.statemem.speed = nil
                    inst.sg.statemem.dspeed = nil
                    inst.Physics:Stop()
                end
            end
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                --inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.Physics:Stop()
            end
        end,
    },
	
}
    
CommonStates.AddFossilizedStates(states,
{
	fossilizedtimeline =
	{
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_pre_1") end)
	},
	
    unfossilizingtimeline = 
    { 
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_shake_LP", "shakeloop") end),
    },
	
	unfossilizedtimeline = 
    { 
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end)
    },
}, 
{
	unfossilizing_onexit = function(inst) inst.SoundEmitter:KillSound("shakeloop") end
}
)

return StateGraph("crocommander", states, events, "idle", actionhandlers)

