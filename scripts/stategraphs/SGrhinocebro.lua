require("stategraphs/commonstates")

--Notes and ToDo and whatnot:
--Make the ram start through running. This includes both run_start and run states. Perhaps make a locomotor event to handle this instead.
--On death, boss enemies obtain the obstacle physics. This needs to be done on boarrior too.
--Ram does an AoE infront of itself, its pretty large. Probably offset by 2-3 with a range of 3-4.
--Cheers might need a slight rework, they definetly need to have a timeout for failed buffs (estimated 30 seconds).
--Chestbump kills always list snapback as the killer.


local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .2, .01, .1, inst, 8)
end

local function FootShake(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .2, .01, .1, inst, 8)
end

local function ShakePound(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
    ShakeAllCameras(CAMERASHAKE.FULL, 1.2, .03, .7, inst, 30)
end

local function ShakeRoar(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 0.8, .03, .5, inst, 30)
end

local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, "action"),
	ActionHandler(ACTIONS.ATTACK, "attack"),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "action_long"),
	
}

local REV_RANGE = 25
local BUFF_RANGE = 6

--Leo: Might have this run in the brain instead. We'll see.
local function GetBros(inst, range)
	local pos = inst:GetPosition()
	local bros = {}
	local ents = TheSim:FindEntities(pos.x,0,pos.z, range and range or 6, {"rhinobro"}, {"playerghost", "ghost", "INLIMBO", "notarget" })
	for i, v in ipairs(ents) do
		if v and v.components.health and not v.components.health:IsDead() and v ~= inst then
			table.insert(bros, v)
		end
	end
	
	return bros
end

local function GetNearbyTargets(inst)
	local pos = inst:GetPosition()
	local targets = {}
	local ents = TheSim:FindEntities(pos.x, 0, pos.z, TUNING.FORGE.RHINOCEBRO.AOE_HIT_RANGE, { "player"}, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or { "LA_mob", "fossil", "shadow", "playerghost", "INLIMBO" })
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst.components.combat.target then
			table.insert(targets, ent)
		end
	end
	return #targets
end

local function GetCD(inst)
	local health = inst.components.health:GetPercent()
	
	if health <= 0.2 then
		return 8
	else
		return 20
	end
end

local function GetExcludeTags(inst)
	if inst:HasTag("brainwashed") then
		return {"player", "companion", "brainwashed", "rhinocebro", "notarget", "battlestandard", "INLIMBO"}
	else	
		return {"LA_mob", "rhinocebro", "notarget", "battlestandard", "INLIMBO"}
	end
end

local function DoChestBump(inst)
local posx, posy, posz = inst.Transform:GetWorldPosition()
local angle = -inst.Transform:GetRotation() * DEGREES
local offset = 1.5
local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, TUNING.FORGE.RHINOCEBRO.AOE_HIT_RANGE, { "locomotor"}, { "LA_mob", "fossil", "shadow", "playerghost", "INLIMBO" })
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst.components.combat.target then
			inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, TUNING.FORGE.RHINOCEBRO.DAMAGE + (25*inst.brostacks))
		end
	end
end

local function DoFrontAoe(inst)
local posx, posy, posz = inst.Transform:GetWorldPosition()
local angle = -inst.Transform:GetRotation() * DEGREES
local offset = 3
local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, TUNING.FORGE.RHINOCEBRO.AOE_HIT_RANGE, { "locomotor"}, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or { "LA_mob", "fossil", "shadow", "playerghost", "INLIMBO" })
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst.components.combat.target then
			inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, TUNING.FORGE.RHINOCEBRO.DAMAGE + (25*inst.brostacks))
		end
	end
end

local function DoChargeAoE(inst)
	if inst.sg:HasStateTag("charging") then
		local pos = inst:GetPosition()
		local angle = -inst.Transform:GetRotation() * DEGREES
		local offset = 2
		local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
		local chargedents = {}
		--Leo: ToDo make the aoe infront of the mob when charging.
		local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, 3, nil, GetExcludeTags(inst))
		if ents and #ents > 0 then
			for i, v in ipairs(ents) do
				--Leo: the chargehit thing is temporary. I know the solution its just not coming to me cause late night coding. No really, I'm not this dumb. I swear!
				if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and not (inst.prefab == "rhinocebro" and v.ischargehit) and not (inst.prefab == "rhinocebro2" and v.ischargehit2) then
					if inst.components.combat.target == v then
						inst._hashittarget = true
					end
					v.components.combat:GetAttacked(inst, TUNING.FORGE.RHINOCEBRO.DAMAGE + (25*inst.brostacks))
					--also need to change this. This prevents the other bro from hitting with rams.
					if inst.prefab == "rhinocebro" then
						v.ischargehit = true
						v:DoTaskInTime(1, function(inst) inst.ischargehit = nil end)
					else
						v.ischargehit2 = true
						v:DoTaskInTime(1, function(inst) inst.ischargehit2 = nil end)
					end
					
				end
			end
		end	
    --inst.components.combat:DoAreaAttack(inst, TUNING.FORGE.SNORTOISE.SPIN_HIT_RANGE, nil, nil, nil, { "INLIMBO", "notarget", "playerghost", "battlestandard", "LA_mob" })
	end
end

local function SetDashPhysics(inst)
	inst.Transform:SetEightFaced()
    ToggleOffCharacterCollisions(inst)
end

local function SetNormalPhysics(inst)
	inst.Transform:SetSixFaced()
    ToggleOnCharacterCollisions(inst)
end

-----------------------------------------------------
--Cheering

local function DoCheer(inst, bros)
	if bros then
		inst.bros = bros --to connect each other
		inst.sg:GoToState("cheer_pre")
		for i, v in ipairs(bros) do
			if v.sg then
				v.sg:GoToState("cheer_pre")
			end
		end	
	end
end

local function DoBuff(inst)
	inst.brostacks = inst.brostacks + 1
	--Leo: ToDO just use the ingame rhinobuff instead and just fill the gaps.
	print("Buff succedded, stack is now "..inst.brostacks)
	inst.components.debuffable:AddDebuff("rhinocebro_buff", "rhinocebro_buff")
end
-----------------------------------------------------

local events=
{
	EventHandler("startcheer", function(inst)
		if not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("frozen") and not inst.sg:HasStateTag("busy") and not TheWorld.components.lavaarenaevent.lost then
			inst.sg:GoToState("cheer_pre")
		end
	end),
	EventHandler("victorypose", function(inst)
		if not inst.sg:HasStateTag("busy") then
			inst.sg:GoToState("pose")
		end
	end),
	EventHandler("cheercanceled", function(inst, bro)
		inst.cheer_loop = 0
		if inst.sg:HasStateTag("cheering") then
			inst.sg:GoToState("cheer_pst")	
		else
			--this is for when one of them get cancelled before the other starts the cheer state.
			inst.cancheer = false
			if inst.cheertask then
				inst.cheertask:Cancel()
				inst.cheertask = nil
			end	
			inst.cheertask = inst:DoTaskInTime(TUNING.FORGE.RHINOCEBRO.CHEER_CD, function(inst) inst.cancheer = true end)
		end		
	end),
	EventHandler("respawnfromcorpse", function(inst, reviver) if inst.sg:HasStateTag("corpse") and reviver then inst.sg:GoToState("death_post", reviver) end end),
	EventHandler("chest_bump", function(inst, bro)
		inst.sg:GoToState("chest_bump", bro)
	end),
	EventHandler("attacked", function(inst, data) 		
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("hiding") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			if inst.sg:HasStateTag("cheering") then
				if inst.bro then inst.bro:PushEvent("cheercanceled") end
				inst:PushEvent("cheercanceled")
			end	
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			if inst.sg:HasStateTag("cheering") then
				if inst.bro then inst.bro:PushEvent("cheercanceled") end
				inst:PushEvent("cheercanceled")
			end	
			inst.sg:GoToState("stun", data)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + TUNING.FORGE.HIT_RECOVERY < GetTime()) then 
			inst.sg:GoToState("hit") 
		elseif inst.sg:HasStateTag("corpse") then
			--shouldn't be any need to make a new state for this.
			inst.AnimState:PlayAnimation("death_hit", false)
		end 
	end),
    EventHandler("death", function(inst, data)
        if inst.bro and inst.bro.components.health and inst.bro.components.health:IsDead() then
			--our bro is dead, its over...
			inst.sg:GoToState("corpse", true)
        else
			if inst.bro and inst.bro.sg:HasStateTag("cheering") then
				inst.bro:PushEvent("cheercanceled")
				inst.PushEvent("cheercanceled")
			end
			inst.sg:GoToState("corpse")		
        end
    end),
    EventHandler("doattack", function(inst, data) 
		if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("canattack")) then 
			--if inst.altattack and inst.altattack == true and not inst:IsNear(inst.components.combat.target, TUNING.FORGE.RHINOCEBRO.ATTACK_RANGE) then
				--inst.sg:GoToState("attack2", data.target) 
			--else
				inst.sg:GoToState("attack", data.target) 
			--end
		end 
	end),
	EventHandler("knockback", function(inst, data) if not inst.components.health:IsDead() and not (inst.sg:HasStateTag("hiding") or inst.sg:HasStateTag("charging")) and (data.knocker and data.knocker.epicknockback) then inst.sg:GoToState("knockback", data) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
	CommonHandlers.OnFossilize(),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
			inst.Transform:SetSixFaced()
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },
	
    State{
        name = "attack", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
        local target = inst.components.combat.target or nil
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:SetTarget(target)
		
		inst.Transform:SetSixFaced()
        
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/attack_2")
        inst.AnimState:PlayAnimation("attack")
	end,
	
	onexit = function(inst)
		inst.normalattack = nil
    end,
	
    timeline=
    {
			--TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/swipe") end),
            TimeEvent(13*FRAMES, function(inst) 
				--inst:PerformBufferedAction()
				inst.components.combat:DoAttack(inst.components.combat.target)
				DoFrontAoe(inst)
				--inst.components.combat:DoAreaAttack(inst.components.combat.target and inst.components.combat.target or inst, 3.5, nil, nil, nil, GetExcludeTags(inst)) 
			end),
    },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
        name = "attack2", --ram
        tags = {"busy", "charging"},
		
		onenter = function(inst, target)
		inst._hashittarget = nil
		inst.components.locomotor:RunForward()
		if inst.brain ~= nil then
			inst.brain:Stop()
		end
		
		if inst.components.combat.target and not inst._hashittarget then
            if inst.components.combat.target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = inst.components.combat.target
            end
		else
			--we hit our target, time to stop
			if inst.rams < (TUNING.FORGE.RHINOCEBRO.MAXRAMS) then
				inst.rams = (TUNING.FORGE.RHINOCEBRO.MAXRAMS)
			end
        end
		
		SetDashPhysics(inst)
		inst.altattack = false
        --inst.components.combat:StartAttack()
        
		inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/attack")
        inst.AnimState:PlayAnimation("attack2_pre")
        
		
		if inst._chargetask == nil then
			inst._chargetask = inst:DoPeriodicTask(0.15, DoChargeAoE)
		end
		--have no idea why I can't move in this state.
		inst:DoTaskInTime(0, function(inst) inst.Physics:SetMotorVelOverride(inst.components.locomotor.runspeed * 1.15, 0, 0) end)
	end,
	
	onexit = function(inst)
		inst:DoTaskInTime(TUNING.FORGE.RHINOCEBRO.ATTACK2_CD, function(inst) inst.altattack = true end)
		--inst.components.combat:SetRange(TUNING.FORGE.RHINOCEBRO.ATTACK_RANGE, TUNING.FORGE.RHINOCEBRO.HIT_RANGE)
		SetNormalPhysics(inst)
		if inst.brain ~= nil then
			inst.brain:Start()
		end
    end,
	
    timeline=
    {
		TimeEvent(9*FRAMES, PlayFootstep),
		TimeEvent(9*FRAMES, FootShake),
		TimeEvent(18*FRAMES, PlayFootstep),
		TimeEvent(18*FRAMES, FootShake),
    },

        events=
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("attack2_loop", inst.sg.statemem.attacktarget)  
			end),
        },
	
    },
	
	State{
        name = "attack2_loop", --ram
        tags = {"attack", "busy", "charging"},
		
		onenter = function(inst, target)
		if inst.brain ~= nil then
			inst.brain:Stop()
		end
		if inst.components.combat.target and not inst._hashittarget then
            if inst.components.combat.target:IsValid() then
                --inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = inst.components.combat.target
            end
		else
			--we hit our target, time to stop
			if inst.rams < (TUNING.FORGE.RHINOCEBRO.MAXRAMS) then
				inst.rams = (TUNING.FORGE.RHINOCEBRO.MAXRAMS)
			end
        end
		
		SetDashPhysics(inst)
		inst.Transform:SetEightFaced()
		inst.altattack = false
		inst.rams = inst.rams + 1
		
		if inst._chargetask == nil then
			inst._chargetask = inst:DoPeriodicTask(0.15, DoChargeAoE)
		end
        
        --inst.components.combat:StartAttack()
        --inst.components.locomotor:Stop()
        --inst.Physics:Stop()
		inst.Physics:SetMotorVelOverride(inst.components.locomotor.runspeed*1.15, 0, 0)
		--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
        inst.AnimState:PlayAnimation("attack2_loop")
        
	end,
	
	onupdate = function(inst)
				-- if target is behind, slow down and turn around
				-- otherwise keep speeding up while trying to still face the target
				-- might wanna limit how frequently all this onupdate stuff happens
				
		if not inst.sg.statemem.charged_target then		
			local target = inst.components.combat.target
			if target == nil or target.components.health:IsDead() or inst._hashittarget then
				if not target or target.components.health:IsDead() then
					if inst.rams < 9 then
						inst.rams = 9
					end
				end
			else
				
			local angle = inst.Transform:GetRotation() - inst:GetAngleToPoint(target:GetPosition())
			if angle > 180 then 
				angle = 360 - angle 
			elseif angle < -180 then 
				angle = 360 + angle 
			end
				
			--if angle > 45 or angle < -45 then
				--inst.sg.statemem.speed = inst.sg.statemem.speed <= 0 and inst.sg.statemem.speed or inst.sg.statemem.speed - accel
			--else
				--inst.sg.statemem.speed = inst.sg.statemem.speed >= maxspeed and inst.sg.statemem.speed or inst.sg.statemem.speed + accel
			--end
				
			local rot = 0
			local maxrott = 4
				
			if angle >= maxrott then
				rot = maxrott
			elseif angle <= -maxrott then
				rot = -maxrott
			else
				rot = angle
			end
				
			inst.Transform:SetRotation(inst.Transform:GetRotation() - rot)
			end
		end	
		inst.Physics:SetMotorVelOverride(inst.components.locomotor.runspeed*1.15, 0, 0)
	end,
	
	onexit = function(inst)
		inst.components.combat:SetRange(TUNING.FORGE.RHINOCEBRO.ATTACK_RANGE, TUNING.FORGE.RHINOCEBRO.HIT_RANGE)
		--inst.components.combat:SetRange(3, 5)
		SetNormalPhysics(inst)
		--inst.sg.statemem.attacktarget = nil
		if inst._chargetask ~= nil then
			inst._chargetask:Cancel()
			inst._chargetask = nil
		end
		if inst.brain ~= nil then
			inst.brain:Start()
		end
    end,
	
    timeline=
    {
		TimeEvent(9*FRAMES, PlayFootstep),
		TimeEvent(9*FRAMES, FootShake),
		TimeEvent(18*FRAMES, PlayFootstep),
		TimeEvent(18*FRAMES, FootShake),
    },

        events=
        {
            EventHandler("animover", function(inst) 
				if inst.rams <= TUNING.FORGE.RHINOCEBRO.MAXRAMS then
					inst.sg:GoToState("attack2_loop")
				else
					inst.rams = 0
					if inst._chargetask ~= nil then
						inst._chargetask:Cancel()
						inst._chargetask = nil
					end
					inst.sg:GoToState("attack2_pst")  
				end	
			end),
			EventHandler("onareaattackother", function(inst, data) 
				if data.target == inst.sg.statemem.attacktarget then
					inst.rams = (TUNING.FORGE.RHINOCEBRO.MAXRAMS)
					inst.sg.statemem.charged_target = true
				end
			end),
        },
	
    },
	
	State{
        name = "attack2_pst", 
        tags = {"busy"},
		
		onenter = function(inst, target)
			inst.components.combat:SetRange(TUNING.FORGE.RHINOCEBRO.ATTACK_RANGE, TUNING.FORGE.RHINOCEBRO.HIT_RANGE)
			inst.Transform:SetSixFaced()
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("attack2_pst")
			
		end,
	
		timeline=
		{

		},

        events=
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("idle")  
			end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst)
			--just incase
			if inst.brain ~= nil then
				inst.brain:Start()
			end
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit")
			inst.rams = 0
			inst.cheer_loop = 0
        end,
		
		onexit = function(inst)
			if inst.brain ~= nil then
				inst.brain:Start()
			end		
		end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "pose",
        tags = {"busy", "posing" , "idle"},

        onenter = function(inst)
			inst.Physics:Stop()
			if inst.bro and inst.bro.components.health and not inst.bro.components.health:IsDead() then
				local pos = inst.bro:GetPosition()
				inst:ForceFacePoint(pos.x, 0, pos.z)
				local rotation = inst.Transform:GetRotation()
				--inst:ForceFacePoint(pos.x*-1, 0, pos.z*-1)
				inst.Transform:SetRotation(rotation - 180)
			end	
			inst.AnimState:PlayAnimation("pose_pre", false)
			inst.AnimState:PushAnimation("pose_loop", true)
        end,

		timeline=
        {
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/cheer")end),
			--TimeEvent(10*FRAMES, ShakeRoar),
        },

        events=
        {
			--EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "spawn",
        tags = {"busy", "canattack"},

        onenter = function(inst, force)			
            inst.Physics:Stop()
			inst.Transform:SetSixFaced()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/taunt") end),
			--TimeEvent(10*FRAMES, ShakeRoar),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "taunt",
        tags = {"busy"},

        onenter = function(inst, force)			
            inst.Physics:Stop()
			inst.Transform:SetSixFaced()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/taunt") end),
			--TimeEvent(10*FRAMES, ShakeRoar),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "cheer_pre",
        tags = {"busy", "cheering", "nosleep"},

        onenter = function(inst)
			inst.sg.mem.wants_to_cheer = nil
			if inst.bro and inst.bro.components.health and not inst.bro.components.health:IsDead() then
				local pos = inst.bro:GetPosition()
				inst:ForceFacePoint(pos.x, 0, pos.z)
				if inst.bro.sg:HasStateTag("sleeping") then
					inst.bro.sg.mem.wants_to_cheer = true
					inst.bro.sg:GoToState("wake")
				end
			end	
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("cheer_pre")
			inst.cancheer = false
        end,

		timeline=
        {
		
        },
		
		onexit = function(inst)
			if inst.cheertask then
				inst.cheertask:Cancel()
				inst.cheertask = nil
			end
			inst.cheertask = inst:DoTaskInTime(TUNING.FORGE.RHINOCEBRO.CHEER_CD, function(inst) inst.cancheer = true end)
		end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("cheer_loop") end),
        },
    },
	
	State{
		name = "cheer_loop",
        tags = {"busy", "cheering", "nosleep"},

        onenter = function(inst, cb)
			--keep cheering until your bro cheers with you or until he gets cancelled.
			if inst.bro and inst.bro.sg and inst.bro.sg:HasStateTag("cheering") then
				inst.cheer_loop = inst.cheer_loop + 1
			elseif inst.bro and inst:IsNear(inst.bro, 6) and inst.bro.cancheer and inst.bro.cancheer == true then
				if inst.bro.sg:HasStateTag("sleeping") then
					inst.bro.sg.mem.wants_to_cheer = true
					inst.bro.sg:GoToState("wake")
				end
				inst.bro:PushEvent("startcheer")
			end	
			if inst.cheer_loop == 2 and not inst.buffsuccess then
				inst.buffsuccess = true
				DoBuff(inst)
			end
            inst.Physics:Stop()		
			inst.AnimState:PlayAnimation("cheer_loop")
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/cheer")
        end,

		timeline=
        {
		
        },
		
		onexit = function(inst)

		end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.cheer_loop >= 4 then
					inst.buffsuccess = nil
					inst.sg:GoToState("cheer_pst")
				else
					inst.sg:GoToState("cheer_loop")
				end
			end),
			EventHandler("cheercanceled", function(inst) 
				inst.sg:GoToState("cheer_pst")
			end),
        },
    },
	
	State{
		name = "cheer_pst",
        tags = {"busy", "nosleep"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("cheer_post")
			inst.cheer_loop = 0
			inst.cancheer = false
			if inst.cheertask then
				inst.cheertask:Cancel()
				inst.cheertask = nil
			end
			inst.cheertask = inst:DoTaskInTime(TUNING.FORGE.RHINOCEBRO.CHEER_CD, function(inst) inst.cancheer = true end)
        end,

		timeline=
        {
		
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("run_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action_long",
        tags = {"doing", "busy"},

        onenter = function(inst)
			inst.Transform:SetSixFaced()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("revive_pre", false)
			inst.AnimState:PushAnimation("revive_loop", false)
			inst.AnimState:PushAnimation("revive_loop", false)
			inst.AnimState:PushAnimation("revive_pst", false)
			
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/revive_LP", "reviveLP")
        end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("reviveLP")
		end,
		
		timeline=
        {
			TimeEvent(7 * FRAMES, function(inst)
                inst:PerformBufferedAction()
				if inst.bro and inst.bro.sg and inst.bro.sg:HasStateTag("corpse") then
					inst.bro:PushEvent("respawnfromcorpse")
				end
            end),
			TimeEvent(25 * FRAMES, function(inst)
				--inst.SoundEmitter:KillSound("reviveLP")
            end),
        },

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "action_loop",
        tags = { "doing", "busy", "reviving" },

        onenter = function(inst, timeout)
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("revive_pre", false)
			inst.AnimState:PushAnimation("revive_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/revive_LP", "reviveLP")
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation("revive_pst")
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
			inst.SoundEmitter:KillSound("reviveLP")
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    },
	
	--We aren't really dead, but we need help from a bro! if bro doesn't come to save us then just die.
	State{
        name = "corpse",
        tags = {"busy", "nointerrupt"},

        onenter = function(inst, isdead)
			inst.cheer_loop = 0
			
			inst.Transform:SetSixFaced()
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/death")
            inst.AnimState:PlayAnimation("death")
			if isdead and isdead == true then
				if inst.bro and inst.bro.components.health and inst.bro.components.health:IsDead() then
					inst.bro:DoTaskInTime(1.25, function(inst) inst.sg:GoToState("death") end)
				end
				inst.sg.statemem.isdead = true
			end
            inst.Physics:Stop()
			inst:AddTag("NOCLICK")
			--ChangeToObstaclePhysics(inst)
            --RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
        end,
		
		timeline=
        {
			--TimeEvent(30 + (31*FRAMES), function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/death_final_final") end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
				
				inst.sg:AddStateTag("corpse") --added for death_hit animations
                if inst.AnimState:AnimDone() and (inst.sg.statemem.isdead or not inst.bro) then
					inst.sg:GoToState("death")
                end
            end),
        },

    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.Transform:SetSixFaced()
			inst.AnimState:PlayAnimation("death_finalfinal")
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/death_final_final")
            inst.Physics:Stop()
			ChangeToObstaclePhysics(inst)
            --RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
        end,
		
		timeline=
        {
		
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					if TheWorld.components.lavaarenaevent:GetCurrentRound() == #TheWorld.components.lavaarenaevent.rounds_data then
						inst.components.health.nofadeout = true
					else
						inst.components.health.nofadeout = nil
						inst:DoTaskInTime(5, ErodeAway)
					end
					inst:PushEvent(inst.deathevent)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			inst.Transform:SetSixFaced()
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(38*FRAMES, function(inst) 
				ShakeIfClose(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/bodyfall") 
			end),
		},

        events =
        {
            EventHandler("animover", function(inst) 
				inst.sg:GoToState("sleeping") 
			end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/sleep_in")
			inst.AnimState:PlayAnimation("sleep_loop")
		end,
			
		timeline=
        {
			TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/sleep_out") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("attacked", function(inst) inst.sg:GoToState("wake") end ),
			EventHandler("wantstocheer", function(inst) inst.sg:GoToState("wake") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking", "nosleep" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) 
				if inst.sg.mem.wants_to_cheer then
					--print("DEBUG: Waking up to cheer!")
					inst.sg:GoToState("cheer_pre")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			if inst.altattack == true and inst.components.combat.target and not (inst.bro and inst.bro.sg:HasStateTag("corpse")) and inst.cancheer == false then
				inst.sg:GoToState("attack2", inst.components.combat.target)
			else
				inst.components.locomotor:RunForward()
				inst.AnimState:PlayAnimation("run_pre")
			end
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
		
		onexit = function(inst)
			inst.Transform:SetSixFaced()
        end,
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.Transform:SetEightFaced()
			if inst.altattack == true and inst.components.combat.target and not (inst.bro and inst.bro.sg:HasStateTag("corpse")) and inst.cancheer == false then
				inst.sg:GoToState("attack2", inst.components.combat.target)
			else
				inst.components.locomotor:RunForward()
				inst.AnimState:PlayAnimation("run_loop")
			end
        end,
		
		timeline = {

			TimeEvent(9*FRAMES, PlayFootstep),
			TimeEvent(9*FRAMES, FootShake),
			TimeEvent(18*FRAMES, PlayFootstep),
			TimeEvent(18*FRAMES, FootShake),
		},
		
        events= {
			EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("run")  
			end),
		},
			
		onexit = function(inst)
			inst.Transform:SetSixFaced()
        end,	

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")           
        end,
		
		timeline = {

			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/grunt") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
		
		onexit = function(inst)
			inst.Transform:SetSixFaced()
        end,
    },
	
	State
    {
        name = "chest_bump",
        tags = { "busy", "nointerrupt" },

        onenter = function(inst, bro) 
			inst.Transform:SetEightFaced()
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("chest_bump")
			if inst.bro and not inst.bro.components.health:IsDead() then
				local pos = inst.bro:GetPosition()
				inst:ForceFacePoint(pos:Get())
			end
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/grunt")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,
		
		timeline = {

			TimeEvent(16*FRAMES, function(inst) if inst.prefab == "rhinocebro" then DoChestBump(inst) end end),
		},
		
		onexit = function(inst)
			inst.Transform:SetSixFaced()
		end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy", "stunned", "nofreeze"},

        onenter = function(inst, data)
			--just incase
			if inst.brain ~= nil then
				inst.brain:Start()
			end
            inst.Physics:Stop()
			inst.cheer_loop = 0
			inst.rams = 0
			inst.sg.statemem.stimuli = data.stimuli
			if data.stimuli and data.stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/grunt")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(1)
				inst.components.combat:SetTarget(data.attacker and data.attacker or nil)
				inst.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_STUN
			else
				inst.components.combat:SetTarget(nil)
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/grunt")
			end
			inst.sg.statemem.flash = 0
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit") end),
        },
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			inst.components.health:SetAbsorptionAmount(0)           
			--inst.components.bloomer:PopBloom("leap")
            --inst.components.colouradder:PopColour("leap")
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,
		
        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli and stimuli == "explosive" and (not inst.last_taunt_time or (inst.last_taunt_time + TUNING.FORGE.TAUNT_CD < GetTime())) then
				inst.last_taunt_time = GetTime()
				inst.sg.statemem.needstotaunt = true
			end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.sg.statemem.needstotaunt then
					inst.sg.statemem.needstotaunt = nil
					inst.sg:GoToState("taunt")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	
	State{
        name = "death_post",
        tags = { "busy", "nointerrupt"},

        onenter = function(inst)
			inst:RemoveTag("NOCLICK")
			--ChangeToCharacterPhysics(inst)
			inst.AnimState:PlayAnimation("death_post")
			inst.components.health:SetPercent(TUNING.FORGE.RHINOCEBRO.REV_PERCENT)
            inst.components.health:SetInvincible(true)
        end,

		timeline=
		{

		},

        events =
        {
            EventHandler("animqueueover", function(inst)
				if inst.bro and inst.bro.components.health and not (inst.bro.components.health:IsDead() or inst.bro.sg:HasStateTag("hit") or inst.bro.sg:HasStateTag("stun")) and GetNearbyTargets(inst) > 0 then
					inst:PushEvent("chest_bump", inst.bro)
					inst.bro:PushEvent("chest_bump", inst)
				else
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
        end,
    },
	
	State
    {
        name = "revivecorpse",

        onenter = function(inst)
            --inst.components.talker:Say(GetString(inst, "ANNOUNCE_REVIVING_CORPSE"))
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.sg:GoToState("action_loop", 1)
        end,
    },
	
	--Fossilized States--
	State
    {
        name = "fossilized",
        tags = { "busy", "fossilized", "caninterrupt" },

        onenter = function(inst, data)
			inst.cheer_loop = 0
            --ClearStatusAilments(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("fossilized")
			--inst.AnimState:PushAnimation("fossilized_loop", true)
            inst.components.fossilizable:OnFossilize(data ~= nil and data.duration*0.75 or nil, data ~= nil and data.doer or nil)
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_pre_2") end),
		},

        events =
        {
            EventHandler("fossilize", function(inst, data)
                inst.components.fossilizable:OnExtend(data ~= nil and data.duration*0.75 or nil, data ~= nil and data.doer or nil)
            end),
            EventHandler("unfossilize", function(inst)
                inst.sg.statemem.unfossilizing = true
                inst.sg:GoToState("unfossilizing")
            end),
        },

        onexit = function(inst)
            inst.components.fossilizable:OnUnfossilize()
            if not inst.sg.statemem.unfossilizing then
                --Interrupted
                inst.components.fossilizable:OnSpawnFX()
            end
        end,
    },
	
	State
    {
        name = "unfossilizing",
        tags = { "busy", "caninterrupt", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("fossilized_shake")
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_shake_LP", "shakeloop") end),
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.unfossilized = true
                    inst.sg:GoToState("unfossilized")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.unfossilized then
                --Interrupted
                inst.components.fossilizable:OnSpawnFX()
            end
            inst.SoundEmitter:KillSound("shakeloop")
        end,
    },
	
	State
    {
        name = "unfossilized",
        tags = { "busy", "caninterrupt", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("fossilized_pst_r")
			inst.AnimState:PushAnimation("fossilized_pst_l")
            inst.components.fossilizable:OnSpawnFX()
			if inst.brain ~= nil then
				inst.brain:Start()
			end
        end,

        timeline = 
		{ 
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end)
		},

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "knockback",
        tags = { "busy", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst:ClearBufferedAction()
			
			if inst.brain ~= nil then
				inst.brain:Stop()
			end

            inst.AnimState:PlayAnimation("stun_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve/forge2/rhino_drill/hit")
			--inst.AnimState:SetTime(10* FRAMES)

            if data ~= nil and data.radius ~= nil and data.knocker ~= nil and data.knocker:IsValid() then
                local x, y, z = data.knocker.Transform:GetWorldPosition()
                local distsq = inst:GetDistanceSqToPoint(x, y, z)
                local rangesq = data.radius/2 * data.radius/2
                local rot = inst.Transform:GetRotation()
                local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                local drot = math.abs(rot - rot1)
                while drot > 180 do
                    drot = math.abs(drot - 360)
                end
                local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                inst.sg.statemem.speed = (data.strengthmult or 1) * 12 * k
                inst.sg.statemem.dspeed = 0
                if drot > 90 then
                    inst.sg.statemem.reverse = true
                    inst.Transform:SetRotation(rot1 + 180)
                    inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                else
                    inst.Transform:SetRotation(rot1)
                    inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                end
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.dspeed
                if inst.sg.statemem.speed < 0 then
                    inst.sg.statemem.dspeed = inst.sg.statemem.dspeed + .075
                    inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
                else
					inst.AnimState:PlayAnimation("stun_pst")
                    inst.sg.statemem.speed = nil
                    inst.sg.statemem.dspeed = nil
                    inst.Physics:Stop()
                end
            end
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                --inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
			if inst.brain ~= nil then
				inst.brain:Start()
			end
            if inst.sg.statemem.speed ~= nil then
                inst.Physics:Stop()
            end
        end,
    },
	
}
--[[
CommonStates.AddFossilizedStates(states,
{
	fossilizedtimeline =
	{
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_pre_1") end)
	},
	
    unfossilizingtimeline = 
    { 
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_shake_LP", "shakeloop") end),
    },
	
	unfossilizedtimeline = 
    { 
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end)
    },
}, 
{

	unfossilizing_onexit = function(inst) inst.SoundEmitter:KillSound("shakeloop") end
	unfossilizing_onexit = function(inst) inst.SoundEmitter:KillSound("shakeloop") end

}
)
]]
--CommonStates.AddFrozenStates(states)



    
return StateGraph("rhinocebro", states, events, "spawn", actionhandlers)

