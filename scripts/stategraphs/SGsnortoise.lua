--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("stategraphs/commonstates")

local DURATION_MULT = 1.40
local FLIP_DURATION = 10

local function DoSpinningAoE(inst)
	if inst.sg:HasStateTag("spinning") then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x,0,pos.z, TUNING.FORGE.SNORTOISE.SPIN_HIT_RANGE, nil, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO", "notarget" } )
		if ents and #ents > 0 then
			for i, v in ipairs(ents) do
				if v:IsValid() and v.components.health and not v.components.health:IsDead() and not v.isspinhit then
					v.components.combat:GetAttacked(inst, TUNING.FORGE.SNORTOISE.DAMAGE)
					v.isspinhit = true
					v:DoTaskInTime(0.2, function(inst) inst.isspinhit = nil end)
				end
			end
		end	
    --inst.components.combat:DoAreaAttack(inst, TUNING.FORGE.SNORTOISE.SPIN_HIT_RANGE, nil, nil, nil, { "INLIMBO", "notarget", "playerghost", "battlestandard", "LA_mob" })
	end
end

local function EnableAltAttack(inst)
	inst.altattack = true
	inst.components.combat:SetRange(TUNING.FORGE.SNORTOISE.SPIN_ATTACK_RANGE, 0) -- attack from farther away
end

local function DisableAltAttack(inst)
	inst.altattack = false
	inst.components.combat:SetRange(TUNING.FORGE.SNORTOISE.ATTACK_RANGE, TUNING.FORGE.SNORTOISE.HIT_RANGE)
end

local function FindPlayerTarget(inst)
	if #AllPlayers > 0 then 
		local tar = AllPlayers[math.random(1,#AllPlayers)]
		if tar then
			return tar
		else
			return nil
		end	
	end
	--local player, distsq = inst:GetNearestPlayer()
    --return distsq ~= nil and distsq < 225 and not (player:HasTag("notarget") or player:HasTag("LA_mob")) and player
end

local function AttemptNewTarget(inst, target)
	local player, distsq = inst:GetNearestPlayer()
    if target ~= nil and inst:IsNear(target, 20) then
		return target
	else
		return FindPlayerTarget(inst)
	end
end



local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, "action"),
	ActionHandler(ACTIONS.ATTACK, "attack"),
}

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("hiding") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif ((data.weapon and data.weapon.prefab == "riledlucy" and data.stimuli and data.stimuli == "strong") or (inst.dartalt_hits and inst.dartalt_hits >= 3)) and inst.sg:HasStateTag("spinning") and not inst.sg:HasStateTag("nocancel") and not inst.components.health:IsDead() then
			inst.sg:GoToState("attack2_pst_forced")	
		elseif inst.sg:HasStateTag("flipped") then
            inst.sg:GoToState("flip_pst", data.attacker)
			if TheWorld and TheWorld.components.stat_tracker and data.attacker then
				TheWorld.components.stat_tracker:AdjustStat("ccbroken", data.attacker, 1)
			end
        --[[elseif data.weapon and data.weapon.prefab == "pithpike" and data.stimuli and data.stimuli == "explosive" and not inst.components.health:IsDead() then
            inst.sg:GoToState("flip_pre")]]
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data)			
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("hiding") and not inst.sg:HasStateTag("spinning") and not inst.sg:HasStateTag("flipped") and (inst.sg.mem.last_hit_time == nil or inst.sg.mem.last_hit_time + TUNING.FORGE.HIT_RECOVERY < GetTime()) then 
			inst.sg:GoToState("hit") 
		elseif not inst.components.health:IsDead() and inst.sg:HasStateTag("hiding") then
			inst.sg:GoToState("hide_hit")
		end 
	end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
	EventHandler("flipped", function(inst, data)
		--print("Flipping Snortoise")
		if not inst.components.health:IsDead() then
			inst.sg:GoToState("flip_pre")
			if TheWorld and TheWorld.components.stat_tracker and data.flipper then
				TheWorld.components.stat_tracker:AdjustStat("turtillusflips", data.flipper, 1)
			end
		end
	end),
    EventHandler("doattack", function(inst, data) 
		if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then 
			if inst.components.health:GetPercent() <= 0.3 and inst.altattack == true then
				inst.sg:GoToState("attack2", data.target)
			else
				inst.sg:GoToState("attack", data.target) 
			end 
		end
	end),
	--EventHandler("start_shellattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack2_pre", data.target) end end),
	EventHandler("entershield", function(inst) 
		if not inst.sg:HasStateTag("busy") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hide_pre") 
		end
	end),	
    EventHandler("exitshield", function(inst) 
		if not inst.components.health:IsDead() and not (inst.sg:HasStateTag("hit") or inst.sg:HasStateTag("stunned")) and not inst.sg:HasStateTag("fossilized") then
			inst.sg:GoToState("hide_pst") 
		end	
	end),
    --CommonHandlers.OnSleep(),
	EventHandler("gotosleep", function(inst) 
		if inst.sg:HasStateTag("spinning") then
			inst.sg.statemem.wants_to_sleep = true	
		else
			inst.sg:GoToState(inst.sg:HasStateTag("sleeping") and "sleeping" or "sleep")
		end 
	end),
	EventHandler("knockback", function(inst, data) if not inst.components.health:IsDead() and not (inst.sg:HasStateTag("flipped") or inst.sg:HasStateTag("hiding")) then inst.sg:GoToState("knockback", data) end end),
    CommonHandlers.OnLocomote(true,false),
	CommonHandlers.OnFossilize(),
    CommonHandlers.OnFreeze(),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --spikes
        tags = {"attack", "busy"},
		
		  onenter = function(inst, target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack1")
		local target = inst.components.combat.target
        if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
            end
        end
	end,
	
	onexit = function(inst)
       
    end,
	
    timeline=
    {

			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/attack1a") 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
			end), --spikes out
            TimeEvent(12*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, TUNING.FORGE.SNORTOISE.HIT_RANGE, nil, nil, nil, inst:HasTag("brainwashed") and {"INLIMBO", "playerghost", "notarget", "companion", "player", "brainwashed"} or {"shadow", "LA_mob", "playerghost", "notarget", "battlestandard"})
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack")
			end),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/attack1b") end), --spikes in
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle", "atk_pst")  end),
        },
	
    },
	
	State{
        name = "attack2", --shell attack
        tags = {"attack", "busy", "nofreeze", "spinning"},
		
		onenter = function(inst, target)
		inst.altattack = false
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack2_pre")
        inst.sg.statemem.attacktarget = inst.components.combat.target
		DisableAltAttack(inst)
		inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SPIN_CD, EnableAltAttack)
	end,
	
    timeline=
    {

			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
			TimeEvent(10*FRAMES, function(inst) inst.sg:AddStateTag("nointerrupt") end),

    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("attack2_loop", inst.sg.statemem.attacktarget)  end),
        },
	
    },
	
	State{
        name = "attack2_loop", 
        tags = {"attack", "busy", "nointerrupt", "spinning", "nobuff", "nofreeze"},
		--use ram attempts maybe.
		
		onenter = function(inst, target)
		inst.components.debuffable:RemoveDebuff("shield_buff", "shield_buff")
		inst.components.sleeper:SetResistance(0)
		--DisableAltAttack(inst)
		inst.ram_attempt = inst.ram_attempt + 1
		inst:SetShellPhysics()
		inst.components.health.absorb = 1
		--inst.components.combat.externaldamagetakenmultipliers:SetModifier("shell", 0.01)
		inst.components.combat.multiplier = 0.5
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack2_loop", true)
		inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/attack2_LP", "shell_loop")
		inst.components.locomotor:WalkForward()
		inst.sg.statemem.initpos = inst:GetPosition()
		inst.sg.statemem.spinstate = "init"
		inst.sg.statemem.spinspeed = 5
		inst.sg.statemem.attacktarget = inst.components.combat.target
		if inst.brain ~= nil then
			inst.brain:Stop()
		end
		if inst._spintask == nil then
			inst._spintask = inst:DoPeriodicTask(0.15, DoSpinningAoE)
		end
		
		inst.components.item_launcher:Enable(true)
		
		if inst.sg.statemem.attacktarget ~= nil then
			inst:FacePoint(inst.sg.statemem.attacktarget:GetPosition())
			inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
			local offset = inst.sg.statemem.attacktarget:GetPosition() - inst:GetPosition()
			if offset:Length() < 8 then
				offset = offset:GetNormalized()*8
			end
			inst.sg.statemem.targetpos = inst:GetPosition() + offset
		else 
			if inst._spintask ~= nil then
				inst._spintask:Cancel()
				inst._spintask = nil
			end
			inst.sg:GoToState("attack2_pst")
			inst.ram_attempt = 0
			--inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SPIN_CD, EnableAltAttack)
			-- no target, just cut the attack we're done here there's nothing to it it's the end it's all over it's all gone literally nothing we can do
		end
	end,
	
	onupdate = function(inst)
		if inst.sg.statemem.spinstate == "init" then
			if inst.sg.statemem.spinspeed < 7 then
				inst.sg.statemem.spinspeed =  inst.sg.statemem.spinspeed * 1.25
			else 
				inst.sg.statemem.spinstate = "full"
			end
			inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
		elseif inst.sg.statemem.spinstate == "full" then
			local initoffset = inst.sg.statemem.initpos - inst.sg.statemem.targetpos 
			local currentoffset = inst:GetPosition() - inst.sg.statemem.targetpos 
			-- check if we're past that
			if ((((initoffset.x >= 0) and (currentoffset.x <= 0)) or ((initoffset.x <= 0) and (currentoffset.x >= 0))) and
			(((initoffset.z >= 0) and (currentoffset.z <= 0)) or ((initoffset.z <= 0) and (currentoffset.z >= 0)))) or (inst.sg.statemem.spinspeed > 50) then
				inst.sg.statemem.spinstate = "slowing"
			else
				inst.sg.statemem.spinspeed = inst.sg.statemem.spinspeed * 1.1
				inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
			end
		elseif inst.sg.statemem.spinstate == "slowing" then
			if inst.sg.statemem.spinspeed > 7 then
				inst.sg.statemem.spinspeed = inst.sg.statemem.spinspeed / 1.2
			else
				inst.sg.statemem.spinstate = "stopping"
			end
			inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
		elseif inst.sg.statemem.spinstate == "stopping" then
			if inst.sg.statemem.spinspeed > 1 then
				inst.sg.statemem.spinspeed = inst.sg.statemem.spinspeed / 2
			else
				inst.sg.statemem.spinspeed = 0
				inst.sg.statemem.spinstate = "done"
			end
			inst.Physics:SetMotorVel(inst.sg.statemem.spinspeed, 0, 0)
		elseif inst.sg.statemem.spinstate == "done" then
			if inst.ram_attempt >= 10 or (inst.sg.statemem.wants_to_sleep and inst.sg.statemem.wants_to_sleep == true) then
				inst._spintask:Cancel()
				inst._spintask = nil
				inst.sg:GoToState("attack2_pst", inst.sg.statemem.wants_to_sleep or nil)
				--inst.components.combat.externaldamagetakenmultipliers:SetModifier("shell", 1)
				inst.ram_attempt = 0
				--inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SPIN_CD, EnableAltAttack)
				
			else
				inst.sg:GoToState("attack2_loop")
			end
		end
    end,
	
	onexit = function(inst)
		inst.components.sleeper:SetResistance(1)
		inst.SoundEmitter:KillSound("shell_loop")
		if inst._spintask ~= nil then
			inst._spintask:Cancel()
			inst._spintask = nil
		end
		inst.sg.statemem.shell = nil
		inst.sg.statemem.spinstate = nil
		inst.sg.statemem.spinspeed = nil
		inst.sg.statemem.targetpos = nil
		inst.sg.statemem.initpos = nil
		if not inst.sg.statemem.attack then
            inst.components.combat.externaldamagetakenmultipliers:SetModifier("shell", 1)
        end
		if inst.brain ~= nil then
			inst.brain:Start()
		end
		if inst.sg.statemem.attacktarget ~= nil then
			inst.components.combat:SetTarget(inst.sg.statemem.attacktarget)
			inst.sg.statemem.attacktarget = nil
		end
		inst:SetNormalPhysics()
		--inst.components.health.absorb = 0
		inst.components.combat.multiplier = 1		
		inst.components.locomotor.walkspeed = 0
		
		inst.components.item_launcher:Enable(false)
    end,
	
    timeline=
    {

			TimeEvent(3*FRAMES, function(inst) inst.sg.statemem.shell = true end),
    },
	
        events=
        {
            --EventHandler("animqueueover", function(inst)  end),
			EventHandler("attacked", function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_impact") end)
        },
	
    },
	
	State{
		name = "attack2_pst",
        tags = {"busy", "nobuff"},

        onenter = function(inst, wants_to_sleep)
			inst.sg.statemem.wants_to_sleep = wants_to_sleep
			inst.components.health:SetAbsorptionAmount(0)
            inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack2_pst") 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst")
			inst:PutBackOnGround()
			if inst._spintask ~= nil then
			inst._spintask:Cancel()
			inst._spintask = nil
			end
        end,

        events=
        {
			EventHandler("animover", function(inst)
				if inst.sg.statemem.wants_to_sleep and inst.sg.statemem.wants_to_sleep == true then
					inst.sg.statemem.wants_to_sleep = nil
					inst.components.sleeper:AddSleepiness(5,3)
					inst.sg:GoToState("idle")
				else
					inst.sg:GoToState("idle") 
				end
			end),
        },
    },
	
	State{
		name = "attack2_pst_forced",
        tags = {"busy", "nobuff"},

        onenter = function(inst, cb)
			inst.components.health:SetAbsorptionAmount(0)
            inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)
			
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("hide_hit")
            inst.AnimState:PushAnimation("hide_pst", false) 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_impact")
			inst:PutBackOnGround()
			if inst._spintask ~= nil then
			inst._spintask:Cancel()
			inst._spintask = nil
			end
        end,
		
		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
        },

        events=
        {
			EventHandler("animqueueover", function(inst)
				inst.sg:GoToState("idle") 
			end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit", "nofreeze"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit") 
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hit")
			inst.sg.mem.last_hit_time = GetTime()
			if inst.sg.statemem.attempttoshield then
				inst.sg.statemem.attempttoshield = nil
				inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)
			end
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "taunt",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
			inst.should_taunt = false
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/taunt")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
        },

        events=
        {
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle") 
			end),
        },
    },
	
	State{
		name = "hide_pre",
        tags = {"busy", "nosleep"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_pre")
			inst.sg.statemem.attempttoshield = true
        end,
		
		onexit = function(inst)
            
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre") 
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
			TimeEvent(5*FRAMES, function(inst) inst.sg:AddStateTag("nointerrupt") inst.components.health:SetAbsorptionAmount(1) end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hiding",
        tags = {"busy", "hiding"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.components.sleeper:SetResistance(9999)
			inst.sg.statemem.attempttoshield = false
            inst.AnimState:PlayAnimation("hide_idle")
			inst.components.debuffable:RemoveDebuff("shield_buff", "shield_buff")
            inst.components.health:SetAbsorptionAmount(1)
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pre") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "hide_pst",
        tags = {"busy", "nobuff", "nosleep"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_pst")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            --if not inst.defbuffed then --Leo: shouldn't be buffed when entering through the shielded state.
			inst.components.health:SetAbsorptionAmount(0) --(-(0.02*inst.components.armorbreak_debuff.debufflevel))
			--end
            inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)
			inst.components.sleeper:SetResistance(1)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	
	State{
		name = "hide_hit",
        tags = {"busy", "hiding"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hide_hit")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_impact")
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_dull")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("hiding") end),
        },
    },
	
	State{
		name = "flip_pre",
        tags = {"busy", "nosleep"},

        onenter = function(inst, cb)
			inst.components.health:SetAbsorptionAmount(0)
            inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)
			inst.components.sleeper:SetResistance(1)
            inst.Physics:Stop()
			if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("flip_pre")
        end,
		
		onexit = function(inst)
			inst:DoTaskInTime(FLIP_DURATION, function(inst) 
				if inst.sg:HasStateTag("flipped") then
					inst.sg:GoToState("flip_pst")
				end	
				--print("[SGsnortoise] flip time done")
			end)	
		end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_walk") 
			--inst.SoundEmitter:PlaySound("dontstarve/impacts/impact_mech_med_sharp")
			end),
        },

        events=
        {
			EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("flip") 
			end),
        },
    },
	
	State{
		name = "flip",
        tags = {"busy", "flipped"},

        onenter = function(inst, cb)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flip_loop")
        end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/grunt") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("flip") end),
        },
    },
	
	State{
		name = "flip_pst",
        tags = {"busy", "nosleep"},

        onenter = function(inst, attacker)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flip_pst")
			--print("[SGsnortoise] enter flip_pst")
			if attacker ~= nil then
				--print("DEBUG: Snortoise got his life back on track, and as a result, back on his feet")
				inst.AnimState:SetTime(30* FRAMES)
			end
        end,

		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_walk") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.sg.statemem.attacktarget ~= nil and inst.sg.statemem.attacktarget:IsValid() then
				local target = AttemptNewTarget(inst, inst.sg.statemem.attacktarget)
					inst.components.combat:SetTarget(target)  
				end
				inst.sg:GoToState("idle") 
			end),
        },
    },
	
	State{
		name = "flip_hit",
        tags = {"busy", "nosleep"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flip_hit")
        end,

		timeline=
        {
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy", "stunned", "nofreeze"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			inst.components.sleeper:SetResistance(1)
			inst.sg.statemem.stimuli = data.stimuli or nil
			if data.stimuli and data.stimuli == "electric" then
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(TUNING.FORGE.STUN_TIME)
				inst.components.combat:SetTarget(data.attacker and data.attacker or nil)
				inst.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_STUN
			else
				inst.AnimState:PlayAnimation("stun_loop")
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hit")
				inst.lasttarget = nil --should allow previous target to be targeted again if hes closest.
				inst.components.combat:SetTarget(nil)
			end
			
			inst.components.health:SetAbsorptionAmount(0)
        end,

		timeline=
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
        },
		
		onexit = function(inst)
		
			inst.components.health:SetInvincible(false)
			inst.components.health:SetAbsorptionAmount(0)
			inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)            
			--inst.components.bloomer:PopBloom("leap")
            --inst.components.colouradder:PopColour("leap")
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
			if stimuli and stimuli == "explosive" and (not inst.last_taunt_time or (inst.last_taunt_time + TUNING.FORGE.TAUNT_CD < GetTime())) then
				inst.last_taunt_time = GetTime()
				inst.sg.statemem.needstotaunt = true
			end
			inst.components.health:SetAbsorptionAmount(0)
            inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				inst.components.combat:SetTarget(AttemptNewTarget(inst, inst.components.combat.target)) 
				inst.sg:GoToState("idle") 
			end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.components.armorbreak_debuff:RemoveDebuff()
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
        end,

		timeline =
		{
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/death") end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then

                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			inst.components.health:SetAbsorptionAmount(0)
            inst.canshield = false
			inst:DoTaskInTime(TUNING.FORGE.SNORTOISE.SHEILD_CD, function(inst) inst.canshield = true end)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)

		end,

		timeline=
        {

        },

        events =
        {
            --Leo: Temp fix for perma sleeping bug:
			EventHandler("attacked", function(inst) inst.sg:GoToState("wake") end ),
			
			---
			EventHandler("animover", function(inst) 
				if inst.components.sleeper.sleepiness <= 0 then
					inst.sg:GoToState("wake") 
				else
					inst.sg:GoToState("sleeping")
				end
			end),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst)
			inst.components.combat:SetTarget(AttemptNewTarget(inst, inst.components.combat.target)) 
			inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_walk") 
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour")
			end),
			TimeEvent(0*FRAMES, PlayFootstep),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/shell_walk") 
			--inst.SoundEmitter:PlaySound("dontstarve/movement/foley/metalarmour")
			end),
			TimeEvent(12*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
        name = "knockback",
        tags = { "busy", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation("stun_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/snortoise/hit")
			--inst.AnimState:SetTime(10* FRAMES)

            if data ~= nil and data.radius ~= nil and data.knocker ~= nil and data.knocker:IsValid() then
                local x, y, z = data.knocker.Transform:GetWorldPosition()
                local distsq = inst:GetDistanceSqToPoint(x, y, z)
                local rangesq = data.radius/1.5 * data.radius/1.5
                local rot = inst.Transform:GetRotation()
                local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                local drot = math.abs(rot - rot1)
                while drot > 180 do
                    drot = math.abs(drot - 360)
                end
                local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                inst.sg.statemem.speed = (data.strengthmult or 1) * 12 * k
                inst.sg.statemem.dspeed = 0
                if drot > 90 then
                    inst.sg.statemem.reverse = true
                    inst.Transform:SetRotation(rot1 + 180)
                    inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                else
                    inst.Transform:SetRotation(rot1)
                    inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                end
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.dspeed
                if inst.sg.statemem.speed < 0 then
                    inst.sg.statemem.dspeed = inst.sg.statemem.dspeed + .075
                    inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
                else
					inst.AnimState:PlayAnimation("stun_pst")
                    inst.sg.statemem.speed = nil
                    inst.sg.statemem.dspeed = nil
                    inst.Physics:Stop()
                end
            end
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                --inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.Physics:Stop()
            end
        end,
    },
	
}
CommonStates.AddFossilizedStates(states,
{
	fossilizedtimeline =
	{
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_pre_1") end)
	},
	
    unfossilizingtimeline = 
    { 
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fossilized_shake_LP", "shakeloop") end),
    },
	
	unfossilizedtimeline = 
    { 
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/fossilized_break") end)
    },
}, 
{
	unfossilizing_onexit = function(inst) inst.SoundEmitter:KillSound("shakeloop") end
}
)
--CommonStates.AddFrozenStates(states)
    
return StateGraph("tortankp", states, events, "taunt", actionhandlers)

