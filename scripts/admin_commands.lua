--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local function SendCommand(fnstr)
	local x, _, z = TheSim:ProjectScreenPos(TheSim:GetPosition())
	local is_valid_time_to_use_remote = TheNet:GetIsClient() and TheNet:GetIsServerAdmin()
	if is_valid_time_to_use_remote then
		TheNet:SendRemoteExecute(fnstr, x, z)
	else
		ExecuteConsoleCommand(fnstr)
	end
end
--[[
local x,y,z = ThePlayer.Transform:GetWorldPosition() local ents = TheSim:FindEntities(x,y,z, 9001) for k,v in pairs(ents) do if (v.components and v.components.health and not v:HasTag("player") and not v:HasTag("companion")) then v.components.health:Kill() end end

local x,y,z = ThePlayer.Transform:GetWorldPosition() local ents = TheSim:FindEntities(x,y,z, 9001) for k,v in pairs(ents) do print("1") end

]]
-- All entities are spawned at the players feet
function SpawnEntity(prefab_name, amount, player)
	print("[AdminCommand] Spawning " .. tostring(amount) .. " " .. tostring(prefab_name))
	local fnstr = string.format("local amount = " .. amount .. " or 1 local player = UserToPlayer(\"" .. player.userid .. "\") or ThePlayer for i = 1, amount, 1 do SpawnPrefab(\"" .. prefab_name .. "\").Transform:SetPosition(player.Transform:GetWorldPosition()) end")
	
	SendCommand(fnstr)
end

-- Gives the BattleCry buff to the given player
function SpawnBattleCry(player)
	if player and player.battlecry_fx == nil then
		print("[AdminCommand] Spawning battlecry for " .. tostring(player.userid))
		local fnstr = string.format("local player = UserToPlayer(\"" .. player.userid .. "\") or ThePlayer player.battlecry_fx = SpawnPrefab(\"passive_battlecry_fx_self\") player.battlecry_fx:SetTarget(player, TUNING.FORGE.BATTLECRY.DAMAGE_MULTIPLIER_INCREASE, TUNING.FORGE.BATTLECRY.STORE_TIME)")
		
		SendCommand(fnstr)
	end
end

-- TODO: Check for item and not enemy?
function EquipItem(prefab_name, player)
	print("[AdminCommand] Equipping " .. tostring(prefab_name))
	local fnstr = "local player = UserToPlayer(\"" .. player.userid .. "\") or ThePlayer	local player_inv = player.components.inventory local item = SpawnPrefab(\"" .. prefab_name .. "\") local slot = item.components.equippable.equipslot local old_item = player_inv:GetEquippedItem(slot) player_inv:RemoveItem(old_item, true) if old_item then old_item:Remove() end player_inv:Equip(item)"
	
	SendCommand(fnstr)
end

-- TODO: Check for valid waves
function SelectRoundAndWave(round, wave)
	local round = round or 1
	local wave = wave or 1
	
	if TheWorld then
		print("[AdminCommand] Starting Round " .. tostring(round))
		SendCommand("TheWorld.components.lavaarenaevent:ForceStartRound(" .. round .. ", " .. wave .. ")")
	end
end

-- TODO: make ToggleWaves instead?
function DisableWaves()
	print("[AdminCommand] Forge is now disabled!")
	SendCommand("TheWorld.components.lavaarenaevent:Disable()")
end

function EnableWaves()
	print("[AdminCommand] Forge is now enabled!")
	SendCommand("TheWorld.components.lavaarenaevent:Enable()")
end

--[[
	Despawn everything on the ground and all enemies
	Reequip everyone with default equipment
	Give full health to everyone
	Reposition all players
	can I just use the reload?
--]]
function RestartForge()
	SendCommand("TheNet:SendWorldResetRequestToServer()")
end

--[[
	Despawn all enemies
	Despawn new items? is there a way to know what is from that wave?
	Dont need parameter for num since you are just restarting the current round
--]]
function RestartCurrentWave()
	local current_round = TheWorld.components.lavaarenaevent.current_round
	print("[AdminCommand] Restarting Round " .. tostring(current_round))
	SendCommand("TheWorld.components.lavaarenaevent:ForceRestartCurrentRound()")
end

function SuperGodMode(player)
	print("player: " .. tostring(player))
	print("- id: " .. tostring(player.userid))
	SendCommand("c_supergodmode(UserToPlayer(\"" .. player.userid .. "\"))")
end

function DebugSelect(player)
	SendCommand("c_select(UserToPlayer(\"" .. player.userid .. "\"))")
end

function ChangeCharacter(player)
	SendCommand("c_despawn(UserToPlayer(\"" .. player.userid .. "\"))")
end

function ResetToSave()
	SendCommand("c_reset()")
end

function KillAllMobs()
	print("[AdminCommand] Killing all mobs...")
	local fnstr = "local x,y,z = ThePlayer.Transform:GetWorldPosition() local ents = TheSim:FindEntities(x,y,z, 9001) for k,v in pairs(ents) do if v.components and v.components.health and not v:HasTag(\"player\") and not v:HasTag(\"companion\") then v.components.health:Kill() print(\"- \" .. tostring((v.nameoverride ~= nil and STRINGS.NAMES[string.upper(v.nameoverride)]) or v.name) .. \" killed\") end end"
	
	SendCommand(fnstr)
end

-- Spawns a petrification circle around the player
function SpawnCircle(self, player)
	print("[AdminCommand] Petrifying mobs near " .. tostring(player.name))
	local fnstr = "local player = UserToPlayer(\"" .. player.userid .. "\") for _, comp in ipairs({\"reticule_spawner\", \"fossilizer\"}) do if player.components[comp] == nil then player:AddComponent(comp) end player:DoTaskInTime(2.55, player.RemoveComponent, comp) end player.components.reticule_spawner:Setup(\"aoecctarget\", 3)	player.components.fossilizer:Fossilize(player:GetPosition())"
	
	SendCommand(fnstr)
end

-- toggles damage override on call
function Over9000(player)
	print("[AdminCommand] His power level is over 9000!!!!!!!!")
	local fnstr = "local player = UserToPlayer(\"" .. player.userid .. "\") if player.components.combat:HasDamageBuff(\"over_9000\") then player.components.combat:RemoveDamageBuff(\"over_9000\") print(\"REMOVE\") else	player.components.combat:AddDamageBuff(\"over_9000\", {buff = 9001, addtype = \"flat\"}) print(\"ADD\") end"
	
	SendCommand(fnstr)
end

-- Immediatley ends the current forge match in failure
function EndForge()
	print("[AdminCommand] Ending Forge and returning to lobby screen.")
	SendCommand("TheWorld.components.lavaarenaevent:End(false)")
end

-- Spawns a scorpeon projectile at the location of the given player
function SpawnPoison(player)
	print("[AdminCommand] Spawning Poison.")
	SendCommand("local player = UserToPlayer(\"" .. player.userid .. "\") SpawnAt(\"scorpeon_projectile\", player).components.complexprojectile:Launch(player:GetPosition(), player)")
end

-- Revives the current player if they are dead
function RevivePlayer(player)
	print("[AdminCommand] Reviving " .. tostring(player.userid) .. ".")
	SendCommand("local player = UserToPlayer(\"" .. player.userid .. "\") if player.components.health:IsDead() then player.components.revivablecorpse:Revive(player) player.components.health:SetPercent(1, nil, \"admin_revive\") end")
end