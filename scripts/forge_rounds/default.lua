--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]


--[[Wave order
    pigX3 -> pigX6 -> pigX9 -> pigX12
    pigX6 + crocX2 -> pigX6 + crocX2
    snortoiseX7 -> scorpX7
    snortoiseX2 + scorpX2 -> boarillaX1 (a set time after previous wave)
    boarillaX2 -> crocX1 + pigX2 (when first boarilla hits 50% HP) -> crocX1 + pigX2 + scorpX2 + snortoiseX2 (When first boarilla dies) ->
    boariorX1 (when second boarilla hits 33% HP) -> pigX10 (When boarior hits half HP)
--]]

-- { {left portal mobs}, {center portal mobs}, {right portal mobs} },
-- { {all portals share same mobs} },
-- { {sides share this table}, {middle uses this} }

local banners = {"battlestandard_heal", "battlestandard_damager", "battlestandard_shield"}

local rounds_data = {
    { --round 1
        waves = {
			--{ repeatprefab("pitpig",9) },
            { {"pitpig" } }, -- 1
            { repeatprefab("pitpig",2) }, -- 2
            { repeatprefab("pitpig",3) }, -- 3
            { repeatprefab("pitpig",4) }, -- 4
        },
        wave_introspeech = {[1] = "ROUND1_START", [4] = "ROUND1_FIGHT_BANTER"},
        banner = banners[math.random(1, #banners)],
        roundend = function(self)
            local ents = TheSim:FindEntities(0, 0, 0, 9001, {"battlestandard"})
            for k, v in pairs(ents) do
                if v and v:IsValid() and v.components.health then
                    v.components.health:Kill()
                end
            end
        end,
    },
    { --round 2
        waves = {
			--Leo: To make croco's spawn in center I used "empty_slot" to pad the spawn so croco will always spawn in the 5th mob position.
            { {"empty_slot", "pitpig","pitpig", "pitpig", "crocommander"}, {} }, -- 1 
            { {"empty_slot", "pitpig", "pitpig", "pitpig", "crocommander"}, {} }, -- 2
        },
        wave_introspeech = {[1] = "ROUND2_START", [2] = "ROUND2_FIGHT_BANTER"},
        banner = banners[math.random(1, #banners)],
        spawningfinished = {
            [1] = function(self, spawnedmobs)
                print("[ForgeRounds] Leashing Pitpig's")
                local left = table.getkeys(spawnedmobs.left)
                local right = table.getkeys(spawnedmobs.right)
                local left_crocommander
                local right_crocommander
                for i, v in ipairs(left) do
                    if v.prefab == "crocommander" then
                        left_crocommander = v
                        table.remove(left, i)
                        break
                    end
                end
                for i, v in ipairs(right) do
                    if v.prefab == "crocommander" then
                        right_crocommander = v
                        table.remove(right, i)
                        break
                    end
                end
                if left_crocommander ~= nil then
                    for i, v in ipairs(left) do
                        if v.prefab == "pitpig" then
                            left_crocommander.components.leader:AddFollower(v)
                        end
                    end
                end
                if right_crocommander ~= nil then
                    for i, v in ipairs(right) do
                        if v.prefab == "pitpig" then
                            right_crocommander.components.leader:AddFollower(v)
                        end
                    end
                end
            end,
            [2] = function(self, spawnedmobs)
                print("[ForgeRounds] Leashing Pitpig's")
                local left = table.getkeys(spawnedmobs.left)
                local right = table.getkeys(spawnedmobs.right)
                local left_crocommander
                local right_crocommander
                for i, v in ipairs(left) do
                    if v.prefab == "crocommander" then
                        left_crocommander = v
                        table.remove(left, i)
                        break
                    end
                end
                for i, v in ipairs(right) do
                    if v.prefab == "crocommander" then
                        right_crocommander = v
                        table.remove(right, i)
                        break
                    end
                end
                if left_crocommander ~= nil then
                    for i, v in ipairs(left) do
                        if v.prefab == "pitpig" then
                            left_crocommander.components.leader:AddFollower(v)
                        end
                    end
                end
                if right_crocommander ~= nil then
                    for i, v in ipairs(right) do
                        if v.prefab == "pitpig" then
                            right_crocommander.components.leader:AddFollower(v)
                        end
                    end
                end
            end,
        },
        roundend = function(self)
            local ents = TheSim:FindEntities(0, 0, 0, 9001, {"battlestandard"})
            for k, v in pairs(ents) do
                if v and v:IsValid() and v.components.health then
                    v.components.health:Kill()
                end
            end
        end,
    },
    { --round 3 
        waves = {
            --{ repeatprefab("snortoise",2), repeatprefab("snortoise",3) }, -- 1
			{{"snortoise","empty_slot", "snortoise", "empty_slot"}, {"snortoise", "snortoise", "snortoise"} },
        },
        wave_introspeech = {[1] = "ROUND3_START"},
        banner = banners[math.random(1, #banners)],
        roundend = function(self)
            local ents = TheSim:FindEntities(0, 0, 0, 9001, {"battlestandard"})
            for k, v in pairs(ents) do
                if v and v:IsValid() and v.components.health then
                    v.components.health:Kill()
                end
            end
        end,
    },
    { --round 4
        waves = {
            --{ repeatprefab("scorpeon", 2), repeatprefab("scorpeon", 3) }, -- 1
			{ {"scorpeon","empty_slot", "scorpeon", "empty_slot"}, {"scorpeon", "scorpeon", "scorpeon"} },
            { {"scorpeon", "snortoise"}, {} }, -- 2
            { {}, {"boarilla"} } -- 3
        },
        wave_introspeech = {[1] = "ROUND4_START", [2] = "ROUND4_FIGHT_BANTER", [3] = "ROUND4_TRAILS_INTRO"},
        banner = banners[math.random(1, #banners)],
        talk_pre_delay = {[3] = 0},
        --15 second timer
		spawningfinished = {
			[3] = function(self, spawnedmobs)
				print("[ForgeRounds] Applying Victor's shoulder pads")
				--Fid: The reason I'm doing this instead of table.getkeys(spawnedmobs.middle)[1]:SetVariation(1) is incase a mod ever adds mobs to this wave for whatever reason
				local middle = table.getkeys(spawnedmobs.middle)
				local boarilla
				for i, v in ipairs(middle) do
					if v and v.prefab == "boarilla" then
						boarilla = v
						table.remove(middle, i)
                        break
					end
				end
				if boarilla ~= nil then
					boarilla:SetVariation(1)
				end
			end,
		},
        wavemanager = {
            onenter = function(self, mem)
                mem.wavesstarted = 0
                mem.wavequeue = {}
                table.insert(mem.wavequeue, 1)
            end,
            onupdate = function(self, mem, dt)
                if mem.timer ~= nil and mem.wavesstarted < 3 and not table.contains(mem.wavequeue, 3) then
                    mem.timer = mem.timer + dt
                    if mem.timer > 15 then
                        table.insert(mem.wavequeue, 3)
                    end
                end
                if not mem.wavequeued then
                    if mem.wavequeue[1] ~= nil then
                        mem.wavequeued = true
                        mem.wavesstarted = mem.wavesstarted + 1
                        self:PushWave(mem.wavequeue[1])
                        table.remove(mem.wavequeue, 1)
                    end
                end
            end,
            onallmobsdied = function(self, mem)
                if mem.wavesstarted < 3 then
                    if not table.contains(mem.wavequeue, mem.wavesstarted + 1) then
                        table.insert(mem.wavequeue, mem.wavesstarted + 1)
                    end
                    return false
                else
                    return true
                end
            end,
            onspawningfinished = function(self, mem)
                mem.wavequeued = false
                if mem.wavesstarted == 2 then
                    mem.timer = 0
                end
            end,
        },
        roundend = function(self)
            local ents = TheSim:FindEntities(0, 0, 0, 9001, {"battlestandard"})
            for k, v in pairs(ents) do
                if v and v:IsValid() and v.components.health then
                    v.components.health:Kill()
                end
            end
        end,
    },
    { --round 5
        waves = {
            { {"boarilla"}, {} }, -- 1
            { {}, {"pitpig", "crocommander", "pitpig"} }, -- 2
            { {"scorpeon", "empty_slot", "snortoise", "empty_slot"}, {"pitpig", "crocommander", "pitpig"} }, -- 3
            { {}, {"boarrior"} }, -- 4
            { repeatprefab("pitpig",3), repeatprefab("pitpig",4) } -- 5
        },
        wave_introspeech = {[1] = "ROUND5_START", [2] = "ROUND5_FIGHT_BANTER1", [3] = "ROUND5_FIGHT_BANTER2", [4] = "ROUND5_BOARRIOR_INTRO"},
        banner = banners[math.random(1, #banners)],
        talk_pre_delay = {[1] = 3.5, [2] = 0.5, [3] = 0.5, [4] = 3.5, [5] = 0.5},
        spawningfinished = {
			[1] = function(self, spawnedmobs)
				print("[ForgeRounds] Applying Oliver's shoulder pads (Harry already has his)")
				local left = table.getkeys(spawnedmobs.left)
				local boarilla
				for i, v in ipairs(left) do
					if v and v.prefab == "boarilla" then
						boarilla = v
						table.remove(left, i)
                        break
					end
				end
				if boarilla then
					boarilla:SetVariation(2)
				end
			end,
            [2] = function(self, spawnedmobs)
                print("[ForgeRounds] Leashing Pitpig's")
                local middle = table.getkeys(spawnedmobs.middle)
                local crocommander
                for i, v in ipairs(middle) do
                    if v.prefab == "crocommander" then
                        crocommander = v
                        table.remove(middle, i)
                        break
                    end
                end
                if crocommander ~= nil then
                    for i, v in ipairs(middle) do
                        if v.prefab == "pitpig" then
                            crocommander.components.leader:AddFollower(v)
                        end
                    end
                end
            end,
            [3] = function(self, spawnedmobs)
                print("[ForgeRounds] Leashing Pitpig's")
                local middle = table.getkeys(spawnedmobs.middle)
                local crocommander
                for i, v in ipairs(middle) do
                    if v.prefab == "crocommander" then
                        crocommander = v
                        table.remove(middle, i)
                        break
                    end
                end
                if crocommander ~= nil then
                    for i, v in ipairs(middle) do
                        if v.prefab == "pitpig" then
                            crocommander.components.leader:AddFollower(v)
                        end
                    end
                end
            end,
        },
        wavemanager = {
            onenter = function(self, mem)

                mem.queuewave = function(wave)
                    if not mem.haswavestarted(wave) then
                        table.insert(mem.wavequeue, wave)
                    end
                end
                mem.haswavestarted = function(wave) return mem.wavesstartedlist[wave] == true or table.contains(mem.wavequeue, wave) end
                mem.getboarillapercent = function(num) return mem.boarillas[num] and mem.boarillas[num].components.health and mem.boarillas[num].components.health:GetPercent() or 0 end
                mem.getboarriorpercent = function() return mem.boarrior[1] and mem.boarrior[1].components.health and mem.boarrior[1].components.health:GetPercent() or 0 end

                mem.lastwavestarted = 0
                mem.wavesstartedlist = {}
                mem.wavequeue = {}
                mem.queuewave(1)
            end,
            onupdate = function(self, mem, dt)
                if not mem.haswavestarted(2) and mem.boarillas ~= nil then
                    if (mem.getboarillapercent(1) + mem.getboarillapercent(2)) / 2 <= .6 or
                    mem.getboarillapercent(1) <= .4 or mem.getboarillapercent(2) <= .4 then
                        mem.queuewave(2)
                    end
                end

                if not mem.haswavestarted(3) and mem.boarillas ~= nil then
                    if #mem.boarillas <= 1 or
                    (mem.getboarillapercent(1) <= .4 and mem.getboarillapercent(2) <= .4) then
                        mem.queuewave(3)
                    end
                end

                if not mem.haswavestarted(4) and mem.boarillas ~= nil then
                    if (mem.getboarillapercent(1) + mem.getboarillapercent(2)) / 2 <= .2 then
                        mem.queuewave(4)
                    end
                end

                if not mem.haswavestarted(5) and mem.boarrior ~= nil then
                    if mem.getboarriorpercent() <= .5 then
                        mem.queuewave(5)
                    end
                end

                if mem.timer ~= nil and not mem.haswavestarted(4) then
                    mem.timer = mem.timer + dt
                    if mem.timer > 5 * 60 then
                        mem.queuewave(4)
                    end
                end

                if not mem.wavequeued then
                    if mem.wavequeue[1] ~= nil then
                        local wave = mem.wavequeue[1]

                        mem.wavequeued = true
                        mem.wavesstartedlist[wave] = true
                        mem.lastwavestarted = wave
                        table.remove(mem.wavequeue, 1)

                        self:PushWave(wave)
                    end
                end
            end,
            onallmobsdied = function(self, mem)
                if not mem.wavequeued and #mem.wavesstartedlist == 5 then
                    return true
                end
                return false
            end,
            onspawningfinished = function(self, mem)
                mem.wavequeued = false
                if mem.lastwavestarted == 1 then
                    mem.boarillas = table.getkeys(self.inst.components.lavaarenalivemobtracker.livemobs[5][1].all)
                    for i, v in pairs(mem.boarillas) do
                        v:ListenForEvent("death", function(inst)
                            table.removearrayvalue(mem.boarillas, v)
                        end)
                    end
                end
                if mem.lastwavestarted == 3 then
                    mem.timer = 0
                end
                if mem.lastwavestarted == 4 then
                    mem.boarrior = table.getkeys(self.inst.components.lavaarenalivemobtracker.livemobs[5][4].all)
                    mem.boarrior[1]:ListenForEvent("death", function(inst)
                        mem.boarrior[1] = nil
                    end)
                end
                mem.lastwavestarted = 0
            end,
        },
        roundend = function(self)
            local ents = TheSim:FindEntities(0, 0, 0, 9001, {"battlestandard"})
            for k, v in pairs(ents) do
                if v and v:IsValid() and v.components.health then
                    v.components.health:Kill()
                end
            end
        end,
    },
	{ --round 6
        waves = {
            { {"rhinocebro"}, {}, {"rhinocebro2"} }, 
        },
        wave_introspeech = {[1] = "ROUND6_START"},
        banner = banners[math.random(1, #banners)],
		spawningfinished = {
            [1] = function(self, spawnedmobs)
                print("[ForgeRounds] Forging the bond between brothers")
                local left = table.getkeys(spawnedmobs.left)
                local right = table.getkeys(spawnedmobs.right)
                local left_rhinocebro
                local right_rhinocebro
                for i, v in ipairs(left) do
                    if v.prefab == "rhinocebro" then
                        left_rhinocebro = v
                        table.remove(left, i)
                        break
                    end
                end
                for i, v in ipairs(right) do
                    if v.prefab == "rhinocebro2" then
                        right_rhinocebro = v
                        table.remove(right, i)
                        break
                    end
                end
                if left_rhinocebro ~= nil then
                    if right_rhinocebro ~= nil then
						left_rhinocebro.bro = right_rhinocebro
					end
                end
                if right_rhinocebro ~= nil then
                    if left_rhinocebro ~= nil then
						right_rhinocebro.bro = left_rhinocebro
					end
                end
            end
		},
        roundend = function(self)
            local ents = TheSim:FindEntities(0, 0, 0, 9001, {"battlestandard"})
            for k, v in pairs(ents) do
                if v and v:IsValid() and v.components.health then
                    v.components.health:Kill()
                end
            end
        end,
    },
	{ --round 7
        waves = {
            { {}, {"swineclops"}, {} }, 
        },
        wave_introspeech = {[1] = "ROUND7_START"},
        banner = banners[math.random(1, #banners)],
        roundend = function(self)
            local ents = TheSim:FindEntities(0, 0, 0, 9001, {"battlestandard"})
            for k, v in pairs(ents) do
                if v and v:IsValid() and v.components.health then
                    v.components.health:Kill()
                end
            end
        end,
    },
    player_victory = "ROUND7_PLAYER_VICTORY",
    player_loss = "PLAYERS_DEFEATED_BATTLECRY",
}

for i, v in ipairs(rounds_data) do
    local drop_tbl1 = {}
    local drop_tbl2 = {}
    for j = 1,#v.waves do
        drop_tbl1[j] = {left = {}, middle = {}, right = {}, all = {}}
        drop_tbl2[j] = {left = {}, middle = {}, right = {}, all = {}}
    end
    rounds_data[i].randomitems = drop_tbl1
    rounds_data[i].finalitems = drop_tbl2
end



--These items get spawned across rounds 1&2 (3 items in round 1 and 2 items in round 2)
local itempack1 = { "barbedhelm", "crystaltiara", "jaggedarmor", "silkenarmor", "featheredwreath" }
local ip1_1, ip1_2 = splittable(itempack1, {3,2})

--3 items drop this round
spreadtableovertables(ip1_1, {
    --rounds_data[1].randomitems[1], --First wave doesn't drop anything
    rounds_data[1].randomitems[2].all,
    rounds_data[1].randomitems[3].all,
    rounds_data[1].randomitems[4].all
})

--Living staff always drops from round 1, wave 3
table.insert(rounds_data[1].finalitems[3].all, "livingstaff")

--Firebomb always drops after first croco wave
table.insert(rounds_data[2].finalitems[1].all, "firebomb")

--Splintmail always spawns during 2nd round
table.insert(ip1_2, "splintmail")

--Items can spawn in either wave
spreadtableovertablesunique(ip1_2, {
    rounds_data[2].randomitems[1].left,
    rounds_data[2].randomitems[1].right,
    rounds_data[2].randomitems[2].left,
    rounds_data[2].randomitems[2].right,
})

--Molten darts will always spawn at the end of round 2, along with the whispering grand armor
table.insert(rounds_data[2].finalitems[2].all, "moltendarts")
table.insert(rounds_data[2].finalitems[2].all, "whisperinggrandarmor")

--Items guaranteed to drop during snortoise wave
local turtle_drops = { "noxhelm" }

--Seems like silkgrand armor always drops during snortioses.
table.insert(turtle_drops, "silkengrandarmor")

--These items get spawned across rounds 3 and 4
local itempack2 = { "flowerheadband", "wovengarland" }

--1 itempack2 item will spawn during snortoise wave
local item = math.random(#itempack2)
table.insert(turtle_drops, itempack2[item])
table.remove(itempack2, item)

--Applying snortoise drops
spreadtableovertables(turtle_drops, {rounds_data[3].randomitems[1].all})

--Steadfast and infernal staff always spawn at the end of snortoise wave
table.insert(rounds_data[3].finalitems[1].all, "steadfastarmor")
table.insert(rounds_data[3].finalitems[1].all, "infernalstaff")

--Tome of baconing is part of the itempack2 but won't spawn in snortoise round so must be added to the table after
table.insert(itempack2, "bacontome")
table.insert(itempack2, "jaggedgrandarmor")

--Last itempack2 items can spawn in scorpeon wave and round 4
spreadtableovertables(itempack2, {
    rounds_data[4].randomitems[1].all,
    rounds_data[4].randomitems[2].all
})

--Drops for 1st boarilla
local boarilla_drops = { "steadfastgrandarmor", "clairvoyantcrown", "blacksmithsedge" }

--1st boarilla will drop either molten darts or spiral spear depending on if Wigfrid is on the team
local team_has_wiggy = false
for k,v in pairs(AllPlayers) do
    if v and v.prefab == "wathgrithr" then team_has_wiggy = true end
end
table.insert(boarilla_drops, team_has_wiggy and "spiralspear" or "moltendarts")

--Applying 1st boarilla's drops
spreadtableovertables(boarilla_drops, {rounds_data[4].finalitems[3].all})

local hat_chance = math.random()
--Final item will either be blossomed wreath or resplended nox helm from first crocomander group in round 5
table.insert(rounds_data[5].finalitems[2].all, hat_chance <= 0.5 and "resplendentnoxhelm" or "blossomedwreath")
table.insert(rounds_data[5].finalitems[4].all, hat_chance > 0.5 and "resplendentnoxhelm" or "blossomedwreath")
table.insert(rounds_data[5].finalitems[4].all, "jaggedgrandarmor")
table.insert(rounds_data[5].finalitems[4].all, "silkengrandarmor")

return rounds_data