--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

UseShield_Forge = Class(BehaviourNode, function(self, inst, damageforshield, shieldtime, hidefromheals, hidefromprojectiles, hidewhenscared)
    BehaviourNode._ctor(self, "UseShield_Forge")
    self.inst = inst
    self.damageforshield = damageforshield or 100
    self.hidefromprojectiles = hidefromprojectiles or false
    self.scareendtime = 0
    self.damagetaken = 0
    self.timelastattacked = 1
    self.shieldtime = shieldtime or 2
    self.projectileincoming = false
	self.hidefromheals = hidefromheals or false
	self.hit_in_heal = false

    if hidewhenscared then
        self.onepicscarefn = function(inst, data) self.scareendtime = math.max(self.scareendtime, data.duration + GetTime() + math.random()) end
        self.inst:ListenForEvent("epicscare", self.onepicscarefn)
    end

	if not self.onattackedfn then
		self.onattackedfn = function(inst, data) self:OnAttacked(data.attacker, data.damage) end
		self.onhostileprojectilefn = function() self:OnAttacked(nil, 0, true) end
	end
    self.onfiredamagefn = function() self:OnAttacked() end

    self.inst:ListenForEvent("attacked", self.onattackedfn)
    self.inst:ListenForEvent("hostileprojectile", self.onhostileprojectilefn)
    self.inst:ListenForEvent("firedamage", self.onfiredamagefn)
    self.inst:ListenForEvent("startfiredamage", self.onfiredamagefn)
end)

function UseShield_Forge:OnStop()
    if self.onepicscarefn ~= nil then
        self.inst:RemoveEventCallback("epicscare", self.onepicscarefn)
    end
    self.inst:RemoveEventCallback("attacked", self.onattackedfn)
    self.inst:RemoveEventCallback("hostileprojectile", self.onhostileprojectilefn)
    self.inst:RemoveEventCallback("firedamage", self.onfiredamagefn)
    self.inst:RemoveEventCallback("startfiredamage", self.onfiredamagefn)
end

function UseShield_Forge:TimeToEmerge()
    local t = GetTime()
    return t - self.timelastattacked > self.shieldtime
        and t >= self.scareendtime and (not self.inst:HasTag("_isinheals") or self.inst:HasTag("_isinheals") and self.hit_in_heal)
		--         and t >= self.scareendtime and ((self.hidefromheals and self.hidefromheals == true and not self.inst:HasTag("_isinheals")) and (self.inst.components.sleeper and self.inst.components.sleeper.sleepiness < 1) or self.hidefromheals == false)

end

function UseShield_Forge:ShouldShield()
    return not self.inst.components.health:IsDead() and not self.inst.sg:HasStateTag("busy")
        and self.damagetaken > self.damageforshield and self.inst.canshield == true
end

function UseShield_Forge:OnAttacked(attacker, damage, projectile, stimuli)
	
    if not self.inst.sg:HasStateTag("frozen") then
		if self.inst.shieldlastattacked and self.inst.shieldlastattacked + 3 < GetTime() then
			--print("DEBUG: shieldtime ran out, resetting to 0")
			self.damagetaken = 0
		end
		self.inst.shieldlastattacked = GetTime()
		

        if self.inst.sg.currentstate.name == "shield" and not projectile then
			if self.inst.components.sleeper then self.inst.components.sleeper.sleepiness = 0 end
            self.inst.AnimState:PlayAnimation("hit_shield")
            self.inst.AnimState:PushAnimation("hide_loop")
            return
        end

        if damage then
			--print("DEBUG: damage check for useshield_forge ran, damage is "..damage)
			if not (self.inst.sg:HasStateTag("hiding") or self.inst.sg:HasStateTag("stun") or self.inst.sg:HasStateTag("flipped") or self.inst.sg:HasStateTag("spinning")) and self.inst.canshield and self.inst.canshield == true and not (stimuli and stimuli == "strong" or stimuli == "explosive") then
				--print("DEBUG: damage check SUCCESS, adding to counter")
				self.damagetaken = self.damagetaken + damage
			else
				--print("DEBUG: damage check for useshield_forge to add damage to counter failed, resetting to 0")
				self.damagetaken = 0
			end
        end
		
		if self.inst.sg:HasStateTag("hiding") and self.inst:HasTag("_isinheals") and ((GetTime() - self.timelastattacked) > self.shieldtime) then
			self.hit_in_heal = true
		end

        --if projectile and self.hidefromprojectiles then
            --self.projectileincoming = true
            --return
        --end
    end
end

function UseShield_Forge:Visit()
    local combat = self.inst.components.combat
    local statename = self.inst.sg.currentstate.name

    if self.status == READY then
        if self:ShouldShield() or self.inst.sg:HasStateTag("shield") then
            self.damagetaken = 0
            self.projectileincoming = false
            self.inst:PushEvent("entershield")
			self.timelastattacked = GetTime()
            --self.inst.sg:GoToState("shield")
            self.status = RUNNING
        else
            self.status = FAILED
        end
    end

    if self.status == RUNNING then
        if not self:TimeToEmerge() and not self.inst.sg:HasStateTag("stunned") and not self.inst.sg:HasStateTag("hit") and not self.inst.sg:HasStateTag("fossilized") then 
            self.status = RUNNING
        else
            self.inst:PushEvent("exitshield")
            self.status = SUCCESS
        end
    end
end
