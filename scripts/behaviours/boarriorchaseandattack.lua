--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

local MAX_ANGLE_DIFF = 5

BoarriorChaseAndAttack = Class(BehaviourNode, function(self, inst, findavoidanceobjectfn, max_chase_time, give_up_dist, max_attacks, findnewtargetfn, walk)
    BehaviourNode._ctor(self, "BoarriorChaseAndAttack")
    self.inst = inst
    self.findnewtargetfn = findnewtargetfn
    self.max_chase_time = max_chase_time
    self.give_up_dist = give_up_dist
    self.max_attacks = max_attacks
    self.numattacks = 0
    self.walk = walk
    self.findavoidanceobjectfn = findavoidanceobjectfn
	
	self.target_location = nil
	self.target_angle = nil

    -- we need to store this function as a key to use to remove itself later
    self.onattackfn = function(inst, data)
		if data and data.target then
			self:OnAttackOther(data.target) 
		end
    end

    self.inst:ListenForEvent("onattackother", self.onattackfn)
    self.inst:ListenForEvent("onmissother", self.onattackfn)
end)

function BoarriorChaseAndAttack:__tostring()
    return string.format("target %s", tostring(self.inst.components.combat.target))
end

function BoarriorChaseAndAttack:OnStop()
    self.inst:RemoveEventCallback("onattackother", self.onattackfn)
    self.inst:RemoveEventCallback("onmissother", self.onattackfn)
end

function BoarriorChaseAndAttack:OnAttackOther(target)
    --print ("on attack other", target)
    self.numattacks = self.numattacks + 1
    self.startruntime = nil -- reset max chase time timer
end
--[[
function BoarriorChaseAndAttack:Reset()
	self.target_location = nil
	self.target_angle = nil
end
--]]
function BoarriorChaseAndAttack:Visit()
    local combat = self.inst.components.combat
    if self.status == READY then
        combat:ValidateTarget()

        if combat.target == nil and self.findnewtargetfn ~= nil then
            combat.target = self.findnewtargetfn(self.inst)
        end

        if self.findavoidanceobjectfn ~= nil then
            self.avoidtarget = self.findavoidanceobjectfn(self.inst)
			self.target_angle = nil
        end

        if combat.target ~= nil then
            self.inst.components.combat:BattleCry()
            self.startruntime = GetTime()
            self.numattacks = 0
            self.status = RUNNING
        else
            self.status = FAILED
        end
    end

    if self.status == RUNNING then
        local is_attacking = self.inst.sg:HasStateTag("attack")

        if combat.target == nil or not combat.target.entity:IsValid() then
            self.status = FAILED
            combat:SetTarget(nil)
            self.inst.components.locomotor:Stop()
        elseif combat.target.components.health ~= nil and combat.target.components.health:IsDead() then
            self.status = SUCCESS
            combat:SetTarget(nil)
            self.inst.components.locomotor:Stop()
        else
            local hp = Point(combat.target.Transform:GetWorldPosition())
            local pt = Point(self.inst.Transform:GetWorldPosition())
            local dsq = distsq(hp, pt)
            local angle = self.inst:GetAngleToPoint(self.target_location or hp)
            local r = self.inst:GetPhysicsRadius(0) + combat.target:GetPhysicsRadius(-.1) + .1
            local running = self.inst.components.locomotor:WantsToRun()
			local ap = Point(self.avoidtarget.Transform:GetWorldPosition()) --this should never crash.... right?
            -- this is probably the biggest hack i've done in my life...
			local boarrior_to_heal = distsq(ap ,pt)
			local heal_radius = 16
            if self.avoidtarget ~= nil and self.avoidtarget:IsValid() and boarrior_to_heal > heal_radius then
				if self.target_angle == nil then
					self.target_location = Point(combat.target.Transform:GetWorldPosition())
					self.target_angle = combat.target:GetAngleToPoint(ap)
				end
               local radius = 30
			   if boarrior_to_heal <= radius then
                    local delta_dir = anglediff(angle, self.inst:GetAngleToPoint(ap))
					local offset = (ap - pt):GetNormalized()
					if delta_dir > 0 then
                        offset.x, offset.z = offset.z, -offset.x
                    else
                        offset.x, offset.z = -offset.z, offset.x
                    end
					hp = pt + offset -- offset * (stargate radius + stalker radius + some breathing room)
				end
            end
			
            if (running and dsq > r * r) or (not running and dsq > combat:CalcAttackRangeSq()) then
				if (self.target_angle == nil) or (math.abs(anglediff(self.target_angle, self.inst:GetAngleToPoint(ap))) > MAX_ANGLE_DIFF) or (boarrior_to_heal <= heal_radius) then
               -- self.inst.components.locomotor:RunInDirection(angle)
					self.inst.components.locomotor:GoToPoint(hp, nil, not self.walk)
				else
					--self.status = SUCCESS
					--self.inst.components.combat:SetTarget(nil)
					--self.inst.components.locomotor:Stop()
					self.target_location = nil
					self.target_angle = nil
				end
            elseif not (self.inst.sg ~= nil and self.inst.sg:HasStateTag("jumping")) then
                self.inst.components.locomotor:Stop()
                if self.inst.sg:HasStateTag("canrotate") then
                    self.inst:FacePoint(hp)
                end
            end

            if combat:TryAttack() then
                -- reset chase timer when attack hits, not on attempts
            elseif self.startruntime == nil then
                self.startruntime = GetTime()
                self.inst.components.combat:BattleCry()
            end

            if self.max_attacks ~= nil and self.numattacks >= self.max_attacks then
                self.status = SUCCESS
                self.inst.components.combat:SetTarget(nil)
                self.inst.components.locomotor:Stop()
                return
            elseif (self.give_up_dist ~= nil and dsq >= self.give_up_dist * self.give_up_dist)
                or (self.max_chase_time ~= nil and self.startruntime ~= nil and GetTime() - self.startruntime > self.max_chase_time) then
                self.status = FAILED
                self.inst.components.combat:GiveUp()
                self.inst.components.locomotor:Stop()
                return
            end

            self:Sleep(.125)
        end
    end
end
