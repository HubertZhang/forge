--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details
The source codes does not come with any warranty including
the implied warranty of merchandise.
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

name = "Forged Forge"
version = "0.3.6.5"

description = "Fight the system! Play the Forge on your own server!"

author = "Forged Forge Team"

forumthread = ""

api_version = 10
api_version_dst = 10

dont_starve_compatible = false
reign_of_giants_compatible = false
dst_compatible = true

all_clients_require_mod = true
client_only_mod = false

forge_compatible = true

icon_atlas = "images/modicon.xml"
icon = "modicon.tex"

mim_assets = {
	custom_menus = {
		["Forge Menu"] = {desc = "Make your menu Forge themed!", script = "forgemenu"}
	}
}

folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
    name = " "..name.." - GitLab Ver."
end

priority = 2147483646

server_filter_tags = {
    "Forge",
    "Forged_Forge",
    "the_forge",
}

game_modes = {
	{
        name = "lavaarena",
		label = "The Forge",
		settings = {
			level_type = "LAVAARENA",
			spawn_mode = "fixed",
			resource_renewal = false,
			ghost_sanity_drain = false,
			ghost_enabled = false,
			revivable_corpse = true,
			spectator_corpse = true,
			portal_rez = false,
			reset_time = nil,
			invalid_recipes = nil,
			--
			override_item_slots = 0,
            drop_everything_on_despawn = true,
			no_air_attack = true,
			no_crafting = true,
			no_minimap = true,
			no_hunger = true,
			no_sanity = true,
			no_avatar_popup = true,
			no_morgue_record = true,
			override_normal_mix = "lavaarena_normal",
			override_lobby_music = "dontstarve/music/lava_arena/FE2",
			cloudcolour = { .4, .05, 0 },
			cameraoverridefn = function(camera)
				camera.mindist = 20
				camera.mindistpitch = 32
				camera.maxdist = 55
				camera.maxdistpitch = 60
				camera.distancetarget = 32
			end,
			lobbywaitforallplayers = true,
			hide_worldgen_loading_screen = true,
			hide_received_gifts = false,
			skin_tag = "LAVA",
		},
	}
}

local function AddCustomConfig(name, label, hover, options, default)
    return {name = name, label = label, hover = hover or "", options = options, default = default}
end

local function AddSectionTitle(title)
    return AddCustomConfig(title, "", "", {{description = "", data = 0}}, 0)
end

local function BuildBooleanConfig(desc_str, hover_str)
    return {
        {description = desc_str and desc_str.off or "No", hover = hover_str and hover_str.off or "", data = false},
        {description = desc_str and desc_str.on or "Yes", hover = hover_str and hover_str.on or "", data = true}
    }
end

local function BuildNumConfig(startNum, endNum, step, percent)
    local numTable = {}
    local iterator = 1
    local suffix = percent and "%" or ""
    for i = startNum, endNum, step or 1 do
        numTable[iterator] = {description = i..suffix, data = percent and i / 100 or i}
        iterator = iterator + 1
    end
    return numTable
end

local string = ""
local keys = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","LAlt","RAlt","LCtrl","RCtrl","LShift","RShift","Tab","Capslock","Space","Minus","Equals","Backspace","Insert","Home","Delete","End","Pageup","Pagedown","Print","Scrollock","Pause","Period","Slash","Semicolon","Leftbracket","Rightbracket","Backslash","Up","Down","Left","Right"}
local keylist = {}
for i = 1, #keys do
    keylist[i] = {description = keys[i], data = "KEY_"..string.upper(keys[i])}
end
keylist[#keylist + 1] = {description = "Disabled", data = false}

local filters = {"Forge","Fall Day","Fall Dusk","Fall Night","Snow Day","Snow Dusk","Snow Night","Summer Day","Summer Dusk","Summer Night","Spring Day","Spring Dusk","Spring Night","Insane Day","Insane dusk","Insane Night","Purple Moon","SW Mild Day","SW Wet Day","SW Green Day","SW Volcano","Beaver Vision","Caves","Fungus","Ghost","Identity","Mole Vision Off","Mole Vision On","Ruins Dark","Ruins Dim","Ruins Light","Sinkhole","Gorge"}
local filter_list = {}
for i = 1, #filters do
	filter_list[i] = {description = filters[i], data = i}
end

local wave_sets = {
    {description = "Forge 2018", data = "default", hover = "The 2018 Forge experience!"},
    {description = "Forge Classic", data = "classic", hover = "The OG Forge experience!"},
	{description = "Rhinocebros", data = "rhinocebros", hover = "Play up to the rhinocebros"},
	{description = "Boarillas", data = "boarilla", hover = "Play up to the double boarilla wave"},
	
	{description = "Double Trouble S2", data = "double2018", hover = "Forgex2 of the season 2 of Forge!"},
    {description = "Double Trouble S1", data = "double", hover = "Forgex2 of the original Forge!"},
	
	{description = "Half the Wrath", data = "easy", hover = "An easier Forge 2018 experience"},
    {description = "Beta Content", data = "beta", hover = "Test out new mobs and items!"},
	{description = "Sandbox", data = "sandbox", hover = "Nothing spawns, free to do whatever you want, useful for testing!"},
}

configuration_options =
{
	AddSectionTitle("Lobby Options"),
	AddCustomConfig("FORCE_START_DELAY_TIME", "Force Start Delay", "Choose how long the force start delay timer is.", BuildNumConfig(5, 12 * 5, 5), 30),

	AddSectionTitle("Player HUD Options"),
	AddCustomConfig("HIDE_INDICATORS", "Hide Player Indicators", "Disable player indicators from showing at all.", BuildBooleanConfig()),
	AddCustomConfig("GIFT_SIDE", "Gift Location", "Location of the gift item icon.", {{description = "Top Left", data = "left"},{description = "Top Right", data = "right"},}, "right"),

	AddSectionTitle("Damage Number Options"),
	AddCustomConfig("DAMAGE_NUMBER_OPTIONS", "Damage Number Display", "Options for which damage numbers to display.", {{description = "Default", data = "default"},{description = "Elemental", data = "elemental"},{description = "Off", data = "off"},}, "default"),
	--{description = "All Damage", data = "all"}, -- TODO make this for each client, currently if one person has it on everyone sees all dmg numbers.
	AddCustomConfig("DAMAGE_NUMBER_FONT_SIZE", "Damage Number Font Size", "Choose the size of the font for damage numbers. 32 is default.", BuildNumConfig(1, 100), 32),
	AddCustomConfig("DAMAGE_NUMBER_HEIGHT", "Damage Number Height", "Choose how high damage numbers are displayed above the target. 40 is default.", BuildNumConfig(1, 100), 40),

	AddSectionTitle("Filter Options"),
	AddCustomConfig("DEFAULT_FILTER", "Default Filter", "Set the default filter (aka changes the colour cube used).", filter_list, 1),
	AddCustomConfig("ADJUST_FILTER", "Changes the current filter", "Assign the key you want to change the current filter.", keylist, "KEY_R"),

	AddSectionTitle("Detailed Summary Options"),
	AddCustomConfig("ONLY_SHOW_NONZERO_STATS", "Display Nonzero Stats Only", "Only displays stats from a match that have a value greater than zero", BuildBooleanConfig(), true),
	AddCustomConfig("DISPLAY_COLORED_STATS", "Display Colored Stats", "Colors the stat text gold, silver, bronze, or white based on your team stat ranking.", BuildBooleanConfig(), true),
	AddCustomConfig("DEFAULT_ROTATION", "Mvp Badge default rotation", "The MVP badge will have the same rotation on both default summary screen and the detailed summary screen", BuildBooleanConfig(), false),
	AddCustomConfig("ROTATION", "MVP badge custom rotation", "Adjusts the rotation of the MVP badge on the detailed summary screen if default rotation is false", BuildNumConfig(-10, 10), 0),

	AddSectionTitle("Modifiers (TEMPORARY)"),
	AddCustomConfig("WAVESET", "Wave Set", "Change the wave data to something NEW!", wave_sets, "default"),
	AddCustomConfig("MOBDAMAGE_DEALT", "Mob Damage Dealt", "Modify how much damage the mobs deal.", BuildNumConfig(50, 200, 25, true), 1),
	AddCustomConfig("MOBDAMAGE_TAKEN", "Mob Damage Received", "Modify how much damage the mobs take.", BuildNumConfig(50, 200, 25, true), 1),
	AddCustomConfig("BATTLESTANDARD_EFFICIENCY", "Battlestandard Effectiveness", "Modify how effective battlestandards are.", BuildNumConfig(50, 200, 25, true), 1),
	AddCustomConfig("REDLIGHT", "Red Light Green Light (WIP)", "Play red light green light with Pugna!", BuildBooleanConfig({off = "Off", on = "On"},{on = "WARNING: Will disable invisibility modifiers"}), false),
	AddCustomConfig("INVISIBLE_PLAYERS", "Invisible Players", "Makes the players turn invisible!", BuildBooleanConfig({off = "Off", on = "On"}), false),
	AddCustomConfig("INVISIBLE_MOBS", "Invisible Mobs", "Makes the mobs turn invisible!", BuildBooleanConfig({off = "Off", on = "On"}), false),
	--[[ Leaving these for Leo
	AddCustomConfig("NOSLEEPS", "Heals Don't Sleep", "Makes it so that heals no longer sleep mobs.", BuildBooleanConfig({off = "Off", on = "On"}), false),
	AddCustomConfig("STRONGERSHIELDS", "Shields Can't Be Broken", "Alt attacks won't break shields anymore.", BuildBooleanConfig({off = "Off", on = "On"}), false),
	--]]
	AddCustomConfig("SUPERKNOCKBACK", "Lightweight Mode", "Make all mobs have heavy knockback.", BuildBooleanConfig({off = "Off", on = "On"}), false),

	--AddSectionTitle("Other"),

	AddSectionTitle("Performance"),
}
